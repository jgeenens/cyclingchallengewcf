﻿using System;
using System.ServiceModel;
using CyclingChallenge.Services.Contracts.ServiceContracts;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Contracts.DataContracts.Race;
using CyclingChallenge.Services.Contracts.DataContracts.YouthRider;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Game;

namespace CyclingChallenge.Services.Client
{
    public class CyclingChallengeClient: ClientBase<ICyclingChallengeService>, ICyclingChallengeService, IDisposable
    {
        #region Fields

        private bool _disposed = false;

        #endregion


        #region Constructors

        ~CyclingChallengeClient()
        {
            Dispose(false);
        }

        #endregion


        #region Client Get Methods

        public AccountDetails GetAccountDetails(long idUser, long? idRequestingUser = null)
        {
            return Channel.GetAccountDetails(idUser, idRequestingUser);
        }

        public Board GetBoard(byte idBoard, long? idUser = null)
        {
            return Channel.GetBoard(idBoard, idUser);
        }

        public MessageBoard GetBoards()
        {
            return Channel.GetBoards();
        }

        public TrainingCamp GetCampForTeam(long idTeam)
        {
            return Channel.GetCampForTeam(idTeam);
        }

        public Dictionary GetDictionary(byte idLanguage)
        {
            return Channel.GetDictionary(idLanguage);
        }

        public Economy GetEconomy(long idTeam)
        {
            return Channel.GetEconomy(idTeam);
        }

        public Forum GetForum(short idForum, long? idUser = null)
        {
            return Channel.GetForum(idForum, idUser);
        }

        public Inbox GetInbox(long idTeam, int? page = null)
        {
            return Channel.GetInbox(idTeam, page);
        }

        public Message GetMessage(long idMessage, long? idUser = null)
        {
            return Channel.GetMessage(idMessage, idUser);
        }

        public OfficialMessages GetOfficialMessages(long idTeam, int? page = null)
        {
            return Channel.GetOfficialMessages(idTeam, page);
        }

        public Outbox GetOutbox(long idTeam, int? page = null)
        {
            return Channel.GetOutbox(idTeam, page);
        }

        public PressMessages GetPressMessages(long idTeam, int? page = null)
        {
            return Channel.GetPressMessages(idTeam, page);
        }

        public RaceDescription GetRace(long idRace)
        {
            return Channel.GetRace(idRace);
        }

        public RaceHistory GetRaceHistory(long idRace, bool includeStages = false)
        {
            return Channel.GetRaceHistory(idRace, includeStages);
        }

        public RaceRanking GetRaceParticipants(long idRace)
        {
            return Channel.GetRaceParticipants(idRace);
        }

        public RaceDescription GetRaceProfile(long idRace)
        {
            return Channel.GetRaceProfile(idRace);
        }

        public RaceRanking GetRaceRanking(long idRace)
        {
            return Channel.GetRaceRanking(idRace);
        }

        public RaceReport GetRaceReport(long idRace)
        {
            return Channel.GetRaceReport(idRace);
        }

        public Ranking GetRanking(long idDivision)
        {
            return Channel.GetRanking(idDivision);
        }

        public Rider GetRider(long idRider)
        {
            return Channel.GetRider(idRider);
        }

        public RiderEvents GetRiderEvents(long idRider)
        {
            return Channel.GetRiderEvents(idRider);
        }

        public RiderHistory GetRiderHistory(long idRider)
        {
            return Channel.GetRiderHistory(idRider);
        }

        public TeamRiders GetRidersForTeam(long idTeam)
        {
            return Channel.GetRidersForTeam(idTeam);
        }

        public SearchResults GetSearchResults(byte searchType, string firstSearchValue, string secondSearchValue = null)
        {
            return Channel.GetSearchResults(searchType, firstSearchValue, secondSearchValue);
        }

        public Shop GetShop(long idTeam)
        {
            return Channel.GetShop(idTeam);
        }

        public Sponsors GetSponsors(long idTeam)
        {
            return Channel.GetSponsors(idTeam);
        }

        public StageDescription GetStage(long idStage)
        {
            return Channel.GetStage(idStage);
        }

        public RaceRanking GetStageRanking(long idRace, short idStage)
        {
            return Channel.GetStageRanking(idRace, idStage);
        }

        public RaceReport GetStageReport(long idRace, short idStage)
        {
            return Channel.GetStageReport(idRace, idStage);
        }

        public Store GetStore(long idUser)
        {
            return Channel.GetStore(idUser);
        }

        public TeamDetails GetTeamDetails(long idTeam, long? idUser = null)
        {
            return Channel.GetTeamDetails(idTeam, idUser);
        }

        public TeamDetails GetTeamDetailsForFacebookId(string facebookId)
        {
            return Channel.GetTeamDetailsForFacebookId(facebookId);
        }

        public TeamHistory GetTeamHistory(long idTeam)
        {
            return Channel.GetTeamHistory(idTeam);
        }

        public Thread GetThread(long idThread, long? idUser = null)
        {
            return Channel.GetThread(idThread, idUser);
        }

        public Training GetTraining(long idTeam)
        {
            return Channel.GetTraining(idTeam);
        }

        public TeamTransactions GetTransactions(long idTeam, DateTime from, DateTime? to = null)
        {
            return Channel.GetTransactions(idTeam, from, to);
        }

        public RiderTransferHistory GetTransferHistoryForRider(long idRider, DateTime from, DateTime? to = null)
        {
            return Channel.GetTransferHistoryForRider(idRider, from, to);
        }

        public TeamTransferHistory GetTransferHistoryForTeam(long idTeam, DateTime from, DateTime? to = null)
        {
            return Channel.GetTransferHistoryForTeam(idTeam, from, to);
        }

        public TeamTransfers GetTransfers(long idTeam)
        {
            return Channel.GetTransfers(idTeam);
        }

        public TeamYouthRiders GetYouthRidersForTeam(long idTeam)
        {
            return Channel.GetYouthRidersForTeam(idTeam);
        }

        #endregion


        #region Client Post Methods

        public ResultBase BuyShopProducts(long idTeam, params ProductBase[] products)
        {
            return Channel.BuyShopProducts(idTeam, products);
        }

        public ResultBase CreateUser(string facebookId, CreateUserRequest createUserRequest)
        {
            return Channel.CreateUser(facebookId, createUserRequest);
        }

        public ResultBase EditMessage(long idMessage, long idUser, string message)
        {
            return Channel.EditMessage(idMessage, idUser, message);
        }

        public ResultBase PostMessage(long idThread, long idUser, string userIp, string message, long? idMessageRepliedTo = null)
        {
            return Channel.PostMessage(idThread, idUser, userIp, message, idMessageRepliedTo);
        }

        public ResultBase SendMessage(long idUser, string recipientUserName, string subject, string message)
        {
            return Channel.SendMessage(idUser, recipientUserName, subject, message);
        }

        public ResultBase SetRiderCategory(long idTeam, long idRider, byte category)
        {
            return Channel.SetRiderCategory(idTeam, idRider, category);
        }

        public ResultBase SetShopPrices(long idTeam, params ProductBase[] products)
        {
            return Channel.SetShopPrices(idTeam, products);
        }

        public ResultBase SetTeamDetails(long idTeam, string shortName, string longName, byte idLogo)
        {
            return Channel.SetTeamDetails(idTeam, shortName, longName, idLogo);
        }

        public ResultBase SetTraining(long idTeam, byte training)
        {
            return Channel.SetTraining(idTeam, training);
        }

        public ResultBase SetUserDetails(long idUser, string userName, string email, byte language, bool hideEmail)
        {
            return Channel.SetUserDetails(idUser, userName, email, language, hideEmail);
        }

        public ResultBase StartShop(long idTeam)
        {
            return Channel.StartShop(idTeam);
        }

        #endregion


        #region IDisposable implementation

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    try
                    {
                        if (State != CommunicationState.Faulted)
                        {
                            Close();
                        }
                    }
                    finally
                    {
                        if (State != CommunicationState.Closed)
                        {
                            Abort();
                        }
                        _disposed = true;
                    }
                }
            }
        }

        #endregion
    }
}
