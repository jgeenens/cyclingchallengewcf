﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Race;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class Achievement
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public DateTime When { get; set; }

        [DataMember]
        public TeamBase Team { get; set; }

        [DataMember]
        public int Item { get; set; }

        [DataMember]
        public RaceBase Race { get; set; }

        [DataMember]
        public Stage Stage { get; set; }

        [DataMember]
        public short Season { get; set; }
    }
}
