﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class Board: ResultBase
    {
        [DataMember]
        public byte Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<Forum> Forums { get; set; }
    }
}
