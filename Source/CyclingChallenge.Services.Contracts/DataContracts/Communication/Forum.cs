﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class Forum: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public byte Type { get; set; }

        [DataMember]
        public int NumberOfThreads { get; set; }

        [DataMember]
        public bool HasNewMessages { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public List<Thread> Threads { get; set; }
    }
}