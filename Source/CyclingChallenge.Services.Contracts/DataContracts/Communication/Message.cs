﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class Message: MessageBase
    {
        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public User User { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public bool CanChange { get; set; }

        [DataMember]
        public DateTime? ChangeDate { get; set; }

        [DataMember]
        public User ChangeUser { get; set; }

        [DataMember]
        public MessageBase ReplyTo { get; set; }
    }
}
