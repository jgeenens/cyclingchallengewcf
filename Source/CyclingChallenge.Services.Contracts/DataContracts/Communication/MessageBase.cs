﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class MessageBase: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long Thread { get; set; }

        [DataMember]
        public byte MessageInThread { get; set; }
    }
}
