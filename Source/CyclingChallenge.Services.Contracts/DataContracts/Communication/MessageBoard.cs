﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class MessageBoard : ResultBase
    {        
        [DataMember]
        public List<Board> Boards { get; set; }
    }
}
