﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class PrivateMessage
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public User Sender { get; set; }

        [DataMember]
        public User Recipient { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public bool IsNew { get; set; }
    }
}
