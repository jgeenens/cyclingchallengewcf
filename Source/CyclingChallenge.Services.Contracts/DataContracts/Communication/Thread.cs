﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class Thread: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int NumberOfMessages { get; set; }

        [DataMember]
        public bool HasNewMessages { get; set; }

        [DataMember]
        public User Originator { get; set; }

        [DataMember]
        public DateTime LastChange { get; set; }

        [DataMember]
        public User LastOriginator { get; set; }

        [DataMember]
        public List<Message> Messages { get; set; }
    }
}