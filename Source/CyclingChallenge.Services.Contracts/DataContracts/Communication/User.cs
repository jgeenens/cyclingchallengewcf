﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Communication
{
    [DataContract]
    public class User
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Alias { get; set; }

        [DataMember]
        public TeamBase Team { get; set; }
    }
}
