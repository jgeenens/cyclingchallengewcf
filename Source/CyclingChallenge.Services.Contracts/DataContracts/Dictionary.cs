﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class Dictionary: ResultBase
    {
        [DataMember]
        public short Id { get; set; }

        [DataMember]
        public string Language { get; set; }

        [DataMember]
        public List<Item> Items { get; set; }
    }
}
