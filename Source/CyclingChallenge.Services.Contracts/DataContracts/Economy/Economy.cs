﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Economy : ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public long Money { get; set; }

        [DataMember]
        public long MoneyProjected { get; set; }

        [DataMember]
        public List<Revenue> Revenues { get; set; }

        [DataMember]
        public List<Expense> Expenses { get; set; }
    }
}
