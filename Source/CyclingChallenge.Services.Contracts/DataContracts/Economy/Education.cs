﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Education
    {
        [DataMember]
        public byte Level { get; set; }

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public short Price { get; set; }

        [DataMember]
        public short Duration { get; set; }
    }
}
