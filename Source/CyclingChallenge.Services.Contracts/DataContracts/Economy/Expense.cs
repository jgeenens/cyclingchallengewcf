﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Expense
    {
        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public long Amount { get; set; }
    }
}
