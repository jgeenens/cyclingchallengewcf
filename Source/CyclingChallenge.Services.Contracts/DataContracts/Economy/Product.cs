﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Product: ProductBase
    {
        [DataMember]
        public short Name { get; set; }

        [DataMember]
        public int Buy { get; set; }

        [DataMember]
        public int Sell { get; set; }
    }
}
