﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class ProductBase
    {
        [DataMember]
        public byte Id { get; set; }

        [DataMember]
        public short Quantity { get; set; }

        [DataMember]
        public short? Price { get; set; }
    }
}
