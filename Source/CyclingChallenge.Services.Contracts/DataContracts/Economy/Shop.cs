﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Shop: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte Level { get; set; }

        [DataMember]
        public bool IsClosed { get; set; }

        [DataMember]
        public List<Product> Products { get; set; }

        [DataMember]
        public Manager Manager { get; set; }

        [DataMember]
        public short? Hint { get; set; }
    }
}
