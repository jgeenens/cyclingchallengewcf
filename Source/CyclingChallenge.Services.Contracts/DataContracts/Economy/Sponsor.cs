﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Sponsor: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public byte WeeksLeft { get; set; }

        [DataMember]
        public byte WeeksTotal { get; set; }

        [DataMember]
        public long Signing { get; set; }

        [DataMember]
        public long Bonus { get; set; }
    }
}
