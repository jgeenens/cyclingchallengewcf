﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Team;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Sponsors: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool HasSponsor { get; set; }

        [DataMember]
        public Division Division { get; set; }

        [DataMember]
        public List<Sponsor> Sponsor { get; set; }
    }
}
