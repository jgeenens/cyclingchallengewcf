﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class TeamTransactions : ResultBase
    {
        [DataMember]
        public List<Transaction> Transactions { get; set; }
    }
}
