﻿using System;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Race;
using CyclingChallenge.Services.Contracts.DataContracts.Training;

namespace CyclingChallenge.Services.Contracts.DataContracts.Economy
{
    [DataContract]
    public class Transaction
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public bool IsRevenue { get; set; }

        [DataMember]
        public long Amount { get; set; }

        [DataMember]
        public short Description { get; set; }

        [DataMember]
        public Rider.Rider Rider { get; set; }

        [DataMember]
        public RaceBase Race { get; set; }

        [DataMember]
        public Stage Stage { get; set; }

        [DataMember]
        public Trainer Trainer { get; set; }

        [DataMember]
        public Training.Location TrainingCamp { get; set; }
    }
}
