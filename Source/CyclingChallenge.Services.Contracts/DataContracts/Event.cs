﻿using CyclingChallenge.Services.Contracts.DataContracts.Training;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class Event
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public Race.RaceBase Race { get; set; }

        [DataMember]
        public Camp Camp { get; set; }
    }
}
