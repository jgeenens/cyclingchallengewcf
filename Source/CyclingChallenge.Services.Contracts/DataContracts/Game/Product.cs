﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Game
{
    [DataContract]
    public class Product
    {
        [DataMember]
        public short Id { get; set; }

        [DataMember]
        public short Title { get; set; }

        [DataMember]
        public short Description { get; set; }

        [DataMember]
        public int Value { get; set; }

        [DataMember]
        public long? Price { get; set; }

        [DataMember]
        public Currency Currency { get; set; }
    }
}
