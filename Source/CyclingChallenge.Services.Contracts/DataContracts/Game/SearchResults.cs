﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Team;

namespace CyclingChallenge.Services.Contracts.DataContracts.Game
{
    [DataContract]
    public class SearchResults: ResultBase
    {
        [DataMember]
        public byte SearchType { get; set; }

        [DataMember]
        public List<Division> Divisions { get; set; }

        [DataMember]
        public List<RiderBase> Riders { get; set; }

        [DataMember]
        public List<TeamBase> Teams { get; set; }
    }
}
