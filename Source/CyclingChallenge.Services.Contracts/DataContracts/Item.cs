﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class Item
    {
        [DataMember]
        public short Id { get; set; }

        [DataMember]
        public string Text { get; set; }
    }
}