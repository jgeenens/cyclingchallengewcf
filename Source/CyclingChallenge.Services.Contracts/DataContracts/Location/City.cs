﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Location
{
    [DataContract]
    public class City
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Region Region { get; set; }
    }
}
