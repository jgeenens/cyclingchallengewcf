﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class Picture
    {
        [DataMember]
        public byte Face { get; set; }

        [DataMember]
        public byte Hair { get; set; }

        [DataMember]
        public byte Eyes { get; set; }

        [DataMember]
        public byte Nose { get; set; }

        [DataMember]
        public byte Lips { get; set; }
    }
}