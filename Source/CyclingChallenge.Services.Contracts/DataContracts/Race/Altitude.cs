﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class Altitude
    {
        [DataMember]
        public int Km { get; set; }

        [DataMember]
        public long Height { get; set; }
    }
}
