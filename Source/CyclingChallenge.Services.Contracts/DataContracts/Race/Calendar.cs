﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class Calendar : ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public DateTime StartSeason { get; set; }

        [DataMember]
        public List<Race> Races { get; set; }
    }
}
