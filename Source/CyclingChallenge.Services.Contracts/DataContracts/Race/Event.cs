﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class Event
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public DateTime When { get; set; }

        [DataMember]
        public Stage Stage { get; set; }

        [DataMember]
        public TeamBase Team { get; set; }

        [DataMember]
        public RiderBase Rider { get; set; }

        [DataMember]
        public short Type { get; set; }

        [DataMember]
        public short Season { get; set; }
    }
}
