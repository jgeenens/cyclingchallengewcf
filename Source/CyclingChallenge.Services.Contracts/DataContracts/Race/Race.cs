﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class Race: RaceBase
    {
        [DataMember]
        public byte Status { get; set; }

        [DataMember]
        public short? Stage { get; set; }

        [DataMember]
        public RiderBase Yellow { get; set; }
    }
}
