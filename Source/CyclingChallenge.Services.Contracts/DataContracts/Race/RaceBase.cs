﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class RaceBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public short Name { get; set; }

        [DataMember]
        public DateTime Start { get; set; }

        [DataMember]
        public DateTime End { get; set; }

        [DataMember]
        public bool IsTour { get; set; }
    }
}
