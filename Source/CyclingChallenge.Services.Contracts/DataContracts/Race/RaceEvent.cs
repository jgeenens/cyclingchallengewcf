﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class RaceEvent
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public byte Class { get; set; }

        [DataMember]
        public byte Type { get; set; }

        [DataMember]
        public byte Index { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int Distance { get; set; }

        [DataMember]
        public short Time { get; set; }
    }
}
