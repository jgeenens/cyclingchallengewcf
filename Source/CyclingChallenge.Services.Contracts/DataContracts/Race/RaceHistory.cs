﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class RaceHistory : ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public short Name { get; set; }

        [DataMember]
        public List<Event> Events { get; set; }
    }
}
