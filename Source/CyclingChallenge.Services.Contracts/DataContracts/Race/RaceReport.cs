﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Team;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class RaceReport : ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public short Name { get; set; }

        [DataMember]
        public bool IsTour { get; set; }

        [DataMember]
        public Stage Stage { get; set; }

        [DataMember]
        public List<RiderBase> Riders { get; set; }

        [DataMember]
        public List<RaceEvent> Events { get; set; }
    }
}
