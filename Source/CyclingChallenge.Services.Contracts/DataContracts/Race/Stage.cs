﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class Stage
    {
        [DataMember]
        public short Id { get; set; }

        [DataMember]
        public bool IsTimeTrial { get; set; }

        [DataMember]
        public bool IsTeamTimeTrial { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
