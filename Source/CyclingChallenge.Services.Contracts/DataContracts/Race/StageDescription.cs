﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Location;

namespace CyclingChallenge.Services.Contracts.DataContracts.Race
{
    [DataContract]
    public class StageDescription: ResultBase
    {
        [DataMember]
        public short Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte? Day { get; set; }

        [DataMember]
        public short CobblestonesPercentage { get; set; }

        [DataMember]
        public short PlainsPercentage { get; set; }

        [DataMember]
        public short MountainsPercentage { get; set; }

        [DataMember]
        public short HillsPercentage { get; set; }

        [DataMember]
        public int Weather { get; set; }

        [DataMember]
        public byte Profile { get; set; }

        [DataMember]
        public byte Terrain { get; set; }

        [DataMember]
        public short? Distance { get; set; }

        [DataMember]
        public City Start { get; set; }

        [DataMember]
        public City Finish { get; set; }

        [DataMember]
        public List<Climb> Hills { get; set; }

        [DataMember]
        public List<Climb> Mountains { get; set; }

        [DataMember]
        public List<PavedSector> PavedSectors { get; set; }

        [DataMember]
        public List<Altitude> Altitudes { get; set; }
    }
}
