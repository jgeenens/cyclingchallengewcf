﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class ResultBase
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string ErrorCode { get; set; }

        #region Constructor

        public ResultBase()
        {
            Success = false;
            ErrorCode = null;
        }

        public ResultBase(string errorCode)
        {
            Success = false;
            ErrorCode = errorCode;
        }

        #endregion
    }
}
