﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Rider
{
    [DataContract]
    public class Rider: RiderBase
    {
        [DataMember]
        public string Residence { get; set; }

        [DataMember]
        public long Wage { get; set; }

        [DataMember]
        public long Value { get; set; }

        [DataMember]
        public byte Category { get; set; }

        [DataMember]
        public bool IsRetired { get; set; }

        [DataMember]
        public bool IsOnTrainingCamp { get; set; }

        [DataMember]
        public bool IsOnTransfer { get; set; }

        [DataMember]
        public Transfer Transfer { get; set; }

        [DataMember]
        public Picture Picture { get; set; }

        [DataMember]
        public RiderValues Values { get; set; }

        [DataMember]
        public RiderMedals Medals { get; set; }
    }
}
