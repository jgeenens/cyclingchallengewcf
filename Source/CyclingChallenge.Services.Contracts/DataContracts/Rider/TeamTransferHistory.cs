﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Rider
{
    [DataContract]
    public class TeamTransferHistory: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<TransferHistory> Incoming { get; set; }

        [DataMember]
        public List<TransferHistory> Outgoing { get; set; }
    }
}