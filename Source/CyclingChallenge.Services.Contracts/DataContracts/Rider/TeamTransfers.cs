﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Rider
{
    [DataContract]
    public class TeamTransfers: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<Transfer> Incoming { get; set; }

        [DataMember]
        public List<Transfer> Outgoing { get; set; }
    }
}