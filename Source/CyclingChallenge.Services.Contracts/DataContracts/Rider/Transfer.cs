﻿using System;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Rider
{
    [DataContract]
    public class Transfer
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime Deadline { get; set; }

        [DataMember]
        public TeamBase Team { get; set; }

        [DataMember]
        public TeamBase BidTeam { get; set; }

        [DataMember]
        public long StartBid { get; set; }

        [DataMember]
        public long Bid { get; set; }
    }
}