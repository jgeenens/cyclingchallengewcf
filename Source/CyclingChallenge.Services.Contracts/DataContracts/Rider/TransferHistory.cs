﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Rider
{
    [DataContract]
    public class TransferHistory
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public RiderBase Rider { get; set; }

        [DataMember]
        public TeamBase PreviousTeam { get; set; }

        [DataMember]
        public TeamBase CurrentTeam { get; set; }

        [DataMember]
        public long Bid { get; set; }
    }
}