﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class RiderHistory : ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<Achievement> Achievements { get; set; }
    }
}
