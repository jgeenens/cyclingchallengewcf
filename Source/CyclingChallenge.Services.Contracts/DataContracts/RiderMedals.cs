﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class RiderMedals
    {
        [DataMember]
        public byte Gold { get; set; }

        [DataMember]
        public byte Silver { get; set; }

        [DataMember]
        public byte Bronze { get; set; }
    }
}
