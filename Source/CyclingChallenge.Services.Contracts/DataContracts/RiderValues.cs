﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class RiderValues
    {
        [DataMember]
        public long Plains { get; set; }

        [DataMember]
        public long Cobblestones { get; set; }

        [DataMember]
        public long Hills { get; set; }

        [DataMember]
        public long Mountains { get; set; }

        [DataMember]
        public long Technique { get; set; }

        [DataMember]
        public long Stamina { get; set; }

        [DataMember]
        public long Descending { get; set; }

        [DataMember]
        public long Sprint { get; set; }

        [DataMember]
        public long TimeTrial { get; set; }

        [DataMember]
        public long Climbing { get; set; }

        [DataMember]
        public int Form { get; set; }

        [DataMember]
        public int Fatigue { get; set; }
    }
}
