﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Staff
{
    [DataContract]
    public class Manager: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte Type { get; set; }

        [DataMember]
        public byte Level { get; set; }

        [DataMember]
        public bool IsActivated { get; set; }

        [DataMember]
        public bool IsLearning { get; set; }

        [DataMember]
        public int? TrainingCost { get; set; }

        [DataMember]
        public int? TrainingTime { get; set; }

        [DataMember]
        public DateTime? StartTraining { get; set; }

        [DataMember]
        public Picture Picture { get; set; }
    }
}
