﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Team
{
    [DataContract]
    public class Division
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public byte Rank { get; set; }
    }
}
