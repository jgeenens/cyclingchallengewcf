﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Team
{
    [DataContract]
    public class Ranking: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public List<Team> Teams { get; set; }

        [DataMember]
        public List<RiderBase> Riders { get; set; }
    }
}
