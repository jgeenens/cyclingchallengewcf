﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Team
{
    [DataContract]
    public class RiderBase: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte Age { get; set; }

        [DataMember]
        public short Nationality { get; set; }

        [DataMember]
        public int? Points { get; set; }

        [DataMember]
        public long? Time { get; set; }

        [DataMember]
        public int? MountainPoints { get; set; }

        [DataMember]
        public byte? Rank { get; set; }

        [DataMember]
        public TeamBase Team { get; set; }
    }
}
