﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Team
{
    [DataContract]
    public class Team: TeamBase
    {
        [DataMember]
        public byte Rank { get; set; }

        [DataMember]
        public int Points { get; set; }

        [DataMember]
        public byte Wins { get; set; }

        [DataMember]
        public byte Stage { get; set; }

        [DataMember]
        public byte Champions { get; set; }

        [DataMember]
        public bool WillPromote { get; set; }

        [DataMember]
        public bool WillDemote { get; set; }
    }
}
