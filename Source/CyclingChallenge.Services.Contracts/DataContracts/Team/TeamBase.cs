﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Team
{
    [DataContract]
    public class TeamBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string LongName { get; set; }
    }
}