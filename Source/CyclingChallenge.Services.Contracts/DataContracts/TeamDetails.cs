﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System.Collections.Generic;
using System.Runtime.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Team;


namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class TeamDetails: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string LongName { get; set; }

        [DataMember]
        public bool IsUser { get; set; }

        [DataMember]
        public bool IsOnline { get; set; }

        [DataMember]
        public long? Money { get; set; }

        [DataMember]
        public User.User User { get; set; }

        [DataMember]
        public Division Division { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public List<PressMessage> PressMessages { get; set; }
    }
}
