﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace CyclingChallenge.Services.Contracts.DataContracts
{
    [DataContract]
    public class TeamRiders: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string LongName { get; set; }

        [DataMember]
        public List<Rider.Rider> Riders { get; set; }
    }
}
