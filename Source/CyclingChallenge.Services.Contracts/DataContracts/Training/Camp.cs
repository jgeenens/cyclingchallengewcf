﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Training
{
    [DataContract]
    public class Camp
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public short Name { get; set; }

        [DataMember]
        public City City { get; set; }

        [DataMember]
        public string Picture { get; set; }

        [DataMember]
        public byte PrimarySkill { get; set; }

        [DataMember]
        public byte SecondarySkill { get; set; }

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public DateTime? End { get; set; }
    }
}
