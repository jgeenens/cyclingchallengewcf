﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Training
{
    [DataContract]
    public class Improvement
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public byte Skill { get; set; }

        [DataMember]
        public byte Level { get; set; }

        [DataMember]
        public DateTime Change { get; set; }
    }
}
