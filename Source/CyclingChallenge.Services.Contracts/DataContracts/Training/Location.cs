﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Training
{
    [DataContract]
    public class Location
    {
        [DataMember]
        public short Id { get; set; }

        [DataMember]
        public short Name { get; set; }

        [DataMember]
        public short Country { get; set; }
    }
}
