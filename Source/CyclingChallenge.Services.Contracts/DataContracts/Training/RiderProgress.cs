﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Training
{
    [DataContract]
    public class RiderProgress
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<Improvement> Improvements { get; set; }
    }
}