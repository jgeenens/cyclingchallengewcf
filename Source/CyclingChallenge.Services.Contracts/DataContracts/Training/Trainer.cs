﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Training
{
    [DataContract]
    public class Trainer: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte Age { get; set; }

        [DataMember]
        public short Nationality { get; set; }

        [DataMember]
        public long Wage { get; set; }

        [DataMember]
        public byte Training { get; set; }

        [DataMember]
        public byte Skill { get; set; }

        [DataMember]
        public Picture Picture { get; set; }
    }
}
