﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.Training
{
    [DataContract]
    public class Training : ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte CurrentTraining { get; set; }

        [DataMember]
        public Trainer Trainer { get; set; }
    }
}
