﻿using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.User
{
    [DataContract]
    public class AccountDetails: ResultBase
    {
        [DataMember]
        public User User { get; set; }

        [DataMember]
        public bool HasRaceManager { get; set; }

        [DataMember]
        public bool IsOnHoliday { get; set; }

        [DataMember]
        public DateTime? StartOfHoliday { get; set; }

        [DataMember]
        public bool IsNameChangeAllowed { get; set; }
    }
}