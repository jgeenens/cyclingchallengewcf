﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.User
{
    [DataContract]
    public class CreateUserRequest
    {
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Birthday { get; set; }

        [DataMember]
        public string Language { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Country { get; set; }
    }
}
