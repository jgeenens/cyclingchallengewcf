﻿using CyclingChallenge.Services.Contracts.DataContracts.Location;
using System;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.User
{
    [DataContract]
    public class User
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Alias { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public byte Level { get; set; }

        [DataMember]
        public DateTime? Birthday { get; set; }

        [DataMember]
        public byte Language { get; set; }

        [DataMember]
        public short Currency { get; set; }

        [DataMember]
        public Country Country { get; set; }

        [DataMember]
        public int? Tokens { get; set; }
    }
}
