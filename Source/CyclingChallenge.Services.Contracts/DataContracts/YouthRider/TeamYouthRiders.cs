﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace CyclingChallenge.Services.Contracts.DataContracts.YouthRider
{
    [DataContract]
    public class TeamYouthRiders: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string LongName { get; set; }

        [DataMember]
        public List<YouthRider> Riders { get; set; }
    }
}
