﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.YouthRider
{
    [DataContract]
    public class YouthRider: ResultBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public byte Age { get; set; }

        [DataMember]
        public short Nationality { get; set; }

        [DataMember]
        public string Residence { get; set; }

        [DataMember]
        public TeamBase Team { get; set; }

        [DataMember]
        public Picture Picture { get; set; }

        [DataMember]
        public YouthRiderValues Values { get; set; }
    }
}
