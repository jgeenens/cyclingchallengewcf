﻿using System.Runtime.Serialization;

namespace CyclingChallenge.Services.Contracts.DataContracts.YouthRider
{
    [DataContract]
    public class YouthRiderValues
    {
        [DataMember]
        public byte Terrain { get; set; }

        [DataMember]
        public byte Skill { get; set; }
    }
}
