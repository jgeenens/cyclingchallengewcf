﻿using System;
using System.ServiceModel;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Contracts.DataContracts.Game;
using CyclingChallenge.Services.Contracts.DataContracts.Race;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using CyclingChallenge.Services.Contracts.DataContracts.YouthRider;

namespace CyclingChallenge.Services.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface ICyclingChallengeService
    {
        #region Get Methods

        [OperationContract]
        AccountDetails GetAccountDetails(long idUser, long? idRequestingUser = null);

        [OperationContract]
        Board GetBoard(byte idBoard, long? idUser = null);

        [OperationContract]
        MessageBoard GetBoards();

        [OperationContract]
        TrainingCamp GetCampForTeam(long idTeam);

        [OperationContract]
        Dictionary GetDictionary(byte idLanguage);

        [OperationContract]
        Economy GetEconomy(long idTeam);

        [OperationContract]
        Forum GetForum(short idForum, long? idUser = null);

        [OperationContract]
        Inbox GetInbox(long idTeam, int? page = null);

        [OperationContract]
        Message GetMessage(long idMessage, long? idUser = null);

        [OperationContract]
        OfficialMessages GetOfficialMessages(long idTeam, int? page = null);

        [OperationContract]
        Outbox GetOutbox(long idTeam, int? page = null);

        [OperationContract]
        PressMessages GetPressMessages(long idTeam, int? page = null);

        [OperationContract]
        RaceDescription GetRace(long idRace);

        [OperationContract]
        RaceHistory GetRaceHistory(long idRace, bool includeStages = false);

        [OperationContract]
        RaceRanking GetRaceParticipants(long idRace);

        [OperationContract]
        RaceDescription GetRaceProfile(long idRace);

        [OperationContract]
        RaceRanking GetRaceRanking(long idRace);

        [OperationContract]
        RaceReport GetRaceReport(long idRace);

        [OperationContract]
        Ranking GetRanking(long idDivision);

        [OperationContract]
        Rider GetRider(long idRider);

        [OperationContract]
        RiderEvents GetRiderEvents(long idRider);

        [OperationContract]
        RiderHistory GetRiderHistory(long idRider);

        [OperationContract]
        TeamRiders GetRidersForTeam(long idTeam);

        [OperationContract]
        SearchResults GetSearchResults(byte searchType, string firstSearchValue, string secondSearchValue = null);

        [OperationContract]
        Shop GetShop(long idTeam);

        [OperationContract]
        Sponsors GetSponsors(long idTeam);

        [OperationContract]
        StageDescription GetStage(long idStage);

        [OperationContract]
        RaceRanking GetStageRanking(long idRace, short idStage);

        [OperationContract]
        RaceReport GetStageReport(long idRace, short idStage);

        [OperationContract]
        Store GetStore(long idUser);

        [OperationContract]
        TeamDetails GetTeamDetails(long idTeam, long? idUser = null);

        [OperationContract]
        TeamDetails GetTeamDetailsForFacebookId(string facebookId);

        [OperationContract]
        TeamHistory GetTeamHistory(long idTeam);

        [OperationContract]
        Thread GetThread(long idThread, long? idUser = null);

        [OperationContract]
        Training GetTraining(long idTeam);

        [OperationContract]
        TeamTransactions GetTransactions(long idTeam, DateTime from, DateTime? to = null);

        [OperationContract]
        RiderTransferHistory GetTransferHistoryForRider(long idRider, DateTime from, DateTime? to = null);

        [OperationContract]
        TeamTransferHistory GetTransferHistoryForTeam(long idTeam, DateTime from, DateTime? to = null);

        [OperationContract]
        TeamTransfers GetTransfers(long idTeam);

        [OperationContract]
        TeamYouthRiders GetYouthRidersForTeam(long idTeam);

        #endregion


        #region Post Methods

        [OperationContract]
        ResultBase BuyShopProducts(long idTeam, params ProductBase[] products);

        [OperationContract]
        ResultBase CreateUser(string facebookId, CreateUserRequest createUserRequest);

        [OperationContract]
        ResultBase EditMessage(long idMessage, long idUser, string message);

        [OperationContract]
        ResultBase PostMessage(long idThread, long idUser, string userIp, string message, long? idMessageRepliedTo = null);

        [OperationContract]
        ResultBase SendMessage(long idUser, string recipientUserName, string subject, string message);

        [OperationContract]
        ResultBase SetRiderCategory(long idTeam, long idRider, byte category);

        [OperationContract]
        ResultBase SetShopPrices(long idTeam, params ProductBase[] products);

        [OperationContract]
        ResultBase SetTeamDetails(long idTeam, string shortName, string longName, byte idLogo);

        [OperationContract]
        ResultBase SetTraining(long idTeam, byte training);

        [OperationContract]
        ResultBase SetUserDetails(long idUser, string userName, string email, byte language, bool hideEmail);

        [OperationContract]
        ResultBase StartShop(long idTeam);

        #endregion
    }
}