﻿using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class CommandsBaseTest : TestFixtureBase
    {
        protected static void AssertFaceOk(byte face)
        {
            Assert.Less(0, face);
            Assert.Greater(7, face);
        }

        protected static void AssertHairOk(byte hair)
        {
            Assert.Less(0, hair);
            Assert.Greater(26, hair);
        }

        protected static void AssertEyesOk(byte eyes)
        {
            Assert.Less(0, eyes);
            Assert.Greater(14, eyes);
        }

        protected static void AssertLipsOk(byte lips)
        {
            Assert.Less(0, lips);
            Assert.Greater(18, lips);
        }

        protected static void AssertNoseOk(byte nose)
        {
            Assert.Less(0, nose);
            Assert.Greater(14, nose);
        }
    }
}