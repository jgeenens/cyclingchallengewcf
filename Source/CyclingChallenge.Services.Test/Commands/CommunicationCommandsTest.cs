﻿using System;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class CommunicationCommandsTest : TestFixtureBase
    {
        #region CreatePrivateMessage

        [Test, TestCase(Enums.Message.Type.Private), TestCase(Enums.Message.Type.Official)]
        public void CreatePrivateMessage_ValuesAssignedCorrectly(byte type)
        {
            var sender = CreateForTestingPurpose<User>();
            var recipient = CreateForTestingPurpose<User>();
            var subject = RandomUtil.RandomString();
            var message = RandomUtil.RandomString();
            var result = type == Enums.Message.Type.Private 
                ? CommunicationCommands.CreatePrivateMessage(Context, sender, recipient, subject, message)
                : CommunicationCommands.CreatePrivateMessage(Context, sender, recipient, subject, message, type);

            Assert.NotNull(result);
            Assert.AreEqual(sender.Id, result.Sender);
            Assert.AreEqual(recipient.Id, result.Recipient);
            Assert.AreEqual(subject, result.Subject);
            Assert.AreEqual(message, result.Message);
            Assert.AreEqual(type, result.Type);
            Assert.AreEqual(DateTime.Today.Date, result.Date.Date);
            Assert.False(result.Read);
            Assert.False(result.DeletedBySender);
            Assert.False(result.DeletedByRecipient);
        }

        #endregion


        #region CreateMessage

        [Test, TestCase(false), TestCase(true)]
        public void CreateMessage_ValuesAssignedCorrectly(bool withMessageRepliedTo)
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var userIp = RandomUtil.RandomString();
            var message = RandomUtil.RandomString();
            var messageRepliedTo = withMessageRepliedTo ? CreateForTestingPurpose<Message>(new { threadId, userId }) : null;
            var result = CommunicationCommands.CreateMessage(Context, threadId, userId, userIp, message, messageRepliedTo?.Id);

            Assert.NotNull(result);
            Assert.AreEqual(threadId, result.Thread);
            Assert.AreEqual(userId, result.User);
            Assert.AreEqual(DateTime.Today.Date, result.Date.Date);
            Assert.AreEqual(message, result.Content);
            Assert.AreEqual(userIp, result.UserIp);
            Assert.False(result.Obsolete);
            if (withMessageRepliedTo)
            {
                Assert.AreEqual(messageRepliedTo?.Id, result.ReplyTo);
                Assert.AreEqual(messageRepliedTo?.MessageInThread + 1, result.MessageInThread);
            }
            else
            {
                Assert.Null(result.ReplyTo);
                Assert.AreEqual(1, result.MessageInThread);
            }
        }

        #endregion


        #region UpdateMessage

        [Test]
        public void UpdateMessage_MessageNotNull_ChangesLastChangeUserChangeAndContent()
        {
            var userId= CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var message = CreateForTestingPurpose<Message>(new { threadId, userId });
            Assert.NotNull(message);
            Assert.Null(message.UserChange);
            var text = RandomUtil.RandomString();
            Assert.AreNotEqual(text, message.Content);

            CommunicationCommands.UpdateMessage(message, userId, text);

            Assert.NotNull(message.UserChange);
            Assert.AreEqual(userId, message.UserChange);
            Assert.AreEqual(text, message.Content);
            Assert.NotNull(message.LastChange);
            Assert.AreEqual(DateTime.Today.Date, message.LastChange.Value.Date);
        }

        #endregion
    }
}