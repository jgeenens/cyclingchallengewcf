﻿using System;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class EconomyCommandsTest : TestFixtureBase
    {
        #region CreateEconomy

        [Test]
        public void CreateEconomy_ValuesAssignedCorrectly()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var wages = RandomUtil.RandomNumber(5);
            var weekAndDay = GameQueries.GetWeekAndDayInSeason(Context);

            var result = EconomyCommands.CreateEconomy(Context, idTeam, wages);

            Assert.NotNull(result);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(Enums.Economy.Status.Active, result.Active);
            Assert.AreEqual(wages, result.Expense2);
            Assert.AreEqual(DateTime.Now.AddDays(7).Date, result.ReferenceDate.Date);
            Assert.AreEqual(weekAndDay.Week, result.Week);
            Assert.AreEqual(weekAndDay.Day, result.Day);
        }

        #endregion


        #region InitializeSponsor

        [Test]
        public void InitializeSponsor_SponsorNotNull_SetsTeamToNullAndWeeksToGoToZero()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var sponsor = CreateForTestingPurpose<Sponsor>(new { teamId = idTeam });
            Assert.NotNull(sponsor.Team);
            Assert.AreNotEqual(0, sponsor.WeeksToGo);

            EconomyCommands.InitializeSponsor(sponsor);
            Context.SaveChanges();

            Assert.Null(sponsor.Team);
            Assert.AreEqual(0, sponsor.WeeksToGo);
        }

        #endregion
    }
}