﻿using System;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Team;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class GameCommandsTest : TestFixtureBase
    {
        #region CreateRegistration

        [Test]
        public void CreateRegistration_ValuesAssignedCorrectly()
        {
            var idCountry = CreateIdForTestingPurpose<Country>();
            var result = GameCommands.CreateRegistration(Context, (short)idCountry);

            Assert.NotNull(result);
            Assert.AreEqual(DateTime.Today.Date, result.Date);
            Assert.AreEqual(1, result.Quantity);
            Assert.AreEqual(idCountry, result.Country);
        }

        #endregion


        #region CreateSale

        [Test]
        public void CreateSale_ValuesAssignedCorrectly()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var logo = CreateForTestingPurpose<Logo>();
            var result = GameCommands.CreateSale(Context, idUser, logo);

            Assert.NotNull(result);
            Assert.AreEqual(DateTime.Today.Date, result.Date.Date);
            Assert.AreEqual(1, result.Amount);
            Assert.AreEqual(AddOns.Logo, result.Addon);
            Assert.AreEqual(logo.Tokens, result.Price);
            Assert.AreEqual(logo.Id.ToString(), result.Status);
        }

        #endregion
    }
}