﻿using System;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class PlayerCommandsTest : CommandsBaseTest
    {
        #region CreatePlayer

        [Test, TestCase(false), TestCase(true)]
        public void CreatePlayer_ValuesAssignedCorrectly(bool isYouth)
        {
            var team = CreateForTestingPurpose<Team>(new { country = LocationQueries.GetRandomActiveCountry(Context).Id });
            var result = PlayerCommands.CreatePlayer(Context, team.Id, team.Country, isYouth);

            Assert.NotNull(result);
            Assert.IsNotNullOrEmpty(result.FirstName);
            Assert.IsNotNullOrEmpty(result.LastName);
            Assert.AreEqual(team.Id, result.Team);
            Assert.AreEqual(team.Country, result.Nationality);
            Assert.AreEqual(team.Country, result.PlayerCity.CityRegion.Country);
            Assert.False(result.Retired);
            AssertFaceOk(result.Face);
            AssertHairOk(result.Hair);
            AssertEyesOk(result.Eyes);
            AssertLipsOk(result.Lips);
            AssertNoseOk(result.Nose);
            if (isYouth)
            {
                Assert.Greater(18, result.Age);
            }
            else
            {
                Assert.LessOrEqual(18, result.Age);
            }
        }

        #endregion


        #region CreatePlayerValues

        [Test]
        public void CreatePlayerValues_ValuesAssignedCorrectly()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var randomValue1 = RandomUtil.RandomNumber(5);
            var randomValue2 = RandomUtil.RandomNumber(5);
            var minValues = Math.Min(randomValue1, randomValue2);
            var maxValues = Math.Max(randomValue1, randomValue2);
            var result = PlayerCommands.CreatePlayerValues(Context, player, minValues, maxValues);

            Assert.NotNull(result);
            Assert.AreEqual(player.Id, result.Player);
            Assert.AreEqual(team.ShortName, result.Team);
            Assert.AreEqual(1000, result.Form);
            Assert.AreEqual(1000, result.FormTarget);
            Assert.AreEqual(player.City, result.City);
            Assert.AreEqual(player.Age < 18, result.IsYouth);
            AssertValuesOk(result, minValues, maxValues);
        }

        #endregion


        #region SetWage

        [Test]
        public void SetWage_IsYouth_WageIsZero()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id, age = 16 });
            var playerValues = CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id, isYouth = true, wage = 0 });

            PlayerCommands.SetWage(playerValues);
            Assert.AreEqual(0, playerValues.Wage);
        }

        [Test, TestCase(false), TestCase(true)]
        public void SetWage_CalculatesAndAssignsTheWage(bool allZero)
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var playerValues = allZero
                ? CreateForTestingPurpose<PlayerValue>(new
                {
                    playerId = player.Id,
                    climbing = 0, technique = 0, descending = 0, stamina = 0, experience = 0, sprint = 0, timetrial = 0,
                    cobblestones = 0, plains = 0, hills = 0, mountains = 0
                })
                : CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id });

            PlayerCommands.SetWage(playerValues);
            if (allZero)
            {
                Assert.AreEqual(1000, playerValues.Wage);
            }
            else
            {
                var wage = Math.Pow(playerValues.Plains + playerValues.Mountains + playerValues.Hills + playerValues.Cobblestones, 2) / 3000000;
                wage *= playerValues.Stamina / 12.0;
                wage += Math.Pow(playerValues.Technique + playerValues.Climbing + playerValues.Descending + playerValues.Timetrial + playerValues.Sprint, 2) / 300;
                Assert.AreEqual((long)Math.Floor(Math.Max(1000, wage / 2500000)), playerValues.Wage);
            }
        }

        #endregion


        #region Private Methods

        private static void AssertValuesOk(PlayerValue playerValues, long minValues, long maxValues)
        {
            var sum = playerValues.Climbing + playerValues.Stamina + playerValues.Sprint + playerValues.Descending +
                      playerValues.Technique + playerValues.Cobblestones + playerValues.Hills + playerValues.Mountains + 
                      playerValues.Plains + playerValues.Timetrial + playerValues.Experience;
            Assert.LessOrEqual(minValues, sum / 11);
            Assert.Greater(maxValues, sum / 11);
        }

        #endregion
    }
}