﻿using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class StaffCommandsTest : CommandsBaseTest
    {
        #region CreateManager

        [Test]
        public void CreateManager_ValuesAssignedCorrectly()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var result = StaffCommands.CreateManager(Context, idTeam, Managers.Type.Race);

            Assert.NotNull(result);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(Managers.Type.Race, result.Type);
            Assert.IsNotNullOrEmpty(result.FirstName);
            Assert.IsNotNullOrEmpty(result.LastName);
            AssertFaceOk(result.Face);
            AssertHairOk(result.Hair);
            AssertEyesOk(result.Eyes);
            AssertLipsOk(result.Lips);
            AssertNoseOk(result.Nose);
        }

        #endregion


        #region InitializeManager

        [Test]
        public void InitializeManager_ManagerNotNull_SetsLearningToLevelToApprenticeAndStatusToLocked()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var manager = CreateForTestingPurpose<Manager>(new { teamId = idTeam, level = Managers.Level.Expert });
            Assert.NotNull(manager.Learning);
            Assert.AreNotEqual(Managers.Level.Apprentice, manager.Level);
            Assert.AreNotEqual(Managers.Status.Locked, manager.Status);

            StaffCommands.InitializeManager(manager);

            Assert.Null(manager.Learning);
            Assert.AreEqual(Managers.Level.Apprentice, manager.Level);
            Assert.AreEqual(Managers.Status.Locked, manager.Status);
        }

        #endregion


        #region CreateScout

        [Test, TestCase(Scouts.Status.Locked), TestCase(Scouts.Status.Active)]
        public void CreateScout_ValuesAssignedCorrectly(byte status)
        {
            var team = CreateForTestingPurpose<Team>(new { country = LocationQueries.GetRandomActiveCountry(Context).Id });
            var result = StaffCommands.CreateScout(Context, team.Id, team.Country, status);

            Assert.NotNull(result);
            Assert.AreEqual(team.Id, result.Team);
            Assert.AreEqual(team.Country, result.Nationality);
            Assert.AreEqual(status, result.Status);
            Assert.IsNotNullOrEmpty(result.FirstName, $"{team.Country}");
            Assert.IsNotNullOrEmpty(result.LastName);
            AssertFaceOk(result.Face);
            AssertHairOk(result.Hair);
            AssertEyesOk(result.Eyes);
            AssertLipsOk(result.Lips);
            AssertNoseOk(result.Nose);
        }

        #endregion


        #region InitializeScout

        [Test]
        public void InitializeScout_ScoutNotNull_SetsWeeksToZeroYouthToFalseSkillToApprenticeAndWageToThousandAndChangesAge()
        {
            var team = CreateForTestingPurpose<Team>();
            var scout = CreateForTestingPurpose<Scout>(new
            {
                teamId = team.Id,
                status = Scouts.Status.Scouting,
                skill = Scouts.Level.Expert,
                weeksToGo = RandomUtil.RandomNumber(1) + 1,
                totalWeeks = RandomUtil.RandomNumber(),
                age = 18
            });
            Assert.AreEqual(18, scout.Age);
            Assert.AreNotEqual(0, scout.WeeksToGo);
            Assert.AreNotEqual(0, scout.TotalWeeks);
            Assert.AreNotEqual(1000, scout.Wage);
            Assert.AreNotEqual(Scouts.Level.Apprentice, scout.Skill);
            Assert.True(scout.LookingForYouth);

            StaffCommands.InitializeScout(scout);

            Assert.AreNotEqual(18, scout.Age);
            Assert.AreEqual(0, scout.WeeksToGo);
            Assert.AreEqual(0, scout.TotalWeeks);
            Assert.AreEqual(1000, scout.Wage);
            Assert.AreEqual(Scouts.Level.Apprentice, scout.Skill);
            Assert.False(scout.LookingForYouth);
        }

        #endregion
    }
}