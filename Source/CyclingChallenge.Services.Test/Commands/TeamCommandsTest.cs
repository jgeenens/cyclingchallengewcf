﻿using System;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class TeamCommandsTest : TestFixtureBase
    {
        #region InitializeTeam

        [Test]
        public void InitializeTeam_TeamNotNull_SetsInitialValuesForTeam()
        {
            var newTeamName = RandomUtil.RandomCharString();
            var initialMoney = RandomUtil.RandomNumber(5);
            var initialSupporters = (short)RandomUtil.RandomNumber(3);

            var team = CreateForTestingPurpose<Team>(new
            {
                isActive = false,
                isBot = true,
                hasShop = true,
                money = -100000,
                predicted = -100000,
                transactions = 100000,
                training = Rider.Skill.Technique
            });
            Assert.False(team.IsActive);
            Assert.True(team.IsBot);
            Assert.True(team.HasShop);
            Assert.AreNotEqual(newTeamName, team.Name);
            Assert.AreNotEqual(initialMoney, team.Money);
            Assert.AreNotEqual(initialMoney, team.Predicted);
            Assert.AreNotEqual(0, team.Transactions);
            Assert.AreNotEqual(initialSupporters, team.Supporters);
            Assert.AreNotEqual(Rider.Skill.Stamina, team.Training);
            Assert.AreNotEqual(Rider.Terrain.Plains, team.YouthTraining);
            Assert.AreNotEqual(0, team.NotParticipated);
            Assert.NotNull(team.LastCamp);

            TeamCommands.InitializeTeam(Context, team, newTeamName, initialMoney, initialSupporters);

            Assert.True(team.IsActive);
            Assert.False(team.IsBot);
            Assert.False(team.HasShop);
            Assert.AreEqual(newTeamName, team.Name);
            Assert.AreEqual(initialMoney, team.Money);
            Assert.AreEqual(initialMoney, team.Predicted);
            Assert.AreEqual(0, team.Transactions);
            Assert.AreEqual(initialSupporters, team.Supporters);
            Assert.AreEqual(Rider.Skill.Stamina, team.Training);
            Assert.AreEqual(Rider.Terrain.Plains, team.YouthTraining);
            Assert.AreEqual(0, team.NotParticipated);
            Assert.Null(team.LastCamp);
            Assert.NotNull(team.TeamLogo);
        }

        #endregion


        #region CreateTeamHistory

        [Test]
        public void CreateTeamHistory_ValuesAssignedCorrectly()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = TeamCommands.CreateTeamHistory(Context, team.Id, Events.Team.NewTeam, team.Name);

            Assert.NotNull(result);
            Assert.AreEqual(team.Id, result.Team);
            Assert.AreEqual(DateTime.Today.Date, result.Date.Date);
            Assert.AreEqual(Events.Team.NewTeam, result.Event);
            Assert.AreEqual(team.Name, result.Data);
            Assert.False(result.Seen);
        }

        #endregion


        #region GetUniqueTeamName

        [Test]
        public void GetUniqueTeamName_ReturnsNonEmptyStringAndTeamNameIsUnique()
        {
            var result = TeamCommands.GetUniqueTeamName(Context, FixedID.ActiveCountry);
            Assert.IsInstanceOf<string>(result);
            Assert.IsNotNullOrEmpty(result);
            Assert.IsEmpty(TeamQueries.GetTeamsByName(Context, result));
        }

        #endregion


        #region GetTeamNameSuggestion

        [Test]
        public void GetTeamNameSuggestion_ReturnsNonEmptyString()
        {
            var result = TeamCommands.GetTeamNameSuggestion(Context, FixedID.ActiveCountry);
            Assert.IsInstanceOf<string>(result);
            Assert.IsNotNullOrEmpty(result);
        }

        #endregion
    }
}