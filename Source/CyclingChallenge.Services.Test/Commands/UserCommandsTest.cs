﻿using System;
using System.Collections.Generic;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Commands
{
    [TestFixture]
    public class UserCommandsTest : TestFixtureBase
    {
        #region CreateUser

        [Test]
        public void CreateUser_SettingsDoesNotContainTokensKey_ReturnsNull()
        {
            var country = LocationQueries.GetCountryById(Context, FixedID.Country);
            Assert.Null(UserCommands.CreateUser(Context, new Dictionary<long, long>(), 1, country, Communication.Language.Dutch));
        }

        [Test]
        public void CreateUser_CountryIsNull_ReturnsNull()
        {
            var settings = new Dictionary<long, long> { { Settings.Tokens, 0 } };
            Assert.Null(UserCommands.CreateUser(Context, settings, 1, null, Communication.Language.Dutch));
        }

        [Test]
        public void CreateUser_FirstNameLastNameAndBirthdayIsNull_ReturnsNull()
        {
            var settings = new Dictionary<long, long> { { Settings.Tokens, 0 } };
            var country = LocationQueries.GetCountryById(Context, FixedID.Country);
            Assert.Null(UserCommands.CreateUser(Context, settings, 1, country, Communication.Language.Dutch));
        }

        [Test]
        public void CreateUser_OnlyOneFromFirstNameLastNameAndBirthdayIsNotNull_ReturnsNull()
        {
            var settings = new Dictionary<long, long> { { Settings.Tokens, 0 } };
            var country = LocationQueries.GetCountryById(Context, FixedID.Country);
            Assert.Null(UserCommands.CreateUser(Context, settings, 1, country, Communication.Language.Dutch, RandomUtil.RandomString()));
            Assert.Null(UserCommands.CreateUser(Context, settings, 1, country, Communication.Language.Dutch, lastName: RandomUtil.RandomString()));
            Assert.Null(UserCommands.CreateUser(Context, settings, 1, country, Communication.Language.Dutch, birthDate: DateTime.Now));
        }

        [Test]
        public void CreateUser_ParametersValid_ValuesAssignedCorrectly()
        {
            var settings = new Dictionary<long, long> { { Settings.Tokens, RandomUtil.RandomNumber(4) } };
            var facebookId = RandomUtil.RandomNumber(6);
            var country = LocationQueries.GetCountryById(Context, FixedID.Country);
            var firstName = RandomUtil.RandomCharString();
            var lastName = RandomUtil.RandomCharString();
            var email = RandomUtil.RandomString();
            var birthDate = DateTime.Today.AddYears(-20);

            var result = UserCommands.CreateUser(Context, settings, facebookId, country, Communication.Language.Dutch, firstName, lastName, email, birthDate);

            Assert.NotNull(result);
            Assert.AreEqual(facebookId, result.FacebookId);
            Assert.AreEqual(firstName, result.FirstName);
            Assert.AreEqual(lastName, result.LastName);
            Assert.AreEqual(email, result.Email);
            Assert.AreEqual(birthDate, result.Birthday);
            Assert.True(result.UserCountry.Active);
            if (country.Active)
            {
                Assert.AreEqual(country.Id, result.Country);
            }
            Assert.AreEqual(country.Id, result.RealCountry);
            Assert.IsNotNullOrEmpty(result.Username);
            Assert.AreEqual(DateTime.Today.Date, result.Registration.Date);
            Assert.IsNotNullOrEmpty(result.Reference);
            Assert.True(result.Online);
            Assert.GreaterOrEqual(DateTime.Now, result.OnlineSince);
            Assert.True(result.EmailHidden);
            Assert.GreaterOrEqual(DateTime.Now, result.LastHit);
            Assert.AreEqual(settings[Settings.Tokens], result.Tokens);
            Assert.IsEmpty(result.Live);
            Assert.False(result.Vacation);
            Assert.GreaterOrEqual(DateTime.Now.AddYears(-1), result.VacationSince);
            Assert.AreEqual(0, result.UserLevel);
            Assert.AreEqual(Communication.Language.Dutch, result.Language);
            Assert.AreEqual(DateTime.Today.ToString("yyyyMMddHHmm").Substring(0,8), result.LoginHistory.Substring(0,8));
        }

        #endregion


        #region GetUserNameSuggestions

        [Test]
        public void GetUserNameSuggestions_OnlyOneParameter_ReturnsNull()
        {
            var result = UserCommands.GetUserNameSuggestions(null, RandomUtil.RandomCharString(), null);
            Assert.IsNull(result);

            result = UserCommands.GetUserNameSuggestions(RandomUtil.RandomCharString(), null, null);
            Assert.IsNull(result);

            result = UserCommands.GetUserNameSuggestions(null, null, DateTime.Now);
            Assert.IsNull(result);
        }

        [Test]
        public void GetUserNameSuggestions_MoreThanOneParameter_ReturnsListOfSuggestions()
        {
            var result = UserCommands.GetUserNameSuggestions(RandomUtil.RandomCharString(), null, DateTime.Now);
            Assert.IsNotEmpty(result);

            result = UserCommands.GetUserNameSuggestions(null, RandomUtil.RandomCharString(), DateTime.Now);
            Assert.IsNotEmpty(result);

            result = UserCommands.GetUserNameSuggestions(RandomUtil.RandomCharString(), RandomUtil.RandomCharString(), null);
            Assert.IsNotEmpty(result);

            result = UserCommands.GetUserNameSuggestions(RandomUtil.RandomCharString(), RandomUtil.RandomCharString(), DateTime.Now);
            Assert.IsNotEmpty(result);
        }

        #endregion
    }
}