﻿using System;
using System.Collections.Generic;
using System.Web.Routing;
using CyclingChallenge.Services.Models;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test
{
    public abstract class TestFixtureBase
    {
        public ICyclingChallengeModelContainer Context;

        #region Constructor

        protected TestFixtureBase()
        {
            Context = RuntimeManager.CreateDbContext();
        }

        #endregion


        #region CRUD Methods

        protected T CreateForTestingPurpose<T>(object parameters = null) where T: class
        {
            return CreateForTestingPurpose<T>(parameters != null ? new RouteValueDictionary(parameters) : null);
        }

        protected T CreateForTestingPurpose<T>(IDictionary<string, object> parameters) where T: class
        {
            var id = CreateForTestPurpose.CreateForTestingPurpose.Process<T>(GetJArrayWithParameters(parameters));
            return GetEntityById<T>(id);
        }

        protected long CreateIdForTestingPurpose<T>(object parameters = null)
        {
            return CreateIdForTestingPurpose<T>(parameters != null ? new RouteValueDictionary(parameters) : null);
        }

        protected long CreateIdForTestingPurpose<T>(IDictionary<string, object> parameters)
        {
            return CreateForTestPurpose.CreateForTestingPurpose.Process<T>(GetJArrayWithParameters(parameters));
        }

        #endregion


        #region Private methods

        private static JArray GetJArrayWithParameters(IDictionary<string, object> parameters)
        {
            var jObj = new JObject
            {
                ["remarks"] = "Unit Test (Cycling Challenge Service WCF)"
            };

            if (parameters != null && parameters.Count > 0)
            {
                foreach (var param in parameters)
                {
                    jObj[param.Key] = Convert.ToString(param.Value);
                }
            }

            return new JArray(jObj);
        }

        private T GetEntityById<T>(long id) where T: class
        {
            return Context.Set<T>().Find(id);
        }

        #endregion
    }
}
