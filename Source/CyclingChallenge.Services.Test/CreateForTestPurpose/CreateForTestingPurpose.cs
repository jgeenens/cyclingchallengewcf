﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CyclingChallenge.Services.Models;
using CreateMethod = System.Action<CyclingChallenge.Services.Models.ICyclingChallengeModelContainer, Newtonsoft.Json.Linq.JArray, Newtonsoft.Json.Linq.JToken>;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public partial class CreateForTestingPurpose
    {
        private static T GetEntry<T>(JToken entries, string name)
            where T : IConvertible
        {
            if (entries[name] == null)
            {
                throw new Exception($"Expected '{name}' argument for CreateForTestingPurpose.");
            }

            // Custom logic for datetime to avoid parsing issues
            if (typeof(T) == typeof(DateTime?) || typeof(T) == typeof(DateTime))
            {
                return (T)(object)DateTime.Parse(entries[name].ToString());
            }

            return (T)Convert.ChangeType(entries[name], typeof(T));
        }

        private static T GetOptionalEntry<T>(JToken entries, string name, T defaultValue)
        {
            try
            {
                if (entries[name] == null || string.IsNullOrEmpty(entries[name].ToString()))
                {
                    return defaultValue;
                }

                // Custom logic for datetime to avoid parsing issues
                if (typeof(T) == typeof(DateTime?) || typeof(T) == typeof(DateTime))
                {
                    return (T)(object)DateTime.Parse(entries[name].ToString());
                }

                return (T)Convert.ChangeType(entries[name], typeof(T));
            }
            catch (Exception ex)
            {
                throw new Exception($"Could not format '{entries[name]}' to type of '{typeof(T)}'", ex);
            }
        }

        private static readonly Dictionary<string, CreateMethod> methods = new Dictionary<string, CreateMethod>();

        static CreateForTestingPurpose()
        {
            // Get all methods starting with Create_ and keep them as create methods
            foreach (var method in typeof(CreateForTestingPurpose).GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            {
                if (method.Name.StartsWith("Create_"))
                {
                    methods[method.Name.Substring(7)] = (CreateMethod)Delegate.CreateDelegate(typeof(CreateMethod), method);
                }
            }
        }

        public static long Process<T>(JArray content)
        {
            var result = new JArray();
            using (var context = RuntimeManager.CreateDbContext())
            {
                foreach (var entry in content)
                {
                    var type = typeof(T).Name;
                    if (!methods.TryGetValue(type, out var method))
                    {
                        throw new Exception($"Unknown type supplied for CreateForTestingPurpose: {type}.");
                    }

                    method(context, result, entry);
                }
            }

            var obj = result.FirstOrDefault();
            return obj != null ? long.Parse(obj["objectId"].ToString()) : 0;
        }
    }
}
