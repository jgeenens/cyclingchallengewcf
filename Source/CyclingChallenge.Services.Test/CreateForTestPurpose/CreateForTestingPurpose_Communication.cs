﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Message(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var threadId = GetEntry<long>(entry, "threadId");
            var userId = GetEntry<long>(entry, "userId");
            var obsolete = GetOptionalEntry(entry, "obsolete", false);
            var replyTo = GetOptionalEntry<long?>(entry, "replyTo", null);
            var userChange = GetOptionalEntry<long?>(entry, "userChange", null);

            var message = new Message
            {
                Thread = threadId,
                MessageInThread = (byte)RandomUtil.RandomNumber(),
                User = userId,
                ReplyTo = replyTo,
                Date = DateTime.Now,
                Content = RandomUtil.RandomString(),
                UserChange = userChange,
                LastChange = userChange.HasValue ? DateTime.Now : (DateTime?)null,
                UserIp = RandomUtil.RandomString(),
                Obsolete = obsolete
            };
            context.Messages.AddOrUpdate(message);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Message"), new JProperty("objectId", message.Id)));
        }

        private static void Create_PrivateMessage(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var sender = GetOptionalEntry<long?>(entry, "sender", null);
            var recipient = GetOptionalEntry<long?>(entry, "recipient", null);
            var subject = GetOptionalEntry(entry, "subject", RandomUtil.RandomCharString());
            var body = GetOptionalEntry(entry, "body", RandomUtil.RandomString(50));
            var deletedByRecipient = GetOptionalEntry(entry, "deletedByRecipient", false);
            var deletedBySender = GetOptionalEntry(entry, "deletedBySender", false);
            var type = GetOptionalEntry(entry, "type", Enums.Message.Type.Private);

            var message = new PrivateMessage
            {
                Sender = sender,
                Recipient = recipient,
                Subject = subject,
                Message = body,
                Date = DateTime.Now,
                Read = false,
                DeletedByRecipient = deletedByRecipient,
                DeletedBySender = deletedBySender,
                Type = type
            };
            context.PrivateMessages.AddOrUpdate(message);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "PrivateMessage"), new JProperty("objectId", message.Id)));
        }

        private static void Create_PressMessage(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var userId = GetEntry<long>(entry, "userId");
            var messageId = GetOptionalEntry<long?>(entry, "messageId", null);

            var message = new PressMessage
            {
                User = userId,
                Title = RandomUtil.RandomCharString(20),
                Body = RandomUtil.RandomCharString(255),
                Date = DateTime.Now,
                Message = messageId
            };
            context.PressMessages.AddOrUpdate(message);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "PressMessage"), new JProperty("objectId", message.Id)));
        }

        private static void Create_Thread(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var userId = GetEntry<long>(entry, "userId");
            var boardId = GetOptionalEntry(entry, "boardId", FixedID.Board);
            var lastChange = GetOptionalEntry(entry, "lastChange", DateTime.Now);

            var thread = new Thread
            {
                Board = boardId,
                Title = RandomUtil.RandomCharString(20),
                Date = DateTime.Now,
                Originator = userId,
                LastChange = lastChange,
                LastPost = RandomUtil.RandomNumber(),
                LastOriginator = userId
            };
            context.Threads.AddOrUpdate(thread);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Thread"), new JProperty("objectId", thread.Id)));
        }

        private static void Create_ThreadViewed(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var userId = GetEntry<long>(entry, "userId");
            var threadId = GetEntry<long>(entry, "threadId");

            var threadViewed = new ThreadViewed
            {
                Thread = threadId,
                User = userId,
                LastView = DateTime.Now
            };
            context.ThreadsViewed.AddOrUpdate(threadViewed);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "ThreadViewed"), new JProperty("objectId", threadViewed.Id)));
        }
    }
}
