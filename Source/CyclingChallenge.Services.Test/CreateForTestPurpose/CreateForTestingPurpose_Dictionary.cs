﻿using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Language(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var nameId = GetOptionalEntry(entry, "name", FixedID.LanguageName);
            var abbreviations = GetOptionalEntry(entry, "abbreviations", RandomUtil.RandomCharString(5));

            var language = new Language
            {
                Name = nameId,
                Abbreviations = abbreviations
            };
            context.Languages.AddOrUpdate(language);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Language"), new JProperty("objectId", language.Id)));
        }
    }
}