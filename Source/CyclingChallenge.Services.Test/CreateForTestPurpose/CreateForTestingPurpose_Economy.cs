﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Clubshop(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var quantity1 = GetOptionalEntry(entry, "quantity1", (short)RandomUtil.RandomNumber());

            var clubshop = new Clubshop
            {
                Positions = 0,
                Top = 0,
                Points = 0,
                Wins = 0,
                Participated = 0,
                Hint = Items.ParticipationHint,
                Team = teamId,
                Quantity1 = quantity1,
                Quantity2 = (short)RandomUtil.RandomNumber(),
                Quantity3 = (short)RandomUtil.RandomNumber(),
                Quantity4 = (short)RandomUtil.RandomNumber(),
                Quantity5 = (short)RandomUtil.RandomNumber()
            };
            context.Clubshops.AddOrUpdate(clubshop);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Clubshop"), new JProperty("objectId", clubshop.Id)));
        }

        private static void Create_Currency(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var countryId = GetOptionalEntry(entry, "countryId", FixedID.Country);

            var currency = new Currency
            {
                Country = countryId,
                Rate = 1,
                Description = RandomUtil.RandomCharString(),
                Separator = ".",
                DecimalSeparator = ",",
                Code = RandomUtil.RandomCharString(3)
            };
            context.Currencies.AddOrUpdate(currency);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Currency"), new JProperty("objectId", currency.Id)));
        }

        private static void Create_Economy(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var active = GetOptionalEntry(entry, "active", Enums.Economy.Status.Active);

            var economy = new Services.Models.Economy.Economy
            {
                Team = teamId,
                ReferenceDate = DateTime.Now,
                Week = (byte)(RandomUtil.RandomNumber() % 16 + 1),
                Day = (byte)(RandomUtil.RandomNumber() % 7 + 1),
                Income1 = RandomUtil.RandomNumber(4),
                Income2 = RandomUtil.RandomNumber(4),
                Expense1 = RandomUtil.RandomNumber(4),
                Expense2 = RandomUtil.RandomNumber(4),
                Expense3 = RandomUtil.RandomNumber(4),
                Expense4 = RandomUtil.RandomNumber(4),
                Expense5 = RandomUtil.RandomNumber(4),
                TransactionIn1 = (int)RandomUtil.RandomNumber(3),
                TransactionIn2 = (int)RandomUtil.RandomNumber(3),
                TransactionIn3 = (int)RandomUtil.RandomNumber(3),
                TransactionIn4 = (int)RandomUtil.RandomNumber(3),
                TransactionOut1 = (int)RandomUtil.RandomNumber(3),
                TransactionOut2 = (int)RandomUtil.RandomNumber(3),
                TransactionOut3 = (int)RandomUtil.RandomNumber(3),
                TransactionOut4 = (int)RandomUtil.RandomNumber(3),
                TransactionOut5 = (int)RandomUtil.RandomNumber(3),
                Active = active
            };
            context.Economies.AddOrUpdate(economy);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Economy"), new JProperty("objectId", economy.Id)));
        }

        private static void Create_Sponsor(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry<long?>(entry, "teamId", null);
            var divisionId = GetOptionalEntry(entry, "divisionId", FixedID.Division);
            var countryId = GetOptionalEntry(entry, "countryId", FixedID.Country);
            var type = GetOptionalEntry(entry, "type", (byte)(RandomUtil.RandomNumber(1) % 4 + 1));
            var weeksToGo = GetOptionalEntry(entry, "weeksToGo", (byte)(RandomUtil.RandomNumber(1) + 1));

            var sponsor = new Sponsor
            {
                Name = RandomUtil.RandomCharString(),
                Team = teamId,
                Division = divisionId,
                Country = countryId,
                Weekly = (int)RandomUtil.RandomNumber(5),
                Signing = RandomUtil.RandomNumber(6),
                Weeks = (byte)(RandomUtil.RandomNumber() + 9),
                WeeksToGo = weeksToGo,
                Type = type
            };
            context.Sponsors.AddOrUpdate(sponsor);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Sponsor"), new JProperty("objectId", sponsor.Id)));
        }

        private static void Create_Transaction(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var outgoing = GetOptionalEntry(entry, "outgoing", false);
            var description = GetOptionalEntry<short>(entry, "description", Items.TransactionShopPurchase);
            var alreadyChecked = GetOptionalEntry(entry, "alreadyChecked", false);
            var info = GetOptionalEntry(entry, "info", string.Empty);

            var transaction = new Transaction
            {
                Team = teamId,
                Outgoing = outgoing,
                DateTransaction = DateTime.Now,
                Value = RandomUtil.RandomNumber(3),
                Description = description,
                Checked = alreadyChecked,
                Info = info
            };
            context.Transactions.AddOrUpdate(transaction);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Transaction"), new JProperty("objectId", transaction.Id)));
        }
    }
}
