﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Commodity(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var product = new Commodity
            {
                Title = Products.CCDiamonds12000,
                Description = ProductDescriptions.CCDiamonds12000,
                Value = (int)RandomUtil.RandomNumber(4)
            };
            context.Commodities.AddOrUpdate(product);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Commodity"), new JProperty("objectId", product.Id)));
        }

        private static void Create_Price(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var idProduct = GetOptionalEntry(entry, "productId", FixedID.StoreProduct);
            var idCurrency = GetOptionalEntry(entry, "currencyId", Economy.Currency.Dollar);

            var price = new Price
            {
                Commodity = idProduct,
                Currency = idCurrency,
                Amount = RandomUtil.RandomNumber()
            };
            context.Prices.AddOrUpdate(price);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Price"), new JProperty("objectId", price.Id)));
        }

        private static void Create_Registration(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var idCountry = GetOptionalEntry(entry, "countryId", FixedID.Country);

            var registration = new Registration
            {
                Country = idCountry,
                Date = DateTime.Today,
                Quantity = (short)(RandomUtil.RandomNumber() + 1)
            };
            context.Registrations.AddOrUpdate(registration);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Registration"), new JProperty("objectId", registration.Id)));
        }

        private static void Create_Sale(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var idUser = GetEntry<long>(entry, "userId");
            var addon = GetOptionalEntry(entry, "addon", AddOns.Logo);
            var status = GetOptionalEntry(entry, "status", RandomUtil.RandomString());

            var sale = new Sale
            {
                User = idUser,
                Addon = addon,
                Date = DateTime.Now,
                Price = RandomUtil.RandomNumber(),
                Status = status
            };
            context.Sales.AddOrUpdate(sale);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Sale"), new JProperty("objectId", sale.Id)));
        }
    }
}