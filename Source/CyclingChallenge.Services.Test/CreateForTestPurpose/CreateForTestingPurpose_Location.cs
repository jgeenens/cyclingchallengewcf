﻿using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_City(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var cityName = GetOptionalEntry(entry, "city", RandomUtil.RandomCharString());
            var village = GetOptionalEntry(entry, "village", cityName);

            var city = new City
            {
                Name = cityName,
                Village = village,
                Region = FixedID.Region
            };
            context.Cities.AddOrUpdate(city);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "City"), new JProperty("objectId", city.Id)));
        }

        private static void Create_Climb(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var climb = new Climb
            {
                Name = RandomUtil.RandomCharString(),
                Top = (byte)RandomUtil.RandomNumber(),
                Altitude = (short)RandomUtil.RandomNumber(4),
                ShowStart = false,
                ShowTop = false,
                IsClimb = false
            };
            context.Climbs.AddOrUpdate(climb);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Climb"), new JProperty("objectId", climb.Id)));
        }

        private static void Create_Country(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var country = new Country
            {
                Name = RandomUtil.RandomString(),
                Abbreviation = RandomUtil.RandomCharString(3),
                Tld = RandomUtil.RandomString(),
                Active = false,
                Item = Countries.Belgium,
                Nationality = Nationalities.Belgian
            };
            context.Countries.AddOrUpdate(country);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Country"), new JProperty("objectId", country.Id)));
        }
    }
}
