﻿using Newtonsoft.Json.Linq;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Queries;
using System;
using System.Data.Entity.Migrations;
using System.Runtime.InteropServices;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Player(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var cityId = GetOptionalEntry(entry, "cityId", FixedID.City);
            var nationality = GetOptionalEntry(entry, "nationality", FixedID.Country);
            var retired = GetOptionalEntry(entry, "retired", true);
            var age = GetOptionalEntry(entry, "age", (byte)RandomUtil.RandomNumber());

            var player = new Player
            {
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(15),
                Team = teamId,
                Nationality = nationality,
                Age = age,
                Retired = retired,
                City = cityId,
                Face = (byte)(RandomUtil.RandomNumber(1) % 2 + 1),
                Hair = (byte)(RandomUtil.RandomNumber() % 4 + 1),
                Eyes = (byte)RandomUtil.RandomNumber(1),
                Lips = (byte)RandomUtil.RandomNumber(1),
                Nose = (byte)RandomUtil.RandomNumber(1)
            };
            context.Players.AddOrUpdate(player);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Player"), new JProperty("objectId", player.Id)));
        }

        private static void Create_PlayerValue(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var isYouth = GetOptionalEntry(entry, "isYouth", false);
            var raceId = GetOptionalEntry(entry, "raceId", FixedID.Race);
            var plains = GetOptionalEntry(entry, "plains", RandomUtil.RandomNumber(5));
            var mountains = GetOptionalEntry(entry, "mountains", RandomUtil.RandomNumber(5));
            var hills = GetOptionalEntry(entry, "hills", RandomUtil.RandomNumber(5));
            var cobblestones = GetOptionalEntry(entry, "cobblestones", RandomUtil.RandomNumber(5));
            var technique = GetOptionalEntry(entry, "technique", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var experience = GetOptionalEntry(entry, "experience", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var stamina = GetOptionalEntry(entry, "stamina", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var descending = GetOptionalEntry(entry, "descending", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var sprint = GetOptionalEntry(entry, "sprint", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var timetrial = GetOptionalEntry(entry, "timetrial", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var climbing = GetOptionalEntry(entry, "climbing", RandomUtil.RandomNumber(isYouth ? 4 : 5));
            var wage = GetOptionalEntry(entry, "wage", RandomUtil.RandomNumber(3));

            var playerValue = new PlayerValue
            {
                Player = playerId,
                Team = RandomUtil.RandomCharString(),
                Value = RandomUtil.RandomNumber(4),
                Plains = plains,
                Mountains = mountains,
                Hills = hills,
                Cobblestones = cobblestones,
                Technique = technique,
                Experience = experience,
                Stamina = stamina,
                Descending = descending,
                Sprint = sprint,
                Timetrial = timetrial,
                Climbing = climbing,
                Wage = wage,
                Race = raceId,
                IsOnTransfer = false,
                StartBid = 0,
                Bid = 0,
                IsYouth = isYouth,
                Type = 0,
                Form = (int)RandomUtil.RandomNumber(3),
                FormTarget = (int)RandomUtil.RandomNumber(3),
                Fatigue = (int)RandomUtil.RandomNumber(),
                IsChampion = false,
                City = FixedID.City,
                Points = 0,
                Popularity = RandomUtil.RandomNumber(3),
                CampDays = 0,
                TotalCampDays = 0
            };
            context.PlayerValues.AddOrUpdate(playerValue);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "PlayerValue"), new JProperty("objectId", playerValue.Id)));
        }

        private static void Create_ScoutedPlayer(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var scoutId = GetEntry<long>(entry, "scoutId");
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var cityId = GetOptionalEntry(entry, "cityId", FixedID.City);
            var nationality = GetOptionalEntry(entry, "nationality", FixedID.Country);
            var isYouth = GetOptionalEntry(entry, "isYouth", false);

            var scoutedPlayer = new ScoutedPlayer
            {
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(15),
                Team = teamId,
                Nationality = nationality,
                Age = (byte)RandomUtil.RandomNumber(),
                City = cityId,
                Scout = scoutId,
                IsYouth = isYouth,
                Value = RandomUtil.RandomNumber(4),
                Plains = RandomUtil.RandomNumber(5),
                Mountains = RandomUtil.RandomNumber(5),
                Hills = RandomUtil.RandomNumber(5),
                Cobblestones = RandomUtil.RandomNumber(5),
                Technique = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Experience = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Stamina = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Descending = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Sprint = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Timetrial = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Climbing = RandomUtil.RandomNumber(isYouth ? 4 : 5),
                Face = (byte)(RandomUtil.RandomNumber(1) % 2 + 1),
                Hair = (byte)(RandomUtil.RandomNumber() % 4 + 1),
                Eyes = (byte)RandomUtil.RandomNumber(1),
                Lips = (byte)RandomUtil.RandomNumber(1),
                Nose = (byte)RandomUtil.RandomNumber(1)
            };
            context.ScoutedPlayers.AddOrUpdate(scoutedPlayer);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "ScoutedPlayer"), new JProperty("objectId", scoutedPlayer.Id)));
        }

        private static void Create_PlayerProgress(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var skill = GetOptionalEntry<byte>(entry, "skill", Enums.Rider.Skill.Sprint);
            var level = GetOptionalEntry(entry, "level", (byte)(RandomUtil.RandomNumber() % 20 + 1));

            var playerProgress = new PlayerProgress
            {
                Player = playerId,
                Skill = skill,
                Level = level,
                Change = DateTime.Now
            };
            context.PlayerProgresses.AddOrUpdate(playerProgress);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "PlayerProgress"), new JProperty("objectId", playerProgress.Id)));
        }

        private static void Create_Calendar(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var teamId = GetEntry<long>(entry, "teamId");
            var raceId = GetOptionalEntry<long?>(entry, "raceId", null);
            var locationId = GetOptionalEntry<short?>(entry, "locationId", null);
            var endDate = GetOptionalEntry<DateTime?>(entry, "endDate", null);

            Race race = null;
            if (raceId.HasValue)
            {
                race = RaceQueries.GetRaceById(context, raceId.Value);
            }
            var eventType = raceId.HasValue ? "1" : "2";
            var calendar = new Calendar
            {
                Player = playerId,
                Team = teamId,
                Event = $"{eventType}-{raceId?.ToString() ?? string.Empty}",
                Description = race?.RaceRaceDescription.Name.ToString() ?? locationId.ToString(),
                StartDate = race?.StartDate ?? endDate?.Date.AddDays(-2) ?? DateTime.Today.AddDays(2),
                EndDate = race?.EndDate ?? endDate ?? DateTime.Today.AddDays(10)
            };
            context.Calendars.AddOrUpdate(calendar);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Calendar"), new JProperty("objectId", calendar.Id)));
        }

        private static void Create_TransferHistory(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var fromTeamId = GetOptionalEntry(entry, "fromTeamId", FixedID.Team);
            var date = GetOptionalEntry(entry, "date", RandomUtil.RandomDateTime());
            var type = GetOptionalEntry(entry, "type", Enums.Rider.Transfer.Finished);

            var transferHistory = new TransferHistory
            {
                Team = teamId,
                FromTeam = fromTeamId,
                DateTransfer = date,
                Type = type,
                Player = playerId,
                Bid = RandomUtil.RandomNumber(6),
                Value = RandomUtil.RandomNumber(4)
            };
            context.TransferHistories.AddOrUpdate(transferHistory);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "TransferHistory"), new JProperty("objectId", transferHistory.Id)));
        }

        private static void Create_Youth(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var history = GetOptionalEntry(entry, "history", RandomUtil.RandomString());

            var youth = new Youth
            {
                Player = playerId,
                History = history
            };
            context.Youths.AddOrUpdate(youth);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Youth"), new JProperty("objectId", youth.Id)));
        }
    }
}
