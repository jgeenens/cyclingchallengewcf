﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Race(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var divisionId = GetOptionalEntry(entry, "divisionId", FixedID.Division);
            var raceDescriptionId = GetOptionalEntry(entry, "raceDescriptionId", FixedID.RaceDescription);
            var stageId = GetOptionalEntry<short?>(entry, "stageId", null);
            var startDate = GetOptionalEntry(entry, "startDate", DateTime.Today.AddDays(2));
            var endDate = GetOptionalEntry(entry, "endDate", DateTime.Today.AddDays(2));
            var liveTime = GetOptionalEntry(entry, "liveTime", 0);
            var status = GetOptionalEntry(entry, "status", RaceStatus.Open);

            var race = new Race
            {
                RaceDescription = raceDescriptionId,
                Division = divisionId,
                StartDate = startDate,
                EndDate = endDate,
                DaysInSeason = (byte)RandomUtil.RandomNumber(),
                Winner = null,
                Yellow = null,
                Participants = 0,
                LiveTime = liveTime,
                CurrentStage = stageId,
                CurrentDay = 0,
                Status = status
            };
            context.Races.AddOrUpdate(race);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Race"), new JProperty("objectId", race.Id)));
        }

        private static void Create_RaceDescription(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var countryId = GetOptionalEntry(entry, "countryId", FixedID.Country);
            var name = GetOptionalEntry(entry, "name", FixedID.RaceName);
            var description = GetOptionalEntry(entry, "description", FixedID.RaceName);
            var isTour = GetOptionalEntry(entry, "isTour", false);

            var raceDescription = new RaceDescription
            {
                Country = countryId,
                Name = name,
                IsTour = isTour,
                Type = Enums.Type.GrandDayRaces,
                Description = description,
                Source = RandomUtil.RandomString(),
                IsOfficial = false
            };
            context.RaceDescriptions.AddOrUpdate(raceDescription);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "RaceDescription"), new JProperty("objectId", raceDescription.Id)));
        }

        private static void Create_RaceEvent(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var raceId = GetOptionalEntry(entry, "raceId", FixedID.Race);
            var stageId = GetOptionalEntry(entry, "stageId", FixedID.Stage);
            var index = GetOptionalEntry<short>(entry, "index", 1);
            var eventId = GetOptionalEntry(entry, "eventId", FixedID.Event);
            var info = GetOptionalEntry(entry, "info", string.Empty);
            var kilometer = GetOptionalEntry(entry, "kilometer", (int)RandomUtil.RandomNumber());
            var seconds = GetOptionalEntry(entry, "seconds", (short)RandomUtil.RandomNumber(4));

            var raceEvent = new RaceEvent
            {
                Race = raceId,
                Stage = stageId,
                Index = index,
                Seconds = seconds,
                Km = kilometer,
                Event = eventId,
                Info = info
            };
            context.RaceEvents.AddOrUpdate(raceEvent);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "RaceEvent"), new JProperty("objectId", raceEvent.Id)));
        }

        private static void Create_RaceHistory(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var raceId = GetOptionalEntry(entry, "raceId", FixedID.Race);
            var stageId = GetOptionalEntry<short?>(entry, "stageId", null);
            var position = GetOptionalEntry(entry, "position", (byte)(RandomUtil.RandomNumber() + 4));

            var raceHistory = new RaceHistory
            {
                When = RandomUtil.RandomDateTime(),
                Race = raceId,
                Stage = stageId,
                Position = position,
                Player = playerId,
                Team = teamId,
                TeamName = RandomUtil.RandomCharString(),
                Season = (byte)RandomUtil.RandomNumber(1)
            };
            context.RaceHistories.AddOrUpdate(raceHistory);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "RaceHistory"), new JProperty("objectId", raceHistory.Id)));
        }

        private static void Create_RaceOrder(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var playerId = GetEntry<long>(entry, "playerId");
            var raceId = GetOptionalEntry(entry, "raceId", FixedID.Race);
            var stageId = GetOptionalEntry<short>(entry, "stageId", 1);
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);

            var raceOrder = new RaceOrder
            {
                Race = raceId,
                Stage = stageId,
                Team = teamId,
                Rank = (byte)(RandomUtil.RandomNumber(2) % 6 + 1),
                Player = playerId,
                MaxAhead = null,
                Head = false,
                Type = Strategy.Type.Finisher,
                Job = Strategy.Job.Finish,
                Intensity = Strategy.Intensity.Normal
            };
            context.RaceOrders.AddOrUpdate(raceOrder);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "RaceOrder"), new JProperty("objectId", raceOrder.Id)));
        }

        private static void Create_RaceRanking(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var raceId = GetOptionalEntry(entry, "raceId", FixedID.Race);
            var stageId = GetOptionalEntry(entry, "stageId", FixedID.Stage);
            var ranking = GetOptionalEntry(entry, "ranking", RandomUtil.RandomString());
            var pointsRanking = GetOptionalEntry<string>(entry, "pointsRanking", null);
            var mountainsRanking = GetOptionalEntry<string>(entry, "mountainsRanking", null);
            var teams = GetOptionalEntry(entry, "teams", RandomUtil.RandomString());

            var raceRanking = new RaceRanking
            {
                Race = raceId,
                Stage = stageId,
                Ranking = ranking,
                PointsRanking = pointsRanking,
                MountainsRanking = mountainsRanking,
                Teams = teams
            };
            context.RaceRankings.AddOrUpdate(raceRanking);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "RaceRanking"), new JProperty("objectId", raceRanking.Id)));
        }

        private static void Create_Stage(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var trackId = GetOptionalEntry(entry, "trackId", FixedID.Track);
            var raceDescriptionId = GetOptionalEntry(entry, "raceDescriptionId", FixedID.RaceDescription);
            var stageId = GetOptionalEntry(entry, "stageId", (byte)RandomUtil.RandomNumber());
            var day = GetOptionalEntry(entry, "day", (byte)RandomUtil.RandomNumber());

            var stage = new Stage
            {
                RaceDescription = raceDescriptionId,
                Index = stageId,
                Day = day,
                Track = trackId
            };
            context.Stages.AddOrUpdate(stage);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Stage"), new JProperty("objectId", stage.Id)));
        }

        private static void Create_Track(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var track = new Track
            {
                Name = RandomUtil.RandomString(),
                DifficultyPlains = (short)RandomUtil.RandomNumber(3),
                DifficultyMountains = (short)RandomUtil.RandomNumber(3),
                DifficultyHills = (short)RandomUtil.RandomNumber(3),
                DifficultyCobblestones = (short)RandomUtil.RandomNumber(3),
                Sum = RandomUtil.RandomNumber(4),
                Kilometers = (short)RandomUtil.RandomNumber(3),
                Terrain = RandomUtil.RandomString(),
                Description = RandomUtil.RandomString(),
                Altitude = RandomUtil.RandomString(),
                Climbs = RandomUtil.RandomString(),
                Checkpoints = RandomUtil.RandomString(),
                Profile = Enums.Profile.Plains,
                Scale = (byte)RandomUtil.RandomNumber(),
                CobbleHillMountain = (byte)RandomUtil.RandomNumber(),
                LastChange = DateTime.Now
            };
            context.Tracks.AddOrUpdate(track);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Track"), new JProperty("objectId", track.Id)));
        }
    }
}