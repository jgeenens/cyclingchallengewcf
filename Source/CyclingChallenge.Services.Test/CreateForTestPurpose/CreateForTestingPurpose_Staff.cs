﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Enums;
using Newtonsoft.Json.Linq;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Staff;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Coach(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);

            var coach = new Coach
            {
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(15),
                Team = teamId,
                Nationality = FixedID.Country,
                Age = (byte)RandomUtil.RandomNumber(),
                Training = (byte)(RandomUtil.RandomNumber(1) % 6),
                Skill = (byte)RandomUtil.RandomNumber(1),
                Wage = (short)RandomUtil.RandomNumber(3),
                Face = (byte)(RandomUtil.RandomNumber(1) % 2 + 1),
                Hair = (byte)(RandomUtil.RandomNumber() % 4 + 1),
                Eyes = (byte)RandomUtil.RandomNumber(1),
                Lips = (byte)RandomUtil.RandomNumber(1),
                Nose = (byte)RandomUtil.RandomNumber(1)
            };
            context.Coaches.AddOrUpdate(coach);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Coach"), new JProperty("objectId", coach.Id)));
        }

        private static void Create_Scout(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var status = GetOptionalEntry(entry, "status", Scouts.Status.Active);
            var skill = GetOptionalEntry(entry, "skill", Scouts.Level.Apprentice);
            var weeksToGo = GetOptionalEntry<byte>(entry, "weeksToGo", 0);
            var totalWeeks = GetOptionalEntry<byte>(entry, "totalWeeks", 0);
            var age = GetOptionalEntry(entry, "age", (byte)(RandomUtil.RandomNumber() + 18));

            var scout = new Scout
            {
                Team = teamId,
                Status = status,
                Skill = skill,
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(15),
                Nationality = FixedID.Country,
                Region = FixedID.Region,
                LookingForYouth = true,
                WeeksToGo = weeksToGo,
                TotalWeeks = totalWeeks,
                Age = age,
                Wage = (short)(RandomUtil.RandomNumber(4) + 1),
                Face = (byte)(RandomUtil.RandomNumber(1) % 2 + 1),
                Hair = (byte)(RandomUtil.RandomNumber() % 4 + 1),
                Eyes = (byte)RandomUtil.RandomNumber(1),
                Lips = (byte)RandomUtil.RandomNumber(1),
                Nose = (byte)RandomUtil.RandomNumber(1),
                Created = DateTime.Now
            };
            context.Scouts.AddOrUpdate(scout);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Scout"), new JProperty("objectId", scout.Id)));
        }

        private static void Create_Manager(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var type = GetOptionalEntry(entry, "type", Managers.Type.Shop);
            var level = GetOptionalEntry(entry, "level", Managers.Level.Apprentice);

            var manager = new Manager
            {
                Team = teamId,
                Type = type,
                Status = Managers.Status.Active,
                Level = level,
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(15),
                Learning = new DateTime(2000,1,1),
                Face = (byte)(RandomUtil.RandomNumber(1) % 2 + 1),
                Hair = (byte)(RandomUtil.RandomNumber() % 4 + 1),
                Eyes = (byte)RandomUtil.RandomNumber(1),
                Lips = (byte)RandomUtil.RandomNumber(1),
                Nose = (byte)RandomUtil.RandomNumber(1)
            };
            context.Managers.AddOrUpdate(manager);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Manager"), new JProperty("objectId", manager.Id)));
        }
    }
}
