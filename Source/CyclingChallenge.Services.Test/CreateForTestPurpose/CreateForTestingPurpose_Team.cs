﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Division(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var countryId = GetOptionalEntry(entry, "countryId", FixedID.Country);

            var division = new Division
            {
                Country = countryId,
                Name = RandomUtil.RandomCharString(7),
                Level = (byte)RandomUtil.RandomNumber()
            };
            context.Divisions.AddOrUpdate(division);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Division"), new JProperty("objectId", division.Id)));
        }

        private static void Create_Logo(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var tokens = GetOptionalEntry(entry, "tokens", (short)RandomUtil.RandomNumber(3));
            var logo = new Logo
            {
                Tokens = tokens
            };
            context.Logos.AddOrUpdate(logo);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Logo"), new JProperty("objectId", logo.Id)));
        }

        private static void Create_Team(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var divisionId = GetOptionalEntry(entry, "divisionId", FixedID.Division);
            var country = GetOptionalEntry(entry, "country", FixedID.Country);
            var hasShop = GetOptionalEntry(entry, "hasShop", false);
            var training = GetOptionalEntry(entry, "training", (byte) (RandomUtil.RandomNumber(1) % 6));
            var money = GetOptionalEntry(entry, "money", 200000);
            var predicted = GetOptionalEntry(entry, "predicted", 200000);
            var transactions = GetOptionalEntry(entry, "transactions", 0);
            var rank = GetOptionalEntry<byte>(entry, "rank", 0);
            var isActive = GetOptionalEntry(entry, "isActive", true);
            var isBot = GetOptionalEntry(entry, "isBot", false);

            var team = new Team
            {
                ShortName = RandomUtil.RandomCharString(),
                Name = RandomUtil.RandomCharString(30),
                Logo = FixedID.Logo,
                Division = divisionId,
                Money = money,
                Predicted = predicted,
                Points = 0,
                Wins = 0,
                Stage = 0,
                Champions = 0,
                Price = 0,
                Rank = rank,
                Transactions = transactions,
                Training = training,
                YouthTraining = Rider.Terrain.Mountains,
                NotParticipated = (byte)(RandomUtil.RandomNumber() + 1),
                Supporters = (short)RandomUtil.RandomNumber(3),
                Country = country,
                LastCamp = DateTime.Now,
                HasShop = hasShop,
                IsActive = isActive,
                IsBot = isBot
            };
            context.Teams.AddOrUpdate(team);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Team"), new JProperty("objectId", team.Id)));
        }

        private static void Create_TeamHistory(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var date = GetOptionalEntry(entry, "date", RandomUtil.RandomDateTime());
            var eventType = GetOptionalEntry(entry, "eventType", (byte)Events.Team.NewTeam);
            var data = GetOptionalEntry(entry, "data", string.Empty);

            var teamHistory = new TeamHistory
            {
                Date = date,
                Event = eventType,
                Team = teamId,
                Data = data,
                Seen = false
            };
            context.TeamHistories.AddOrUpdate(teamHistory);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "TeamHistory"), new JProperty("objectId", teamHistory.Id)));
        }
    }
}
