﻿using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Training;
using CyclingChallenge.Services.Test.Utils;
using Newtonsoft.Json.Linq;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_Booking(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var team = GetOptionalEntry(entry, "teamId", FixedID.Team);
            var location = GetOptionalEntry(entry, "locationId", FixedID.Location);
            var departure = GetOptionalEntry(entry, "departure", DateTime.Now);
            var duration = GetOptionalEntry(entry, "duration", (byte)RandomUtil.RandomNumber(1));
            var players = GetOptionalEntry(entry, "players", RandomUtil.RandomString());

            var booking = new Booking
            {
                Location = location,
                Team = team,
                Departure = departure,
                Week = (short)(RandomUtil.RandomNumber() % 12 + 1),
                Day = (byte)(RandomUtil.RandomNumber() % 7 + 1),
                Duration = duration,
                Players = players,
                Checked = false
            };
            context.Bookings.AddOrUpdate(booking);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "Booking"), new JProperty("objectId", booking.Id)));
        }
    }
}
