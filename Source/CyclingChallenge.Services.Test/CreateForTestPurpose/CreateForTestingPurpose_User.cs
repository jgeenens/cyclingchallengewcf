﻿using Newtonsoft.Json.Linq;
using CyclingChallenge.Services.Test.Utils;
using System;
using System.Data.Entity.Migrations;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Test.CreateForTestPurpose
{
    public static partial class CreateForTestingPurpose
    {
        private static void Create_User(ICyclingChallengeModelContainer context, JArray result, JToken entry)
        {
            var teamId = GetOptionalEntry<long?>(entry, "teamId", null);
            var userLvl = GetOptionalEntry<byte>(entry, "userLevel", 1);
            var tokens = GetOptionalEntry(entry, "tokens", 500);
            var language = GetOptionalEntry(entry, "language", FixedID.Language);
            var emailHidden = GetOptionalEntry(entry, "emailHidden", false);
            var email = GetOptionalEntry(entry, "email", RandomUtil.RandomString());

            var user = new User
            {
                FacebookId = RandomUtil.RandomNumber(6),
                Username = RandomUtil.RandomCharString(),
                Password = RandomUtil.RandomString(),
                Team = teamId,
                Reference = RandomUtil.RandomCharString(8),
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(15),
                Email = email,
                Birthday = DateTime.Now.AddYears(-18 - (int)RandomUtil.RandomNumber() % 50),
                Info = 0,
                Referrer = 0,
                Registration = DateTime.Now,
                Language = language,
                Country = FixedID.Country,
                TeamName = teamId.HasValue ? TeamQueries.GetTeamById(context, teamId.Value)?.Name ?? RandomUtil.RandomCharString() : string.Empty,
                UserLevel = userLvl,
                Online = false,
                OnlineSince = DateTime.Now,
                EmailHidden = emailHidden,
                LoginHistory = $"{DateTime.Now:yyyyMMdd}127.0.0.1",
                Prefix = string.Empty,
                LastHit = DateTime.Now,
                Tokens = tokens,
                Live = string.Empty,
                Vacation = false,
                Currency = FixedID.Currency,
                RealCountry = FixedID.Country
            };
            context.Users.AddOrUpdate(user);
            context.SaveChanges();
            result.Add(new JObject(new JProperty("type", "User"), new JProperty("objectId", user.Id)));
        }
    }
}