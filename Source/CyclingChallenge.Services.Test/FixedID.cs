﻿using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using System;
using System.Linq;
using System.Text;

namespace CyclingChallenge.Services.Test
{
    public static class FixedID
    {
        #region Properties
        
        public static short ActiveCountry { get; private set; }
        public static short Addon { get; private set; }
        public static short Board { get; private set; }
        public static long City { get; private set; }
        public static long Climb { get; private set; }
        public static short Country { get; private set; }
        public static short Currency { get; private set; }
        public static long Division { get; private set; }
        public static long Event { get; private set; }
        public static long FacebookId { get; private set; }
        public static byte Language { get; private set; }
        public static short LanguageName { get; private set; }
        public static byte LanguageNotInDictionary { get; private set; }
        public static short Location { get; private set; }
        public static byte Logo { get; private set; }
        public static byte Product { get; private set; }
        public static long Race { get; private set; }
        public static short RaceDescription { get; private set; }
        public static short RaceName { get; private set; }
        public static short Region { get; private set; }
        public static short Stage { get; private set; }
        public static short StoreProduct { get; private set; }
        public static long Team { get; private set; }
        public static long TourRace { get; private set; }
        public static short TourRaceDescription { get; private set; }
        public static short TourStage { get; private set; }
        public static short Track { get; private set; }

        #endregion


        #region Delegates

        private delegate void SetFixedIdDelegate(ICyclingChallengeModelContainer context);

        #endregion


        #region Static Constructor

        static FixedID()
        {
            SetFixedIds();
        }

        #endregion


        #region Methods

        public static void SetFixedIds()
        {
            using (var context = RuntimeManager.CreateDbContext())
            {
                HandleSetFixedIdDelegate(context, SetBoardFixedIds);
                HandleSetFixedIdDelegate(context, SetDictionaryFixedIds);
                HandleSetFixedIdDelegate(context, SetEconomyFixedIds);
                HandleSetFixedIdDelegate(context, SetGameFixedIds);
                HandleSetFixedIdDelegate(context, SetLocationFixedIds);
                HandleSetFixedIdDelegate(context, SetRaceFixedIds);
                HandleSetFixedIdDelegate(context, SetTeamFixedIds);
                HandleSetFixedIdDelegate(context, SetUserFixedIds);
            }
        }

        private static void HandleSetFixedIdDelegate(ICyclingChallengeModelContainer context, SetFixedIdDelegate del)
        {
            try
            {
                del(context);
            }
            catch (Exception e)
            {
                var sb = new StringBuilder();
                sb.AppendLine("Unable to set FixedID value!");
                sb.AppendLine("Exception: " + e.Message);
                sb.AppendLine("Method: " + del.Method.Name);
                sb.AppendLine("StackTrace:");
                sb.AppendLine(e.StackTrace);
                throw new Exception(sb.ToString(), e);
            }
        }

        #endregion


        #region Setter methods

        private static void SetBoardFixedIds(ICyclingChallengeModelContainer context)
        {
            Board = context.Boards.Random().Id;
        }

        private static void SetDictionaryFixedIds(ICyclingChallengeModelContainer context)
        {
            Language = context.Dictionaries.Random().Language;
            LanguageName = context.Items.Random(i => i.Topic == Topics.Languages).Id;
            LanguageNotInDictionary = context.Languages.Random(l => !context.Dictionaries.Any(d => d.Language == l.Id)).Id;
        }

        private static void SetEconomyFixedIds(ICyclingChallengeModelContainer context)
        {
            Product = context.Products.Random().Id;
        }

        private static void SetGameFixedIds(ICyclingChallengeModelContainer context)
        {
            Addon = context.Addons.Random().Id;
            StoreProduct = context.Prices.Random(p => p.Currency == Economy.Currency.Dollar).Commodity;
        }

        private static void SetLocationFixedIds(ICyclingChallengeModelContainer context)
        {
            ActiveCountry = context.Countries.Random(c => c.Active && c.Name != "World").Id;
            City = context.Cities.Random().Id;
            Climb = context.Climbs.Random().Id;
            Country = context.Countries.Random().Id;
            Location = context.Locations.Random().Id;
            Region = context.Regions.Random().Id;
        }

        private static void SetRaceFixedIds(ICyclingChallengeModelContainer context)
        {
            var stage = context.Stages.Random(s => !s.StageRaceDescription.IsTour && s.StageTrack.Description.Contains("|"));
            Stage = stage.Id;
            RaceDescription = stage.RaceDescription;
            Race = context.Races.First(r => r.RaceDescription == RaceDescription).Id;
            var tourStage = context.Stages.Random(s => s.StageRaceDescription.IsTour && s.StageTrack.Description.Contains("|")
                && (s.StageRaceDescription.Source == string.Empty || s.StageRaceDescription.Source.Contains("////")));
            TourStage = tourStage.Id;
            TourRaceDescription = tourStage.RaceDescription;
            TourRace = context.Races.First(r => r.RaceDescription == TourRaceDescription).Id;
            Event = context.Events.Random().Id;
            RaceName = context.Items.Random(i => i.Topic == Topics.Races).Id;
            Track = context.Tracks.Random().Id;
        }

        private static void SetTeamFixedIds(ICyclingChallengeModelContainer context)
        {
            Team = context.Teams.Random(t => t.IsActive && !t.IsBot).Id;
            Division = context.Divisions.Random().Id;
            Logo = context.Logos.Random().Id;
        }

        private static void SetUserFixedIds(ICyclingChallengeModelContainer context)
        {
            Currency = context.Currencies.Random().Id;
            FacebookId = context.Users.Random(u => u.Team != null).FacebookId;
        }

        #endregion
    }
}