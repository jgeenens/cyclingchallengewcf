﻿using System.Linq;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Models
{
    [TestFixture]
    public class TrackDescriptionTest
    {
        #region Constructor Tests

        [Test]
        public void TrackDescriptionConstructor_TerrainOrDescriptionNullOrEmpty_ReturnsSuccessFalse()
        {
            Assert.False(new TrackDescription(null, RandomUtil.RandomString()).Success);
            Assert.False(new TrackDescription(string.Empty, RandomUtil.RandomString()).Success);
            Assert.False(new TrackDescription(RandomUtil.RandomString(), null).Success);
            Assert.False(new TrackDescription(RandomUtil.RandomString(), string.Empty).Success);
        }

        [Test]
        public void TrackDescriptionConstructor_InvalidTerrain_ReturnsSuccessFalse()
        {
            Assert.False(new TrackDescription(RandomUtil.RandomString(), $"0-{FixedID.City}").Success);
        }

        [Test]
        public void TrackDescriptionConstructor_InvalidCities_ReturnsSuccessFalse()
        {
            Assert.False(new TrackDescription($"{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber(3)}#", RandomUtil.RandomString()).Success);
        }

        [Test]
        public void TrackDescriptionConstructor_ValidTerrain_ReturnsSuccessTrue()
        {
            Assert.True(new TrackDescription($"{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber(3)}#", $"0-{FixedID.City}").Success);
        }

        [Test]
        public void TrackDescriptionConstructor_AltitudesNullOrEmpty_ReturnsSuccessTrue()
        {
            Assert.True(new TrackDescription($"{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber(3)}#", $"0-{FixedID.City}").Success);
            Assert.True(new TrackDescription($"{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber(3)}#", $"0-{FixedID.City}", string.Empty).Success);
        }

        [Test]
        public void TrackDescriptionConstructor_InvalidAltitudes_ReturnsSuccessFalse()
        {
            Assert.False(new TrackDescription($"{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber(3)}#", $"0-{FixedID.City}", RandomUtil.RandomString()).Success);
        }

        [Test]
        public void TrackDescriptionConstructor_ValidAltitudes_ReturnsSuccessTrue()
        {
            var altitudes = $"0-{RandomUtil.RandomNumber()}|{RandomUtil.RandomNumber()}-{RandomUtil.RandomNumber(4)}";
            Assert.True(new TrackDescription($"{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber(3)}#", $"0-{FixedID.City}", altitudes).Success);
        }

        #endregion


        #region GetHills

        [Test]
        public void GetHills_InvalidConstructorParameters_ReturnsNull()
        {
            Assert.Null(new TrackDescription(null, $"0-{FixedID.City}").GetHills());
            Assert.Null(new TrackDescription(string.Empty, $"0-{FixedID.City}").GetHills());
            Assert.Null(new TrackDescription(RandomUtil.RandomString(), $"0-{FixedID.City}").GetHills());
        }

        [Test]
        public void GetHills_NoHills_ReturnsEmptyList()
        {
            var trackDescription = new TrackDescription($"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}c{RandomUtil.RandomNumber()}#", $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetHills();
            Assert.NotNull(result);
            Assert.IsEmpty(result);
        }

        [Test]
        public void GetHills_WithHills_ReturnsListOfHills()
        {
            var km = RandomUtil.RandomNumber(3);
            var idHill = RandomUtil.RandomNumber();
            var firstPartLength = RandomUtil.RandomNumber();
            var terrain = $"{firstPartLength}v|{km - firstPartLength}c{RandomUtil.RandomNumber()}#|{RandomUtil.RandomNumber()}h{idHill}#|{RandomUtil.RandomNumber()}h";
            var trackDescription = new TrackDescription(terrain, $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetHills();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(km, result.First().Key);
            Assert.AreEqual(idHill, result.First().Value.Climb);
            Assert.AreEqual('h', result.First().Value.Type);
        }

        #endregion


        #region GetMountains

        [Test]
        public void GetMountains_InvalidConstructorParameters_ReturnsNull()
        {
            Assert.Null(new TrackDescription(null, $"0-{FixedID.City}").GetMountains());
            Assert.Null(new TrackDescription(string.Empty, $"0-{FixedID.City}").GetMountains());
            Assert.Null(new TrackDescription(RandomUtil.RandomString(), $"0-{FixedID.City}").GetMountains());
        }

        [Test]
        public void GetMountains_NoMountains_ReturnsEmptyList()
        {
            var trackDescription = new TrackDescription($"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}c{RandomUtil.RandomNumber()}#", $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetMountains();
            Assert.NotNull(result);
            Assert.IsEmpty(result);
        }

        [Test]
        public void GetMountains_WithMountains_ReturnsListOfMountains()
        {
            var km = RandomUtil.RandomNumber(3);
            var idMountain = RandomUtil.RandomNumber();
            var firstPartLength = RandomUtil.RandomNumber();
            var terrain = $"{firstPartLength}v|{km - firstPartLength}c{RandomUtil.RandomNumber()}#|{RandomUtil.RandomNumber()}b{idMountain}#|{RandomUtil.RandomNumber()}b";
            var trackDescription = new TrackDescription(terrain, $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetMountains();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(km, result.First().Key);
            Assert.AreEqual(idMountain, result.First().Value.Climb);
            Assert.AreEqual('b', result.First().Value.Type);
        }

        #endregion


        #region GetPavedSectors

        [Test]
        public void GetPavedSectors_InvalidTerrain_ReturnsNull()
        {
            Assert.Null(new TrackDescription(null, $"0-{FixedID.City}").GetPavedSectors());
            Assert.Null(new TrackDescription(string.Empty, $"0-{FixedID.City}").GetPavedSectors());
            Assert.Null(new TrackDescription(RandomUtil.RandomString(), $"0-{FixedID.City}").GetPavedSectors());
        }

        [Test]
        public void GetPavedSectors_NoPavedSectors_ReturnsEmptyList()
        {
            var trackDescription = new TrackDescription($"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber()}#", $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetPavedSectors();
            Assert.NotNull(result);
            Assert.IsEmpty(result);
        }

        [Test]
        public void GetPavedSectors_WithPavedSectors_ReturnsListOfPavedSectors()
        {
            var km = RandomUtil.RandomNumber(3);
            var idPavedSector = RandomUtil.RandomNumber();
            var type = 'k';
            var firstPartLength = RandomUtil.RandomNumber();
            var terrain = $"{firstPartLength}v|{km - firstPartLength}b{RandomUtil.RandomNumber()}#|{RandomUtil.RandomNumber()}{type}{idPavedSector}#|{RandomUtil.RandomNumber()}c";
            var trackDescription = new TrackDescription(terrain, $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetPavedSectors();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(km, result.First().Key);
            Assert.AreEqual(idPavedSector, result.First().Value.Climb);
            Assert.AreEqual(type, result.First().Value.Type);
        }

        #endregion


        #region GetStart

        [Test]
        public void GetStart_InvalidCities_ReturnsNull()
        {
            var terrain = $"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber()}#";
            Assert.Null(new TrackDescription(terrain, null).GetStart());
            Assert.Null(new TrackDescription(terrain, string.Empty).GetStart());
            Assert.Null(new TrackDescription(terrain, RandomUtil.RandomString()).GetStart());
        }

        [Test]
        public void GetStart_ValidCities_ReturnsCityAtDistanceZero()
        {
            var terrain = $"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber()}#";
            var trackDescription = new TrackDescription(terrain, $"0-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetStart();
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.City, result);
        }

        #endregion


        #region GetFinish

        [Test]
        public void GetFinish_InvalidCities_ReturnsNull()
        {
            var terrain = $"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber()}#";
            Assert.Null(new TrackDescription(terrain, null).GetFinish());
            Assert.Null(new TrackDescription(terrain, string.Empty).GetFinish());
            Assert.Null(new TrackDescription(terrain, RandomUtil.RandomString()).GetFinish());
        }

        [Test]
        public void GetFinish_ValidCities_ReturnsCityAtDistanceZero()
        {
            var terrain = $"{RandomUtil.RandomNumber()}v|{RandomUtil.RandomNumber()}h{RandomUtil.RandomNumber()}#";
            var trackDescription = new TrackDescription(terrain, $"0-{RandomUtil.RandomNumber()}|{RandomUtil.RandomNumber()}-{FixedID.City}");
            Assert.True(trackDescription.Success);
            var result = trackDescription.GetFinish();
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.City, result);
        }

        #endregion
    }
}