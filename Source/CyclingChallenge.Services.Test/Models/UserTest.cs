﻿using CyclingChallenge.Services.Models.Team;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Models
{
    [TestFixture]
    public class UserTest
    {
        [Test, TestCase(1), TestCase(4), TestCase(5), TestCase(8), TestCase(9), TestCase(12), TestCase(13)]
        public void IsModerator_NoModerator_ReturnsFalse(byte userLevel)
        {
            Assert.False(new User { UserLevel = userLevel }.IsModerator);
        }

        [Test, TestCase(2), TestCase(3), TestCase(6), TestCase(7), TestCase(10), TestCase(11), TestCase(14), TestCase(15)]
        public void IsModerator_Moderator_ReturnsTrue(byte userLevel)
        {
            Assert.True(new User { UserLevel = userLevel }.IsModerator);
        }
    }
}
