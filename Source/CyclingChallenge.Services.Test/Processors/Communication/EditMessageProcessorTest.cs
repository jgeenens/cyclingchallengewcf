﻿using NUnit.Framework;
using System;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Communication;
using ErrorCodes = CyclingChallenge.Services.Enums.ErrorCodes;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class EditMessageProcessorTest: ProcessorBaseTest<EditMessageProcessor, ResultBase>
    {
        #region Tests

        [Test, TestCase(1), TestCase(2), TestCase(3)]
        public void EditMessageProcessor_LessThanThreeParameters_ErrorCodeParameterMissing(byte missingParameter)
        {
            var result = missingParameter == 1 
                ? CreateProcessor(null).Process()
                : missingParameter == 2 
                    ? CreateProcessor(RandomUtil.RandomNumber()).Process()
                    : CreateProcessor(RandomUtil.RandomNumber(), RandomUtil.RandomNumber()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(1), TestCase(2), TestCase(3)]
        public void EditMessageProcessor_InvalidParameter_ErrorCodeInvalidParameterType(byte invalidParameter)
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            ResultBase result;
            switch (invalidParameter)
            {
                case 1:
                    result = CreateProcessor(string.Empty, userId, RandomUtil.RandomString()).Process();
                    break;
                case 2:
                    result = CreateProcessor(CreateIdForTestingPurpose<Message>(new { threadId, userId }), string.Empty, RandomUtil.RandomString()).Process();
                    break;
                default:
                    result = CreateProcessor(CreateIdForTestingPurpose<Message>(new { threadId, userId }), userId, 0).Process();
                    break;
            }

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void EditMessageProcessor_InvalidIdMessage_ErrorCodeIdMessageNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, RandomUtil.RandomNumber(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdMessageNotFound, result.ErrorCode);
        }

        [Test]
        public void EditMessageProcessor_InvalidIdUser_ErrorCodeIdUserNotFound()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var idMessage = CreateIdForTestingPurpose<Message>(new { threadId, userId });
            long invalidId = -1;
            var result = CreateProcessor(idMessage, invalidId, RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdUserNotFound, result.ErrorCode);
        }

        [Test]
        public void EditMessageProcessor_MessageEmpty_ErrorCodeMessageCannotBeEmpty()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var idMessage = CreateIdForTestingPurpose<Message>(new { threadId, userId });
            var result = CreateProcessor(idMessage, userId, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.MessageCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void EditMessageProcessor_ValidInformationProvided_ChangesMessage()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var message = CreateForTestingPurpose<Message>(new { threadId, userId });
            var text = RandomUtil.RandomString();
            var result = CreateProcessor(message.Id, userId, text).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            AssertMessageOk(message, userId, text);
        }

        #endregion


        #region Private Methods

        private void AssertMessageOk(Message message, long idUser, string text)
        {
            Context.Entry(message).Reload();
            Assert.NotNull(message);
            Assert.AreEqual(idUser, message.UserChange);
            Assert.NotNull(message.LastChange);
            Assert.AreEqual(DateTime.Today.Date, message.LastChange.Value.Date);
            Assert.AreEqual(text, message.Content);
        }

        #endregion
    }
}