﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Linq;
using Thread = CyclingChallenge.Services.Models.Communication.Thread;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetBoardProcessorTest: ProcessorBaseTest<GetBoardProcessor, Board>
    {
        #region Tests

        [Test]
        public void GetBoardProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetBoardProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsUser)
        {
            var result = invalidParameterIsUser
                ? CreateProcessor(Enums.Communication.BoardType.Game, string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetBoardProcessor_InvalidId_ErrorCodeIdNotFound(bool invalidIdIsUser)
        {
            long invalidIdUser = -1;
            byte invalidIdBoard = 0;
            var result = invalidIdIsUser
                ? CreateProcessor(Enums.Communication.BoardType.Game, invalidIdUser).Process()
                : CreateProcessor(invalidIdBoard).Process();

            Assert.NotNull(result);
            Assert.AreEqual(invalidIdIsUser ? ErrorCodes.IdUserNotFound : ErrorCodes.IdBoardNotFound, result.ErrorCode);
        }

        [Test, TestCase(Enums.Communication.BoardType.Game), TestCase(Enums.Communication.BoardType.National)]
        public void GetBoardProcessor_ValidId_ReturnsBoard(byte type)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var boards = CommunicationQueries.GetByType(Context, type).ToList();
            CreateIdForTestingPurpose<Thread>(new { userId = idUser, boardId = boards.First().Id });
            var result = CreateProcessor(type).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(type, result.Id);
            Assert.AreEqual(Enums.Communication.BoardType.GetDescription(type), result.Name);
            Assert.NotNull(result.Forums);
            Assert.IsNotEmpty(result.Forums);
            Assert.AreEqual(boards.Count, result.Forums.Count);
            var forumWithThread = result.Forums.FirstOrDefault(f => f.Id == boards.First().Id);
            Assert.NotNull(forumWithThread);
            Assert.Greater(forumWithThread.NumberOfThreads, 0);
            foreach (var forum in result.Forums)
            {
                AssertForumOk(forum, type);
            }
        }

        #endregion


        #region Helper Methods

        private static void AssertForumOk(Forum forum, byte type)
        {
            Assert.NotNull(forum.Id);
            Assert.NotNull(forum.Name);
            Assert.NotNull(forum.Description);
            Assert.NotNull(forum.Type);
            Assert.AreEqual(type, forum.Type);
            Assert.True(forum.HasNewMessages);
            AssertCountryOk(forum.Country);
        }

        private static void AssertCountryOk(Contracts.DataContracts.Location.Country country)
        {
            Assert.NotNull(country);
            Assert.NotNull(country.Id);
            Assert.NotNull(country.Name);
        }

        #endregion
    }
}
