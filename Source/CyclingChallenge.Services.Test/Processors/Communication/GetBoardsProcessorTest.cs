﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Processors;
using NUnit.Framework;
using System.Linq;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetBoardsProcessorTest: ProcessorBaseTest<GetBoardsProcessor, MessageBoard>
    {
        #region Tests

        [Test]
        public void GetBoardsProcessor_ReturnsBoards()
        {
            var numberOfBoards = typeof(Enums.Communication.BoardType).GetFields().Count();
            var result = CreateProcessor().Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.NotNull(result.Boards);
            Assert.IsNotEmpty(result.Boards);
            Assert.AreEqual(numberOfBoards, result.Boards.Count);
            foreach (var board in result.Boards)
            {
                AssertBoardOk(board);
            }
        }

        #endregion


        #region Helper Methods

        private void AssertBoardOk(Board board)
        {
            Assert.NotNull(board.Id);
            Assert.NotNull(board.Name);
        }

        #endregion
    }
}
