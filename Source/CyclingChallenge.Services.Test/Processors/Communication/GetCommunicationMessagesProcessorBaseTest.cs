﻿using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Collections.Generic;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    public abstract class GetCommunicationMessagesProcessorBaseTest<TProcessor, TResult> : GetMessagesProcessorBaseTest<TProcessor, TResult>
        where TProcessor : GetCommunicationMessagesProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Shared methods

        protected internal void AssertMessagesOk(List<Contracts.DataContracts.Communication.PrivateMessage> messages)
        {
            Assert.NotNull(messages);
            Assert.IsNotEmpty(messages);
            messages.ForEach(msg => AssertMessageOk(CommunicationQueries.GetPrivateMessageById(Context, msg.Id), msg));
        }

        protected internal void AssertMessageOk(PrivateMessage message, Contracts.DataContracts.Communication.PrivateMessage messageInResult)
        {
            Assert.NotNull(messageInResult);
            AssertNotDeleted(message);
            Assert.AreEqual(message.Id, messageInResult.Id);
            Assert.AreEqual(message.Date, messageInResult.Date);
            Assert.AreEqual(message.Subject, messageInResult.Title);
            Assert.AreEqual(message.Message, messageInResult.Text);
            Assert.AreEqual(message.Read, !messageInResult.IsNew);
            AssertUserOk(message.PrivateMessageRecipient, messageInResult.Recipient);
            AssertUserOk(message.PrivateMessageSender, messageInResult.Sender);
        }

        protected internal static void AssertUserOk(User user, Contracts.DataContracts.Communication.User resultUser)
        {
            Assert.NotNull(resultUser);
            Assert.AreEqual(user.Id, resultUser.Id);
            Assert.AreEqual($"{user.FirstName} {user.LastName}".Trim(), resultUser.Name);
            Assert.AreEqual(user.Username, resultUser.Alias);
        }

        #endregion


        #region Abstract methods

        protected internal abstract void AssertNotDeleted(PrivateMessage message);

        #endregion
    }
}
