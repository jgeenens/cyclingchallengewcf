﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System;
using System.Linq;
using Message = CyclingChallenge.Services.Models.Communication.Message;
using Thread = CyclingChallenge.Services.Models.Communication.Thread;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetForumProcessorTest: ProcessorBaseTest<GetForumProcessor, Forum>
    {
        #region Tests

        [Test]
        public void GetForumProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetForumProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsUser)
        {
            var result = invalidParameterIsUser
                ? CreateProcessor(FixedID.Board, string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetForumProcessor_InvalidId_ErrorCodeIdNotFound(bool invalidIdIsUser)
        {
            long invalidIdUser = -1;
            short invalidIdForum = -1;
            var result = invalidIdIsUser
                ? CreateProcessor(FixedID.Board, invalidIdUser).Process()
                : CreateProcessor(invalidIdForum).Process();

            Assert.NotNull(result);
            Assert.AreEqual(invalidIdIsUser ? ErrorCodes.IdUserNotFound : ErrorCodes.IdForumNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetForumProcessor_ValidId_ReturnsForum(bool withUser)
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idUser = CreateIdForTestingPurpose<User>(new { teamId = idTeam });
            var thread = CreateForTestingPurpose<Thread>(new { userId = idUser, lastChange = DateTime.Now.AddDays(-1) });
            CreateForTestingPurpose<ThreadViewed>(new { threadId = thread.Id, userId = idUser });
            CreateForTestingPurpose<Message>(new { threadId = thread.Id, userId = idUser });

            var result = withUser
                ? CreateProcessor(FixedID.Board, idUser).Process()
                : CreateProcessor(FixedID.Board).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            AssertForumOk(result, FixedID.Board, thread, withUser);
        }

        #endregion


        #region Helper Methods

        private void AssertForumOk(Forum forum, short idBoard, Thread thread, bool withUser)
        {
            var board = CommunicationQueries.GetBoardById(Context, idBoard);
            Assert.NotNull(board);
            Assert.AreEqual(idBoard, forum.Id);
            Assert.AreEqual(board.Name, forum.Name);
            Assert.AreEqual(board.Type, forum.Type);
            Assert.AreEqual(!withUser, forum.HasNewMessages);
            AssertThreadsOk(forum, thread, withUser);
        }

        private static void AssertThreadsOk(Forum forum, Thread expectedThread, bool withUser)
        {
            Assert.IsNotEmpty(forum.Threads);
            var thread = forum.Threads.FirstOrDefault(t => t.Id == expectedThread.Id);
            Assert.NotNull(thread);
            Assert.AreEqual(expectedThread.Id, thread.Id);
            Assert.AreEqual(expectedThread.Title, thread.Name);
            Assert.AreEqual(expectedThread.LastChange, thread.LastChange);
            Assert.AreEqual(1, thread.NumberOfMessages);
            Assert.AreEqual(!withUser, thread.HasNewMessages);
            AssertUserOk(thread.Originator, expectedThread.ThreadOriginator);
            AssertUserOk(thread.LastOriginator, expectedThread.ThreadLastOriginator);
        }

        private static void AssertUserOk(Contracts.DataContracts.Communication.User originator, User user)
        {
            Assert.NotNull(originator);
            Assert.AreEqual(user.Id, originator.Id);
            Assert.AreEqual($"{user.FirstName} {user.LastName}".Trim(), originator.Name);
            Assert.AreEqual(user.Username, originator.Alias);
            Assert.NotNull(originator.Team);
            Assert.AreEqual(user.Team, originator.Team.Id);
            Assert.AreEqual(user.UserTeam.ShortName, originator.Team.ShortName);
            Assert.AreEqual(user.UserTeam.Name, originator.Team.LongName);
        }

        #endregion
    }
}
