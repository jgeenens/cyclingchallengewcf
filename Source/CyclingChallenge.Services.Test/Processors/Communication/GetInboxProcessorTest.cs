﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Models.Team;
using PrivateMessage = CyclingChallenge.Services.Models.Communication.PrivateMessage;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetInboxProcessorTest: GetCommunicationMessagesProcessorBaseTest<GetInboxProcessor, Inbox>
    {
        [Test, TestCase(false), TestCase(true)]
        public void GetInboxProcessor_ValidIdNoPage_ReturnsInboxMessages(bool usePage)
        {
            var team = CreateForTestingPurpose<Team>();
            var recipient = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var sender = CreateIdForTestingPurpose<User>();
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender, deletedByRecipient = true });
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender });
            var result = usePage ? CreateProcessor(team.Id, 1).Process() : CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertMessagesOk(result.Messages);
        }

        protected internal override void AssertNotDeleted(PrivateMessage message)
        {
            Assert.False(message.DeletedByRecipient);
        }
    }
}