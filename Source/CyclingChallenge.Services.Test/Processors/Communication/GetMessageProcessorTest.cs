﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Processors;
using NUnit.Framework;
using Message = CyclingChallenge.Services.Models.Communication.Message;
using Thread = CyclingChallenge.Services.Models.Communication.Thread;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetMessageProcessorTest : ProcessorBaseTest<GetMessageProcessor, Contracts.DataContracts.Communication.Message>
    {
        #region Tests

        [Test]
        public void GetMessageProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetMessageProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsUser)
        {
            long idMessage = 0;
            if (invalidParameterIsUser)
            {
                var idUser = CreateIdForTestingPurpose<User>();
                var idThread = CreateIdForTestingPurpose<Thread>(new { userId = idUser });
                idMessage = CreateIdForTestingPurpose<Message>(new { threadId = idThread, userId = idUser });
            }

            var result = invalidParameterIsUser
                ? CreateProcessor(idMessage, string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetMessageProcessor_InvalidId_ErrorCodeIdNotFound(bool invalidIdIsUser)
        {
            long invalidId = -1;
            long idMessage = 0;
            if (invalidIdIsUser)
            {
                var idUser = CreateIdForTestingPurpose<User>();
                var idThread = CreateIdForTestingPurpose<Thread>(new { userId = idUser });
                idMessage = CreateIdForTestingPurpose<Message>(new { threadId = idThread, userId = idUser });
            }

            var result = invalidIdIsUser
                ? CreateProcessor(idMessage, invalidId).Process()
                : CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(invalidIdIsUser ? ErrorCodes.IdUserNotFound : ErrorCodes.IdMessageNotFound, result.ErrorCode);
        }

        [Test]
        public void GetMessageProcessor_MessageObsoleteUserNotModerator_ErrorCodeIdMessageNotFound()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId = idUser });
            var idMessage = CreateIdForTestingPurpose<Message>(new { threadId = idThread, userId = idUser, obsolete = true });

            var result = CreateProcessor(idMessage, idUser).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdMessageNotFound, result.ErrorCode);
        }

        [Test, TestCase(false, false), TestCase(true, false), TestCase(true, true)]
        public void GetMessageProcessor_ValidId_ReturnsMessage(bool withUser, bool obsolete)
        {
            var idUser1 = CreateIdForTestingPurpose<User>(new { teamId = CreateIdForTestingPurpose<Team>(), userLevel = 2 });
            var idUser2 = CreateIdForTestingPurpose<User>(new { teamId = CreateIdForTestingPurpose<Team>() });
            var thread = CreateForTestingPurpose<Thread>(new { userId = idUser1 });
            var replyToMessage = CreateForTestingPurpose<Message>(new
            {
                threadId = thread.Id,
                userId = idUser1,
                userChange = idUser1
            });
            var message = CreateForTestingPurpose<Message>(new
            {
                threadId = thread.Id,
                userId = idUser2,
                replyTo = replyToMessage.Id,
                obsolete
            });

            var result = withUser
                ? CreateProcessor(message.Id, idUser1).Process()
                : CreateProcessor(message.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(message.Id, result.Id);
            Assert.AreEqual(message.Thread, result.Thread);
            Assert.AreEqual(message.MessageInThread, result.MessageInThread);
            Assert.AreEqual(message.Content, result.Text);
            Assert.AreEqual(message.Date, result.Date);
            Assert.AreEqual(message.LastChange, result.ChangeDate);
            Assert.AreEqual(message.Obsolete, result.IsDeleted);
            Assert.AreEqual(withUser, result.CanChange);
            AssertUserOk(result.User, message.MessageUser);
            Assert.Null(result.ChangeUser);
            AssertReplyToMessageOk(result.ReplyTo, message.MessageReplyTo);
        }

        #endregion


        #region Helper Methods

        private static void AssertUserOk(Contracts.DataContracts.Communication.User user, User expectedUser)
        {
            Assert.NotNull(user);
            Assert.AreEqual(expectedUser.Id, user.Id);
            Assert.AreEqual($"{expectedUser.FirstName} {expectedUser.LastName}".Trim(), user.Name);
            Assert.AreEqual(expectedUser.Username, user.Alias);
            Assert.NotNull(user.Team);
            Assert.AreEqual(expectedUser.Team, user.Team.Id);
            Assert.AreEqual(expectedUser.UserTeam.ShortName, user.Team.ShortName);
            Assert.AreEqual(expectedUser.UserTeam.Name, user.Team.LongName);
        }

        private static void AssertReplyToMessageOk(MessageBase message, Message expectedMessage)
        {
            if (expectedMessage == null)
            {
                return;
            }

            Assert.NotNull(expectedMessage);
            Assert.AreEqual(expectedMessage.Id, message.Id);
            Assert.AreEqual(expectedMessage.Thread, message.Thread);
            Assert.AreEqual(expectedMessage.MessageInThread, message.MessageInThread);
        }

        #endregion
    }
}