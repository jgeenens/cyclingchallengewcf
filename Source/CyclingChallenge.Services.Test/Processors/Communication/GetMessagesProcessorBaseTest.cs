﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public abstract class GetMessagesProcessorBaseTest<TProcessor, TResult>: ProcessorBaseTest<TProcessor, TResult>
        where TProcessor : GetMessagesProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Shared tests

        [Test]
        public void GetMessagesProcessorBase_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetMessagesProcessorBase_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetMessageProcessorBase_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetMessagesProcessorBase_InvalidPageParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(FixedID.Team, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        #endregion
    }
}