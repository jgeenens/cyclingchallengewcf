﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Economy;
using PrivateMessage = CyclingChallenge.Services.Models.Communication.PrivateMessage;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetOfficialMessagesProcessorTest: GetCommunicationMessagesProcessorBaseTest<GetOfficialMessagesProcessor, OfficialMessages>
    {
        #region Tests

        [Test, TestCase(false), TestCase(true)]
        public void GetOfficialMessagesProcessor_ValidIdNoPage_ReturnsOfficialMessages(bool usePage)
        {
            var team = CreateForTestingPurpose<Team>();
            var recipient = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var sender = CreateIdForTestingPurpose<User>();
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender, type = Enums.Message.Type.Official, deletedByRecipient = true });
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender, type = Enums.Message.Type.Official });
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender });
            var result = usePage ? CreateProcessor(team.Id, 1).Process() : CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertMessagesOk(result.Messages);
            AssertOnlyOfficialMessages(result.Messages);
        }

        private void AssertOnlyOfficialMessages(IEnumerable<Contracts.DataContracts.Communication.PrivateMessage> messages)
        {
            foreach (var message in messages)
            {
                var dbMessage = CommunicationQueries.GetPrivateMessageById(Context, message.Id);
                Assert.AreEqual(Enums.Message.Type.Official, dbMessage?.Type);
                AssertNotDeleted(dbMessage);
            }
        }

        protected internal override void AssertNotDeleted(PrivateMessage message)
        {
            Assert.False(message.DeletedByRecipient);
        }

        #region Training

        [Test]
        public void GetOfficialMessagesProcessor_ValidIdNoPageForTrainingMessage_ReturnsProcessedOfficialMessage()
        {
            var team = CreateForTestingPurpose<Team>();
            var recipient = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var message = CreateOfficialMessage(recipient, true);
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertTrainingMessageOk(message, result.Messages);
        }

        private static void AssertTrainingMessageOk(PrivateMessage message, List<Contracts.DataContracts.Communication.PrivateMessage> messages)
        {
            Assert.AreEqual($"[[{Items.EconomyTraining}]]", messages.First().Title);
            var messageParts = message.Message.Split('|');
            var skill = int.Parse(messageParts[1]);

            var expected = $"[[REP([[{Items.TrainingImprovement}]]||[cycl]||[[RIDER({messageParts[0]})]])]]";
            expected = $"[[REP({expected}||[type]||[[{Items.TrainingTechnique + skill + (skill > 0 ? 1 : 0)}]])]]";
            expected = $"[[REP([[REP({expected}||[before]||{messageParts[2]})]]||[after]||{messageParts[3]})]]";
            Assert.AreEqual(expected, messages.First().Text);
        }

        #endregion

        #region Shop

        [Test]
        public void GetOfficialMessagesProcessor_ValidIdNoPageForShopMessage_ReturnsProcessedOfficialMessage()
        {
            var team = CreateForTestingPurpose<Team>();
            var recipient = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var shopMessage = CreateOfficialMessage(recipient, isShop: true, hasShop: true);
            var noShopMessage = CreateOfficialMessage(recipient, isShop: true);
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertShopMessageOk(Context, shopMessage, result.Messages.Where(msg => msg.Text.Contains(Items.MessageShopSales.ToString())).ToList(), true);
            AssertShopMessageOk(Context, noShopMessage, result.Messages.Where(msg => !msg.Text.Contains(Items.MessageShopSales.ToString())).ToList());
        }

        private static void AssertShopMessageOk(ICyclingChallengeModelContainer db, PrivateMessage message, List<Contracts.DataContracts.Communication.PrivateMessage> messages, bool hasShop = false)
        {
            Assert.AreEqual($"[[{Items.EconomyShopRevenues}]]", messages.First().Title);
            if (hasShop)
            {
                const string regex = @"(\d+)\|(\d+)\|(\d+)";
                var expectedLine = "[[{0}]]: [[REP([[" + Items.MessageShopUnitsSold + "]]||[nsold]||{1})]], [[CRNCY({2})]] ";
                var expected = Regex.Replace(message.Message, regex, m =>
                    string.Format(expectedLine, ProductQueries.GetById(db, m.Groups[1].Value.ToNullableByte() ?? 0)?.Name, m.Groups[2].Value, m.Groups[3].Value));
                expected = expected.Replace(" |", Environment.NewLine);
                expected = $"[[{Items.MessageShopSales}]]{Environment.NewLine}{expected}";
                Assert.AreEqual(expected, messages.First().Text);
            }
            else
            {
                Assert.AreEqual($"[[{Items.MessageShopNoSales}]]", messages.First().Text);
            }
        }

        #endregion

        #region Camp

        [Test]
        public void GetOfficialMessagesProcessor_ValidIdNoPageForCampMessage_ReturnsProcessedOfficialMessage()
        {
            var team = CreateForTestingPurpose<Team>();
            var recipient = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var departureMessage = CreateOfficialMessage(recipient, isCampStart: true);
            var progressMessage = CreateOfficialMessage(recipient);
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertCampMessageOk(departureMessage, result.Messages.Where(msg => msg.Text.Contains(Items.MessageCampDeparture.ToString())).ToList());
            AssertCampMessageOk(progressMessage, result.Messages.Where(msg => !msg.Text.Contains(Items.MessageCampDeparture.ToString())).ToList());
        }

        private void AssertCampMessageOk(PrivateMessage message, List<Contracts.DataContracts.Communication.PrivateMessage> messages)
        {
            Assert.AreEqual($"[[{Items.TransactionTrainingCamp}]]", messages.First().Title);
            if (IsDepartureMessage(message))
            {
                const string regex = @"(\d+)\|(.*)";
                var expectedContainer = $"[[REP([[REP([[{Items.MessageCampDeparture}]]||[all]||{{0}})]]||[loc]||{{1}})]]";
                var expected = Regex.Replace(message.Message, regex, m =>
                    string.Format(expectedContainer, GetRiderText(m.Groups[2].Value), GetLocationText(m.Groups[1].Value)));
                Assert.AreEqual(expected, messages.First().Text);
            } else
            {
                const string regex = @"(\d+),(\d+),(\d+),(\d+)";
                var expectedContainer = $"[[REP([[REP([[REP([[REP([[{{0}}]]||[where]||[[{{1}}]])]]||[before]||{{2}})]]||[after]||{{3}})]]||[cycl]||[[RIDER({{4}})]])]]";
                var expected = Regex.Replace(message.Message, regex, m => 
                    string.Format(expectedContainer, Items.MessageCampImprovement, GetTerrainText(m.Groups[2].Value), m.Groups[3].Value, m.Groups[4].Value, m.Groups[1].Value));
                expected = $"[[{Items.MessageCampProgress}]]{Environment.NewLine}{Environment.NewLine}" + expected;
                Assert.AreEqual(expected, messages.First().Text);
            }
        }

        private static int GetTerrainText(string value)
        {
            int.TryParse(value, out var terrain);
            return Items.TrainingPlains + terrain;
        }

        private string GetLocationText(string value)
        {
            Assert.True(short.TryParse(value, out var idLocation));
            var locationText = LocationQueries.GetLocationTextById(Context, idLocation);
            Assert.NotNull(locationText);
            return locationText;
        }

        private static string GetRiderText(string value)
        {
            return string.Join(Environment.NewLine, value.Split(',').Select(r => $"[[RIDER({r})]]"));
        }

        private static bool IsDepartureMessage(PrivateMessage message)
        {
            var riders = message.Message.Split('|');
            return riders[0].Split(',').Length < 2;
        }

        #endregion

        #endregion


        #region Messages

        private PrivateMessage CreateOfficialMessage(long recipient, bool isTraining = false, bool isShop = false, bool hasShop = false, bool isCampStart = false)
        {
            var message = isTraining ? CreateTrainingMessage(recipient) 
                : isShop ? CreateShopMessage(Context, recipient, hasShop)
                    : isCampStart ? CreateStartCampMessage(Context, recipient)
                        : CreateCampProgressMessage(recipient);
            Context.PrivateMessages.AddOrUpdate(message);
            Context.SaveChanges();
            return message;
        }

        private static PrivateMessage CreateTrainingMessage(long recipient)
        {
            var previousLevel = RandomUtil.RandomNumber() % 19;
            var skill = RandomUtil.RandomElement(new List<int>
            {
                Items.TrainingTechnique - Items.TrainingTechnique,
                Items.TrainingStamina - Items.TrainingTechnique - 1,
                Items.TrainingDescending - Items.TrainingTechnique - 1,
                Items.TrainingSpring - Items.TrainingTechnique - 1,
                Items.TrainingTimeTrial - Items.TrainingTechnique - 1,
                Items.TrainingClimbing - Items.TrainingTechnique - 1
            });
            return new PrivateMessage
            {
                Subject = "[training]",
                Date = DateTime.Now,
                Message = $"{RandomUtil.RandomNumber(4)}|{skill}|{previousLevel}|{previousLevel + 1}",
                Type = Enums.Message.Type.Official,
                Recipient = recipient
            };
        }

        private static PrivateMessage CreateShopMessage(ICyclingChallengeModelContainer context, long recipient, bool hasShop)
        {
            return new PrivateMessage
            {
                Subject = "[shop]",
                Date = DateTime.Now,
                Message = hasShop 
                    ? ProductQueries.GetAll(context).ToList().Select(CreateProductLine).Aggregate((cur,next) => $"{cur}|{next}") 
                    : string.Empty,
                Type = Enums.Message.Type.Official,
                Recipient = recipient
            };
        }

        private static string CreateProductLine(Product p)
        {
            return $"{p.Id}|{RandomUtil.RandomNumber()}|{RandomUtil.RandomNumber()}";
        }

        private static PrivateMessage CreateStartCampMessage(ICyclingChallengeModelContainer context, long recipient)
        {
            var location = RandomUtil.RandomElement(LocationQueries.GetAll(context).ToList());
            var riders = new List<long>();
            for (var i = 0; i < RandomUtil.RandomNumber(1) + 1; i++) riders.Add(RandomUtil.RandomNumber(4));

            return new PrivateMessage
            {
                Subject = "[camp]",
                Date = DateTime.Now,
                Message = $"{location.Id}|{string.Join(",",riders)}",
                Type = Enums.Message.Type.Official,
                Recipient = recipient
            };
        }

        private static PrivateMessage CreateCampProgressMessage(long recipient)
        {
            var previousLevel = RandomUtil.RandomNumber() % 19;
            var terrain = RandomUtil.RandomElement(new List<int>
            {
                Rider.Terrain.Plains,
                Rider.Terrain.Mountains,
                Rider.Terrain.Hills,
                Rider.Terrain.Cobblestones
            });
            return new PrivateMessage
            {
                Subject = "[camp]",
                Date = DateTime.Now,
                Message = $"{RandomUtil.RandomNumber(4)},{terrain},{previousLevel},{previousLevel + 1}",
                Type = Enums.Message.Type.Official,
                Recipient = recipient
            };
        }
        
        #endregion
    }
}