﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Models.Team;
using PrivateMessage = CyclingChallenge.Services.Models.Communication.PrivateMessage;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetOutboxProcessorTest: GetCommunicationMessagesProcessorBaseTest<GetOutboxProcessor, Outbox>
    {
        [Test, TestCase(false), TestCase(true)]
        public void GetOutboxProcessor_ValidIdNoPage_ReturnsOutboxMessages(bool usePage)
        {
            var team = CreateForTestingPurpose<Team>();
            var sender = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var recipient = CreateIdForTestingPurpose<User>();
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender, deletedBySender = true });
            CreateForTestingPurpose<PrivateMessage>(new { recipient, sender });
            var result = usePage ? CreateProcessor(team.Id, 1).Process() : CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertMessagesOk(result.Messages);
        }

        protected internal override void AssertNotDeleted(PrivateMessage message)
        {
            Assert.False(message.DeletedBySender);
        }
    }
}