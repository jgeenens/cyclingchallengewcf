﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using System.Collections.Generic;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;
using PressMessage = CyclingChallenge.Services.Models.Communication.PressMessage;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetPressMessagesProcessorTest: GetMessagesProcessorBaseTest<GetPressMessagesProcessor, PressMessages>
    {
        [Test, TestCase(false), TestCase(true)]
        public void GetPressMessagesProcessor_ValidIdNoPage_ReturnsPressMessages(bool usePage)
        {
            var team = CreateForTestingPurpose<Team>();
            var author = CreateIdForTestingPurpose<User>(new { teamId = team.Id });
            var parent = CreateIdForTestingPurpose<PressMessage>(new { userId = author });
            var child = CreateForTestingPurpose<PressMessage>(new { userId = author, messageId = parent });
            Assert.NotNull(child);
            var result = usePage ? CreateProcessor(team.Id, 1).Process() : CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertMessagesOk(result.Messages);
        }

        private void AssertMessagesOk(List<Contracts.DataContracts.Communication.PressMessage> messages)
        {
            Assert.NotNull(messages);
            Assert.IsNotEmpty(messages);
            messages.ForEach(msg => AssertMessageOk(CommunicationQueries.GetPressMessageById(Context, msg.Id), msg));
        }

        private static void AssertMessageOk(PressMessage message, Contracts.DataContracts.Communication.PressMessage msg)
        {
            Assert.NotNull(msg);
            Assert.AreEqual(message.Id, msg.Id);
            Assert.AreEqual(message.Date, msg.Date);
            Assert.AreEqual(message.Title, msg.Title);
            Assert.AreEqual(message.Body, msg.Text);
            Assert.AreEqual(message.Message, msg.Parent);
        }
    }
}