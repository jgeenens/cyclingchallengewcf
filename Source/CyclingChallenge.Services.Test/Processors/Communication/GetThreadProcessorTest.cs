﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Processors;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Message = CyclingChallenge.Services.Models.Communication.Message;
using Thread = CyclingChallenge.Services.Models.Communication.Thread;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors.Communication
{
    [TestFixture]
    public class GetThreadProcessorTest : ProcessorBaseTest<GetThreadProcessor, Contracts.DataContracts.Communication.Thread>
    {
        #region Tests

        [Test]
        public void GetThreadProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetThreadProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsUser)
        {
            var result = invalidParameterIsUser
                ? CreateProcessor(CreateIdForTestingPurpose<Thread>(new { userId = CreateIdForTestingPurpose<User>() }), string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetThreadProcessor_InvalidId_ErrorCodeIdNotFound(bool invalidIdIsUser)
        {
            long invalidId = -1;
            var result = invalidIdIsUser
                ? CreateProcessor(CreateIdForTestingPurpose<Thread>(new { userId = CreateIdForTestingPurpose<User>() }), invalidId).Process()
                : CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(invalidIdIsUser ? ErrorCodes.IdUserNotFound : ErrorCodes.IdThreadNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetThreadProcessor_ValidId_ReturnsThread(bool withUser)
        {
            var idUser1 = CreateIdForTestingPurpose<User>(new { teamId = CreateIdForTestingPurpose<Team>() });
            var idUser2 = CreateIdForTestingPurpose<User>(new { teamId = CreateIdForTestingPurpose<Team>() });
            var thread = CreateForTestingPurpose<Thread>(new { userId = idUser1 });
            var replyToMessage = CreateForTestingPurpose<Message>(new { threadId = thread.Id, userId = idUser1, userChange = idUser1 });
            var messages = new List<Message>()
            {
                replyToMessage,
                CreateForTestingPurpose<Message>(new { threadId = thread.Id, userId = idUser2, replyTo = replyToMessage.Id }),
                CreateForTestingPurpose<Message>(new { threadId = thread.Id, userId = idUser2, obsolete = true })
            };

            var result = withUser
                ? CreateProcessor(thread.Id, idUser1).Process()
                : CreateProcessor(thread.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(thread.Id, result.Id);
            Assert.AreEqual(thread.Title, result.Name);
            foreach (var message in messages)
            {
                AssertMessageOk(result.Messages, message, withUser ? idUser1 : (long?)null);
            }
        }

        #endregion


        #region Helper Methods

        private void AssertMessageOk(List<Contracts.DataContracts.Communication.Message> messages, Message expectedMessage, long? idUser)
        {
            var message = messages.FirstOrDefault(m => m.Id == expectedMessage.Id);
            Assert.NotNull(message);
            Assert.AreEqual(expectedMessage.Thread, message.Thread);
            Assert.AreEqual(expectedMessage.MessageInThread, message.MessageInThread);
            Assert.AreEqual(expectedMessage.Obsolete ? string.Empty : expectedMessage.Content, message.Text);
            Assert.AreEqual(expectedMessage.Date, message.Date);
            Assert.AreEqual(expectedMessage.LastChange, message.ChangeDate);
            Assert.AreEqual(expectedMessage.Obsolete, message.IsDeleted);
            Assert.AreEqual(expectedMessage.User == idUser, message.CanChange);
            AssertUserOk(message.User, expectedMessage.MessageUser);
            Assert.Null(message.ChangeUser);
            AssertReplyToMessageOk(message.ReplyTo, expectedMessage.MessageReplyTo);
        }

        private void AssertUserOk(Contracts.DataContracts.Communication.User user, User expectedUser)
        {
            Assert.NotNull(user);
            Assert.AreEqual(expectedUser.Id, user.Id);
            Assert.AreEqual($"{expectedUser.FirstName} {expectedUser.LastName}".Trim(), user.Name);
            Assert.AreEqual(expectedUser.Username, user.Alias);
            Assert.NotNull(user.Team);
            Assert.AreEqual(expectedUser.Team, user.Team.Id);
            Assert.AreEqual(expectedUser.UserTeam.ShortName, user.Team.ShortName);
            Assert.AreEqual(expectedUser.UserTeam.Name, user.Team.LongName);
        }

        private void AssertReplyToMessageOk(MessageBase message, Message expectedMessage)
        {
            if (expectedMessage == null)
            {
                return;
            }

            Assert.NotNull(expectedMessage);
            Assert.AreEqual(expectedMessage.Id, message.Id);
            Assert.AreEqual(expectedMessage.Thread, message.Thread);
            Assert.AreEqual(expectedMessage.MessageInThread, message.MessageInThread);
        }

        #endregion
    }
}