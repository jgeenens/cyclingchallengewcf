﻿using NUnit.Framework;
using System;
using System.Linq;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Communication;
using ErrorCodes = CyclingChallenge.Services.Enums.ErrorCodes;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class PostMessageProcessorTest: ProcessorBaseTest<PostMessageProcessor, ResultBase>
    {
        #region Tests

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4)]
        public void PostMessageProcessor_LessThanFourParameters_ErrorCodeParameterMissing(byte missingParameter)
        {
            var result = missingParameter == 1 
                ? CreateProcessor(null).Process()
                : missingParameter == 2 
                    ? CreateProcessor(RandomUtil.RandomNumber()).Process()
                    : missingParameter == 3
                        ? CreateProcessor(RandomUtil.RandomNumber(), RandomUtil.RandomNumber()).Process()
                        : CreateProcessor(RandomUtil.RandomNumber(), RandomUtil.RandomNumber(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4), TestCase(5)]
        public void PostMessageProcessor_InvalidParameter_ErrorCodeInvalidParameterType(byte invalidParameter)
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var userIp = $"{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}";
            ResultBase result;
            switch (invalidParameter)
            {
                case 1:
                    result = CreateProcessor(string.Empty, userId, userIp, RandomUtil.RandomString()).Process();
                    break;
                case 2:
                    result = CreateProcessor(threadId, string.Empty, userIp, RandomUtil.RandomString()).Process();
                    break;
                case 3:
                    result = CreateProcessor(threadId, userId, 0, RandomUtil.RandomString()).Process();
                    break;
                case 4:
                    result = CreateProcessor(threadId, userId, userIp, 0).Process();
                    break;
                default:
                    result = CreateProcessor(threadId, userId, userIp, RandomUtil.RandomString(), string.Empty).Process();
                    break;
            }

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void PostMessageProcessor_InvalidIdThread_ErrorCodeIdThreadNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, RandomUtil.RandomNumber(), RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdThreadNotFound, result.ErrorCode);
        }

        [Test]
        public void PostMessageProcessor_InvalidIdUser_ErrorCodeIdUserNotFound()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId });
            long invalidId = -1;
            var result = CreateProcessor(idThread, invalidId, RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdUserNotFound, result.ErrorCode);
        }

        [Test]
        public void PostMessageProcessor_UserIpEmpty_ErrorCodeUserIpCannotBeEmpty()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId });
            var result = CreateProcessor(idThread, userId, string.Empty, RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.UserIpCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void PostMessageProcessor_UserIpIsInvalid_ErrorCodeInvalidFormatUserIp()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId });
            var result = CreateProcessor(idThread, userId, RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidFormatUserIp, result.ErrorCode);
        }

        [Test]
        public void PostMessageProcessor_MessageEmpty_ErrorCodeMessageCannotBeEmpty()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId });
            var userIp = $"{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}";
            var result = CreateProcessor(idThread, userId, userIp, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.MessageCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void PostMessageProcessor_InvalidIdMessageRepliedTo_ErrorCodeIdMessageNotFound()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId });
            var userIp = $"{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}";
            long invalidId = -1;
            var result = CreateProcessor(idThread, userId, userIp, RandomUtil.RandomString(), invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdMessageNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void PostMessageProcessor_ValidInformationProvided_CreatesAMessage(bool withMessageRepliedTo)
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var userIp = $"{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}.{RandomUtil.RandomNumber()}";
            var message = RandomUtil.RandomString();
            var messageRepliedTo = withMessageRepliedTo ? CreateForTestingPurpose<Message>(new { threadId, userId }) : null;
            var result = CreateProcessor(threadId, userId, userIp, message, messageRepliedTo?.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            AssertMessageOk(threadId, userId, userIp, message, messageRepliedTo);
        }

        #endregion


        #region Private Methods

        private void AssertMessageOk(long idThread, long idUser, string userIp, string content, Message messageRepliedTo)
        {
            var message = messageRepliedTo != null 
                ? CommunicationQueries.GetMessagesByThread(Context, idThread).First(m => m.MessageInThread != messageRepliedTo.MessageInThread)
                : CommunicationQueries.GetMessagesByThread(Context, idThread).First();
            Assert.NotNull(message);
            Assert.AreEqual(idUser, message.User);
            Assert.AreEqual(DateTime.Today.Date, message.Date.Date);
            Assert.AreEqual(content, message.Content);
            Assert.AreEqual(userIp, message.UserIp);
            Assert.AreEqual(messageRepliedTo?.Id, message.ReplyTo);
            Assert.AreEqual((messageRepliedTo?.MessageInThread ?? 0) + 1, message.MessageInThread);
            Assert.False(message.Obsolete);
        }

        #endregion
    }
}