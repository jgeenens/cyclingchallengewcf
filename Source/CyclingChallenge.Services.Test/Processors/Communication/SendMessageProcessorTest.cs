﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using System;
using System.Linq;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class SendMessageProcessorTest: ProcessorBaseTest<SendMessageProcessor, ResultBase>
    {
        #region Tests

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4)]
        public void SendMessageProcessor_LessThanFourParameters_ErrorCodeParameterMissing(byte missingParameter)
        {
            var result = missingParameter == 1 
                ? CreateProcessor(null).Process()
                : missingParameter == 2 
                    ? CreateProcessor(RandomUtil.RandomNumber()).Process()
                    : missingParameter == 3
                        ? CreateProcessor(RandomUtil.RandomNumber(), RandomUtil.RandomString()).Process()
                        : CreateProcessor(RandomUtil.RandomNumber(), RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4)]
        public void SendMessageProcessor_InvalidParameter_ErrorCodeInvalidParameterType(byte invalidParameter)
        {
            var user = CreateForTestingPurpose<User>();
            ResultBase result;
            switch (invalidParameter)
            {
                case 1:
                    result = CreateProcessor(string.Empty, RandomUtil.RandomString(), RandomUtil.RandomString(), RandomUtil.RandomString()).Process();
                    break;
                case 2:
                    result = CreateProcessor(user.Id, 0, RandomUtil.RandomString(), RandomUtil.RandomString()).Process();
                    break;
                case 3:
                    result = CreateProcessor(user.Id, user.Username, 0, RandomUtil.RandomString()).Process();
                    break;
                default:
                    result = CreateProcessor(user.Id, user.Username, RandomUtil.RandomString(), 0).Process();
                    break;
            }

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void SendMessageProcessor_InvalidId_ErrorCodeIdUserNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, RandomUtil.RandomString(), RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdUserNotFound, result.ErrorCode);
        }

        [Test]
        public void SendMessageProcessor_RecipientUserNameEmpty_ErrorCodeRecipientUserNameCannotBeEmpty()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, string.Empty, RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.RecipientUserNameCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void SendMessageProcessor_RecipientUserNameInvalid_ErrorCodeUserNameNotFound()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, RandomUtil.RandomString(), RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.UserNameNotFound, result.ErrorCode);
        }

        [Test]
        public void SendMessageProcessor_SubjectEmpty_ErrorCodeSubjectCannotBeEmpty()
        {
            var user = CreateForTestingPurpose<User>();
            var result = CreateProcessor(user.Id, user.Username, string.Empty, RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.SubjectCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void SendMessageProcessor_MessageEmpty_ErrorCodeMessageCannotBeEmpty()
        {
            var user = CreateForTestingPurpose<User>();
            var result = CreateProcessor(user.Id, user.Username, RandomUtil.RandomString(), string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.MessageCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void SendMessageProcessor_ValidInformationProvided_CreatesAPrivateMessage()
        {
            var idSender = CreateIdForTestingPurpose<User>();
            var recipient = CreateForTestingPurpose<User>();
            var subject = RandomUtil.RandomString();
            var message = RandomUtil.RandomString();
            var result = CreateProcessor(idSender, recipient.Username, subject, message).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            AssertMessageOk(idSender, recipient, subject, message);
        }

        #endregion


        #region Private Methods

        private void AssertMessageOk(long idSender, User recipient, string subject, string message)
        {
            var lastMessages = CommunicationQueries.GetMessagesForUser(Context, idSender, 0, false).ToList();
            Assert.IsNotEmpty(lastMessages);
            Assert.True(lastMessages.Any(m => m.Recipient == recipient.Id && m.Subject.Equals(subject, StringComparison.InvariantCultureIgnoreCase)
                                              && m.Message.Equals(message, StringComparison.InvariantCultureIgnoreCase)));
        }

        #endregion
    }
}