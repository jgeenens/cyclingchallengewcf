﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Test.Utils;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class BuyShopProductsProcessorTest: ProcessorBaseTest<BuyShopProductsProcessor, ResultBase>
    {
        #region Tests

        [Test]
        public void BuyShopProductsProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            result = CreateProcessor(team.Id, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_TeamWithoutShop_ErrorCodeShopNotExists()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(team.Id, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ShopNotExists, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_InvalidIdProduct_ErrorCodeIdProductNotFound()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new [] { new ProductBase { Id = 0 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdProductNotFound, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_InvalidQuantity_ErrorCodeInvalidProductQuantity()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new [] { new ProductBase { Id = 1, Quantity = -1 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidProductQuantity, result.ErrorCode);

            result = CreateProcessor(team.Id, new [] { new ProductBase { Id = 1, Quantity = 0 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidProductQuantity, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_TeamWithPredictedAtIntMinValue_ErrorCodeNotEnoughMoney()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true, predicted = int.MinValue });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new [] { new ProductBase { Id = 1, Quantity = 100 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.NotEnoughMoney, result.ErrorCode);
        }

        [Test]
        public void BuyShopProductsProcessor_ValidIdHasShopAndEnoughMoney_AddsProductsToStockAndAddsTransaction()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            var expectedShop = CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var products = new []
            {
                new ProductBase { Id = 1, Quantity = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 2, Quantity = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 3, Quantity = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 4, Quantity = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 5, Quantity = (short)RandomUtil.RandomNumber() }
            };
            var result = CreateProcessor(team.Id, products).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            using (var context = RuntimeManager.CreateDbContext())
            {
                AssertShopOk(context, team.Id, expectedShop, products);
                AssertTransactionOk(context, team.Id, products.Sum(p => p.Quantity * ProductQueries.GetById(Context, p.Id).Buy));
            }
        }


        #endregion


        #region Assert Methods

        private static void AssertShopOk(ICyclingChallengeModelContainer context, long idTeam, Clubshop expectedShop, ProductBase[] products)
        {
            var shop = TeamQueries.GetShop(context, idTeam);
            Assert.NotNull(shop);
            Assert.AreEqual(expectedShop.Quantity1 + products.First(p => p.Id == 1).Quantity, shop.Quantity1);
            Assert.AreEqual(expectedShop.Quantity2 + products.First(p => p.Id == 2).Quantity, shop.Quantity2);
            Assert.AreEqual(expectedShop.Quantity3 + products.First(p => p.Id == 3).Quantity, shop.Quantity3);
            Assert.AreEqual(expectedShop.Quantity4 + products.First(p => p.Id == 4).Quantity, shop.Quantity4);
            Assert.AreEqual(expectedShop.Quantity5 + products.First(p => p.Id == 5).Quantity, shop.Quantity5);
        }

        private static void AssertTransactionOk(ICyclingChallengeModelContainer context, long idTeam, long price)
        {
            var transactions = EconomyQueries.GetTransactionsByTeam(context, idTeam);
            Assert.IsNotEmpty(transactions);

            var shopOpening = transactions.FirstOrDefault(t => t.Description == Items.TransactionShopPurchase);
            Assert.NotNull(shopOpening);
            Assert.True(shopOpening.Outgoing);
            Assert.AreEqual(price, shopOpening.Value);
        }

        #endregion
    }
}