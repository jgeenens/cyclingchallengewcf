﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using System.Collections.Generic;
using CyclingChallenge.Services.Models.Team;
using Economy = CyclingChallenge.Services.Models.Economy.Economy;
using Transaction = CyclingChallenge.Services.Models.Economy.Transaction;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetEconomyProcessorTest: ProcessorBaseTest<GetEconomyProcessor, Contracts.DataContracts.Economy.Economy>
    {
        [Test]
        public void GetEconomyProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetEconomyProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetEconomyProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetEconomyProcessor_ValidId_ReturnsEconomy()
        {
            var team = CreateForTestingPurpose<Team>();
            var economy = CreateForTestingPurpose<Economy>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Money, result.Money);
            Assert.AreEqual(team.Predicted, result.MoneyProjected);
            AssertRevenuesOk(economy, result.Revenues);
            AssertExpensesOk(economy, result.Expenses);
        }

        [Test, TestCase(Items.TransactionFirstInRace, false), TestCase(Items.TransactionRiderSold, false), 
            TestCase(Items.TransactionShopSales, false), TestCase(Items.TransactionBonus, false),
            TestCase(Items.EconomyRegistration, true), TestCase(Items.TransactionRiderBought, true), 
            TestCase(Items.TransactionShopPurchase, true), TestCase(Items.TransactionTrainingCamp, true), 
            TestCase(Items.TransactionNewTrainer, true)]
        public void GetEconomyProcessor_ValidIdUncheckedTransactions_ReturnsEconomyAfterProcessingTransactions(int description, bool outgoing)
        {
            var team = CreateForTestingPurpose<Team>();
            var economy = CreateForTestingPurpose<Economy>(new { teamId = team.Id });
            var transaction = CreateForTestingPurpose<Transaction>(new
            {
                teamId = team.Id,
                description,
                outgoing
            });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Money, result.Money);
            Assert.AreEqual(team.Predicted + (outgoing ? -1 : 1) * transaction.Value, result.MoneyProjected);
            AssertRevenuesOk(economy, result.Revenues, transaction);
            AssertExpensesOk(economy, result.Expenses, transaction);
        }

        private static void AssertExpensesOk(Economy economy, List<Expense> expenses, Transaction transaction = null)
        {
            Assert.NotNull(expenses);
            Assert.AreEqual(10, expenses.Count);
            var registration = (transaction?.Outgoing ?? false) && transaction.Description == Items.EconomyRegistration ? transaction.Value : 0;
            var transfers = (transaction?.Outgoing ?? false) && transaction.Description == Items.TransactionRiderBought ? transaction.Value : 0;
            var shop = (transaction?.Outgoing ?? false) && transaction.Description == Items.TransactionShopPurchase ? transaction.Value : 0;
            var training = (transaction?.Outgoing ?? false) && transaction.Description == Items.TransactionTrainingCamp ? transaction.Value : 0;
            var trainer = (transaction?.Outgoing ?? false) && transaction.Description == Items.TransactionNewTrainer ? transaction.Value : 0;
            Assert.AreEqual(economy.Expense1, expenses.FirstOrDefault(e => e.Type == Items.EconomyStaff)?.Amount);
            Assert.AreEqual(economy.Expense2, expenses.FirstOrDefault(e => e.Type == Items.EconomyWages)?.Amount);
            Assert.AreEqual(economy.Expense3, expenses.FirstOrDefault(e => e.Type == Items.EconomyYouth)?.Amount);
            Assert.AreEqual(economy.Expense4, expenses.FirstOrDefault(e => e.Type == Items.EconomyShopOpening)?.Amount);
            Assert.AreEqual(economy.Expense5, expenses.FirstOrDefault(e => e.Type == Items.EconomyTransactions)?.Amount);
            Assert.AreEqual(economy.TransactionOut1 + registration, expenses.FirstOrDefault(e => e.Type == Items.EconomyRegistration)?.Amount);
            Assert.AreEqual(economy.TransactionOut2 + transfers, expenses.FirstOrDefault(e => e.Type == Items.EconomyTransfers)?.Amount);
            Assert.AreEqual(economy.TransactionOut3 + shop, expenses.FirstOrDefault(e => e.Type == Items.EconomyShopExpenses)?.Amount);
            Assert.AreEqual(economy.TransactionOut4 + training, expenses.FirstOrDefault(e => e.Type == Items.EconomyTraining)?.Amount);
            Assert.AreEqual(economy.TransactionOut5 + trainer, expenses.FirstOrDefault(e => e.Type == Items.EconomyOther)?.Amount);
        }

        private static void AssertRevenuesOk(Economy economy, List<Revenue> revenues, Transaction transaction = null)
        {
            Assert.NotNull(revenues);
            Assert.AreEqual(6, revenues.Count);
            var pricemoney = !(transaction?.Outgoing ?? true) && transaction.Description == Items.TransactionFirstInRace ? transaction.Value : 0;
            var transfers = !(transaction?.Outgoing ?? true) && transaction.Description == Items.TransactionRiderSold ? transaction.Value : 0;
            var shop = !(transaction?.Outgoing ?? true) && transaction.Description == Items.TransactionShopSales ? transaction.Value : 0;
            var bonus = !(transaction?.Outgoing ?? true) && transaction.Description == Items.TransactionBonus ? transaction.Value : 0;
            Assert.AreEqual(economy.Income1, revenues.FirstOrDefault(r => r.Type == Items.EconomySponsors)?.Amount);
            Assert.AreEqual(economy.Income2, revenues.FirstOrDefault(r => r.Type == Items.EconomyTransactions)?.Amount);
            Assert.AreEqual(economy.TransactionIn1 + pricemoney, revenues.FirstOrDefault(r => r.Type == Items.EconomyPriceMoney)?.Amount);
            Assert.AreEqual(economy.TransactionIn2 + transfers, revenues.FirstOrDefault(r => r.Type == Items.EconomyTransfers)?.Amount);
            Assert.AreEqual(economy.TransactionIn3 + shop, revenues.FirstOrDefault(r => r.Type == Items.EconomyShopRevenues)?.Amount);
            Assert.AreEqual(economy.TransactionIn4 + bonus, revenues.FirstOrDefault(r => r.Type == Items.EconomyOther)?.Amount);
        }
    }
}