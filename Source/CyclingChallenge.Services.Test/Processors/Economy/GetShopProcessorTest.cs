﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Economy;
using Manager = CyclingChallenge.Services.Models.Staff.Manager;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetShopProcessorTest: ProcessorBaseTest<GetShopProcessor, Shop>
    {
        [Test]
        public void GetShopProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetShopProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetShopProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetShopProcessor_ValidIdNoShop_ReturnsOnlyDivisionLevel()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.True(result.IsClosed);
            Assert.AreEqual(team.TeamDivision.Level, result.Level);
            Assert.Null(result.Manager);
            Assert.Null(result.Products);
        }

        [Test]
        public void GetShopProcessor_ValidIdAndShop_ReturnsShopDetails()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            var shop = CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.False(result.IsClosed);
            Assert.AreEqual(shop.Hint, result.Hint);
            Assert.Null(result.Manager);
            AssertProductsOk(shop, result.Products);
        }

        [Test]
        public void GetShopProcessor_ValidIdShopAndManager_ReturnsShopDetailsAndManager()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            var shop = CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var manager = CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.False(result.IsClosed);
            Assert.AreEqual(shop.Hint, result.Hint);
            AssertProductsOk(shop, result.Products);
            AssertManagerOk(manager, result.Manager);
        }

        private void AssertProductsOk(Clubshop shop, List<Contracts.DataContracts.Economy.Product> products)
        {
            Assert.NotNull(products);
            Assert.IsNotEmpty(products);

            Assert.AreEqual(shop.Quantity1, products.FirstOrDefault(p => p.Id == 1)?.Quantity);
            Assert.AreEqual(shop.Quantity2, products.FirstOrDefault(p => p.Id == 2)?.Quantity);
            Assert.AreEqual(shop.Quantity3, products.FirstOrDefault(p => p.Id == 3)?.Quantity);
            Assert.AreEqual(shop.Quantity4, products.FirstOrDefault(p => p.Id == 4)?.Quantity);
            Assert.AreEqual(shop.Quantity5, products.FirstOrDefault(p => p.Id == 5)?.Quantity);

            Assert.AreEqual(ProductQueries.GetById(Context, 1).Buy, products.FirstOrDefault(p => p.Id == 1)?.Buy);
            Assert.AreEqual(ProductQueries.GetById(Context, 2).Sell, products.FirstOrDefault(p => p.Id == 2)?.Sell);
            Assert.AreEqual(ProductQueries.GetById(Context, 3).Name, products.FirstOrDefault(p => p.Id == 3)?.Name);
            Assert.AreEqual(ProductQueries.GetById(Context, 4).Sell, products.FirstOrDefault(p => p.Id == 4)?.Sell);
            Assert.AreEqual(ProductQueries.GetById(Context, 5).Buy, products.FirstOrDefault(p => p.Id == 5)?.Buy);
        }

        private void AssertManagerOk(Manager manager, Contracts.DataContracts.Economy.Manager message)
        {
            Assert.NotNull(message);
            Assert.AreEqual(manager.Id, message.Id);
            Assert.AreEqual($"{manager.FirstName} {manager.LastName}", message.Name);
            Assert.AreEqual(manager.Status >= Managers.Status.Active, message.IsActive);
            Assert.AreEqual(manager.Status == Managers.Status.Learning, message.IsLearning);
            AssertEducationOk(manager, message.Education);
            AssertPictureOk(manager, message.Picture);
        }

        private void AssertEducationOk(Manager manager, Education education)
        {
            Assert.NotNull(education);
            Assert.AreEqual(manager.Level, education.Level);
            Assert.AreEqual(manager.Learning, education.Start);
            Assert.AreEqual((manager.Level + 1) * 12, education.Duration);
            var tokens = GameQueries.GetAddonById(Context, AddOns.BeginnerShopManager).Tokens;
            Assert.AreEqual(tokens, education.Price);
        }

        private void AssertPictureOk(Manager manager, Picture picture)
        {
            Assert.NotNull(picture);
            Assert.AreEqual(manager.Eyes, picture.Eyes);
            Assert.AreEqual(manager.Nose, picture.Nose);
            Assert.AreEqual(manager.Lips, picture.Lips);
            Assert.AreEqual(manager.Hair, picture.Hair);
            Assert.AreEqual(manager.Face, picture.Face);
        }
    }
}