﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using System;
using CyclingChallenge.Services.Models.Team;
using Sponsor = CyclingChallenge.Services.Models.Economy.Sponsor;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetSponsorsProcessorTest: ProcessorBaseTest<GetSponsorsProcessor, Sponsors>
    {
        #region Tests

        [Test]
        public void GetSponsorsProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetSponsorsProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetSponsorsProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetSponsorsProcessor_ValidIdWithSponsor_ReturnsSponsor()
        {
            var team = CreateForTestingPurpose<Team>();
            var sponsor = CreateForTestingPurpose<Sponsor>(new { teamId = team.Id, weeksToGo = 1 });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.True(result.HasSponsor);
            AssertDivisionOk(team.TeamDivision, result.Division);
            AssertSponsorOk(sponsor, result.Sponsor.FirstOrDefault());
        }

        [Test]
        public void GetSponsorsProcessor_ValidIdNoSponsor_ReturnsAvailableSponsors()
        {
            var team = CreateForTestingPurpose<Team>(new { rank = 5 });
            CreateForTestingPurpose<Sponsor>(new { divisionId = team.Division, type = 2 });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.False(result.HasSponsor);
            AssertDivisionOk(team.TeamDivision, result.Division);
            Assert.IsNotEmpty(result.Sponsor);
            var minType = (int)Math.Floor(team.Rank / 4.0);
            Assert.True(result.Sponsor.All(s => SponsorQueries.GetById(Context, s.Id)?.Type >= minType));
            Assert.True(result.Sponsor.All(s => SponsorQueries.GetById(Context, s.Id)?.Team == null));
        }

        #endregion


        #region Assert Methods

        private static void AssertDivisionOk(Division division, Contracts.DataContracts.Team.Division result)
        {
            Assert.NotNull(division);
            Assert.NotNull(result);

            Assert.AreEqual(division.Id, result.Id);
            Assert.AreEqual(division.Name, result.Name);
            Assert.NotNull(result.Country);
            Assert.AreEqual(division.Country, result.Country.Id);

            Assert.NotNull(division.DivisionCountry);
            Assert.AreEqual(division.DivisionCountry.Item, result.Country.Name);
        }

        private static void AssertSponsorOk(Sponsor sponsor, Contracts.DataContracts.Economy.Sponsor result)
        {
            Assert.NotNull(result);

            Assert.AreEqual(sponsor.Id, result.Id);
            Assert.AreEqual(sponsor.Name, result.Name);
            Assert.AreEqual(sponsor.Signing, result.Signing);
            Assert.AreEqual(sponsor.Weekly, result.Bonus);
            Assert.AreEqual(sponsor.WeeksToGo, result.WeeksLeft);
            Assert.AreEqual(sponsor.Weeks, result.WeeksTotal);
            Assert.AreEqual(sponsor.Country, result.Country.Id);

            Assert.NotNull(sponsor.SponsorCountry);
            Assert.AreEqual(sponsor.SponsorCountry.Item, result.Country.Name);
        }

        #endregion
    }
}