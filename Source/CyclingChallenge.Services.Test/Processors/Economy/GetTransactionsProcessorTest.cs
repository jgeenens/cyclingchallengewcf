﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Staff;
using Transaction = CyclingChallenge.Services.Models.Economy.Transaction;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTransactionsProcessorTest: ProcessorBaseTest<GetTransactionsProcessor, TeamTransactions>
    {
        [Test]
        public void GetTransactionsProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTransactionsProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, DateTime.Now).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(FixedID.Team, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTransactionsProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId, DateTime.Now).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test, TestCase(Items.TransactionFirstInRace, false), TestCase(Items.TransactionRiderSold, false),
        TestCase(Items.TransactionShopSales, false), TestCase(Items.TransactionBonus, false),
        TestCase(Items.EconomyRegistration, true), TestCase(Items.TransactionRiderBought, true),
        TestCase(Items.TransactionShopPurchase, true), TestCase(Items.TransactionTrainingCamp, true),
        TestCase(Items.TransactionNewTrainer, true)]
        public void GetTransactionsProcessor_ValidIdAndDate_ReturnsTransactionsSinceDate(int description, bool outgoing)
        {
            var transaction = CreateForTestingPurpose<Transaction>(new
            {
                teamId = FixedID.Team,
                description,
                outgoing,
                info = GetInfo(description)
            });
            var result = CreateProcessor(FixedID.Team, DateTime.Now.AddDays(-7)).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.IsNotEmpty(result.Transactions);
            AssertTransactionOk(transaction, result.Transactions.FirstOrDefault(t => t.Id == transaction.Id));
        }

        private string GetInfo(int description)
        {
            switch (description)
            {
                case Items.TransactionRiderBought:
                case Items.TransactionRiderSold:
                    return CreateIdForTestingPurpose<Player>().ToString();
                case Items.TransactionNewTrainer:
                    return CreateIdForTestingPurpose<Coach>(new {teamId = FixedID.Team}).ToString();
                case Items.TransactionTrainingCamp:
                    return FixedID.Location.ToString();
                case Items.EconomyRegistration:
                    return FixedID.Race.ToString();
                case Items.TransactionFirstInRace:
                    return $"{FixedID.TourRace}-1";
                default:
                    return string.Empty;
            }
        }

        private static void AssertTransactionOk(Transaction transaction, Contracts.DataContracts.Economy.Transaction result)
        {
            Assert.NotNull(result);
            Assert.AreEqual(transaction.Id, result.Id);
            Assert.AreEqual(transaction.DateTransaction, result.Time);
            Assert.AreEqual(!transaction.Outgoing, result.IsRevenue);
            Assert.AreEqual(transaction.Value, result.Amount);
            Assert.AreEqual(transaction.Description, result.Description);
            AssertRiderOk(transaction, result.Rider);
            AssertTrainerOk(transaction, result.Trainer);
            AssertRaceOk(transaction, result.Race);
            AssertStageOk(transaction, result.Stage);
            AssertTrainingCampOk(transaction, result.TrainingCamp);
        }

        private static void AssertRiderOk(Transaction transaction, Contracts.DataContracts.Rider.Rider rider)
        {
            if (transaction.Description != Items.TransactionRiderBought && transaction.Description != Items.TransactionRiderSold)
                return;
            
            Assert.NotNull(rider);
            Assert.AreEqual(long.Parse(transaction.Info), rider.Id);
        }

        private static void AssertTrainerOk(Transaction transaction, Trainer trainer)
        {
            if (transaction.Description != Items.TransactionNewTrainer)
                return;

            Assert.NotNull(trainer);
            Assert.AreEqual(long.Parse(transaction.Info), trainer.Id);
        }

        private static void AssertRaceOk(Transaction transaction, Contracts.DataContracts.Race.RaceBase race)
        {
            if (transaction.Description != Items.TransactionFirstInRace && transaction.Description != Items.EconomyRegistration)
                return;

            Assert.NotNull(race);
            Assert.AreEqual(long.Parse(transaction.Info.Split('-')[0]), race.Id);
        }

        private static void AssertStageOk(Transaction transaction, Contracts.DataContracts.Race.Stage stage)
        {
            if (transaction.Description != Items.TransactionFirstInRace)
                return;

            Assert.NotNull(stage);
            Assert.AreEqual(short.Parse(transaction.Info.Split('-')[1]), stage.Id);
        }

        private static void AssertTrainingCampOk(Transaction transaction, Location trainingCamp)
        {
            if (transaction.Description != Items.TransactionTrainingCamp)
                return;

            Assert.NotNull(trainingCamp);
            Assert.AreEqual(short.Parse(transaction.Info), trainingCamp.Id);
        }
    }
}