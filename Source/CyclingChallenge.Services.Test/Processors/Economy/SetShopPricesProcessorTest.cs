﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Test.Utils;
using Manager = CyclingChallenge.Services.Models.Staff.Manager;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class SetShopPricesProcessorTest: ProcessorBaseTest<SetShopPricesProcessor, ResultBase>
    {
        #region Tests

        [Test]
        public void SetShopPricesProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            result = CreateProcessor(team.Id, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_TeamWithoutShop_ErrorCodeShopNotExists()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(team.Id, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ShopNotExists, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_TeamWithoutShopManager_ErrorCodeTeamHasNoShopManager()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new ProductBase()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamHasNoShopManager, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_InvalidIdProduct_ErrorCodeIdProductNotFound()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new [] { new ProductBase { Id = 0 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdProductNotFound, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_NoStockForProduct_ErrorCodeNoStockForProduct()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id, quantity1 = 0 });
            CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new [] { new ProductBase { Id = 1 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.NoStockForProduct, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_InvalidPrice_ErrorCodeInvalidProductPrice()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id, new[] { new ProductBase { Id = 1, Price = null } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidProductPrice, result.ErrorCode);

            result = CreateProcessor(team.Id, new[] { new ProductBase { Id = 1, Price = 0 } }).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidProductPrice, result.ErrorCode);
        }

        [Test]
        public void SetShopPricesProcessor_ValidIdHasShopAndManager_SetsProductPrices()
        {
            var team = CreateForTestingPurpose<Team>(new { hasShop = true });
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            var products = new []
            {
                new ProductBase { Id = 1, Price = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 2, Price = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 3, Price = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 4, Price = (short)RandomUtil.RandomNumber() },
                new ProductBase { Id = 5, Price = (short)RandomUtil.RandomNumber() }
            };
            var result = CreateProcessor(team.Id, products).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            using (var context = RuntimeManager.CreateDbContext())
            {
                AssertShopOk(context, team.Id, products);
            }
        }


        #endregion


        #region Assert Methods

        private static void AssertShopOk(ICyclingChallengeModelContainer context, long idTeam, ProductBase[] products)
        {
            var shop = TeamQueries.GetShop(context, idTeam);
            Assert.NotNull(shop);
            Assert.AreEqual(products.First(p => p.Id == 1).Price, shop.Price1);
            Assert.AreEqual(products.First(p => p.Id == 2).Price, shop.Price2);
            Assert.AreEqual(products.First(p => p.Id == 3).Price, shop.Price3);
            Assert.AreEqual(products.First(p => p.Id == 4).Price, shop.Price4);
            Assert.AreEqual(products.First(p => p.Id == 5).Price, shop.Price5);
        }

        #endregion
    }
}