﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using System.Linq;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class StartShopProcessorTest: ProcessorBaseTest<StartShopProcessor, ResultBase>
    {
        #region Tests

        [Test]
        public void StartShopProcessor_NoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void StartShopProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void StartShopProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void StartShopProcessor_TeamWithShop_ErrorCodeShopAlreadyExists()
        {
            var team = CreateForTestingPurpose<Team>();
            CreateForTestingPurpose<Clubshop>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ShopAlreadyExists, result.ErrorCode);
        }

        [Test]
        public void StartShopProcessor_TeamWithPredictedAtIntMinValue_ErrorCodeNotEnoughMoney()
        {
            var team = CreateForTestingPurpose<Team>(new { predicted = int.MinValue });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.NotEnoughMoney, result.ErrorCode);
        }

        [Test]
        public void StartShopProcessor_ValidIdNoShopEnoughMoney_CreatesShopAndAddsTransaction()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            using (var context = RuntimeManager.CreateDbContext())
            {
                AssertShopOk(context, team.Id);
                AssertTransactionOk(context, team.Id);
            }
        }


        #endregion


        #region Assert Methods

        private static void AssertShopOk(ICyclingChallengeModelContainer context, long idTeam)
        {
            var shop = TeamQueries.GetShop(context, idTeam);
            Assert.NotNull(shop);
            Assert.AreEqual(Items.ParticipationHint, shop.Hint);
        }

        private static void AssertTransactionOk(ICyclingChallengeModelContainer context, long idTeam)
        {
            var transactions = EconomyQueries.GetTransactionsByTeam(context, idTeam);
            Assert.IsNotEmpty(transactions);

            var shopOpening = transactions.FirstOrDefault(t => t.Description == Items.TransactionShopOpening);
            Assert.NotNull(shopOpening);
            Assert.True(shopOpening.Outgoing);

            var setting = GameQueries.GetSettingById(context, Settings.Shop);
            Assert.NotNull(setting);
            Assert.AreEqual(setting.Value, shopOpening.Value.ToString());
        }

        #endregion
    }
}