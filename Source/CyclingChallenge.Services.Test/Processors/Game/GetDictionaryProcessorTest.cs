﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using ServiceDictionary = CyclingChallenge.Services.Contracts.DataContracts.Dictionary;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetDictionaryProcessorTest: ProcessorBaseTest<GetDictionaryProcessor, ServiceDictionary>
    {
        [Test]
        public void GetDictionaryProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetDictionaryProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetDictionaryProcessor_InvalidId_ErrorCodeIdLanguageNotFound()
        {
            byte invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdLanguageNotFound, result.ErrorCode);
        }

        [Test]
        public void GetDictionaryProcessor_InvalidId_ErrorCodeNoItemsInDictionary()
        {
            var result = CreateProcessor(FixedID.LanguageNotInDictionary).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.NoItemsInDictionary, result.ErrorCode);
        }

        [Test]
        public void GetDictionaryProcessor_ValidId_ReturnsDictionary()
        {
            var result = CreateProcessor(FixedID.Language).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(FixedID.Language, result.Id);
            Assert.IsNotEmpty(result.Items);
        }
    }
}
