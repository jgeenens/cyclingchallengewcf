﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Contracts.DataContracts.Game;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetSearchResultsProcessorTest: ProcessorBaseTest<GetSearchResultsProcessor, SearchResults>
    {
        #region Tests

        [Test]
        public void GetSearchResultsProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(SearchType.Division).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetSearchResultsProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsSearchType)
        {
            var result = invalidParameterIsSearchType 
                ? CreateProcessor(string.Empty, string.Empty).Process()
                : CreateProcessor(SearchType.Division, 0).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetSearchResultsProcessor_InvalidSearchType_ErrorCodeInvalidSearchType()
        {
            byte invalidSearchType = 0;
            var result = CreateProcessor(invalidSearchType, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidSearchType, result.ErrorCode);
        }

        [Test]
        public void GetSearchResultsProcessor_DivisionNeedleEmpty_ErrorCodeSearchStringEmpty()
        {
            var result = CreateProcessor(SearchType.Division, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.SearchStringEmpty, result.ErrorCode);
        }

        [Test]
        public void GetSearchResultsProcessor_SearchTypeRiderOrTeamNoSecondParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(SearchType.Rider, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetSearchResultsProcessor_SearchTypeRiderOrTeamInvalidSecondParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(SearchType.Rider, string.Empty, 0).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetSearchResultsProcessor_SearchTypeRiderOrTeamBothParametersEmpty_ErrorCodeSearchStringEmpty()
        {
            var result = CreateProcessor(SearchType.Rider, string.Empty, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.SearchStringEmpty, result.ErrorCode);
        }

        [Test]
        public void GetSearchResultsProcessor_ValidDivisionNeedle_ReturnsSearchResultsForDivision()
        {
            var division = CreateForTestingPurpose<Services.Models.Team.Division>();
            Assert.GreaterOrEqual(division.Name.Length, 3);
            var needle = division.Name.Substring(0, 3);

            var result = CreateProcessor(SearchType.Division, needle).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(SearchType.Division, result.SearchType);
            AssertDivisionsOk(needle, division, result.Divisions);
        }

        [Test, TestCase(false, true), TestCase(true, false), TestCase(true, true)]
        public void GetSearchResultsProcessor_ValidRiderNeedle_ReturnsSearchResultsForRider(bool useFirstNameNeedle, bool useLastNameNeedle)
        {
            var idTeam = CreateIdForTestingPurpose<Events.Team>();
            var rider = CreateForTestingPurpose<Player>(new { teamId = idTeam });
            Assert.GreaterOrEqual(rider.FirstName.Length, 3);
            Assert.GreaterOrEqual(rider.LastName.Length, 3);
            var firstNameNeedle = useFirstNameNeedle ? rider.FirstName.Substring(0, 3) : null;
            var lastNameNeedle = useLastNameNeedle ? rider.LastName.Substring(0, 3) : null;

            var result = CreateProcessor(SearchType.Rider, firstNameNeedle, lastNameNeedle).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(SearchType.Rider, result.SearchType);
            AssertRidersOk(firstNameNeedle, lastNameNeedle, rider, result.Riders);
        }

        [Test, TestCase(false, false), TestCase(true, false), TestCase(false, true)]
        public void GetSearchResultsProcessor_ValidTeamNeedle_ReturnsSearchResultsForTeam(bool useNameNeedle, bool useUserNameNeedle)
        {
            var team = CreateForTestingPurpose<Services.Models.Team.Team>();
            var user = CreateForTestingPurpose<Services.Models.Team.User>(new { teamId = team.Id });
            Assert.GreaterOrEqual(team.Name.Length, 3);
            Assert.GreaterOrEqual(user.Username.Length, 3);
            var firstNeedle = useNameNeedle ? team.Name.Substring(0, 3) : !useUserNameNeedle ? team.Id.ToString() : null;
            var secondNeedle = useUserNameNeedle ? user.Username.Substring(0, 3) : null;

            var result = CreateProcessor(SearchType.Team, firstNeedle, secondNeedle).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(SearchType.Team, result.SearchType);
            AssertTeamsOk(firstNeedle, secondNeedle, user, result.Teams);
        }

        #endregion


        #region Private Methods

        private static void AssertDivisionsOk(string needle, Services.Models.Team.Division expectedDivision, List<Division> divisions)
        {
            Assert.NotNull(divisions);
            Assert.IsNotEmpty(divisions);
            Assert.IsNotNullOrEmpty(needle);
            Assert.True(divisions.All(d => d.Name.ToLower().StartsWith(needle.ToLower())));
            var division = divisions.FirstOrDefault(d => d.Id == expectedDivision.Id);
            Assert.NotNull(division);
            Assert.AreEqual(expectedDivision.Name, division.Name);
            AssertCountryOk(expectedDivision.DivisionCountry, division.Country);
        }

        private static void AssertCountryOk(Services.Models.Location.Country expectedCountry, Contracts.DataContracts.Location.Country country)
        {
            Assert.NotNull(country);
            Assert.AreEqual(expectedCountry.Id, country.Id);
            Assert.AreEqual(expectedCountry.Item, country.Name);
        }

        private static void AssertRidersOk(string firstNameNeedle, string lastNameNeedle, Player expectedRider, List<RiderBase> riders)
        {
            Assert.NotNull(riders);
            Assert.IsNotEmpty(riders);
            Assert.IsNotNullOrEmpty($"{firstNameNeedle}{lastNameNeedle}");
            Assert.True(riders.All(r => r.Name.IndexOf(firstNameNeedle ?? "", StringComparison.InvariantCultureIgnoreCase) >= 0 
                                        || r.Name.IndexOf(lastNameNeedle ?? "", StringComparison.InvariantCultureIgnoreCase) >= 0));
            var rider = riders.FirstOrDefault(r => r.Id == expectedRider.Id);
            Assert.NotNull(rider);
            Assert.AreEqual($"{expectedRider.FirstName} {expectedRider.LastName}".Trim(), rider.Name);
            Assert.AreEqual(expectedRider.Nationality, rider.Nationality);
        }

        private void AssertTeamsOk(string firstNeedle, string secondNeedle, Services.Models.Team.User expectedUser, List<TeamBase> teams)
        {
            Assert.NotNull(teams);
            Assert.IsNotEmpty(teams);
            Assert.IsNotNullOrEmpty($"{firstNeedle}{secondNeedle}");
            var team = teams.FirstOrDefault(t => t.Id == expectedUser.Team);
            Assert.NotNull(team);
            Assert.AreEqual(expectedUser.UserTeam.ShortName, team.ShortName);
            Assert.AreEqual(expectedUser.UserTeam.Name, team.LongName);

            if (long.TryParse(firstNeedle, out var idTeam))
            {
                Assert.AreEqual(idTeam, teams.Single().Id);
            }
            else if (firstNeedle != null)
            {
                Assert.True(teams.All(t => t.LongName.ToLower().StartsWith(firstNeedle.ToLower())));
            }
            else
            {
                Assert.True(teams.All(t => UserQueries.GetByTeam(Context, t.Id).Username.ToLower().StartsWith(secondNeedle.ToLower())));
            }
        }

        #endregion
    }
}