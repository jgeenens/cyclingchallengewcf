﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Game;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Game;
using Currency = CyclingChallenge.Services.Models.Economy.Currency;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetStoreProcessorTest: ProcessorBaseTest<GetStoreProcessor, Store>
    {
        #region Tests

        [Test]
        public void GetStoreProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetStoreProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetStoreProcessor_InvalidId_ErrorCodeIdUserNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdUserNotFound, result.ErrorCode);
        }

        [Test]
        public void GetStoreProcessor_ValidId_ReturnsStore()
        {
            var user = CreateForTestingPurpose<User>();
            var product = CreateForTestingPurpose<Commodity>();
            var price = CreateForTestingPurpose<Price>(new { productId = product.Id, currencyId = user.Currency });
            if (user.Currency != Economy.Currency.Dollar)
            {
                CreateForTestingPurpose<Price>(new { productId = product.Id, currencyId = Economy.Currency.Dollar });
            }
            var result = CreateProcessor(user.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual($"{user.FirstName} {user.LastName}".Trim(), result.Name);
            Assert.AreEqual(user.Username, result.Alias);
            Assert.AreEqual(user.Tokens, result.Tokens);
            AssertProductsOk(result.Products, product, price);
        }

        #endregion


        #region Private Methods

        private static void AssertProductsOk(List<Product> products, Commodity product, Price price)
        {
            Assert.IsNotEmpty(products);
            AssertProductOk(products.FirstOrDefault(prod => prod.Id == product.Id), product, price);
        }

        private static void AssertProductOk(Product product, Commodity expectedProduct, Price price)
        {
            Assert.NotNull(product);
            Assert.AreEqual(expectedProduct.Title, product.Title);
            Assert.AreEqual(expectedProduct.Description, product.Description);
            Assert.AreEqual(expectedProduct.Value, product.Value);
            Assert.AreEqual(price.Amount, product.Price);
            AssertCurrencyOk(product.Currency, price.PriceCurrency);
        }

        private static void AssertCurrencyOk(Contracts.DataContracts.Game.Currency currency, Currency expectedCurrency)
        {
            Assert.NotNull(currency);
            Assert.AreEqual(expectedCurrency.Id, currency.Id);
            Assert.AreEqual(expectedCurrency.Description, currency.Name);
            Assert.AreEqual(expectedCurrency.Code, currency.Code);
        }

        #endregion
    }
}