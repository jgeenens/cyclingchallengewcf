﻿using System;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;

namespace CyclingChallenge.Services.Test.Processors
{
    public class ProcessorBaseTest<TProcessor, TResult>: TestFixtureBase
        where TProcessor : ProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Factory Methods

        protected TProcessor CreateProcessor(params object[] parameters)
        {
            var processor = (TProcessor)Activator.CreateInstance(typeof(TProcessor), parameters);
            return processor;
        }

        #endregion
    }
}