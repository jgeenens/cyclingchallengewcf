﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using Rider = CyclingChallenge.Services.Models.Race.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class EventTextProcessorBaseTest: ProcessorBaseTest<GetRaceReportProcessor, Contracts.DataContracts.Race.RaceReport>
    {
        #region

        private const string Item = "[[1234]]";

        #endregion


        #region CreateRider

        [Test]
        public void CreateRider_Null_ReturnsNull()
        {
            Assert.Null(EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.CreateRider((Player)null));
            Assert.Null(EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.CreateRider((PlayerValue)null));
        }

        [Test, TestCase(false), TestCase(true)]
        public void CreateRider_NotNull_ReturnsRider(bool usePlayerValue)
        {
            var rider = CreateForTestingPurpose<Player>();
            var values = CreateForTestingPurpose<PlayerValue>(new { playerId = rider.Id });
            var result = usePlayerValue
                ? EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.CreateRider(values)
                : EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.CreateRider(rider);

            Assert.NotNull(result);
            Assert.AreEqual(rider.Id, result.Id);
            Assert.AreEqual($"{rider.FirstName} {rider.LastName}".Trim(), result.Name);
            Assert.AreEqual(rider.Nationality, result.Nationality);
        }

        #endregion


        #region GetSpeed

        [Test]
        public void GetSpeed_NullOrSecondsIsZero_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[speed]{RandomUtil.RandomString()}";
            var raceEvent = CreateForTestingPurpose<RaceEvent>(new { seconds = 0 });
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetSpeed(original, Item, null, null));
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetSpeed(original, Item, raceEvent, null));
        }

        [Test]
        public void GetSpeed_NotNullAndSecondsGreaterThanZero_ReturnsSpeedInText()
        {
            var original = $"{RandomUtil.RandomString()}[speed]{RandomUtil.RandomString()}";
            var raceEvent = CreateForTestingPurpose<RaceEvent>();
            var speed = $"{((decimal)raceEvent.Km / raceEvent.Seconds * 3600):F2}";
            var expected = $"[[REP({Item}||[speed]||[b]{speed}[/b])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetSpeed(original, Item, raceEvent, null));
        }

        #endregion


        #region GetRank

        [Test]
        public void GetRank_NullOrRankOutOfRange_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[nth]{RandomUtil.RandomString()}";
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider
                    {
                        Id = RandomUtil.RandomNumber(),
                        Rank = (byte)(RandomUtil.RandomNumber() + 15)
                    }
                }
            };
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRank(original, Item, null, null));
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRank(original, Item, null, info));
        }

        [Test, TestCase(1, Items.First), TestCase(2, Items.Second), TestCase(3, Items.Third), TestCase(4, Items.Fourth), TestCase(5, Items.Fifth),
        TestCase(6, Items.Sixth), TestCase(7, Items.Seventh), TestCase(8, Items.Eighth), TestCase(9, Items.Ninth), TestCase(10, Items.Tenth),
        TestCase(11, Items.Eleventh), TestCase(12, Items.Twelfth)]
        public void GetRank_NotNullAndRankWithinRange_ReturnsRankInText(byte rank, int expectedItem)
        {
            var original = $"{RandomUtil.RandomString()}[nth]{RandomUtil.RandomString()}";
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider
                    {
                        Id = RandomUtil.RandomNumber(),
                        Rank = rank
                    }
                }
            };
            var expected = $"[[REP({Item}||[nth]||[b][[{expectedItem}]][/b])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRank(original, Item, null, info));
        }

        #endregion


        #region GetTime

        [Test]
        public void GetTime_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[time]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetSpeed(original, Item, null, null));
        }

        [Test]
        public void GetTime_NotNull_ReturnsFormattedTimeInText()
        {
            var original = $"{RandomUtil.RandomString()}[time]{RandomUtil.RandomString()}";
            var raceEvent = CreateForTestingPurpose<RaceEvent>(new { seconds = 3925 });
            var expected = $"[[REP({Item}||[time]||[b]1h05'25\"[/b])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetTime(original, Item, raceEvent, null));
        }

        #endregion


        #region GetTimeDifference

        [Test]
        public void GetTimeDifference_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[sec]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetTimeDifference(original, Item, null, null));
        }

        [Test]
        public void GetTimeDifference_NotNull_ReturnsTimeDifferenceInText()
        {
            var original = $"{RandomUtil.RandomString()}[sec]{RandomUtil.RandomString()}";
            var time1 = (short)RandomUtil.RandomNumber(4);
            var time2 = (short)(time1 + RandomUtil.RandomNumber());
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider
                    {
                        Id = RandomUtil.RandomNumber(),
                        Time = time1
                    },
                    new Rider
                    {
                        Id = RandomUtil.RandomNumber(),
                        Time = time2
                    }
                }
            };
            var expected = $"[[REP({Item}||[sec]||[b]{time2 - time1}[/b])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetTimeDifference(original, Item, null, info));
        }

        #endregion


        #region GetDistance

        [Test]
        public void GetDistance_StageIsNull_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[totkm]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, CreateProcessor().GetDistance(original, Item, null, null));
        }

        [Test]
        public void GetDistance_StageNotNull_ReturnsDistanceInText()
        {
            var stage = RaceQueries.GetStageById(Context, FixedID.Stage);

            var original = $"{RandomUtil.RandomString()}[totkm]{RandomUtil.RandomString()}";
            var processor = CreateProcessor();
            processor.GetProcessedEvents(stage, new List<RaceEvent>());
            var expected = $"[[REP({Item}||[totkm]||[b]{stage.StageTrack.Kilometers}[/b])]]";
            Assert.AreEqual(expected, processor.GetDistance(original, Item, null, null));
        }

        #endregion


        #region GetDistanceLeft

        [Test]
        public void GetDistanceLeft_StageIsNull_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[togo]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, CreateProcessor().GetDistanceLeft(original, Item, null, null));
        }

        [Test]
        public void GetDistanceLeft_StageAndRaceEventKmNotNull_ReturnsDistanceLeftInText()
        {
            var stage = RaceQueries.GetStageById(Context, FixedID.Stage);
            var raceEvent = CreateForTestingPurpose<RaceEvent>();

            var original = $"{RandomUtil.RandomString()}[togo]{RandomUtil.RandomString()}";
            var processor = CreateProcessor();
            processor.GetProcessedEvents(stage, new List<RaceEvent>());
            var distance = stage.StageTrack.Kilometers - raceEvent.Km;
            var expected = $"[[REP({Item}||[togo]||[b]{distance}[/b])]]";
            Assert.AreEqual(expected, processor.GetDistanceLeft(original, Item, raceEvent, null));
        }

        #endregion


        #region GetLeadersGap

        [Test]
        public void GetLeadersGap_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[ahead]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetLeadersGap(original, Item, null, null));
        }

        [Test]
        public void GetLeadersGap_NotNull_ReturnsLeadersGapInText()
        {
            var original = $"{RandomUtil.RandomString()}[ahead]{RandomUtil.RandomString()}";
            var info = new ReportInfo { Ahead = (short)RandomUtil.RandomNumber() };
            var expected = $"[[REP({Item}||[ahead]||[b]{info.Ahead}[/b])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetLeadersGap(original, Item, null, info));
        }

        #endregion


        #region GetGroup

        [Test]
        public void GetGroup_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[group]{RandomUtil.RandomString()}[group2]{RandomUtil.RandomString()}[group3]";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetGroup(original, Item, null, null));
        }

        [Test, TestCase(1), TestCase(2), TestCase(3)]
        public void GetGroup_NotNull_ReturnsGroupsInText(int total)
        {
            var original = $"{RandomUtil.RandomString()}[group]{RandomUtil.RandomString()}[group2]{RandomUtil.RandomString()}[group3]";
            var info = new ReportInfo { Groups = new List<Group>() };
            var expected = Item;
            for (var i = 0; i < total; i++)
            {
                var group = new Group { Id = (byte) (i + 1), IdRider = RandomUtil.RandomNumber(5) };
                info.Groups.Add(group);
                expected = $"[[REP({expected}||[group{(i == 0 ? string.Empty : (i + 1).ToString())}]||[[GROUP({group.Id}||{group.IdRider})]])]]";
            }
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetGroup(original, Item, null, info));
        }

        #endregion


        #region GetNationality

        [Test]
        public void GetNationality_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[nationality]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, CreateProcessor().GetNationality(original, Item, null, null));
        }

        [Test]
        public void GetNationality_NotNull_ReturnsNationalityInText()
        {
            var original = $"{RandomUtil.RandomString()}[nationality]{RandomUtil.RandomString()}";
            var team = CreateForTestingPurpose<Team>();
            var rider = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider
                    {
                        Id = rider.Id
                    }
                }
            };
            var expected = $"[[REP({Item}||[nationality]||[b][[{rider.PlayerNationality.Nationality}]][/b])]]";
            Assert.AreEqual(expected, CreateProcessor().GetNationality(original, Item, null, info));
        }

        #endregion


        #region GetRider

        [Test]
        public void GetRider_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[cycl]{RandomUtil.RandomString()}[cycl2]{RandomUtil.RandomString()}[cycl3]";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRider(original, Item, null, null));
        }

        [Test, TestCase(1), TestCase(2), TestCase(3)]
        public void GetRider_NotNull_ReturnsRidersInText(int total)
        {
            var original = $"{RandomUtil.RandomString()}[cycl]{RandomUtil.RandomString()}[cycl2]{RandomUtil.RandomString()}[cycl3]";
            var info = new ReportInfo { Riders = new List<Rider>() };
            var expected = Item;
            for (var i = 0; i < total; i++)
            {
                var rider = new Rider { Id = RandomUtil.RandomNumber(5) };
                info.Riders.Add(rider);
                expected = $"[[REP({expected}||[cycl{(i == 0 ? string.Empty : (i + 1).ToString())}]||[[RIDER({rider.Id})]])]]";
            }
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRider(original, Item, null, info));
        }

        #endregion


        #region GetTeam

        [Test]
        public void GetTeam_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[team]{RandomUtil.RandomString()}[team2]{RandomUtil.RandomString()}[team3]";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetTeam(original, Item, null, null));
        }

        [Test, TestCase(1), TestCase(2), TestCase(3)]
        public void GetTeam_NotNull_ReturnsTeamsInText(int total)
        {
            var original = $"{RandomUtil.RandomString()}[team]{RandomUtil.RandomString()}[team2]{RandomUtil.RandomString()}[team3]";
            var info = new ReportInfo { Teams = new List<InfoTeam>() };
            var expected = Item;
            for (var i = 0; i < total; i++)
            {
                var team = new InfoTeam { Id = RandomUtil.RandomNumber(5), Name = RandomUtil.RandomCharString() };
                info.Teams.Add(team);
                expected = $"[[REP({expected}||[team{(i == 0 ? string.Empty : (i + 1).ToString())}]||[[TEAM({team.Id}||{team.Name})]])]]";
            }
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetTeam(original, Item, null, info));
        }

        #endregion


        #region GetRiders

        [Test]
        public void GetRiders_NullOrListEmpty_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[all]{RandomUtil.RandomString()}";
            var info = new ReportInfo { Riders = new List<Rider>() };
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRiders(original, Item, null, null));
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRiders(original, Item, null, info));
        }

        [Test]
        public void GetRiders_NotEmpty_ReturnsRidersInText()
        {
            var original = $"{RandomUtil.RandomString()}[all]{RandomUtil.RandomString()}";
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) }
                }
            };
            var expected = $"[[REP({Item}||[all]||{string.Join("[br]", info.Riders.Select(r => $"[[RIDER({r.Id})]]"))})]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRiders(original, Item, null, info));
        }

        #endregion


        #region GetRidersInline

        [Test]
        public void GetRidersInline_NullOrLessThanFourRiders_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[cyclrem]{RandomUtil.RandomString()}";
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) }
                }
            };
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRidersInline(original, Item, null, null));
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRidersInline(original, Item, null, info));
        }

        [Test]
        public void GetRidersInline_FourOrMoreRiders_ReturnsRemainingRidersInText()
        {
            var original = $"{RandomUtil.RandomString()}[cyclrem]{RandomUtil.RandomString()}";
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) },
                    new Rider { Id = RandomUtil.RandomNumber(5) }
                }
            };
            var expected = $"[[REP({Item}||[cyclrem]||[[RIDER({info.Riders.Skip(3).First().Id})]] [[{Items.And}]] [[RIDER({info.Riders.Skip(4).First().Id})]])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetRidersInline(original, Item, null, info));
        }

        #endregion


        #region GetNumberOfRiders

        [Test]
        public void GetNumberOfRiders_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[num]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetNumberOfRiders(original, Item, null, null));
        }

        [Test]
        public void GetNumberOfRiders_NotNull_ReturnsNumberOfRidersInText()
        {
            var original = $"{RandomUtil.RandomString()}[num]{RandomUtil.RandomString()}";
            var info = new ReportInfo
            {
                Riders = new List<Rider>
                {
                    new Rider
                    {
                        Id = RandomUtil.RandomNumber()
                    }
                }
            };
            var expected = $"[[REP({Item}||[num]||[b]1[/b])]]";
            Assert.AreEqual(expected, EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>.GetNumberOfRiders(original, Item, null, info));
        }

        #endregion


        #region GetCity

        [Test]
        public void GetCity_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[city]{RandomUtil.RandomString()}";
            Assert.AreEqual(Item, CreateProcessor().GetCity(original, Item, null, null));
        }

        [Test]
        public void GetCity_NotNull_ReturnsCityInText()
        {
            var original = $"{RandomUtil.RandomString()}[city]{RandomUtil.RandomString()}";
            var raceEvent = CreateForTestingPurpose<RaceEvent>(new { kilometer = 0 });
            var stage = RaceQueries.GetStageById(Context, FixedID.Stage);

            var processor = CreateProcessor();
            processor.GetProcessedEvents(stage, new List<RaceEvent>());

            var cities = stage.StageTrack.Description.Split('|').ToDictionary(c => int.Parse(c.Split('-')[0]), c => long.Parse(c.Split('-')[1]));
            var city = LocationQueries.GetCityText(LocationQueries.GetCityById(Context, cities[raceEvent.Km]));
            var expected = $"[[REP({Item}||[city]||[b]{city}[/b])]]";
            Assert.AreEqual(expected, processor.GetCity(original, Item, raceEvent, null));
        }

        #endregion


        #region GetClimb

        [Test]
        public void GetClimb_Null_ReturnsOriginalText()
        {
            var original = $"{RandomUtil.RandomString()}[climb]{RandomUtil.RandomString()}";
            var info = new ReportInfo();
            Assert.AreEqual(Item, CreateProcessor().GetClimb(original, Item, null, null));
            Assert.AreEqual(Item, CreateProcessor().GetClimb(original, Item, null, info));
        }

        [Test]
        public void GetClimb_NotNull_ReturnsClimbInText()
        {
            var climb = CreateForTestingPurpose<Climb>();
            var original = $"{RandomUtil.RandomString()}[climb]{RandomUtil.RandomString()}";
            var info = new ReportInfo { Climb = climb.Id };
            var expected = $"[[REP({Item}||[climb]||[b]{climb.Name}[/b])]]";
            Assert.AreEqual(expected, CreateProcessor().GetClimb(original, Item, null, info));
        }

        #endregion
    }
}