﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetCalendarProcessorTest: ProcessorBaseTest<GetCalendarProcessor, Contracts.DataContracts.Race.Calendar>
    {
        #region Tests

        [Test]
        public void GetCalendarProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetCalendarProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsSeason)
        {
            var result = invalidParameterIsSeason
                ? CreateProcessor(FixedID.Division, string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetCalendarProcessor_InvalidId_ErrorCodeDivisionNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdDivisionNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetCalendarProcessor_ValidId_ReturnsCalendarForDivisionAndSeason(bool isTour)
        {
            var season = (byte)((DateTime.Now - GameQueries.GetStartSeason(Context)).Days / (12 * 7) + 1);
            var division = CreateForTestingPurpose<Division>();
            var raceDescription = CreateForTestingPurpose<RaceDescription>(new { isTour });
            var stageId = CreateIdForTestingPurpose<Stage>(new
            {
                raceDescriptionId = raceDescription.Id,
                stageId = 1,
                day = 1
            });
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = raceDescription.Id,
                startDate = DateTime.Today,
                divisionId = division.Id,
                stageId
            });
            var teamId = CreateIdForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId });
            CreateForTestingPurpose<RaceRanking>(new
            {
                raceId = race.Id,
                stageId,
                ranking = $"{player.Id},{RandomUtil.RandomNumber(4)}",
                pointsRanking = $"{player.Id},{RandomUtil.RandomNumber()}",
                mountainsRanking = $"{player.Id},{RandomUtil.RandomNumber()}",
                teams = teamId.ToString()
            });
            var result = CreateProcessor(division.Id, season).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(division.Id, result.Id);
            Assert.AreEqual(division.Name, result.Name);
            Assert.AreEqual(GameQueries.GetStartSeason(Context, season), result.StartSeason);
            AssertCountryOk(division.DivisionCountry, result.Country);
            AssertRacesOk(race, raceDescription, result.Races, player);
        }

        #endregion


        #region Private Methods

        private static void AssertRacesOk(Race race, RaceDescription raceDescription, List<Contracts.DataContracts.Race.Race> races, Player rider)
        {
            Assert.NotNull(races);
            Assert.IsNotEmpty(races);
            AssertRaceOk(race, raceDescription, races.FirstOrDefault(r => r.Id == race.Id), rider);
        }

        private static void AssertRaceOk(Race expectedRace, RaceDescription expectedRaceDescription, Contracts.DataContracts.Race.Race race, Player rider)
        {
            Assert.NotNull(race);
            Assert.AreEqual(expectedRaceDescription.Name, race.Name);
            Assert.AreEqual(expectedRace.StartDate, race.Start);
            Assert.AreEqual(expectedRace.EndDate, race.End);
            Assert.AreEqual(expectedRaceDescription.IsTour, race.IsTour);
            Assert.AreEqual(expectedRace.Status, race.Status);
            Assert.AreEqual(expectedRace.CurrentStage, race.Stage);
            Assert.NotNull(race.Yellow);
            Assert.AreEqual(rider.Id, race.Yellow.Id);
            Assert.AreEqual($"{rider.FirstName} {rider.LastName}".Trim(), race.Yellow.Name);
        }

        private static void AssertCountryOk(Country expectedCountry, Contracts.DataContracts.Location.Country country)
        {
            Assert.NotNull(country);
            Assert.AreEqual(expectedCountry.Id, country.Id);
            Assert.AreEqual(expectedCountry.Item, country.Name);
        }

        #endregion
    }
}