﻿using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRaceHistoryProcessorTest: ProcessorBaseTest<GetRaceHistoryProcessor, Contracts.DataContracts.Race.RaceHistory>
    {
        #region Tests

        [Test]
        public void GetRaceHistoryProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRaceHistoryProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(0, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRaceHistoryProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRaceHistoryProcessor_ValidIdNoHistory_ReturnsNoEvents()
        {
            var race = CreateForTestingPurpose<Race>();
            var result = CreateProcessor(race.Id, true).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            Assert.NotNull(result.Events);
            Assert.IsEmpty(result.Events);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceHistoryProcessor_ValidIdHasHistory_ReturnsEvents(bool includeStages)
        {
            var race = CreateForTestingPurpose<Race>();
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            CreateForTestingPurpose<RaceHistory>(new
            {
                playerId = player.Id, teamId = team.Id, raceId = race.Id, position = 1
            });
            CreateForTestingPurpose<RaceHistory>(new
            {
                playerId = player.Id, teamId = team.Id, raceId = race.Id, stageId = FixedID.Stage, position = 1
            });
            var result = CreateProcessor(race.Id, includeStages).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            Assert.NotNull(result.Events);
            Assert.IsNotEmpty(result.Events);
            var raceHistories = RaceQueries.GetRaceHistoryForRace(Context, race.Id, includeStages);
            foreach (var raceHistory in raceHistories)
            {
                AssertEventIsOk(raceHistory, result.Events.FirstOrDefault(e => e.Id == raceHistory.Id));
            }   
        }

        #endregion


        #region Private Methods

        private static void AssertEventIsOk(RaceHistory raceHistory, Contracts.DataContracts.Race.Event raceEvent)
        {
            Assert.NotNull(raceEvent);
            Assert.AreEqual(raceHistory.When, raceEvent.When);
            Assert.AreEqual(raceHistory.Team, raceEvent.Team.Id);
            Assert.AreEqual(raceHistory.Player, raceEvent.Rider.Id);
            Assert.AreEqual(raceHistory.Stage, raceEvent.Stage?.Id);
            Assert.AreEqual(raceHistory.Season, raceEvent.Season);
            Assert.AreEqual(Events.Winner.Yellow, raceEvent.Type);
        }

        #endregion
    }
}