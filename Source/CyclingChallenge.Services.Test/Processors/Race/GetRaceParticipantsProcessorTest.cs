﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using System;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    internal class Results
    {
        public long Time;
        public int Points;
        public int MountainPoints;
    }

    [TestFixture]
    public class GetRaceParticipantsProcessorTest: ProcessorBaseTest<GetRaceParticipantsProcessor, Contracts.DataContracts.Race.RaceRanking>
    {
        #region Tests

        [Test]
        public void GetRaceParticipantsProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRaceParticipantsProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRaceParticipantsProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRaceParticipantsProcessor_ValidIdSingleStageRaceAlreadyStarted_ReturnsErrorCodeRaceAlreadyStarted()
        {
            var raceDescription = CreateForTestingPurpose<RaceDescription>();
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = raceDescription.Id,
                startDate = DateTime.Today.AddDays(-2)
            });
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.RaceAlreadyStarted, result.ErrorCode);
        }

        [Test]
        public void GetRaceParticipantsProcessor_ValidIdTourRaceAlreadyEnded_ReturnsErrorCodeRaceAlreadyEnded()
        {
            var raceDescription = CreateForTestingPurpose<RaceDescription>(new { isTour = true });
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = raceDescription.Id,
                startDate = DateTime.Today.AddDays(-4),
                endDate = DateTime.Today.AddDays(-1)
            });
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.RaceAlreadyEnded, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceParticipantsProcessor_ValidId_ReturnsParticipants(bool isTour)
        {
            var raceDescription = CreateForTestingPurpose<RaceDescription>(new { isTour });
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = raceDescription.Id,
                startDate = DateTime.Now.AddDays(isTour ? -2 : 1),
                endDate = DateTime.Now.AddDays(isTour ? 2 : 1),
                liveTime = 3600
            });
            var team = CreateForTestingPurpose<Team>();
            var players = new Dictionary<Player, Results>()
            {
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() }
            };
            if (isTour)
            {
                for (var i = 1; i <= 3; i++)
                {
                    var stageId = CreateIdForTestingPurpose<Stage>(new
                    {
                        raceDescriptionId = raceDescription.Id,
                        stageId = i,
                        day = i
                    });
                    var stageTimes = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber(4));
                    var stagePoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber(2));
                    var stageMountainPoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber(2));
                    CreateForTestingPurpose<RaceRanking>(new
                    {
                        raceId = race.Id,
                        stageId,
                        ranking = GetRanking(stageTimes),
                        pointsRanking = GetRanking(stagePoints),
                        mountainsRanking = GetRanking(stageMountainPoints),
                        teams = team.Id.ToString()
                    });
                    foreach (var player in players)
                    {
                        player.Value.Time += stageTimes[player.Key.Id];
                        player.Value.Points += (int)stagePoints[player.Key.Id];
                        player.Value.MountainPoints += (int)stageMountainPoints[player.Key.Id];
                    }
                }
            }
            else
            {
                CreateForTestingPurpose<RaceOrder>(new
                {
                    raceId = race.Id,
                    teamId = team.Id,
                    playerId = players.First().Key.Id
                });
            }

            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(raceDescription.Name, result.Name);
            AssertParticipantsOk(players.First(), result.Riders, isTour);
            if (isTour)
            {
                AssertParticipantsOk(players.Skip(1).First(), result.Riders);
                AssertParticipantsOk(players.Last(), result.Riders);
                Assert.AreEqual(players.OrderBy(t => t.Value.Time).First().Key.Id, result.Yellow?.Id);
            } else
            {
                Assert.IsNull(result.Yellow);
            }
            Assert.Null(result.Green);
            Assert.Null(result.Polkadot);
        }

        #endregion


        #region Private Methods

        private string GetRanking(Dictionary<long, long> stageTimes)
        {
            var ranking = string.Empty;
            foreach (var stageTime in stageTimes.OrderBy(st => st.Value).ToList())
            {
                ranking += (ranking == string.Empty ? string.Empty : "|") + $"{stageTime.Key},{stageTime.Value}";
            }
            return ranking;
        }

        private void AssertParticipantsOk(KeyValuePair<Player, Results> player, List<RiderBase> participants, bool isTour = true)
        {
            Assert.IsNotEmpty(participants);
            AssertParticipantOk(player.Key, player.Value, participants.FirstOrDefault(p => p.Id == player.Key.Id), isTour);
        }

        private void AssertParticipantOk(Player player, Results results, RiderBase participant, bool isTour)
        {
            Assert.NotNull(participant);
            Assert.AreEqual(player.Id, participant.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}".Trim(), participant.Name);
            Assert.AreEqual(player.Age, participant.Age);
            Assert.AreEqual(player.Nationality, participant.Nationality);
            if (isTour)
            {
                Assert.AreEqual(results.Time, participant.Time);
                Assert.AreEqual(results.Points, participant.Points);
                Assert.AreEqual(results.MountainPoints, participant.MountainPoints);
            } else
            {
                Assert.Null(participant.Time);
                Assert.Null(participant.Points);
                Assert.Null(participant.MountainPoints);
            }
            AssertTeamOk(player.PlayerTeam, participant.Team);
        }

        private static void AssertTeamOk(Team expectedTeam, TeamBase team)
        {
            Assert.NotNull(team);
            Assert.AreEqual(expectedTeam.Id, team.Id);
            Assert.AreEqual(expectedTeam.ShortName, team.ShortName);
            Assert.AreEqual(expectedTeam.Name, team.LongName);
        }

        #endregion
    }
}