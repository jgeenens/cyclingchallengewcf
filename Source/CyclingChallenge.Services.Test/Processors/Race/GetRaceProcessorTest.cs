﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Contracts.DataContracts.Location;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRaceProcessorTest: ProcessorBaseTest<GetRaceProcessor, Contracts.DataContracts.Race.RaceDescription>
    {
        #region Tests

        [Test]
        public void GetRaceProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRaceProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRaceProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceProcessor_ValidId_ReturnsRaceDescription(bool isTour)
        {
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = isTour ? FixedID.TourRaceDescription : FixedID.RaceDescription
            });
            var team = CreateForTestingPurpose<Team>();
            var players = new Dictionary<Player, Results>()
            {
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() }
            };
            var stages = RaceQueries.GetStagesByRaceDescription(Context, race.RaceDescription).ToList();
            if (isTour)
            {
                foreach (var stage in stages)
                {
                    var stageTimes = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber(4));
                    var stagePoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber());
                    var stageMountainPoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber());
                    CreateForTestingPurpose<RaceRanking>(new
                    {
                        raceId = race.Id,
                        stageId = stage.Id,
                        ranking = GetRanking(stageTimes),
                        pointsRanking = GetRanking(stagePoints),
                        mountainsRanking = GetRanking(stageMountainPoints),
                        teams = team.Id.ToString()
                    });
                    foreach (var player in players)
                    {
                        player.Value.Time += stageTimes[player.Key.Id];
                        player.Value.Points += (int)stagePoints[player.Key.Id];
                        player.Value.MountainPoints += (int)stageMountainPoints[player.Key.Id];
                    }
                }
            }
            else
            {
                CreateForTestingPurpose<RaceOrder>(new
                {
                    raceId = race.Id,
                    teamId = team.Id,
                    playerId = players.First().Key.Id
                });
            }

            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            Assert.AreEqual(race.RaceRaceDescription.Description, result.Description);
            Assert.AreEqual(race.RaceRaceDescription.IsTour, result.IsTour);
            Assert.AreEqual(race.Status, result.Status);
            Assert.Null(result.Green);
            Assert.Null(result.Polkadot);
            if (isTour)
            {
                Assert.AreEqual(players.OrderBy(t => t.Value.Time).First().Key.Id, result.Yellow?.Id);
            }
            else
            {
                Assert.IsNull(result.Yellow);
            }
            AssertStagesOk(stages, result.Stages);
        }

        #endregion


        #region Private Methods

        private static string GetRanking(Dictionary<long, long> stageTimes)
        {
            return stageTimes.OrderBy(st => st.Value).Select(kvp => $"{kvp.Key},{kvp.Value}").Aggregate(string.Empty, (current, next) => $"{current}|{next}").Trim('|');
        }

        private void AssertStagesOk(List<Stage> expectedStages, List<Contracts.DataContracts.Race.StageDescription> stages)
        {
            Assert.NotNull(stages);
            Assert.IsNotEmpty(stages);
            foreach (var expectedStage in expectedStages)
            {
                AssertStageOk(expectedStage, stages.FirstOrDefault(s => s.Id == expectedStage.Id));
            }
        }

        private void AssertStageOk(Stage expectedStage, Contracts.DataContracts.Race.StageDescription stage)
        {
            Assert.NotNull(stage);
            Assert.AreEqual(expectedStage.StageTrack.Name, stage.Name);
            Assert.AreEqual(expectedStage.Day, stage.Day);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyCobblestones, stage.CobblestonesPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyHills, stage.HillsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyMountains, stage.MountainsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyPlains, stage.PlainsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.Profile, stage.Profile);
            Assert.AreEqual(expectedStage.StageTrack.CobbleHillMountain, stage.Terrain);
            var trackDescription = new TrackDescription(expectedStage.StageTrack.Terrain, expectedStage.StageTrack.Description);
            Assert.True(trackDescription.Success);
            AssertWeatherOk(trackDescription.GetStart().Value, stage.Weather);
            AssertClimbsOk(trackDescription.GetHills(), stage.Hills);
            AssertClimbsOk(trackDescription.GetMountains(), stage.Mountains);
            AssertPavedSectorsOk(trackDescription.GetPavedSectors(), stage.PavedSectors);
        }

        private void AssertWeatherOk(long idCity, int weather)
        {
            var city = LocationQueries.GetCityById(Context, idCity);
            Assert.NotNull(city);
            Assert.AreEqual(city.CityRegion.Weather, weather);
        }

        private void AssertClimbsOk(Dictionary<int, Services.Models.Race.Terrain> expectedClimbs, List<Contracts.DataContracts.Location.Climb> climbs)
        {
            if (!expectedClimbs.Any())
            {
                return;
            }

            Assert.NotNull(climbs);
            Assert.IsNotEmpty(climbs);
            foreach (var expectedClimb in expectedClimbs)
            {
                AssertClimbOk(expectedClimb, climbs.FirstOrDefault(c => c.Id == expectedClimb.Value.Climb && c.Kilometer == expectedClimb.Key));
            }
        }

        private void AssertClimbOk(KeyValuePair<int, Services.Models.Race.Terrain> expectedClimb, Contracts.DataContracts.Location.Climb climb)
        {
            Assert.NotNull(climb);
            var dbClimb = LocationQueries.GetClimbById(Context, expectedClimb.Value.Climb ?? 0);
            Assert.NotNull(dbClimb);
            Assert.AreEqual(dbClimb.Name, climb.Name);
        }

        private void AssertPavedSectorsOk(Dictionary<int, Services.Models.Race.Terrain> expectedPavedSectors, List<PavedSector> pavedSectors)
        {
            if (!expectedPavedSectors.Any())
            {
                return;
            }

            Assert.NotNull(pavedSectors);
            Assert.IsNotEmpty(pavedSectors);
            foreach (var expectedPavedSector in expectedPavedSectors)
            {
                AssertPavedSectorOk(expectedPavedSector, pavedSectors.FirstOrDefault(p => p.Id == expectedPavedSector.Value.Climb && p.Kilometer == expectedPavedSector.Key));
            }
        }

        private void AssertPavedSectorOk(KeyValuePair<int, Services.Models.Race.Terrain> expectedPavedSector, PavedSector pavedSector)
        {
            Assert.NotNull(pavedSector);
            var dbPavedSector = LocationQueries.GetClimbById(Context, expectedPavedSector.Value.Climb ?? 0);
            Assert.NotNull(dbPavedSector);
            Assert.AreEqual(dbPavedSector.Name, pavedSector.Name);
        }

        #endregion
    }
}