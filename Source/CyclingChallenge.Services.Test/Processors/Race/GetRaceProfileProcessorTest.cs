﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Contracts.DataContracts.Location;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRaceProfileProcessorTest: ProcessorBaseTest<GetRaceProfileProcessor, Contracts.DataContracts.Race.RaceDescription>
    {
        #region Tests

        [Test]
        public void GetRaceProfileProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRaceProfileProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRaceProfileProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceProfileProcessor_ValidId_ReturnsRaceDescription(bool isTour)
        {
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = isTour ? FixedID.TourRaceDescription : FixedID.RaceDescription
            });
            var expectedStages = RaceQueries.GetStagesByRaceDescription(Context, race.RaceDescription).ToList();
            Assert.NotNull(expectedStages);
            Assert.IsNotEmpty(expectedStages);

            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, $"{result.ErrorCode}{Environment.NewLine}RaceDescription: {race.RaceDescription}{Environment.NewLine}Stage: {expectedStages.First().Track}");
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            Assert.AreEqual(race.RaceRaceDescription.IsTour, result.IsTour);
            AssertStageDescriptionsOk(expectedStages, result.Stages);
        }

        #endregion


        #region Private Methods

        private void AssertStageDescriptionsOk(List<Stage> expectedStages, List<Contracts.DataContracts.Race.StageDescription> stages)
        {
            Assert.IsNotEmpty(stages);
            foreach (var expectedStage in expectedStages)
            {
                AssertStageDescriptionOk(expectedStage, stages.FirstOrDefault(s => s.Id == expectedStage.Id));
            }
        }

        private void AssertStageDescriptionOk(Stage expectedStage, Contracts.DataContracts.Race.StageDescription stage)
        {
            Assert.NotNull(stage);
            Assert.AreEqual(expectedStage.Id, stage.Id);
            Assert.AreEqual(expectedStage.StageTrack.Name, stage.Name);
            Assert.AreEqual(expectedStage.Day, stage.Day);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyCobblestones, stage.CobblestonesPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyHills, stage.HillsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyMountains, stage.MountainsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyPlains, stage.PlainsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.Profile, stage.Profile);
            Assert.AreEqual(expectedStage.StageTrack.CobbleHillMountain, stage.Terrain);
            Assert.AreEqual(expectedStage.StageTrack.Kilometers, stage.Distance);

            var trackDescription = new TrackDescription(expectedStage.StageTrack.Terrain, expectedStage.StageTrack.Description, expectedStage.StageTrack.Altitude);
            Assert.True(trackDescription.Success);
            AssertCityOk(LocationQueries.GetCityById(Context, trackDescription.GetStart().Value), stage.Start);
            AssertCityOk(LocationQueries.GetCityById(Context, trackDescription.GetFinish().Value), stage.Finish);
            AssertWeatherOk(LocationQueries.GetCityById(Context, trackDescription.GetStart().Value), stage.Weather);
            AssertClimbsOk(trackDescription.GetHills(), stage.Hills);
            AssertClimbsOk(trackDescription.GetMountains(), stage.Mountains);
            AssertPavedSectorsOk(trackDescription.GetPavedSectors(), stage.PavedSectors);
            AssertAltitudesOk(trackDescription.Altitudes, stage.Altitudes);
        }

        private static void AssertCityOk(Services.Models.Location.City expectedCity, City city)
        {
            Assert.NotNull(expectedCity);
            Assert.NotNull(city);
            Assert.AreEqual(expectedCity.Id, city.Id);
            Assert.AreEqual(LocationQueries.GetCityText(expectedCity), city.Name);
            AssertRegionOk(expectedCity.CityRegion, city.Region);
        }

        private static void AssertRegionOk(Services.Models.Location.Region expectedRegion, Region region)
        {
            Assert.NotNull(region);
            Assert.AreEqual(expectedRegion.Id, region.Id);
            Assert.AreEqual(expectedRegion.Name, region.Name);
            AssertCountryOk(expectedRegion.RegionCountry, region.Country);
        }

        private static void AssertCountryOk(Services.Models.Location.Country expectedCountry, Country country)
        {
            Assert.NotNull(country);
            Assert.AreEqual(expectedCountry.Id, country.Id);
            Assert.AreEqual(expectedCountry.Item, country.Name);
        }

        private static void AssertWeatherOk(Services.Models.Location.City expectedCity, int weather)
        {
            Assert.NotNull(expectedCity);
            Assert.AreEqual(expectedCity.CityRegion.Weather, weather);
        }

        private void AssertClimbsOk(Dictionary<int, Services.Models.Race.Terrain> expectedClimbs, List<Contracts.DataContracts.Location.Climb> climbs)
        {
            if (!expectedClimbs.Any())
            {
                return;
            }

            Assert.NotNull(climbs);
            Assert.IsNotEmpty(climbs);
            foreach (var expectedClimb in expectedClimbs)
            {
                AssertClimbOk(expectedClimb, climbs.FirstOrDefault(c => c.Id == expectedClimb.Value.Climb && c.Kilometer == expectedClimb.Key));
            }
        }

        private void AssertClimbOk(KeyValuePair<int, Services.Models.Race.Terrain> expectedClimb, Contracts.DataContracts.Location.Climb climb)
        {
            Assert.NotNull(climb);
            var dbClimb = LocationQueries.GetClimbById(Context, expectedClimb.Value.Climb ?? 0);
            Assert.NotNull(dbClimb);
            Assert.AreEqual(dbClimb.Name, climb.Name);
        }

        private void AssertPavedSectorsOk(Dictionary<int, Services.Models.Race.Terrain> expectedPavedSectors, List<PavedSector> pavedSectors)
        {
            if (!expectedPavedSectors.Any())
            {
                return;
            }

            Assert.NotNull(pavedSectors);
            Assert.IsNotEmpty(pavedSectors);
            foreach (var expectedPavedSector in expectedPavedSectors)
            {
                AssertPavedSectorOk(expectedPavedSector, pavedSectors.FirstOrDefault(p => p.Id == expectedPavedSector.Value.Climb && p.Kilometer == expectedPavedSector.Key));
            }
        }

        private void AssertPavedSectorOk(KeyValuePair<int, Services.Models.Race.Terrain> expectedPavedSector, PavedSector pavedSector)
        {
            Assert.NotNull(pavedSector);
            var dbPavedSector = LocationQueries.GetClimbById(Context, expectedPavedSector.Value.Climb ?? 0);
            Assert.NotNull(dbPavedSector);
            Assert.AreEqual(dbPavedSector.Name, pavedSector.Name);
        }

        private static void AssertAltitudesOk(Dictionary<int, long> expectedAltitudes, List<Contracts.DataContracts.Race.Altitude> altitudes)
        {
            if (!(expectedAltitudes?.Any() ?? false))
            {
                return;
            }

            Assert.NotNull(altitudes);
            Assert.IsNotEmpty(altitudes);
            foreach (var expectedAltitude in expectedAltitudes)
            {
                Assert.AreEqual(expectedAltitude.Value, altitudes.FirstOrDefault(a => a.Km == expectedAltitude.Key)?.Height);
            }
        }

        #endregion
    }
}