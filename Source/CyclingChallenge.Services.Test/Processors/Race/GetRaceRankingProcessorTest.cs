﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using System;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRaceRankingProcessorTest: ProcessorBaseTest<GetRaceRankingProcessor, Contracts.DataContracts.Race.RaceRanking>
    {
        #region Tests

        [Test]
        public void GetRaceRankingProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRaceRankingProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRaceRankingProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRaceRankingProcessor_RaceOpenOrClosed_ErrorCodeRaceNotStarted()
        {
            var race = CreateForTestingPurpose<Race>();
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.RaceNotStarted, result.ErrorCode);
        }

        [Test]
        public void GetRaceRankingProcessor_RaceRunningNotATour_ErrorCodeRaceNotFinished()
        {
            var race = CreateForTestingPurpose<Race>(new { status = RaceStatus.Running });
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.RaceNotFinished, result.ErrorCode);
        }

        [Test]
        public void GetRaceRankingProcessor_RaceRunningFirstStage_ErrorCodeFirstStageNotFinished()
        {
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = FixedID.TourRaceDescription,
                status = RaceStatus.Running
            });
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.FirstStageNotFinished, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceRankingProcessor_ValidId_ReturnsRanking(bool isTour)
        {
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = isTour ? FixedID.TourRaceDescription : FixedID.RaceDescription,
                status = isTour ? RaceStatus.BetweenStages : RaceStatus.Finished,
                startDate = DateTime.Now.AddDays(isTour ? -2 : 1),
                endDate = DateTime.Now.AddDays(isTour ? 2 : 1),
                liveTime = 3600
            });
            var team = CreateForTestingPurpose<Team>();
            var players = new Dictionary<Player, Results>
            {
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() }
            };
            var stages = RaceQueries.GetStagesByRaceDescription(Context, race.RaceDescription).ToList();
            foreach (var stage in stages)
            {
                var stageTimes = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber(4));
                var stagePoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber());
                var stageMountainPoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber());
                CreateForTestingPurpose<RaceRanking>(new
                {
                    raceId = race.Id,
                    stageId = stage.Id,
                    ranking = GetRanking(stageTimes),
                    pointsRanking = GetRanking(stagePoints),
                    mountainsRanking = GetRanking(stageMountainPoints),
                    teams = team.Id.ToString()
                });
                foreach (var player in players)
                {
                    player.Value.Time += stageTimes[player.Key.Id];
                    player.Value.Points += (int)stagePoints[player.Key.Id];
                    player.Value.MountainPoints += (int)stageMountainPoints[player.Key.Id];
                }
            }

            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            AssertRankingOk(players, result.Riders);
            Assert.Null(result.Green);
            Assert.Null(result.Polkadot);
            if (isTour)
            {
                Assert.AreEqual(players.OrderBy(t => t.Value.Time).First().Key.Id, result.Yellow?.Id);
            }
            else
            {
                Assert.IsNull(result.Yellow);
            }
        }

        #endregion


        #region Private Methods

        private static string GetRanking(Dictionary<long, long> stageTimes)
        {
            return stageTimes.OrderBy(st => st.Value).Select(kvp => $"{kvp.Key},{kvp.Value}").Aggregate(string.Empty, (current,next) => $"{current}|{next}").Trim('|');
        }

        private static void AssertRankingOk(Dictionary<Player, Results> players, List<RiderBase> ranking)
        {
            Assert.IsNotEmpty(ranking);
            byte rank = 1;
            foreach (var player in players.OrderBy(p => p.Value.Time).ToList())
            {
                AssertRiderOk(player.Key, player.Value, ranking.FirstOrDefault(p => p.Id == player.Key.Id), rank++);
            }
        }

        private static void AssertRiderOk(Player player, Results results, RiderBase rider, byte rank)
        {
            Assert.NotNull(rider);
            Assert.AreEqual(player.Id, rider.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}".Trim(), rider.Name);
            Assert.AreEqual(player.Age, rider.Age);
            Assert.AreEqual(player.Nationality, rider.Nationality);
            Assert.AreEqual(rank, rider.Rank);
            Assert.AreEqual(results.Time, rider.Time);
            Assert.AreEqual(results.Points, rider.Points);
            Assert.AreEqual(results.MountainPoints, rider.MountainPoints);
            AssertTeamOk(player.PlayerTeam, rider.Team);
        }

        private static void AssertTeamOk(Team expectedTeam, TeamBase team)
        {
            Assert.NotNull(team);
            Assert.AreEqual(expectedTeam.Id, team.Id);
            Assert.AreEqual(expectedTeam.ShortName, team.ShortName);
            Assert.AreEqual(expectedTeam.Name, team.LongName);
        }

        #endregion
    }
}