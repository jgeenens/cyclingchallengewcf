﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using System.Web.Script.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using Rider = CyclingChallenge.Services.Models.Race.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRaceReportProcessorTest: ProcessorBaseTest<GetRaceReportProcessor, Contracts.DataContracts.Race.RaceReport>
    {
        #region Tests

        [Test]
        public void GetRaceReportProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRaceReportProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRaceReportProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRaceReportProcessor_RaceOpenOrClosed_ErrorCodeRaceNotStarted()
        {
            var race = CreateForTestingPurpose<Race>();
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.RaceNotStarted, result.ErrorCode);
        }

        [Test, TestCase(RaceStatus.Finished), TestCase(RaceStatus.Running)]
        public void GetRaceReportProcessor_ValidId_ReturnsEvents(byte status)
        {
            var stage = RaceQueries.GetStagesByRaceDescription(Context, FixedID.RaceDescription).FirstOrDefault();
            Assert.NotNull(stage);
            var race = CreateForTestingPurpose<Race>(new { status, stageId = stage.Id });
            var riders = new List<Rider>();
            for (var i = 0; i < 2; i++)
            {
                var team = CreateForTestingPurpose<Team>();
                for (var j = 0; j < 2; j++)
                {
                    var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
                    if (status == RaceStatus.Running)
                    {
                        CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id, raceId = race.Id });
                    }
                    riders.Add(new Rider { Id = player.Id });
                }
            }
            if (status == RaceStatus.Finished)
            {
                var ranking = $"{string.Join("|", riders.Skip(1).Select(r => $"12345,{r.Id}"))}|12399,{riders.First().Id}";
                CreateForTestingPurpose<RaceRanking>(new { raceId = race.Id, stageId = stage.Id, ranking });
            }

            var info = new JavaScriptSerializer().Serialize(new ReportInfo { Riders = riders.Skip(1).ToList() });
            CreateForTestingPurpose<RaceEvent>(new
            {
                raceId = race.Id, stageId = stage.Id, eventId = RaceEvents.RaceStartsOnSunnyDay, kilometer = 0, seconds = 0
            });
            CreateForTestingPurpose<RaceEvent>(new
            {
                raceId = race.Id, stageId = stage.Id, index = 2, eventId = RaceEvents.EscapeFromPelotonThreeRiders, info
            });
            var result = CreateProcessor(race.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            Assert.AreEqual(race.RaceRaceDescription.IsTour, result.IsTour);
            AssertRidersOk(riders, result.Riders);
            AssertEventsOk(stage, riders.Skip(1).ToList(), RaceQueries.GetRaceEvents(Context, race.Id).ToList(), result.Events);
        }

        #endregion


        #region Private Methods

        private static void AssertRidersOk(List<Rider> expectedRiders, List<RiderBase> riders)
        {
            Assert.NotNull(riders);
            Assert.IsNotEmpty(riders);
            expectedRiders.ForEach(expected => Assert.NotNull(riders.FirstOrDefault(r => r.Id == expected.Id)));
        }

        private static void AssertEventsOk(Stage stage, List<Rider> riders, List<RaceEvent> expectedEvents, List<Contracts.DataContracts.Race.RaceEvent> events)
        {
            Assert.NotNull(events);
            Assert.IsNotEmpty(events);
            expectedEvents.ForEach(expected => AssertEventOk(stage, riders, expected, events.FirstOrDefault(e => e.Id == expected.Id)));
        }

        private static void AssertEventOk(Stage stage, List<Rider> riders, RaceEvent expected, Contracts.DataContracts.Race.RaceEvent raceEvent)
        {
            Assert.NotNull(raceEvent);
            Assert.AreEqual(expected.RaceEventEvent.Class, raceEvent.Class);
            Assert.AreEqual(expected.RaceEventEvent.Type, raceEvent.Type);
            Assert.AreEqual(expected.RaceEventEvent.Index, raceEvent.Index);
            Assert.AreEqual(expected.Km, raceEvent.Distance);
            Assert.AreEqual(expected.Seconds, raceEvent.Time);

            var expectedEvent = expected.Index == 1
                ? $"[[REP([[{expected.RaceEventEvent.Item}]]||[totkm]||[b]{stage.StageTrack.Kilometers}[/b])]]"
                : $"[[REP([[{expected.RaceEventEvent.Item}]]||[cycl]||[[RIDER({riders.Skip(1).First().Id})]]";
            if (expected.Index > 1)
            {
                expectedEvent = $"[[REP({expectedEvent}||[cycl2]||{riders.Skip(2).First().Id})]]";
                expectedEvent = $"[[REP({expectedEvent}||[cycl3]||{riders.Skip(3).First().Id})]]";
                var expectedRiders = $"[[RIDER({riders.Skip(1).First().Id})]], [[RIDER({riders.Skip(2).First().Id})]] [[{Items.And}]] [[RIDER({riders.Skip(3).First().Id})]]";
                expectedEvent = $"[[REP({expectedEvent}||[all]||{expectedRiders})]]";
            }
            Assert.AreEqual(expectedEvent, raceEvent.Item);
        }

        #endregion
    }
}