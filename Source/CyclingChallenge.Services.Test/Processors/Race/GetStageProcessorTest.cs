﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Contracts.DataContracts.Location;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetStageProcessorTest: ProcessorBaseTest<GetStageProcessor, Contracts.DataContracts.Race.StageDescription>
    {
        #region Tests

        [Test]
        public void GetStageProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetStageProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetStageProcessor_InvalidId_ErrorCodeStageNotFound()
        {
            short invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdStageNotFound, result.ErrorCode);
        }

        [Test]
        public void GetStageProcessor_NotATourStage_ErrorCodeRaceNotATour()
        {
            var result = CreateProcessor(FixedID.Stage).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.RaceNotATour, result.ErrorCode);
        }

        [Test]
        public void GetStageProcessor_ValidId_ReturnsStageDescription()
        {
            var race = CreateForTestingPurpose<Race>(new { raceDescriptionId = FixedID.TourRaceDescription });
            var expectedStage = RaceQueries.GetStagesByRaceDescription(Context, race.RaceDescription).FirstOrDefault();
            Assert.NotNull(expectedStage);

            var result = CreateProcessor(expectedStage.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            Assert.AreEqual(expectedStage.Id, result.Id);
            Assert.AreEqual(expectedStage.StageTrack.Name, result.Name);
            Assert.AreEqual(expectedStage.Day, result.Day);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyCobblestones, result.CobblestonesPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyHills, result.HillsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyMountains, result.MountainsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.DifficultyPlains, result.PlainsPercentage);
            Assert.AreEqual(expectedStage.StageTrack.Profile, result.Profile);
            Assert.AreEqual(expectedStage.StageTrack.CobbleHillMountain, result.Terrain);
            var trackDescription = new TrackDescription(expectedStage.StageTrack.Terrain, expectedStage.StageTrack.Description);
            Assert.True(trackDescription.Success);
            AssertWeatherOk(trackDescription.GetStart().Value, result.Weather);
            AssertClimbsOk(trackDescription.GetHills(), result.Hills);
            AssertClimbsOk(trackDescription.GetMountains(), result.Mountains);
            AssertPavedSectorsOk(trackDescription.GetPavedSectors(), result.PavedSectors);
        }

        #endregion


        #region Private Methods

        private void AssertWeatherOk(long idCity, int weather)
        {
            var city = LocationQueries.GetCityById(Context, idCity);
            Assert.NotNull(city);
            Assert.AreEqual(city.CityRegion.Weather, weather);
        }

        private void AssertClimbsOk(Dictionary<int, Services.Models.Race.Terrain> expectedClimbs, List<Contracts.DataContracts.Location.Climb> climbs)
        {
            if (!expectedClimbs.Any())
            {
                return;
            }

            Assert.NotNull(climbs);
            Assert.IsNotEmpty(climbs);
            foreach (var expectedClimb in expectedClimbs)
            {
                AssertClimbOk(expectedClimb, climbs.FirstOrDefault(c => c.Id == expectedClimb.Value.Climb && c.Kilometer == expectedClimb.Key));
            }
        }

        private void AssertClimbOk(KeyValuePair<int, Services.Models.Race.Terrain> expectedClimb, Contracts.DataContracts.Location.Climb climb)
        {
            Assert.NotNull(climb);
            var dbClimb = LocationQueries.GetClimbById(Context, expectedClimb.Value.Climb ?? 0);
            Assert.NotNull(dbClimb);
            Assert.AreEqual(dbClimb.Name, climb.Name);
        }

        private void AssertPavedSectorsOk(Dictionary<int, Services.Models.Race.Terrain> expectedPavedSectors, List<PavedSector> pavedSectors)
        {
            if (!expectedPavedSectors.Any())
            {
                return;
            }

            Assert.NotNull(pavedSectors);
            Assert.IsNotEmpty(pavedSectors);
            foreach (var expectedPavedSector in expectedPavedSectors)
            {
                AssertPavedSectorOk(expectedPavedSector, pavedSectors.FirstOrDefault(p => p.Id == expectedPavedSector.Value.Climb && p.Kilometer == expectedPavedSector.Key));
            }
        }

        private void AssertPavedSectorOk(KeyValuePair<int, Services.Models.Race.Terrain> expectedPavedSector, PavedSector pavedSector)
        {
            Assert.NotNull(pavedSector);
            var dbPavedSector = LocationQueries.GetClimbById(Context, expectedPavedSector.Value.Climb ?? 0);
            Assert.NotNull(dbPavedSector);
            Assert.AreEqual(dbPavedSector.Name, pavedSector.Name);
        }

        #endregion
    }
}