﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetStageRankingProcessorTest: ProcessorBaseTest<GetStageRankingProcessor, Contracts.DataContracts.Race.RaceRanking>
    {
        #region Tests

        [Test]
        public void GetStageRankingProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Race).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, FixedID.Stage).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(FixedID.Race, string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_InvalidId_ErrorCodeRaceNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId, FixedID.Stage).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdRaceNotFound, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_NotATour_ErrorCodeRaceNotATour()
        {
            var result = CreateProcessor(FixedID.Race, FixedID.Stage).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.RaceNotATour, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_WrongStage_ErrorCodeInvalidStageForRace()
        {
            var result = CreateProcessor(FixedID.TourRace, FixedID.Stage).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidStageForRace, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_RaceOpenOrClosed_ErrorCodeRaceNotStarted()
        {
            var race = CreateForTestingPurpose<Race>(new { raceDescriptionId = FixedID.TourRaceDescription });
            var result = CreateProcessor(race.Id, FixedID.TourStage).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.RaceNotStarted, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_RaceCurrentStageBeforeStage_ErrorCodeStageNotStarted()
        {
            var stages = RaceQueries.GetStagesByRaceDescription(Context, FixedID.TourRaceDescription).OrderBy(s => s.Index).ToList();
            Assert.Greater(stages.Count, 1);
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = FixedID.TourRaceDescription,
                status = RaceStatus.BetweenStages,
                stageId = stages.First().Id
            });
            var result = CreateProcessor(race.Id, stages.First(s => s.Index > race.RaceCurrentStage.Index).Id).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.StageNotStarted, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_StageIsCurrentStageButStillRunning_ErrorCodeStageNotFinished()
        {
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = FixedID.TourRaceDescription,
                status = RaceStatus.Running,
                stageId = FixedID.TourStage
            });
            var result = CreateProcessor(race.Id, race.CurrentStage).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.StageNotFinished, result.ErrorCode);
        }

        [Test]
        public void GetStageRankingProcessor_ValidId_ReturnsRanking()
        {
            var race = CreateForTestingPurpose<Race>(new
            {
                raceDescriptionId = FixedID.TourRaceDescription,
                status = RaceStatus.BetweenStages,
                stageId = FixedID.TourStage
            });
            var team = CreateForTestingPurpose<Team>();
            var players = new Dictionary<Player, Results>
            {
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() },
                { CreateForTestingPurpose<Player>(new { teamId = team.Id }), new Results() }
            };
            var stageTimes = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber(4));
            var stagePoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber());
            var stageMountainPoints = players.Select(p => p.Key).ToDictionary(p => p.Id, p => RandomUtil.RandomNumber());
            CreateForTestingPurpose<RaceRanking>(new
            {
                raceId = race.Id,
                stageId = race.CurrentStage,
                ranking = GetRanking(stageTimes),
                pointsRanking = GetRanking(stagePoints),
                mountainsRanking = GetRanking(stageMountainPoints),
                teams = team.Id.ToString()
            });
            foreach (var player in players)
            {
                player.Value.Time += stageTimes[player.Key.Id];
                player.Value.Points += (int)stagePoints[player.Key.Id];
                player.Value.MountainPoints += (int)stageMountainPoints[player.Key.Id];
            }

            var result = CreateProcessor(race.Id, FixedID.TourStage).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.RaceRaceDescription.Name, result.Name);
            AssertStageOk(race.RaceCurrentStage, result.Stage);
            AssertRankingOk(players, result.Riders);
            Assert.Null(result.Green);
            Assert.Null(result.Polkadot);
            Assert.AreEqual(players.OrderBy(t => t.Value.Time).First().Key.Id, result.Yellow?.Id);
        }

        #endregion


        #region Private Methods

        private static void AssertStageOk(Stage expectedStage, Contracts.DataContracts.Race.Stage stage)
        {
            Assert.NotNull(stage);
            Assert.AreEqual(expectedStage.Id, stage.Id);
            Assert.AreEqual(expectedStage.StageTrack.Name, stage.Name);
        }

        private static string GetRanking(Dictionary<long, long> stageTimes)
        {
            return stageTimes.OrderBy(st => st.Value).Select(kvp => $"{kvp.Key},{kvp.Value}").Aggregate(string.Empty, (current,next) => $"{current}|{next}").Trim('|');
        }

        private static void AssertRankingOk(Dictionary<Player, Results> players, List<RiderBase> ranking)
        {
            Assert.IsNotEmpty(ranking);
            byte rank = 1;
            foreach (var player in players.OrderBy(p => p.Value.Time).ToList())
            {
                AssertRiderOk(player.Key, player.Value, ranking.FirstOrDefault(p => p.Id == player.Key.Id), rank++);
            }
        }

        private static void AssertRiderOk(Player player, Results results, RiderBase rider, byte rank)
        {
            Assert.NotNull(rider);
            Assert.AreEqual(player.Id, rider.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}".Trim(), rider.Name);
            Assert.AreEqual(player.Age, rider.Age);
            Assert.AreEqual(player.Nationality, rider.Nationality);
            Assert.AreEqual(rank, rider.Rank);
            Assert.AreEqual(results.Time, rider.Time);
            Assert.AreEqual(results.Points, rider.Points);
            Assert.AreEqual(results.MountainPoints, rider.MountainPoints);
            AssertTeamOk(player.PlayerTeam, rider.Team);
        }

        private static void AssertTeamOk(Team expectedTeam, TeamBase team)
        {
            Assert.NotNull(team);
            Assert.AreEqual(expectedTeam.Id, team.Id);
            Assert.AreEqual(expectedTeam.ShortName, team.ShortName);
            Assert.AreEqual(expectedTeam.Name, team.LongName);
        }

        #endregion
    }
}