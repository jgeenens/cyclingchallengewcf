﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using System.Linq;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRiderEventsProcessorTest: ProcessorBaseTest<GetRiderEventsProcessor, RiderEvents>
    {
        [Test]
        public void GetRiderEventsProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRiderEventsProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRiderEventsProcessor_InvalidId_ErrorCodeIdRiderNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRiderNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRiderEventsProcessor_ValidId_ReturnsRiderEvents()
        {
            var player = CreateForTestingPurpose<Player>();
            CreateForTestingPurpose<Calendar>(new
            {
                playerId = player.Id,
                teamId = player.Team,
                locationId = FixedID.Location
            });
            var raceId = CreateIdForTestingPurpose<Race>();
            CreateForTestingPurpose<Calendar>(new
            {
                playerId = player.Id,
                teamId = player.Team,
                raceId
            });

            var result = CreateProcessor(player.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            var camp = result.Events.FirstOrDefault(e => e.Camp?.Id == FixedID.Location);
            Assert.NotNull(camp);
            var race = result.Events.FirstOrDefault(e => e.Race?.Id == raceId);
            Assert.NotNull(race);
        }
    }
}
