﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRiderHistoryProcessorTest: ProcessorBaseTest<GetRiderHistoryProcessor, RiderHistory>
    {
        [Test]
        public void GetRiderHistoryProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRiderHistoryProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRiderHistoryProcessor_InvalidId_ErrorCodeRiderNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRiderNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRiderHistoryProcessor_ValidIdPositionAboveThree_ReturnsNoAchievements()
        {
            var player = CreateForTestingPurpose<Player>();
            CreateForTestingPurpose<RaceHistory>(new { playerId = player.Id });
            var result = CreateProcessor(player.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(player.Id, result.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}", result.Name);
            Assert.NotNull(result.Achievements);
            Assert.IsEmpty(result.Achievements);
        }

        [Test]
        public void GetRiderHistoryProcessor_ValidIdPositionLowerThanFour_ReturnsAchievements()
        {
            var player = CreateForTestingPurpose<Player>();
            var raceHistory = CreateForTestingPurpose<RaceHistory>(new { playerId = player.Id, stageId = FixedID.Stage, position = 3 });
            var result = CreateProcessor(player.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(player.Id, result.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}", result.Name);
            Assert.NotNull(result.Achievements);
            Assert.IsNotEmpty(result.Achievements);
            var achievementForRace = result.Achievements.First(a => a.Race.Id == FixedID.Race);
            Assert.NotNull(achievementForRace);
            Assert.AreEqual(raceHistory.Position, achievementForRace.Item - Items.HistoryFirstInRace + 1);
            Assert.AreEqual(raceHistory.When, achievementForRace.When);
            Assert.AreEqual(raceHistory.Stage, achievementForRace.Stage?.Id);
            Assert.AreEqual(raceHistory.Team, achievementForRace.Team.Id);
        }
    }
}
