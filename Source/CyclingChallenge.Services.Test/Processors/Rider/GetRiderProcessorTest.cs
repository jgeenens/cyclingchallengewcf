﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRiderProcessorTest: ProcessorBaseTest<GetRiderProcessor, Contracts.DataContracts.Rider.Rider>
    {
        [Test]
        public void GetRiderProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRiderProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRiderProcessor_InvalidId_ErrorCodeIdRiderNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRiderNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRiderProcessor_ValidId_ReturnsPlayer()
        {
            var player = CreateForTestingPurpose<Player>();
            var playerValue = CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id });
            var result = CreateProcessor(player.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(player.Id, result.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}", result.Name);
            Assert.AreEqual(player.Age, result.Age);
            Assert.AreEqual(player.Nationality, result.Nationality);
            Assert.AreEqual(playerValue.Value, result.Value);
            Assert.AreEqual(playerValue.Sprint, result.Values.Sprint);
            Assert.AreEqual(playerValue.Descending, result.Values.Descending);
            Assert.AreEqual(playerValue.Stamina, result.Values.Stamina);
            Assert.AreEqual(playerValue.Technique, result.Values.Technique);
        }
    }
}
