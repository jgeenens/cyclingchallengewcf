﻿using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRidersForTeamProcessorTest: ProcessorBaseTest<GetRidersForTeamProcessor, TeamRiders>
    {
        [Test]
        public void GetRidersForTeamProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRidersForTeamProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRidersForTeamProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRidersForTeamProcessor_ValidId_ReturnsTeamRiders()
        {
            var player = CreateForTestingPurpose<Player>(new { teamId = FixedID.Team });
            CreateIdForTestingPurpose<PlayerValue>(new { playerId = player.Id });
            var result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(FixedID.Team, result.Id);
            Assert.NotNull(result.Riders);
            Assert.IsEmpty(result.Riders.Where(r => r.Team.Id != FixedID.Team));
            Assert.Contains(player.Id, result.Riders.Select(r => r.Id).ToList());
        }
    }
}
