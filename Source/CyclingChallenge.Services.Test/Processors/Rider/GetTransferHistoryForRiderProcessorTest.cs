﻿using System;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Models.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;
using TransferHistory = CyclingChallenge.Services.Models.Rider.TransferHistory;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTransferHistoryForRiderProcessorTest: ProcessorBaseTest<GetTransferHistoryForRiderProcessor, RiderTransferHistory>
    {
        #region Tests

        [Test]
        public void GetTransferHistoryForRiderProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(CreateIdForTestingPurpose<Player>()).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTransferHistoryForRiderProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, DateTime.Now).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(CreateIdForTestingPurpose<Player>(), string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTransferHistoryForRiderProcessor_InvalidId_ErrorCodeIdRiderNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, DateTime.Now).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRiderNotFound, result.ErrorCode);
        }

        [Test]
        public void GetTransferHistoryForRiderProcessor_ValidIdAndDate_ReturnsFinishedTransfersSinceDate()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var transfers = new List<TransferHistory>()
            {
                CreateForTestingPurpose<TransferHistory>(new
                {
                    playerId = player.Id,
                    fromTeamId = team.Id,
                    date = DateTime.Now.AddDays(-3)
                }),
                CreateForTestingPurpose<TransferHistory>(new
                {
                    playerId = player.Id,
                    teamId = team.Id,
                    date = DateTime.Now.AddDays(-1)
                })
            };

            var result = CreateProcessor(player.Id, DateTime.Now.AddDays(-7)).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(player.Id, result.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}".Trim(), result.Name);
            AssertTransfersOk(result.Transfers, transfers);
        }

        #endregion


        #region Private Methods

        private void AssertTransfersOk(List<Contracts.DataContracts.Rider.TransferHistory> transfers, List<TransferHistory> expectedTransfers)
        {
            Assert.IsNotEmpty(transfers);
            Assert.AreEqual(2, transfers.Count);
            Assert.GreaterOrEqual(transfers.First().Date, transfers.Last().Date);
            foreach (var transfer in transfers)
            {
                AssertTransferHistoryOk(transfer, expectedTransfers.FirstOrDefault(h => h.Id == transfer.Id));
            }            
        }

        private void AssertTransferHistoryOk(Contracts.DataContracts.Rider.TransferHistory transfer, TransferHistory expectedTransfer)
        {
            Assert.NotNull(transfer);
            Assert.NotNull(expectedTransfer);
            Assert.AreEqual(expectedTransfer.Id, transfer.Id);
            Assert.AreEqual(expectedTransfer.DateTransfer, transfer.Date);
            Assert.AreEqual(expectedTransfer.Bid, transfer.Bid);
            AssertTeamOk(transfer.PreviousTeam, expectedTransfer.TransferHistoryFromTeam);
            AssertTeamOk(transfer.CurrentTeam, expectedTransfer.TransferHistoryTeam);
        }

        private void AssertTeamOk(TeamBase team, Team expectedTeam)
        {
            Assert.NotNull(team);
            Assert.AreEqual(expectedTeam.Id, team.Id);
            Assert.NotNull(team.ShortName);
            Assert.NotNull(team.LongName);
        }

        #endregion
    }
}