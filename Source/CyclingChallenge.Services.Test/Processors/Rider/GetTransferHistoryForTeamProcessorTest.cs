﻿using System;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Models.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;
using TransferHistory = CyclingChallenge.Services.Models.Rider.TransferHistory;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTransferHistoryForTeamProcessorTest: ProcessorBaseTest<GetTransferHistoryForTeamProcessor, TeamTransferHistory>
    {
        #region Tests

        [Test]
        public void GetTransferHistoryForTeamProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTransferHistoryForTeamProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, DateTime.Now).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(FixedID.Team, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTransferHistoryForTeamProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, DateTime.Now).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetTransferHistoryForTeamProcessor_ValidIdAndDate_ReturnsFinishedTransfersSinceDate()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var outgoingTransfer = CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = player.Id,
                fromTeamId = team.Id,
                date = DateTime.Now.AddDays(-3)
            });
            var incomingTransfer = CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = player.Id,
                teamId = team.Id,
                date = DateTime.Now.AddDays(-1)
            });

            var result = CreateProcessor(team.Id, DateTime.Now.AddDays(-7)).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertTransfersOk(result.Incoming, incomingTransfer);
            AssertTransfersOk(result.Outgoing, outgoingTransfer);
        }

        #endregion


        #region Private Methods

        private static void AssertTransfersOk(List<Contracts.DataContracts.Rider.TransferHistory> transfers, TransferHistory expectedTransfer)
        {
            Assert.IsNotEmpty(transfers);
            Assert.AreEqual(1, transfers.Count);
            AssertTransferHistoryOk(transfers.FirstOrDefault(), expectedTransfer);
        }

        private static void AssertTransferHistoryOk(Contracts.DataContracts.Rider.TransferHistory transfer, TransferHistory expectedTransfer)
        {
            Assert.NotNull(transfer);
            Assert.AreEqual(expectedTransfer.Id, transfer.Id);
            Assert.AreEqual(expectedTransfer.DateTransfer, transfer.Date);
            Assert.AreEqual(expectedTransfer.Bid, transfer.Bid);
            AssertRiderOk(transfer.Rider, expectedTransfer.TransferHistoryPlayer);
        }

        private static void AssertRiderOk(RiderBase rider, Player expectedRider)
        {
            Assert.NotNull(rider);
            Assert.AreEqual(expectedRider.Id, rider.Id);
            Assert.AreEqual($"{expectedRider.FirstName} {expectedRider.LastName}".Trim(), rider.Name);
            AssertTeamOk(rider.Team);
        }

        private static void AssertTeamOk(TeamBase team)
        {
            Assert.NotNull(team);
            Assert.AreEqual(FixedID.Team, team.Id);
            Assert.NotNull(team.ShortName);
            Assert.NotNull(team.LongName);
        }

        #endregion
    }
}