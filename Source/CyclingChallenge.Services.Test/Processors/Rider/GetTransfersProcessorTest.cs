﻿using System;
using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Models.Rider;
using Rider = CyclingChallenge.Services.Enums.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;
using TransferHistory = CyclingChallenge.Services.Models.Rider.TransferHistory;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTransfersProcessorTest: ProcessorBaseTest<GetTransfersProcessor, TeamTransfers>
    {
        #region Tests

        [Test]
        public void GetTransfersProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTransfersProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTransfersProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetTransfersProcessor_ValidId_ReturnsUnfinishedTransfers()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var outgoingTransfer = CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = player.Id,
                fromTeamId = team.Id,
                type = Rider.Transfer.InProgress,
                date = DateTime.Now.AddDays(1)
            });
            var incomingTransfer = CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = player.Id,
                teamId = team.Id,
                type = Rider.Transfer.Processing,
                date = DateTime.Now
            });

            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertTransfersOk(result.Incoming, incomingTransfer);
            AssertTransfersOk(result.Outgoing, outgoingTransfer);
        }

        #endregion


        #region Private Methods

        private static void AssertTransfersOk(List<Transfer> transfers, TransferHistory expectedTransfer)
        {
            Assert.IsNotEmpty(transfers);
            Assert.AreEqual(1, transfers.Count);
            AssertTransferOk(transfers.FirstOrDefault(), expectedTransfer);
        }

        private static void AssertTransferOk(Transfer transfer, TransferHistory expectedTransfer)
        {
            Assert.NotNull(transfer);
            Assert.AreEqual(expectedTransfer.Player, transfer.Id);
            Assert.AreEqual(expectedTransfer.DateTransfer, transfer.Deadline);
            Assert.AreEqual(expectedTransfer.Bid, transfer.Bid);
            Assert.AreEqual($"{expectedTransfer.TransferHistoryPlayer.FirstName} {expectedTransfer.TransferHistoryPlayer.LastName}", transfer.Name);
            AssertTeamOk(expectedTransfer.TransferHistoryFromTeam, transfer.Team);
            AssertTeamOk(expectedTransfer.TransferHistoryTeam, transfer.BidTeam);
        }

        private static void AssertTeamOk(Team expectedTeam, TeamBase team)
        {
            Assert.NotNull(team);
            Assert.AreEqual(expectedTeam.Id, team.Id);
            Assert.AreEqual(expectedTeam.ShortName, team.ShortName);
            Assert.AreEqual(expectedTeam.Name, team.LongName);
        }

        #endregion
    }
}