﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using NUnit.Framework;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts.YouthRider;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetYouthRidersForTeamProcessorTest: ProcessorBaseTest<GetYouthRidersForTeamProcessor, TeamYouthRiders>
    {
        [Test]
        public void GetYouthRidersForTeamProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetYouthRidersForTeamProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetYouthRidersForTeamProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetYouthRidersForTeamProcessor_ValidIdNoYouthRiders_ReturnsEmptyList()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.LongName);
            Assert.AreEqual(team.ShortName, result.ShortName);
            Assert.NotNull(result.Riders);
            Assert.IsEmpty(result.Riders);
        }

        [Test]
        public void GetYouthRidersForTeamProcessor_ValidId_ReturnsTeamYouthRiders()
        {
            var player = CreateForTestingPurpose<Player>(new { teamId = FixedID.Team });
            CreateIdForTestingPurpose<PlayerValue>(new { playerId = player.Id, isYouth = true });
            var result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(FixedID.Team, result.Id);
            Assert.NotNull(result.Riders);
            Assert.IsEmpty(result.Riders.Where(r => r.Team.Id != FixedID.Team));
            foreach (var rider in result.Riders)
            {
                var playerValues = PlayerQueries.GetPlayerValuesByPlayer(Context, rider.Id);
                Assert.AreEqual($"{playerValues.PlayerValuePlayer.FirstName} {playerValues.PlayerValuePlayer.LastName}", rider.Name);
                Assert.AreEqual(playerValues.PlayerValuePlayer.Age, rider.Age);
                Assert.AreEqual(playerValues.PlayerValuePlayer.Nationality, rider.Nationality);
                Assert.AreEqual(playerValues.PlayerValuePlayer.Team, rider.Team.Id);
                AssertValuesOk(playerValues, rider.Values);
                AssertPictureOk(playerValues.PlayerValuePlayer, rider.Picture);
            }
        }

        private static void AssertValuesOk(PlayerValue riderValues, YouthRiderValues values)
        {
            Assert.AreEqual(GetYouthSkillValue(riderValues), values.Skill);
            Assert.AreEqual(GetYouthTerrainValue(riderValues), values.Terrain);
        }

        private static byte GetYouthSkillValue(PlayerValue r)
        {
            return (byte)((r.Climbing + r.Descending + r.Stamina + r.Sprint +
                           r.Technique + r.Timetrial + r.Experience) / 218750);
        }

        private static byte GetYouthTerrainValue(PlayerValue r)
        {
            return (byte)((r.Plains + r.Hills + r.Mountains + r.Cobblestones) / 125000);
        }

        private static void AssertPictureOk(Player rider, Picture picture)
        {
            Assert.NotNull(picture);
            Assert.AreEqual(rider.Eyes, picture.Eyes);
            Assert.AreEqual(rider.Nose, picture.Nose);
            Assert.AreEqual(rider.Lips, picture.Lips);
            Assert.AreEqual(rider.Hair, picture.Hair);
            Assert.AreEqual(rider.Face, picture.Face);
        }
    }
}
