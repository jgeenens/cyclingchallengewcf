﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class SetRiderCategoryProcessorTest: ProcessorBaseTest<SetRiderCategoryProcessor, ResultBase>
    {
        [Test]
        public void SetRiderCategoryProcessor_LessThanThreeParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            var rider = CreateForTestingPurpose<Player>();
            result = CreateProcessor(FixedID.Team, rider.Id).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void SetRiderCategoryProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var rider = CreateForTestingPurpose<Player>();
            var result = CreateProcessor(string.Empty, rider.Id, Strategy.Type.Adventurer).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(FixedID.Team, string.Empty, Strategy.Type.Adventurer).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(FixedID.Team, rider.Id, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void SetRiderCategoryProcessor_InvalidIdTeam_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId, 1, Strategy.Type.Adventurer).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void SetRiderCategoryProcessor_InvalidIdRider_ErrorCodeRiderNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(FixedID.Team, invalidId, Strategy.Type.Adventurer).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdRiderNotFound, result.ErrorCode);
        }

        [Test]
        public void SetRiderCategoryProcessor_RiderNotFromTeam_ErrorCodeRiderNotFromTeam()
        {
            var team = CreateForTestingPurpose<Team>();
            var rider = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var result = CreateProcessor(FixedID.Team, rider.Id, Strategy.Type.Adventurer).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.RiderNotFromTeam, result.ErrorCode);
        }

        [Test]
        public void SetRiderCategoryProcessor_InvalidCategory_ErrorCodeInvalidCategoryValue()
        {
            var rider = CreateForTestingPurpose<Player>();
            var result = CreateProcessor(FixedID.Team, rider.Id, byte.MaxValue).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidCategoryValue, result.ErrorCode);
        }

        [Test]
        public void SetRiderCategoryProcessor_ValidIdAndCategory_ChangesRiderCategoryAndReturnsTrue()
        {
            var team = CreateForTestingPurpose<Team>();
            var rider = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var riderValues = CreateForTestingPurpose<PlayerValue>(new { playerId = rider.Id });
            Assert.AreEqual(Strategy.Type.None, riderValues.Type);

            var result = CreateProcessor(team.Id, rider.Id, Strategy.Type.Adventurer).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            using (var context = RuntimeManager.CreateDbContext())
            {
                Assert.AreEqual(Strategy.Type.Adventurer, PlayerQueries.GetPlayerValuesByPlayer(context, rider.Id)?.Type);
            }
        }
    }
}