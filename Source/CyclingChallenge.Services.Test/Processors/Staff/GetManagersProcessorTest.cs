﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Staff;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;
using Manager = CyclingChallenge.Services.Models.Staff.Manager;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetManagersProcessorTest: ProcessorBaseTest<GetManagersProcessor, TeamManagers>
    {
        #region Tests

        [Test]
        public void GetManagersProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetManagersProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetManagersProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetManagersProcessor_ValidId_ReturnsManagers()
        {
            var team = CreateForTestingPurpose<Team>();
            var shopManager = CreateForTestingPurpose<Manager>(new { teamId = team.Id });
            var raceManager = CreateForTestingPurpose<Manager>(new { teamId = team.Id, type = Managers.Type.Race, level = Managers.Level.Basic });
            var staffManager = CreateForTestingPurpose<Manager>(new { teamId = team.Id, type = Managers.Type.Staff, level = Managers.Level.Expert });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.IsNotEmpty(result.Managers);
            AssertManagerOk(shopManager, result.Managers.FirstOrDefault(m => m.Type == Managers.Type.Shop), AddOns.BeginnerShopManager);
            AssertManagerOk(raceManager, result.Managers.FirstOrDefault(m => m.Type == Managers.Type.Race), AddOns.IntermediateRaceManager);
            AssertManagerOk(staffManager, result.Managers.FirstOrDefault(m => m.Type == Managers.Type.Staff), AddOns.WorldClassStaffManager);
        }

        #endregion


        #region Private Methods

        private void AssertManagerOk(Manager expectedManager, Contracts.DataContracts.Staff.Manager manager, byte idAddon)
        {
            Assert.NotNull(manager);
            Assert.AreEqual(expectedManager.Id, manager.Id);
            Assert.AreEqual($"{expectedManager.FirstName} {expectedManager.LastName}".Trim(), manager.Name);
            Assert.AreEqual(expectedManager.Level, manager.Level);
            Assert.AreEqual(expectedManager.Learning, manager.StartTraining);
            AssertTrainingCostOk(idAddon, manager.TrainingCost);
            Assert.AreEqual((expectedManager.Level + 1) * 12, manager.TrainingTime);
            Assert.True(manager.IsActivated);
            Assert.False(manager.IsLearning);
            AssertPictureOk(expectedManager, manager.Picture);

        }

        private void AssertTrainingCostOk(byte idAddon, int? trainingCost)
        {
            Assert.NotNull(trainingCost);
            var addon = GameQueries.GetAddonById(Context, idAddon);
            Assert.NotNull(addon);
            Assert.AreEqual(addon.Tokens, trainingCost);
        }

        private static void AssertPictureOk(Manager manager, Picture picture)
        {
            Assert.NotNull(picture);
            Assert.AreEqual(manager.Eyes, picture.Eyes);
            Assert.AreEqual(manager.Nose, picture.Nose);
            Assert.AreEqual(manager.Lips, picture.Lips);
            Assert.AreEqual(manager.Hair, picture.Hair);
            Assert.AreEqual(manager.Face, picture.Face);
        }

        #endregion
    }
}