﻿using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetRankingProcessorTest : ProcessorBaseTest<GetRankingProcessor, Ranking>
    {
        #region Tests

        [Test]
        public void GetRankingProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetRankingProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetRankingProcessor_InvalidId_ErrorCodeIdNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdDivisionNotFound, result.ErrorCode);
        }

        [Test]
        public void GetRankingProcessor_ValidId_ReturnsRanking()
        {
            var division = TeamQueries.GetDivisionById(Context, FixedID.Division);
            var result = CreateProcessor(FixedID.Division).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(FixedID.Division, result.Id);
            Assert.AreEqual(division.Name, result.Name);
            Assert.NotNull(result.Country);
            Assert.AreEqual(division.Country, result.Country.Id);
            AssertTeamsOk(result.Teams, division);
            AssertRidersOk(result.Riders, result.Teams);
        }

        #endregion


        #region Helper Methods

        private void AssertTeamsOk(List<Team> teams, Services.Models.Team.Division expectedDivision)
        {
            Assert.IsNotEmpty(teams);
            Assert.GreaterOrEqual(teams.First().Points, teams.Last().Points);
            foreach (var team in teams)
            {
                var expected = TeamQueries.GetTeamById(Context, team.Id);
                Assert.NotNull(expected);
                Assert.AreEqual(expected.Division, expectedDivision.Id);
                Assert.AreEqual(expected.ShortName, team.ShortName);
                Assert.AreEqual(expected.Name, team.LongName);
                Assert.AreEqual(expected.Rank, team.Rank);
                Assert.AreEqual(expected.Wins, team.Wins);
                Assert.AreEqual(expected.Stage, team.Stage);
                Assert.AreEqual(expected.Points, team.Points);
                Assert.AreEqual(expected.Champions, team.Champions);
                Assert.AreEqual(WillPromote(expected.Rank, expectedDivision.Level), team.WillPromote);
                Assert.AreEqual(expected.Rank > 8, team.WillDemote);
            }
        }

        private static bool WillPromote(byte rank, byte level)
        {
            switch (level)
            {
                case Promotion.NoPromotion: return false;
                case Promotion.TopTwoPromote: return rank <= 2;
                default: return rank <= 4;
            }
        }

        private static void AssertRidersOk(List<RiderBase> riders, List<Team> teams)
        {
            if (!riders.Any())
            {
                return;
            }

            Assert.GreaterOrEqual(riders.First().Points, riders.Last().Points);
            var idTeams = teams.Select(t => t.Id).ToList();
            foreach (var rider in riders)
            {
                Assert.NotNull(rider.Team);
                Assert.Contains(rider.Team.Id, idTeams);
                Assert.NotNull(rider.Id);
                Assert.NotNull(rider.Name);
                Assert.NotNull(rider.Age);
            }
        }

        #endregion
    }
}