﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTeamDetailsForFacebookIdProcessorTest: ProcessorBaseTest<GetTeamDetailsForFacebookIdProcessor, TeamDetails>
    {
        [Test]
        public void GetTeamDetailsForFacebookIdProcessor_InvalidParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTeamDetailsForFacebookIdProcessor_ParameterIsNumber_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(0).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTeamDetailsForFacebookIdProcessor_FacebookIdString_ErrorCodeFacebookIdNotANumber()
        {
            var result = CreateProcessor(RandomUtil.RandomCharString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.FacebookIdNotANumber, result.ErrorCode);
        }

        [Test]
        public void GetTeamDetailsForFacebookIdProcessor_InvalidFacebookId_ErrorCodeFacebookIdNotFound()
        {
            var result = CreateProcessor(RandomUtil.RandomNumber(6).ToString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.FacebookIdNotFound, result.ErrorCode);
        }

        [Test]
        public void GetTeamDetailsForFacebookIdProcessor_NoTeamLinked_ErrorCodeNoTeamForUser()
        {
            var user = CreateForTestingPurpose<User>();
            var result = CreateProcessor(user.FacebookId.ToString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.NoTeamForUser, result.ErrorCode);
        }

        [Test]
        public void GetTeamDetailsForFacebookIdProcessor_ValidFacebookId_ReturnsTeamDetails()
        {
            var result = CreateProcessor(FixedID.FacebookId.ToString()).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.True(result.IsUser);

            var user = UserQueries.GetUserByFacebookId(Context, FixedID.FacebookId);
            Assert.AreEqual(user?.Id, result.User.Id);
        }
    }
}
