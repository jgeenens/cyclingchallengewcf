﻿using NUnit.Framework;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Queries;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Location;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTeamDetailsProcessorTest : ProcessorBaseTest<GetTeamDetailsProcessor, TeamDetails>
    {
        #region Tests

        [Test]
        public void GetTeamDetailsProcessor_InvalidParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetTeamDetailsProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsUser)
        {
            var result = invalidParameterIsUser
                ? CreateProcessor(FixedID.Team, string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetTeamDetailsProcessor_InvalidId_ErrorCodeIdNotFound(bool invalidIdIsUser)
        {
            long invalidId = -1;
            var result = invalidIdIsUser
                ? CreateProcessor(FixedID.Team, invalidId).Process()
                : CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(invalidIdIsUser ? ErrorCodes.IdUserNotFound : ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetTeamDetailsProcessor_ValidId_ReturnsTeamDetails(bool withUser)
        {
            var team = CreateForTestingPurpose<Team>();
            var user = CreateForTestingPurpose<User>(new { teamId = team.Id });
            for (var i = 0; i < 5; i++)
            {
                CreateForTestingPurpose<PressMessage>(new { userId = user.Id });
            }

            var result = withUser
                ? CreateProcessor(team.Id, user.Id).Process()
                : CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.True(result.IsUser);
            Assert.False(result.IsOnline);
            Assert.AreEqual(team.ShortName, result.ShortName);
            Assert.AreEqual(team.Name, result.LongName);
            Assert.AreEqual(withUser ? team.Money : (long?)null, result.Money);
            AssertCountryOk(result.Country, team.TeamCountry);
            AssertDivisionOk(result.Division, team.Division, team.Rank);
            AssertUserOk(result.User, user, withUser);
            AssertPressMessagesOk(result.PressMessages, user.Id);
        }

        #endregion


        #region Helper Methods

        private static void AssertUserOk(Contracts.DataContracts.User.User user, User expectedUser, bool withUser)
        {
            Assert.NotNull(user);
            Assert.AreEqual(expectedUser.Id, user.Id);
            Assert.AreEqual($"{expectedUser.FirstName} {expectedUser.LastName}".Trim(), user.Name);
            Assert.AreEqual(expectedUser.Username, user.Alias);
            Assert.AreEqual(withUser ? expectedUser.Email : null, user.Email);
            Assert.AreEqual(expectedUser.UserLevel, user.Level);
            Assert.AreEqual(expectedUser.Birthday, user.Birthday);
            Assert.AreEqual(expectedUser.Language, user.Language);
            Assert.AreEqual(expectedUser.Currency, user.Currency);
            Assert.AreEqual(withUser ? expectedUser.Tokens : (int?)null, user.Tokens);
            AssertCountryOk(user.Country, expectedUser.UserCountry);
        }

        private static void AssertDivisionOk(Contracts.DataContracts.Team.Division division, long idDivision, byte rank)
        {
            Assert.NotNull(division);
            Assert.AreEqual(idDivision, division.Id);
            Assert.AreEqual(rank, division.Rank);
        }

        private static void AssertCountryOk(Contracts.DataContracts.Location.Country country, Country expectedCountry)
        {
            Assert.NotNull(country);
            Assert.AreEqual(expectedCountry.Id, country.Id);
        }

        private void AssertPressMessagesOk(List<Contracts.DataContracts.Communication.PressMessage> pressMessages, long idUser)
        {
            Assert.IsNotEmpty(pressMessages);
            Assert.AreEqual(5, pressMessages.Count);
            Assert.True(pressMessages.All(pm => CommunicationQueries.GetPressMessageById(Context, pm.Id)?.User == idUser));
        }

        #endregion
    }
}