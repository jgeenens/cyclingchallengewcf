﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Race;
using TeamHistory = CyclingChallenge.Services.Models.Team.TeamHistory;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTeamHistoryProcessorTest: ProcessorBaseTest<GetTeamHistoryProcessor, Contracts.DataContracts.TeamHistory>
    {
        [Test]
        public void GetTeamHistoryProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTeamHistoryProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTeamHistoryProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetTeamHistoryProcessor_ValidIdPositionNotFirst_ReturnsNoEvents()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            CreateForTestingPurpose<RaceHistory>(new { playerId = player.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.NotNull(result.Events);
            Assert.IsEmpty(result.Events);
        }

        [Test]
        public void GetTeamHistoryProcessor_ValidIdPositionFirst_ReturnsEvents()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var raceHistory = CreateForTestingPurpose<RaceHistory>(new
            {
                playerId = player.Id, teamId = team.Id, stageId = FixedID.Stage, position = 1
            });
            var teamHistory = CreateForTestingPurpose<TeamHistory>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.NotNull(result.Events);
            Assert.IsNotEmpty(result.Events);
            AssertRaceEventIsOk(raceHistory, result.Events.FirstOrDefault(a => a.Race.Id == FixedID.Race));
            AssertTeamEventIsOk(teamHistory, result.Events.FirstOrDefault(a => a.Rider?.Id == null));
        }

        private static void AssertTeamEventIsOk(TeamHistory teamHistory, TeamEvent teamEvent)
        {
            Assert.NotNull(teamEvent);
            Assert.AreEqual(teamHistory.Date, teamEvent.When);
            Assert.AreEqual(teamHistory.Data, teamEvent.Name);
            Assert.AreEqual(Items.HistoryNewTeam, teamEvent.Item);
        }

        private static void AssertRaceEventIsOk(RaceHistory raceHistory, TeamEvent eventForRace)
        {
            Assert.NotNull(eventForRace);
            var itemId = raceHistory.Stage.HasValue ? Items.HistoryFirstInStage : Items.HistoryFirstInRace;
            Assert.AreEqual(raceHistory.Position, eventForRace.Item - itemId + 1);
            Assert.AreEqual(raceHistory.When, eventForRace.When);
            Assert.AreEqual(raceHistory.Stage, eventForRace.Stage?.Id);
            Assert.AreEqual(raceHistory.Player, eventForRace.Rider.Id);
        }
    }
}
