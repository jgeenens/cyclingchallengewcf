﻿using System;
using NUnit.Framework;
using System.Linq;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;
using TeamHistory = CyclingChallenge.Services.Models.Team.TeamHistory;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class SetTeamDetailsProcessorTest: ProcessorBaseTest<SetTeamDetailsProcessor, ResultBase>
    {
        #region Tests

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4)]
        public void SetTeamDetailsProcessor_LessThanFourParameters_ErrorCodeParameterMissing(byte missingParameter)
        {
            var result = missingParameter == 1 
                ? CreateProcessor(null).Process()
                : missingParameter == 2 
                    ? CreateProcessor(FixedID.Team).Process()
                    : missingParameter == 3
                        ? CreateProcessor(FixedID.Team, RandomUtil.RandomString()).Process()
                        : CreateProcessor(FixedID.Team, RandomUtil.RandomString(), RandomUtil.RandomString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4)]
        public void SetTeamDetailsProcessor_InvalidParameter_ErrorCodeInvalidParameterType(byte invalidParameter)
        {
            ResultBase result;
            switch (invalidParameter)
            {
                case 1:
                    result = CreateProcessor(string.Empty, RandomUtil.RandomString(), RandomUtil.RandomString(), FixedID.Logo).Process();
                    break;
                case 2:
                    result = CreateProcessor(FixedID.Team, 0, RandomUtil.RandomString(), FixedID.Logo).Process();
                    break;
                case 3:
                    result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(), 0, FixedID.Logo).Process();
                    break;
                default:
                    result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(), RandomUtil.RandomString(), string.Empty).Process();
                    break;
            }

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, RandomUtil.RandomString(), RandomUtil.RandomString(), FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_ShortNameEmpty_ErrorCodeTeamShortNameCannotBeEmpty()
        {
            var result = CreateProcessor(FixedID.Team, string.Empty, RandomUtil.RandomString(), FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamShortNameCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_ShortNameLessThanFourCharacters_ErrorCodeTeamShortNameTooShort()
        {
            var result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(3), RandomUtil.RandomString(), FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamShortNameTooShort, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_ShortNameAlreadyUsed_ErrorCodeTeamShortNameMustBeUnique()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(FixedID.Team, team.ShortName, RandomUtil.RandomString(), FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamShortNameMustBeUnique, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_LongNameEmpty_ErrorCodeTeamLongNameCannotBeEmpty()
        {
            var result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(), string.Empty, FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamLongNameCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_LongNameLessThanSixCharacters_ErrorCodeTeamLongNameTooShort()
        {
            var result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(), RandomUtil.RandomString(5), FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamLongNameTooShort, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_NameAlreadyUsed_ErrorCodeTeamLongNameMustBeUnique()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(), team.Name, FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamLongNameMustBeUnique, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_TeamNameAlreadyChanged_ErrorCodeTeamNameAlreadyChanged()
        {
            var teamId = CreateIdForTestingPurpose<Team>();
            CreateForTestingPurpose<TeamHistory>(new { teamId, eventType = Events.Team.NameChange, date = DateTime.Now });
            var result = CreateProcessor(teamId, RandomUtil.RandomCharString(), RandomUtil.RandomCharString(), FixedID.Logo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.TeamNameAlreadyChanged, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_InvalidIdLogo_ErrorCodeIdLogoNotFound()
        {
            var result = CreateProcessor(FixedID.Team, RandomUtil.RandomString(), RandomUtil.RandomString(), (byte)0).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdLogoNotFound, result.ErrorCode);
        }

        [Test]
        public void SetTeamDetailsProcessor_NotEnoughTokensForLogo_ErrorCodeNotEnoughTokens()
        {
            var team = CreateForTestingPurpose<Team>();
            CreateForTestingPurpose<User>(new { teamId = team.Id, tokens = 0 });
            var idLogo = (byte)CreateIdForTestingPurpose<Logo>(new { tokens = 500 });
            var result = CreateProcessor(team.Id, RandomUtil.RandomString(), RandomUtil.RandomString(), idLogo).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.NotEnoughTokens, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void SetTeamDetailsProcessor_ValidInformationProvided_ChangesShortNameLongNameAndLogo(bool useNewLogo)
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id });
            var user = CreateForTestingPurpose<User>(new { teamId = team.Id, tokens = 500 });
            var idLogo = (byte)CreateIdForTestingPurpose<Logo>(new { tokens = 250 });
            if (!useNewLogo)
            {
                CreateForTestingPurpose<Sale>(new { userId = user.Id, status = idLogo.ToString() });
            }
            var newShortName = RandomUtil.RandomCharString();
            var newLongName = RandomUtil.RandomCharString();
            var result = CreateProcessor(team.Id, newShortName, newLongName, idLogo).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            AssertUserOk(user, useNewLogo);
            AssertTeamOk(team, newShortName, newLongName, idLogo);
            AssertTeamHistoryOk(team.Id, newLongName);
            AssertRidersOk(team.Id, newShortName);
            Assert.True(GameQueries.UserAlreadyHasLogo(Context, user.Id, idLogo));
        }

        #endregion


        #region Private Methods

        private void AssertUserOk(User user, bool useNewLogo)
        {
            Context.Entry(user).Reload();
            Assert.AreEqual(useNewLogo ? 250 : 500, user.Tokens);
        }

        private void AssertTeamOk(Team team, string newShortName, string newLongName, byte idLogo)
        {
            Context.Entry(team).Reload();
            Assert.AreEqual(idLogo, team.Logo);
            Assert.AreEqual(newShortName, team.ShortName);
            Assert.AreEqual(newLongName, team.Name);
        }

        private void AssertTeamHistoryOk(long idTeam, string longName)
        {
            var teamHistories = TeamQueries.GetTeamHistoryForTeam(Context, idTeam).ToList();
            Assert.IsNotEmpty(teamHistories);
            Assert.True(teamHistories.Any(h => h.Event == Events.Team.NameChange && h.Data == longName));
        }

        private void AssertRidersOk(long idTeam, string shortName)
        {
            var riders = PlayerQueries.GetPlayersForTeam(Context, idTeam).ToList();
            Assert.IsNotEmpty(riders);
            foreach (var rider in riders)
            {
                Context.Entry(rider).Reload();
                Assert.AreEqual(shortName, rider.Team);
            }
        }

        #endregion
    }
}