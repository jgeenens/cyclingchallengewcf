﻿using System.Linq;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using System;
using System.Collections.Generic;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Training;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetCampForTeamProcessorTest: ProcessorBaseTest<GetCampForTeamProcessor, TrainingCamp>
    {
        #region Tests

        [Test]
        public void GetCampForTeamProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetCampForTeamProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetCampForTeamProcessor_InvalidId_ErrorCodeIdTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetCampForTeamProcessor_ValidIdCampRecently_ReturnsNoLocationsAndRiders()
        {
            var team = CreateForTestingPurpose<Team>();
            CreateForTestingPurpose<Booking>(new
            {
                teamId = team.Id,
                departure = DateTime.Today.AddDays(-5),
                duration = 1
            });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.Null(result.Riders);
            Assert.Null(result.Camps);
        }

        [Test]
        public void GetCampForTeamProcessor_ValidIdNoCampPlanned_ReturnsLocationsAndNoRiders()
        {
            var team = CreateForTestingPurpose<Team>();
            CreateForTestingPurpose<Booking>(new
            {
                teamId = team.Id,
                departure = DateTime.Today.AddDays(-50),
                duration = 1
            });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.Null(result.Riders);
            AssertCampsOk(result.Camps, false);
        }

        [Test]
        public void GetCampForTeamProcessor_ValidIdCampOncoming_ReturnsCampAndRiders()
        {
            var team = CreateForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = team.Id });
            var improvement = CreateForTestingPurpose<PlayerProgress>(new { playerId = player.Id });
            CreateForTestingPurpose<Booking>(new
            {
                teamId = team.Id,
                departure = DateTime.Today.AddDays(-1),
                players = player.Id.ToString(),
                duration = 7
            });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            AssertRidersOk(result.Riders, player, improvement);
            AssertCampsOk(result.Camps, true);
        }

        #endregion


        #region Helper Methods

        private static void AssertCampsOk(List<Camp> camps, bool isFromBooking)
        {
            Assert.NotNull(camps);
            foreach (var camp in camps)
            {
                Assert.NotNull(camp.Id);
                Assert.NotNull(camp.Name);
                Assert.NotNull(camp.City);
                Assert.NotNull(camp.City.Region);
                Assert.NotNull(camp.City.Region.Country);
                Assert.NotNull(camp.Picture);
                Assert.NotNull(camp.PrimarySkill);
                Assert.NotNull(camp.SecondarySkill);
                Assert.AreEqual(isFromBooking, camp.Start != null);
                Assert.AreEqual(isFromBooking, camp.End != null);
            }
        }

        private static void AssertRidersOk(List<RiderProgress> riders, Player player, PlayerProgress improvement)
        {
            var riderChanges = riders?.FirstOrDefault();
            Assert.NotNull(riderChanges);
            Assert.AreEqual(player.Id, riderChanges.Id);
            Assert.AreEqual($"{player.FirstName} {player.LastName}".Trim(), riderChanges.Name);
            var riderChange = riderChanges.Improvements?.FirstOrDefault();
            Assert.NotNull(riderChange);
            Assert.AreEqual(improvement.Id, riderChange.Id);
            Assert.AreEqual(improvement.Skill, riderChange.Skill);
            Assert.AreEqual(improvement.Level, riderChange.Level);
            Assert.AreEqual(improvement.Change, riderChange.Change);
        }

        #endregion
    }
}