﻿using NUnit.Framework;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Staff;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetTrainingProcessorTest: ProcessorBaseTest<GetTrainingProcessor, Training>
    {
        [Test]
        public void GetTrainingProcessor_NoParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void GetTrainingProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void GetTrainingProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void GetTrainingProcessor_ValidIdNoTrainer_ReturnsTrainerNull()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.AreEqual(team.Training, result.CurrentTraining);
            Assert.Null(result.Trainer);
        }

        [Test]
        public void GetTrainingProcessor_ValidIdHasTrainer_ReturnsTrainerNotNull()
        {
            var team = CreateForTestingPurpose<Team>();
            var coach = CreateForTestingPurpose<Coach>(new { teamId = team.Id });
            var result = CreateProcessor(team.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.AreEqual(team.Id, result.Id);
            Assert.AreEqual(team.Name, result.Name);
            Assert.AreEqual(team.Training, result.CurrentTraining);
            AssertTrainerOk(coach, result.Trainer);
        }

        private static void AssertTrainerOk(Coach coach, Trainer trainer)
        {
            Assert.NotNull(trainer);
            Assert.AreEqual(coach.Id, trainer.Id);
            Assert.AreEqual($"{coach.FirstName} {coach.LastName}", trainer.Name);
            Assert.AreEqual(coach.Age, trainer.Age);
            Assert.AreEqual(coach.Nationality, trainer.Nationality);
            Assert.AreEqual(coach.Wage, trainer.Wage);
            Assert.AreEqual(coach.Training, trainer.Training);
            Assert.AreEqual(coach.Skill, trainer.Skill);
            AssertPictureOk(coach, trainer.Picture);
        }

        private static void AssertPictureOk(Coach coach, Picture picture)
        {
            Assert.NotNull(picture);
            Assert.AreEqual(coach.Eyes, picture.Eyes);
            Assert.AreEqual(coach.Nose, picture.Nose);
            Assert.AreEqual(coach.Lips, picture.Lips);
            Assert.AreEqual(coach.Hair, picture.Hair);
            Assert.AreEqual(coach.Face, picture.Face);
        }
    }
}
