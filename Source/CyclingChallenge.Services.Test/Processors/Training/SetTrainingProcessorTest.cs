﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class SetTrainingProcessorTest: ProcessorBaseTest<SetTrainingProcessor, ResultBase>
    {
        [Test]
        public void SetTrainingProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(FixedID.Team).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void SetTrainingProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(string.Empty, (byte)Rider.Skill.Climbing).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(FixedID.Team, string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void SetTrainingProcessor_InvalidId_ErrorCodeTeamNotFound()
        {
            long invalidId = 0;
            var result = CreateProcessor(invalidId, (byte)Rider.Skill.Climbing).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdTeamNotFound, result.ErrorCode);
        }

        [Test]
        public void SetTrainingProcessor_InvalidTraining_ErrorCodeInvalidTrainingValue()
        {
            var result = CreateProcessor(FixedID.Team, byte.MaxValue).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidTrainingValue, result.ErrorCode);
        }

        [Test]
        public void SetTrainingProcessor_ValidIdAndTraining_ChangesTrainingAndReturnsTrue()
        {
            var team = CreateForTestingPurpose<Team>(new { training = Rider.Skill.Climbing });
            Assert.AreEqual(Rider.Skill.Climbing, team.Training);

            var result = CreateProcessor(team.Id, (byte)Rider.Skill.Technique).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            using (var context = RuntimeManager.CreateDbContext())
            {
                Assert.AreEqual(Rider.Skill.Technique, TeamQueries.GetTeamById(context, team.Id)?.Training);
            }
        }
    }
}