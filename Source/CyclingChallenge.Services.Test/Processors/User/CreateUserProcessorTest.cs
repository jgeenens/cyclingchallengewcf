﻿using System;
using System.Linq;
using System.Globalization;
using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class CreateUserProcessorTest: ProcessorBaseTest<CreateUserProcessor, ResultBase>
    {
        #region Tests

        [Test]
        public void CreateUserProcessor_LessThanTwoParameters_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);

            result = CreateProcessor(RandomUtil.RandomNumber(6).ToString()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test]
        public void CreateUserProcessor_InvalidParameter_ErrorCodeInvalidParameterType()
        {
            var result = CreateProcessor(0, new CreateUserRequest()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);

            result = CreateProcessor(RandomUtil.RandomNumber(6).ToString(), string.Empty).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void CreateUserProcessor_ExistingFacebookId_ErrorCodeAlreadyRegistered()
        {
            var result = CreateProcessor(FixedID.FacebookId.ToString(), new CreateUserRequest()).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.AlreadyRegistered, result.ErrorCode);
        }

        [Test, TestCase(false, false, false), TestCase(true, false, false), TestCase(false, true, false), TestCase(false, false, true)]
        public void CreateUserProcessor_OnlyBirthdayFirstNameOrLastNameOrNone_ErrorCodeAtLeastTwoNeeded(bool provideFirstName, bool provideLastName, bool provideBirthDay)
        {
            var createUserRequest = new CreateUserRequest
            {
                Language = DictionaryQueries.GetLanguageById(Context, FixedID.Language).Abbreviations,
                FirstName = provideFirstName ? RandomUtil.RandomCharString() : null,
                LastName = provideLastName ? RandomUtil.RandomCharString() : null,
                Birthday = provideBirthDay ? DateTime.Today.AddYears(-20).ToString(CultureInfo.InvariantCulture) : null
            };
            var result = CreateProcessor(RandomUtil.RandomNumber(6).ToString(), createUserRequest).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.AtLeastTwoNeeded, result.ErrorCode);
        }

        [Test]
        public void CreateUserProcessor_BirthdayInvalid_ErrorCodeNotValidDateTime()
        {
            var createUserRequest = new CreateUserRequest
            {
                Language = DictionaryQueries.GetLanguageById(Context, FixedID.Language).Abbreviations,
                Birthday = RandomUtil.RandomString(),
                FirstName = RandomUtil.RandomCharString()
            };
            var result = CreateProcessor(RandomUtil.RandomNumber(6).ToString(), createUserRequest).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.NotValidDateTime, result.ErrorCode);
        }

        [Test]
        public void CreateUserProcessor_CountryProvidedButUnknown_ErrorCodeCountryNameNotFound()
        {
            var createUserRequest = new CreateUserRequest
            {
                Language = DictionaryQueries.GetLanguageById(Context, FixedID.Language).Abbreviations,
                Birthday = DateTime.Today.AddYears(-20).ToString(CultureInfo.InvariantCulture),
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(),
                Country = RandomUtil.RandomString()
            };
            var result = CreateProcessor(RandomUtil.RandomNumber(6).ToString(), createUserRequest).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.CountryNameNotFound, result.ErrorCode);
        }

        [Test]
        public void CreateUserProcessor_ValidInformationProvided_CreatesUserInitializesTeamAndUpdatesRegistrations()
        {
            var facebookId = RandomUtil.RandomNumber(6);
            var country = LocationQueries.GetRandomActiveCountry(Context);
            var createUserRequest = new CreateUserRequest
            {
                Language = DictionaryQueries.GetLanguageById(Context, FixedID.Language).Abbreviations,
                Birthday = DateTime.Today.AddYears(-20).ToString(CultureInfo.InvariantCulture),
                FirstName = RandomUtil.RandomCharString(),
                LastName = RandomUtil.RandomCharString(),
                Country = country.Name
            };
            var result = CreateProcessor(facebookId.ToString(), createUserRequest).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            var user = UserQueries.GetUserByFacebookId(Context, facebookId);
            AssertUserOk(user, country.Id);
            AssertTeamOk(user, user.UserTeam);
            AssertRidersOk(user.UserTeam);
            AssertRidersOk(user.UserTeam, true);
            AssertStaffOk(user.UserTeam);
            AssertEconomyOk(user.UserTeam.Id);
            Assert.NotNull(GameQueries.GetRegistrationsForToday(Context, user.RealCountry));
        }

        #endregion


        #region Private Methods

        private void AssertUserOk(Services.Models.Team.User user, short idCountry)
        {
            Assert.NotNull(user);
            Assert.IsNotNullOrEmpty(user.TeamName);
            Assert.AreEqual(EconomyQueries.GetCurrencyByCountry(Context, idCountry).Id, user.Currency);
            Assert.NotNull(user.Team);
        }

        private void AssertTeamOk(Services.Models.Team.User user, Team team)
        {
            Assert.NotNull(team);
            Assert.AreEqual(user.TeamName, team.Name);
            AssertTeamInitialized(team.Id);
            AssertTeamHistoryOk(team.Name, TeamQueries.GetTeamHistoryForTeam(Context, team.Id).OrderByDescending(th => th.Date).FirstOrDefault());
        }

        private void AssertTeamInitialized(long idTeam)
        {
            Assert.Null(TeamQueries.GetShop(Context, idTeam));
            Assert.Null(StaffQueries.GetCoachByTeam(Context, idTeam));
            Assert.Null(SponsorQueries.GetSponsorByTeam(Context, idTeam));
            Assert.IsEmpty(EconomyQueries.GetTransactionsByTeam(Context, idTeam));
            Assert.IsEmpty(BookingQueries.GetBookingsByTeam(Context, idTeam));
            Assert.IsEmpty(CalendarQueries.GetCalendarByTeam(Context, idTeam));
            Assert.IsEmpty(PlayerQueries.GetScoutedPlayers(Context, idTeam));
            Assert.IsEmpty(PlayerQueries.GetYouthHistoriesByTeam(Context, idTeam));
        }

        private static void AssertTeamHistoryOk(string teamName, Services.Models.Team.TeamHistory teamHistory)
        {
            Assert.NotNull(teamHistory);
            Assert.False(teamHistory.Seen);
            Assert.AreEqual(DateTime.Today.Date, teamHistory.Date.Date);
            Assert.AreEqual(Events.Team.NewTeam, teamHistory.Event);
            Assert.AreEqual(teamName, teamHistory.Data);
        }

        private void AssertEconomyOk(long idTeam)
        {
            var economy = EconomyQueries.GetByTeam(Context, idTeam);
            Assert.NotNull(economy);
            var wages = PlayerQueries.GetPlayersForTeam(Context, idTeam).Sum(r => r.Wage);
            wages += StaffQueries.GetScouts(Context, idTeam).Sum(s => s.Wage);
            Assert.AreEqual(wages, economy.Expense2);
        }

        private void AssertStaffOk(Team team)
        {
            AssertManagersOk(team.Id);
            AssertScoutsOk(team);
        }

        private void AssertManagersOk(long idTeam)
        {
            var managers = StaffQueries.GetManagers(Context, idTeam).ToList();
            Assert.IsNotEmpty(managers);
            AssertManagerOk(managers.FirstOrDefault(m => m.Type == Managers.Type.Race));
            AssertManagerOk(managers.FirstOrDefault(m => m.Type == Managers.Type.Shop));
            AssertManagerOk(managers.FirstOrDefault(m => m.Type == Managers.Type.Staff));
        }

        private static void AssertManagerOk(Manager manager)
        {
            Assert.NotNull(manager);
            Assert.AreEqual(Managers.Level.Apprentice, manager.Level);
            Assert.AreEqual(Managers.Status.Locked, manager.Status);
            Assert.Null(manager.Learning);
        }

        private void AssertScoutsOk(Team team)
        {
            var scouts = StaffQueries.GetScouts(Context, team.Id).ToList();
            Assert.IsNotEmpty(scouts);
            Assert.AreEqual(1, scouts.Count(s => s.Status == Scouts.Status.Active));
            Assert.AreEqual(2, scouts.Count(s => s.Status == Scouts.Status.Locked));
            scouts.ForEach(s => AssertScoutOk(team.Country, s));
        }

        private static void AssertScoutOk(short idCountry, Scout scout)
        {
            Assert.AreEqual(idCountry, scout.Nationality);
            Assert.AreEqual(0, scout.WeeksToGo);
            Assert.AreEqual(0, scout.TotalWeeks);
            Assert.AreEqual(1000, scout.Wage);
            Assert.AreEqual(Scouts.Level.Apprentice, scout.Skill);
            Assert.LessOrEqual(25, scout.Age);
            Assert.False(scout.LookingForYouth);
        }

        private void AssertRidersOk(Team team, bool isYouth = false)
        {
            var riders = PlayerQueries.GetPlayersForTeam(Context, team.Id, isYouth).ToList();
            Assert.IsNotEmpty(riders);
            riders.ForEach(r => AssertRiderOk(team, r, isYouth));
        }

        private static void AssertRiderOk(Team team, PlayerValue riderValues, bool isYouth)
        {
            Assert.NotNull(riderValues.PlayerValuePlayer);
            Assert.AreEqual(team.Id, riderValues.PlayerValuePlayer.Team);
            Assert.AreEqual(team.Country, riderValues.PlayerValuePlayer.Nationality);
            Assert.LessOrEqual(isYouth ? 16 : 18, riderValues.PlayerValuePlayer.Age);
            Assert.GreaterOrEqual(isYouth ? 18 : 38, riderValues.PlayerValuePlayer.Age);
            Assert.False(riderValues.PlayerValuePlayer.Retired);

            Assert.AreEqual(team.ShortName, riderValues.Team);
            Assert.AreEqual(1000, riderValues.Form);
            Assert.AreEqual(1000, riderValues.FormTarget);
            Assert.AreEqual(isYouth, riderValues.IsYouth);
            Assert.AreEqual(team.Country, riderValues.PlayerValueCity.CityRegion.Country);
            if (isYouth)
            {
                Assert.AreEqual(0, riderValues.Wage);
            }
            else
            {
                Assert.LessOrEqual(1000, riderValues.Wage);
            }
        }

        #endregion
    }
}