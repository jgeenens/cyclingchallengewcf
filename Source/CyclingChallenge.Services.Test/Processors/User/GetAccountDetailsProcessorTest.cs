﻿using NUnit.Framework;
using CyclingChallenge.Services.Enums;
using System;
using System.Globalization;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Staff;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class GetAccountDetailsProcessorTest : ProcessorBaseTest<GetAccountDetailsProcessor, AccountDetails>
    {
        #region Tests

        [Test]
        public void GetAccountDetailsProcessor_InvalidParameter_ErrorCodeParameterMissing()
        {
            var result = CreateProcessor(null).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetAccountDetailsProcessor_InvalidParameter_ErrorCodeInvalidParameterType(bool invalidParameterIsRequestingUser)
        {
            var result = invalidParameterIsRequestingUser
                ? CreateProcessor(CreateIdForTestingPurpose<User>(), string.Empty).Process()
                : CreateProcessor(string.Empty).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetAccountDetailsProcessor_InvalidId_ErrorCodeIdUserNotFound(bool invalidIdIsRequestingUser)
        {
            long invalidId = -1;
            var result = invalidIdIsRequestingUser
                ? CreateProcessor(CreateIdForTestingPurpose<User>(), invalidId).Process()
                : CreateProcessor(invalidId).Process();

            Assert.NotNull(result);
            Assert.AreEqual(ErrorCodes.IdUserNotFound, result.ErrorCode);
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetAccountDetailsProcessor_ValidId_ReturnsAccountDetails(bool withRequestingUser)
        {
            var team = CreateForTestingPurpose<Team>();
            var user = CreateForTestingPurpose<User>(new { teamId = team.Id });
            CreateForTestingPurpose<Manager>(new { teamId = team.Id, type = Managers.Type.Race });

            var result = withRequestingUser
                ? CreateProcessor(user.Id, user.Id).Process()
                : CreateProcessor(user.Id).Process();

            Assert.NotNull(result);
            Assert.True(result.Success);
            Assert.True(result.HasRaceManager);
            Assert.False(result.IsOnHoliday);
            Assert.Null(result.StartOfHoliday);
            Assert.AreEqual(withRequestingUser, result.IsNameChangeAllowed);
            AssertUserOk(result.User, user, withRequestingUser);
        }

        #endregion


        #region Helper Methods

        private static void AssertUserOk(Contracts.DataContracts.User.User user, User expectedUser, bool withUser)
        {
            Assert.NotNull(user);
            Assert.AreEqual(expectedUser.Id, user.Id);
            Assert.AreEqual($"{expectedUser.FirstName} {expectedUser.LastName}".Trim(), user.Name);
            Assert.AreEqual(expectedUser.Username, user.Alias);
            Assert.AreEqual(withUser ? expectedUser.Email : null, user.Email);
            Assert.AreEqual(expectedUser.UserLevel, user.Level);
            Assert.AreEqual(expectedUser.Birthday, user.Birthday);
            Assert.AreEqual(expectedUser.Language, user.Language);
            Assert.AreEqual(expectedUser.Currency, user.Currency);
            Assert.AreEqual(withUser ? expectedUser.Tokens : (int?)null, user.Tokens);
            AssertCountryOk(user.Country, expectedUser.Country);
        }

        private static void AssertCountryOk(Contracts.DataContracts.Location.Country country, short idCountry)
        {
            Assert.NotNull(country);
            Assert.AreEqual(idCountry, country.Id);
        }

        #endregion
    }
}