﻿using NUnit.Framework;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Test.Utils;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Test.Processors
{
    [TestFixture]
    public class SetUserDetailsProcessorTest: ProcessorBaseTest<SetUserDetailsProcessor, ResultBase>
    {
        #region Tests

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4), TestCase(5)]
        public void SetUserDetailsProcessor_LessThanFourParameters_ErrorCodeParameterMissing(byte missingParameter)
        {
            var idUser = missingParameter > 1 ? CreateIdForTestingPurpose<User>() : (long?)null;
            var result = missingParameter == 1
                ? CreateProcessor(null).Process()
                : missingParameter == 2
                    ? CreateProcessor(idUser).Process()
                    : missingParameter == 3
                        ? CreateProcessor(idUser, RandomUtil.RandomString()).Process()
                        : missingParameter == 4
                            ? CreateProcessor(idUser, RandomUtil.RandomString(), RandomUtil.RandomString()).Process()
                            : CreateProcessor(idUser, RandomUtil.RandomString(), RandomUtil.RandomString(), FixedID.Language).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.ParameterMissing, result.ErrorCode);
        }

        [Test, TestCase(1), TestCase(2), TestCase(3), TestCase(4), TestCase(5)]
        public void SetUserDetailsProcessor_InvalidParameter_ErrorCodeInvalidParameterType(byte invalidParameter)
        {
            var idUser = invalidParameter > 1 ? CreateIdForTestingPurpose<User>() : (long?)null;
            ResultBase result;
            switch (invalidParameter)
            {
                case 1:
                    result = CreateProcessor(string.Empty, RandomUtil.RandomString(), RandomUtil.RandomString(), FixedID.Language, true).Process();
                    break;
                case 2:
                    result = CreateProcessor(idUser, 0, RandomUtil.RandomString(), FixedID.Language, true).Process();
                    break;
                case 3:
                    result = CreateProcessor(idUser, RandomUtil.RandomString(), 0, FixedID.Language, true).Process();
                    break;
                case 4:
                    result = CreateProcessor(idUser, RandomUtil.RandomString(), $"{RandomUtil.RandomCharString()}@cycling.com", string.Empty, true).Process();
                    break;
                default:
                    result = CreateProcessor(idUser, RandomUtil.RandomString(), $"{RandomUtil.RandomCharString()}@cycling.com", FixedID.Language, string.Empty).Process();
                    break;
            }

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidParameterType, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_InvalidId_ErrorCodeIdUserNotFound()
        {
            long invalidId = -1;
            var result = CreateProcessor(invalidId, RandomUtil.RandomString(), RandomUtil.RandomString(), FixedID.Language, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdUserNotFound, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_UserNameEmpty_ErrorCodeUserNameCannotBeEmpty()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, string.Empty, RandomUtil.RandomString(), FixedID.Language, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.UserNameCannotBeEmpty, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_UserNameLessThanSixCharacters_ErrorCodeUserNameTooShort()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, RandomUtil.RandomString(5), RandomUtil.RandomString(), FixedID.Language, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.UserNameTooShort, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_UserNameAlreadyUsed_ErrorCodeUserNameMustBeUnique()
        {
            var user = CreateForTestingPurpose<User>();
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, user.Username, RandomUtil.RandomString(), FixedID.Language, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.UserNameMustBeUnique, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_EmailHasInvalidFormat_ErrorCodeInvalidFormatEmail()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, RandomUtil.RandomString(), RandomUtil.RandomString(), FixedID.Language, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.InvalidFormatEmail, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_EmailAlreadyUsed_ErrorCodeEmailMustBeUnique()
        {
            var user = CreateForTestingPurpose<User>(new { email = $"{RandomUtil.RandomCharString()}@cycling.com" });
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, RandomUtil.RandomString(), user.Email, FixedID.Language, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.EmailMustBeUnique, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_InvalidIdLanguage_ErrorCodeIdLanguageNotFound()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var result = CreateProcessor(idUser, RandomUtil.RandomString(), $"{RandomUtil.RandomCharString()}@cycling.com", (byte)0, true).Process();

            Assert.NotNull(result);
            Assert.False(result.Success);
            Assert.AreEqual(ErrorCodes.IdLanguageNotFound, result.ErrorCode);
        }

        [Test]
        public void SetUserDetailsProcessor_ValidInformationProvided_ChangesShortNameLongNameAndLogo()
        {
            var user = CreateForTestingPurpose<User>(new { language = Enums.Communication.Language.Dutch, emailHidden = false });
            var newUserName = RandomUtil.RandomCharString();
            var newEmail = $"{RandomUtil.RandomCharString()}@cycling.com";
            var result = CreateProcessor(user.Id, newUserName, newEmail, Enums.Communication.Language.English, true).Process();

            Assert.NotNull(result);
            Assert.True(result.Success, result.ErrorCode);
            Context.Entry(user).Reload();
            Assert.AreEqual(newUserName, user.Username);
            Assert.AreEqual(newEmail, user.Email);
            Assert.AreEqual(Enums.Communication.Language.English, user.Language);
            Assert.True(user.EmailHidden);
        }

        #endregion
    }
}