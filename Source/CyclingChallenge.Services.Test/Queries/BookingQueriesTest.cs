﻿using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Training;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System;
using System.Linq;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class BookingQueriesTest: TestFixtureBase
    {
        #region GetLastBookingByTeam

        [Test]
        public void GetLastBookingByTeam_InvalidIdTeam_ReturnsNoBooking()
        {
            Assert.IsNull(BookingQueries.GetLastBookingByTeam(Context, -1));
        }

        [Test]
        public void GetLastBookingByTeam_ValidIdTeam_ReturnsLastBooking()
        {
            var team = CreateForTestingPurpose<Team>();
            CreateForTestingPurpose<Booking>(new
            {
                teamId = team.Id,
                departure = DateTime.Today,
                duration = 1
            });
            CreateForTestingPurpose<Booking>(new
            {
                teamId = team.Id,
                departure = DateTime.Today.AddDays(-50),
                duration = 1
            });
            var result = BookingQueries.GetLastBookingByTeam(Context, team.Id);
            Assert.IsNotNull(result);
            Assert.AreEqual(team.Id, result.Team);
            Assert.AreEqual(DateTime.Today, result.Departure);
        }

        #endregion


        #region GetBookingsByTeam

        [Test]
        public void GetBookingsByTeam_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(BookingQueries.GetBookingsByTeam(Context, -1));
        }

        [Test]
        public void GetBookingsByTeam_ValidIdTeam_ReturnsBookingsForTeam()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var booking = CreateForTestingPurpose<Booking>(new { teamId = idTeam });

            var result = BookingQueries.GetBookingsByTeam(Context, idTeam).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(m => m.Team == idTeam));
            Assert.Contains(booking, result);
        }

        #endregion
    }
}
