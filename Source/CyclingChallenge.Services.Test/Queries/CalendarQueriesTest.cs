﻿using NUnit.Framework;
using System;
using System.Data.Entity;
using System.Linq;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class CalendarQueriesTest: TestFixtureBase
    {
        #region GetCalendarByRider

        [Test]
        public void GetCalendarByRider_InvalidIdRider_ReturnsNoCalendarEvents()
        {
            Assert.IsEmpty(CalendarQueries.GetCalendarByRider(Context, -1));
        }

        [Test]
        public void GetCalendarByRider_ValidIdRiderAlreadyDone_ReturnsNoCalendarEvents()
        {
            var rider = CreateForTestingPurpose<Player>();
            CreateForTestingPurpose<Calendar>(new
            {
                playerId = rider.Id,
                teamId = rider.Team,
                endDate = DateTime.Today.AddDays(-30)
            });
            Assert.IsEmpty(CalendarQueries.GetCalendarByRider(Context, rider.Id));
        }

        [Test]
        public void GetCalendarByRider_ValidIdRider_ReturnsCalendarEvents()
        {
            var rider = CreateForTestingPurpose<Player>();
            CreateForTestingPurpose<Calendar>(new
            {
                playerId = rider.Id,
                teamId = rider.Team
            });
            var result = CalendarQueries.GetCalendarByRider(Context, rider.Id);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(c => c.Player == rider.Id));
            Assert.True(result.All(c => DbFunctions.TruncateTime(c.EndDate) >= DbFunctions.TruncateTime(DateTime.Now)));
        }

        #endregion


        #region GetCalendarByTeam

        [Test]
        public void GetCalendarByTeam_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(CalendarQueries.GetCalendarByTeam(Context, -1));
        }

        [Test]
        public void GetCalendarByTeam_ValidIdTeam_ReturnsCalendarForTeam()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idRider = CreateIdForTestingPurpose<Player>();
            var calendar = CreateForTestingPurpose<Calendar>(new
            {
                teamId = idTeam,
                playerId = idRider,
                raceId = FixedID.Race
            });

            var result = CalendarQueries.GetCalendarByTeam(Context, idTeam).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(m => m.Team == idTeam));
            Assert.Contains(calendar, result);
        }

        #endregion
    }
}