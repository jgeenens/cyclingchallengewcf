﻿using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System;
using System.Linq;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class CommunicationQueriesTest : TestFixtureBase
    {
        #region GetPrivateMessageById

        [Test]
        public void GetPrivateMessageById_InvalidId_ReturnsNull()
        {
            Assert.Null(CommunicationQueries.GetPrivateMessageById(Context, -1));
        }

        [Test]
        public void GetPrivateMessageById_ValidId_ReturnsMessage()
        {
            var message = CreateForTestingPurpose<PrivateMessage>();
            Assert.NotNull(message);
            Assert.AreEqual(message.Subject, CommunicationQueries.GetPrivateMessageById(Context, message.Id)?.Subject);
        }

        #endregion


        #region GetPressMessageById

        [Test]
        public void GetPressMessageById_InvalidId_ReturnsNull()
        {
            Assert.Null(CommunicationQueries.GetPressMessageById(Context, -1));
        }

        [Test]
        public void GetPressMessageById_ValidId_ReturnsMessage()
        {
            var user = CreateForTestingPurpose<User>();
            var message = CreateForTestingPurpose<PressMessage>(new { userId = user.Id });
            Assert.NotNull(message);
            Assert.AreEqual(message.Title, CommunicationQueries.GetPressMessageById(Context, message.Id)?.Title);
        }

        #endregion


        #region GetPressMessagesForUser

        [Test]
        public void GetPressMessagesForUser_InvalidIdUser_ReturnsNoMessages()
        {
            Assert.IsEmpty(CommunicationQueries.GetPressMessagesForUser(Context, -1, 0));
        }

        [Test, TestCase(1, false), TestCase(4, false), TestCase(4, true)]
        public void GetPressMessagesForUser_ValidIdUser_ReturnsQuantityOrLessMessagesSorted(long numberOfMessages, bool descending)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            for (var i = 1; i <= numberOfMessages; i++)
            {
                CreateForTestingPurpose<PressMessage>(new { userId = idUser });
            }
            var result = CommunicationQueries.GetPressMessagesForUser(Context, idUser, 0, descending).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(pm => pm.User == idUser));
            Assert.GreaterOrEqual(4, result.ToList().Count);
            Assert.AreEqual(descending, result.First().Date > result.Last().Date);
        }

        #endregion


        #region GetMessagesForUser

        [Test, TestCase(true), TestCase(false)]
        public void GetMessagesForUser_InvalidIdUser_ReturnsNoMessages(bool isRecipient)
        {
            Assert.IsEmpty(CommunicationQueries.GetMessagesForUser(Context, -1, 0, isRecipient));
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetMessagesForUser_ValidIdUserAllMessagesDeleted_ReturnsNoMessages(bool isRecipient)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var pm = isRecipient
                ? CreateForTestingPurpose<PrivateMessage>(new { recipient = idUser, deletedByRecipient = true })
                : CreateForTestingPurpose<PrivateMessage>(new { sender = idUser, deletedBySender = true });
            Assert.IsEmpty(CommunicationQueries.GetMessagesForUser(Context, idUser, 0, isRecipient));
        }

        [Test, TestCase(1, false), TestCase(4, false), TestCase(4, true)]
        public void GetMessagesForUser_ValidIdRecipient_ReturnsQuantityOrLessMessagesSorted(long numberOfMessages, bool descending)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            for (var i = 1; i <= numberOfMessages; i++)
            {
                CreateForTestingPurpose<PrivateMessage>(new { recipient = idUser });
            }
            var result = CommunicationQueries.GetMessagesForUser(Context, idUser, 0, true, descending).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(pm => pm.Recipient == idUser));
            Assert.GreaterOrEqual(4, result.ToList().Count);
            Assert.AreEqual(descending, result.First().Date > result.Last().Date);
        }

        [Test, TestCase(1, false), TestCase(4, false), TestCase(4, true)]
        public void GetMessagesForUser_ValidIdSender_ReturnsQuantityOrLessMessagesSorted(long numberOfMessages, bool descending)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            for (var i = 1; i <= numberOfMessages; i++)
            {
                CreateForTestingPurpose<PrivateMessage>(new { sender = idUser });
            }

            var result = CommunicationQueries.GetMessagesForUser(Context, idUser, 0, false, descending).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(pm => pm.Sender == idUser));
            Assert.GreaterOrEqual(4, result.ToList().Count);
            Assert.AreEqual(descending, result.First().Date > result.Last().Date);
        }

        #endregion


        #region GetOfficialMessagesForUser

        [Test]
        public void GetOfficialMessagesForUser_InvalidIdUser_ReturnsNoMessages()
        {
            Assert.IsEmpty(CommunicationQueries.GetOfficialMessagesForUser(Context, -1, 1));
        }

        [Test, TestCase(2, false), TestCase(4, false), TestCase(4, true)]
        public void GetOfficialMessagesForUser_ValidIdUser_ReturnsQuantityOrLessMessagesSorted(long numberOfMessages, bool descending)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            for (var i = 1; i < numberOfMessages; i++)
            {
                CreateForTestingPurpose<PrivateMessage>(new { recipient = idUser, type = Enums.Message.Type.Official });
            }
            var result = CommunicationQueries.GetOfficialMessagesForUser(Context, idUser, 0, descending).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(pm => pm.Recipient == idUser));
            Assert.GreaterOrEqual(4, result.ToList().Count);
            Assert.AreEqual(descending, result.First().Date > result.Last().Date);
        }

        #endregion


        #region GetBoardById

        [Test]
        public void GetBoardById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(CommunicationQueries.GetBoardById(Context, -1));
        }

        [Test]
        public void GetBoardById_ValidId_ReturnsBoard()
        {
            Assert.AreEqual(FixedID.Board, CommunicationQueries.GetBoardById(Context, FixedID.Board)?.Id);
        }

        #endregion


        #region GetThreadById

        [Test]
        public void GetThreadById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(CommunicationQueries.GetThreadById(Context, -1));
        }

        [Test]
        public void GetThreadById_ValidId_ReturnsThread()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var thread = CreateForTestingPurpose<Thread>(new { userId = idUser });

            var result = CommunicationQueries.GetThreadById(Context, thread.Id);
            Assert.NotNull(result);
            Assert.AreEqual(thread.Title, result.Title);
            Assert.AreEqual(thread.Originator, result.Originator);
        }

        #endregion


        #region GetMessageById

        [Test]
        public void GetMessageById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(CommunicationQueries.GetMessageById(Context, -1));
        }

        [Test]
        public void GetMessageById_ValidId_ReturnsMessage()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId = idUser });
            var message = CreateForTestingPurpose<Message>(new { threadId = idThread, userId = idUser });

            var result = CommunicationQueries.GetMessageById(Context, message.Id);
            Assert.NotNull(result);
            Assert.AreEqual(message.Thread, result.Thread);
            Assert.AreEqual(message.User, result.User);
            Assert.AreEqual(message.MessageInThread, result.MessageInThread);
        }

        #endregion
        
        
        #region GetByType

        [Test]
        public void GetByType_InvalidType_ReturnsEmptyList()
        {
            Assert.IsEmpty(CommunicationQueries.GetByType(Context, 0));
        }

        [Test, TestCase(Enums.Communication.BoardType.Game), TestCase(Enums.Communication.BoardType.National)]
        public void GetByType_ValidType_ReturnsBoards(byte type)
        {
            var result = CommunicationQueries.GetByType(Context, type);
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(b => b.Type == type));
        }

        #endregion


        #region GetThreadsByBoard

        [Test]
        public void GetThreadsByBoard_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(CommunicationQueries.GetThreadsByBoard(Context, -1));
        }

        [Test]
        public void GetThreadsByBoard_ValidId_ReturnsList()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            CreateForTestingPurpose<Thread>(new { userId = idUser });

            var result = CommunicationQueries.GetThreadsByBoard(Context, FixedID.Board);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(t => t.Board == FixedID.Board));
        }

        #endregion


        #region GetMessagesByThread

        [Test]
        public void GetMessagesByThread_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(CommunicationQueries.GetMessagesByThread(Context, -1));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetMessagesByThread_ValidId_ReturnsNonObsoleteList(bool includeObsolete)
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var thread = CreateForTestingPurpose<Thread>(new { userId = idUser });
            CreateForTestingPurpose<Message>(new { threadId = thread.Id, userId = idUser });
            CreateForTestingPurpose<Message>(new { threadId = thread.Id, userId = idUser, obsolete = true });

            var result = CommunicationQueries.GetMessagesByThread(Context, thread.Id, includeObsolete);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(m => m.Thread == thread.Id));
            Assert.AreEqual(includeObsolete, result.Any(m => m.Obsolete));
        }

        #endregion


        #region GetAll

        [Test]
        public void GetAll_ReturnsAllBoards()
        {
            Assert.Contains(FixedID.Board, CommunicationQueries.GetAll(Context).Select(b => b.Id).ToList());
        }

        #endregion


        #region GetThreadViewedByUser

        [Test, TestCase(true, false), TestCase(false, true)]
        public void GetThreadViewedByUser_InvalidId_ReturnsMinValueDateTime(bool useInvalidIdThread, bool useInvalidIdUser)
        {
            var user = CreateForTestingPurpose<User>();
            var idUser = useInvalidIdUser ? -1 : user.Id;
            var idThread = useInvalidIdThread ? -1 : CreateIdForTestingPurpose<Thread>(new { userId = user.Id });

            var result = CommunicationQueries.GetThreadViewedByUser(Context, idThread, idUser);
            Assert.IsNotNull(result);
            Assert.AreEqual(DateTime.MinValue, result);
        }

        [Test]
        public void GetThreadViewedByUser_ValidId_ReturnsLastViewTime()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            var idThread = CreateIdForTestingPurpose<Thread>(new { userId = idUser });
            var threadsViewed = CreateForTestingPurpose<ThreadViewed>(new { userId = idUser, threadId = idThread });

            var result = CommunicationQueries.GetThreadViewedByUser(Context, idThread, idUser);
            Assert.IsNotNull(result);
            Assert.AreEqual(threadsViewed.LastView, result);
        }

        #endregion


        #region GetMaxMessageInThread

        [Test]
        public void GetMaxMessageInThread_InvalidId_ReturnsNull()
        {
            Assert.Null(CommunicationQueries.GetMaxMessageInThread(Context, -1));
        }

        [Test]
        public void GetMaxMessageInThread_ValidId_ReturnsMaxMessageInThread()
        {
            var userId = CreateIdForTestingPurpose<User>();
            var threadId = CreateIdForTestingPurpose<Thread>(new { userId });
            var message = CreateForTestingPurpose<Message>(new { threadId, userId });

            var result = CommunicationQueries.GetMaxMessageInThread(Context, threadId);
            Assert.IsNotNull(result);
            Assert.AreEqual(message.MessageInThread, result);
        }

        #endregion
    }
}