﻿using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Linq;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Test.Utils;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class DictionaryQueriesTest: TestFixtureBase
    {
        #region GetLanguageById

        [Test]
        public void GetLanguageById_InvalidIdLanguage_ReturnsNull()
        {
            Assert.IsNull(DictionaryQueries.GetLanguageById(Context, -1));
        }

        [Test]
        public void GetLanguageById_ValidIdLanguage_ReturnsLanguage()
        {
            Assert.IsNotNull(DictionaryQueries.GetLanguageById(Context, FixedID.Language));
        }

        #endregion


        #region GetLanguageByAbbreviation

        [Test]
        public void GetLanguageByAbbreviation_AbbreviationNullOrEmptyString_ReturnsNull()
        {
            Assert.IsNull(DictionaryQueries.GetLanguageByAbbreviation(Context, null));
            Assert.IsNull(DictionaryQueries.GetLanguageByAbbreviation(Context, string.Empty));
        }

        [Test]
        public void GetLanguageByAbbreviation_UnknownAbbreviation_ReturnsNull()
        {
            Assert.IsNull(DictionaryQueries.GetLanguageByAbbreviation(Context, RandomUtil.RandomString()));
        }

        [Test]
        public void GetLanguageByAbbreviation_KnownAbbreviation_ReturnsLanguage()
        {
            var language = CreateForTestingPurpose<Language>();
            var result = DictionaryQueries.GetLanguageByAbbreviation(Context, language.Abbreviations);
            Assert.IsNotNull(result);
            Assert.AreEqual(language.Abbreviations, result.Abbreviations);
        }

        #endregion


        #region GetDictionaryItemsForLanguage

        [Test]
        public void GetDictionaryItemsForLanguage_InvalidIdLanguage_ReturnsEmptyList()
        {
            Assert.IsEmpty(DictionaryQueries.GetDictionaryItemsForLanguage(Context, -1));
        }

        [Test]
        public void GetDictionaryItemsForLanguage_ValidIdLanguage_ReturnsItemsForLanguage()
        {
            var result = DictionaryQueries.GetDictionaryItemsForLanguage(Context, FixedID.Language);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(i => i.Language == FixedID.Language));
        }

        #endregion


        #region GetDictionaryItemForLanguage

        [Test]
        public void GetDictionaryItemForLanguage_InvalidId_ReturnsNull()
        {
            Assert.IsNull(DictionaryQueries.GetDictionaryItemForLanguage(Context, -1, FixedID.Language));
            Assert.IsNull(DictionaryQueries.GetDictionaryItemForLanguage(Context, Enums.Items.TrainingHills, 0));
        }

        [Test]
        public void GetDictionaryItemForLanguage_ValidId_ReturnsItemForLanguage()
        {
            var result = DictionaryQueries.GetDictionaryItemForLanguage(Context, Enums.Items.TrainingHills, FixedID.Language);
            Assert.NotNull(result);
            Assert.AreEqual(Enums.Items.TrainingHills, result.Item);
            Assert.AreEqual(FixedID.Language, result.Language);
            Assert.IsNotEmpty(result.Text);
        }

        #endregion
    }
}