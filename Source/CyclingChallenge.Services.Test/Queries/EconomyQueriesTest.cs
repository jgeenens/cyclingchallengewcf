﻿using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Location;
using Economy = CyclingChallenge.Services.Models.Economy.Economy;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class EconomyQueriesTest: TestFixtureBase
    {
        #region GetByTeam

        [Test]
        public void GetByTeam_InvalidIdTeam_ReturnsNull()
        {
            Assert.Null(EconomyQueries.GetByTeam(Context, -1));
        }

        [Test]
        public void GetByTeam_ValidIdTeam_ReturnsOnlyActiveEconomy()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            CreateForTestingPurpose<Economy>(new { teamId = idTeam, active = Enums.Economy.Status.Inactive });
            var idActiveEconomy = CreateIdForTestingPurpose<Economy>(new { teamId = idTeam });

            var result = EconomyQueries.GetByTeam(Context, idTeam);
            Assert.NotNull(result);
            Assert.AreEqual(idActiveEconomy, result.Id);
            Assert.AreEqual(Enums.Economy.Status.Active, result.Active);
        }

        #endregion


        #region GetEconomiesByTeam

        [Test]
        public void GetEconomiesByTeam_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(EconomyQueries.GetEconomiesByTeam(Context, -1));
        }

        [Test]
        public void GetEconomiesByTeam_ValidIdTeam_ReturnsEconomiesForTeam()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var economy = CreateForTestingPurpose<Economy>(new { teamId = idTeam });

            var result = EconomyQueries.GetEconomiesByTeam(Context, idTeam).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(m => m.Team == idTeam));
            Assert.Contains(economy, result);
        }

        #endregion


        #region GetTransactionsByTeam

        [Test]
        public void GetTransactionsByTeam_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(EconomyQueries.GetTransactionsByTeam(Context, -1));
        }

        [Test]
        public void GetTransactionsByTeam_NotIncludeCheckedButAllChecked_ReturnsEmptyList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            CreateForTestingPurpose<Transaction>(new { teamId = idTeam, alreadyChecked = true });
            Assert.IsEmpty(EconomyQueries.GetTransactionsByTeam(Context, idTeam, false));
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetTransactionsByTeam_ValidIdTeam_ReturnsList(bool includeChecked)
        {
            CreateForTestingPurpose<Transaction>();
            CreateForTestingPurpose<Transaction>(new { alreadyChecked = includeChecked });

            var result = EconomyQueries.GetTransactionsByTeam(Context, FixedID.Team, includeChecked);
            Assert.IsNotEmpty(result);
            Assert.AreEqual(includeChecked, result.Any(t => t.Checked));
            Assert.True(result.All(t => t.Team == FixedID.Team));
        }

        #endregion


        #region GetTransactionsByTeamAndTimeFrame

        [Test]
        public void GetTransactionsByTeamAndTimeFrame_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(EconomyQueries.GetTransactionsByTeamAndTimeFrame(Context, -1, DateTime.Now.AddYears(-10)));
        }

        [Test]
        public void GetTransactionsByTeamAndTimeFrame_NoneWithinTimeFrame_ReturnsEmptyList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            CreateForTestingPurpose<Transaction>(new { teamId = idTeam });
            Assert.IsEmpty(EconomyQueries.GetTransactionsByTeamAndTimeFrame(Context, idTeam, DateTime.Now.AddYears(-10), DateTime.Now.AddYears(-9)));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetTransactionsByTeamAndTimeFrame_ValidIdTeam_ReturnsList(bool withEndLimit)
        {
            CreateForTestingPurpose<Transaction>();

            var from = DateTime.Now.AddDays(-1);
            var to = withEndLimit ? DateTime.Now.AddDays(1) : (DateTime?)null;
            var result = EconomyQueries.GetTransactionsByTeamAndTimeFrame(Context, FixedID.Team, from, to);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(t => t.Team == FixedID.Team));
            Assert.True(result.All(t => t.DateTransaction >= from));
            Assert.True(result.All(t => t.DateTransaction <= (to ?? DateTime.MaxValue)));
        }

        #endregion


        #region GetCurrencyById

        [Test]
        public void GetCurrencyById_InvalidId_ReturnsNull()
        {
            Assert.Null(EconomyQueries.GetCurrencyById(Context, -1));
        }

        [Test]
        public void GetCurrencyById_ValidId_ReturnsCurrency()
        {
            var result = EconomyQueries.GetCurrencyById(Context, Enums.Economy.Currency.Dollar);
            Assert.NotNull(result);
            Assert.AreEqual(Enums.Economy.Currency.Dollar, result.Id);
        }

        #endregion


        #region GetCurrencyByCountry

        [Test]
        public void GetCurrencyByCountry_InvalidCountry_ReturnsNull()
        {
            Assert.Null(EconomyQueries.GetCurrencyByCountry(Context, -1));
        }

        [Test]
        public void GetCurrencyByCountry_ValidCountry_ReturnsCurrency()
        {
            var country = CreateForTestingPurpose<Country>();
            var currency = CreateForTestingPurpose<Currency>(new { countryId = country.Id });
            var result = EconomyQueries.GetCurrencyByCountry(Context, country.Id);
            Assert.NotNull(result);
            Assert.AreEqual(currency.Id, result.Id);
        }

        #endregion


        #region GetBalanceCredit

        [Test]
        public void GetBalanceCredit_InvalidId_ReturnsNull()
        {
            Assert.Null(EconomyQueries.GetBalanceCredit(Context, -1));
        }

        [Test]
        public void GetBalanceCredit_ValidId_ReturnsCreditLimit()
        {
            var team = CreateForTestingPurpose<Team>();
            var transaction = CreateForTestingPurpose<Transaction>(new { teamId = team.Id });

            var result = EconomyQueries.GetBalanceCredit(Context, team.Id);

            Assert.NotNull(result);
            var setting = GameQueries.GetSettingById(Context, Settings.Credit);
            Assert.NotNull(setting);
            var balance = team.Predicted + transaction.Value * (transaction.Outgoing ? -1 : 1) + long.Parse(setting.Value);
            Assert.AreEqual(balance, result);
        }

        #endregion
    }
}