﻿using System;
using System.Collections.Generic;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Team;
using Word = CyclingChallenge.Services.Enums.Word;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class GameQueriesTest: TestFixtureBase
    {
        #region GetAddonById

        [Test]
        public void GetAddonById_InvalidId_ReturnsNull()
        {
            Assert.Null(GameQueries.GetAddonById(Context, -1));
        }

        [Test]
        public void GetAddonById_ValidId_ReturnsAddon()
        {
            Assert.AreEqual(FixedID.Addon, GameQueries.GetAddonById(Context, FixedID.Addon)?.Id);
        }

        #endregion


        #region UserAlreadyHasLogo

        [Test]
        public void UserAlreadyHasLogo_InvalidId_ReturnsFalse()
        {
            var idUser = CreateIdForTestingPurpose<User>();
            Assert.False(GameQueries.UserAlreadyHasLogo(Context, -1, FixedID.Logo));
            Assert.False(GameQueries.UserAlreadyHasLogo(Context, idUser, 0));
        }

        [Test]
        public void UserAlreadyHasLogo_ValidUserAndLogo_ReturnsTrue()
        {
            var userId = CreateIdForTestingPurpose<User>();
            CreateForTestingPurpose<Sale>(new { userId, status = FixedID.Logo });
            Assert.True(GameQueries.UserAlreadyHasLogo(Context, userId, FixedID.Logo));
        }

        #endregion


        #region GetRegistrationsForToday

        [Test]
        public void GetRegistrationsForToday_InvalidIdCountry_ReturnsNull()
        {
            Assert.Null(GameQueries.GetRegistrationsForToday(Context, -1));
        }

        [Test]
        public void GetRegistrationsForToday_ValidIdCountry_ReturnsRegistrations()
        {
            var countryId = CreateIdForTestingPurpose<Country>();
            var registration = CreateForTestingPurpose<Registration>(new { countryId });

            var result = GameQueries.GetRegistrationsForToday(Context, (short)countryId);
            Assert.NotNull(result);
            Assert.AreEqual(DateTime.Today.Date, registration.Date.Date);
            Assert.Less(0, registration.Quantity);
        }

        #endregion


        #region GetAllProducts

        [Test]
        public void GetAllProducts_ReturnsAllProducts()
        {
            Assert.Contains(FixedID.StoreProduct, GameQueries.GetAllProducts(Context).Select(prod => prod.Id).ToList());
        }

        #endregion


        #region GetPriceByProductAndCurrency

        [Test]
        public void GetPriceByProductAndCurrency_InvalidId_ReturnsNull()
        {
            Assert.Null(GameQueries.GetPriceByProductAndCurrency(Context, -1, Enums.Economy.Currency.Dollar));
            Assert.Null(GameQueries.GetPriceByProductAndCurrency(Context, FixedID.StoreProduct, -1));
        }

        [Test]
        public void GetPriceByProductAndCurrency_ValidId_ReturnsPrice()
        {
            var result = GameQueries.GetPriceByProductAndCurrency(Context, FixedID.StoreProduct, Enums.Economy.Currency.Dollar);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.StoreProduct, result.Commodity);
            Assert.AreEqual(Enums.Economy.Currency.Dollar, result.Currency);
        }

        #endregion


        #region GetRandomWordByType

        [Test]
        public void GetRandomWordByType_InvalidType_ReturnsNull()
        {
            Assert.Null(GameQueries.GetRandomWordByType(Context, byte.MaxValue));
        }

        [Test, TestCase(Word.Team), TestCase(Word.Adjective), TestCase(Word.Adverb), TestCase(Word.Name), 
         TestCase(Word.Noun), TestCase(Word.The), TestCase(Word.Royal), TestCase(Word.Sparta)]
        public void GetRandomWordByType_ValidType_ReturnsWordOfType(byte type)
        {
            var result = GameQueries.GetRandomWordByType(Context, type);
            Assert.NotNull(result);
            Assert.AreEqual(type, result.Type);
        }

        #endregion


        #region GetSettingById

        [Test]
        public void GetSettingById_InvalidId_ReturnsNull()
        {
            Assert.Null(GameQueries.GetSettingById(Context, -1));
        }

        [Test]
        public void GetSettingById_ValidId_ReturnsAddon()
        {
            Assert.AreEqual(Settings.CampUpdate, GameQueries.GetSettingById(Context, Settings.CampUpdate)?.Id);
        }

        #endregion


        #region GetSettingsById

        [Test]
        public void GetSettingsById_EmptyList_ReturnsEmptyList()
        {
            Assert.IsEmpty(GameQueries.GetSettingsById(Context, new List<long>()));
        }

        [Test]
        public void GetSettingsById_InvalidIds_ReturnsEmptyList()
        {
            Assert.IsEmpty(GameQueries.GetSettingsById(Context, new List<long> { -1 }));
        }

        [Test]
        public void GetSettingsById_ValidIds_ReturnsListOfRequestedSettings()
        {
            var result = GameQueries.GetSettingsById(Context, new List<long> { Settings.Credit, Settings.MinValues, Settings.Supporters }).ToList();
            Assert.IsNotEmpty(result);
            Assert.Contains(Settings.Credit, result.Select(s => s.Id).ToList());
            Assert.Contains(Settings.MinValues, result.Select(s => s.Id).ToList());
            Assert.Contains(Settings.Supporters, result.Select(s => s.Id).ToList());
        }

        #endregion


        #region GetStartSeason

        [Test, TestCase(false), TestCase(true)]
        public void GetStartSeason_ReturnsDateTime(bool withSeason)
        {
            var result = withSeason
                ? GameQueries.GetStartSeason(Context, 3)
                : GameQueries.GetStartSeason(Context);

            var seasonStart = GameQueries.GetSettingById(Context, Enums.Settings.SeasonStart);
            Assert.NotNull(seasonStart);
            Assert.AreEqual(seasonStart.Value,
                withSeason ? result.AddDays(-(2 * 7 * 12)).ToString("yyyy/MM/dd") : result.ToString("yyyy/MM/dd"));
        }

        #endregion


        #region GetWeekAndDayInSeason

        [Test, TestCase(false), TestCase(true)]
        public void GetWeekAndDayInSeason_ReturnsWeekAndDay(bool withSeason)
        {
            var result = withSeason
                ? GameQueries.GetWeekAndDayInSeason(Context, 3)
                : GameQueries.GetWeekAndDayInSeason(Context);
            Assert.NotNull(result);

            var daysInSeason = (DateTime.Now - GameQueries.GetStartSeason(Context, withSeason ? 3 : (byte?)null)).TotalDays;
            Assert.NotNull(daysInSeason);
            Assert.AreEqual((byte)Math.Floor((daysInSeason - 1) / 7), result.Week);
            Assert.AreEqual((byte)((daysInSeason - 1) % 7 + 1), result.Day);
        }

        #endregion
    }
}