﻿using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Linq;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Test.Utils;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class LocationQueriesTest : TestFixtureBase
    {
        #region GetCityById

        [Test]
        public void GetCityById_InvalidIdCity_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetCityById(Context, -1));
        }

        [Test]
        public void GetCityById_ValidIdCity_ReturnsCity()
        {
            Assert.IsNotNull(LocationQueries.GetCityById(Context, FixedID.City));
        }

        #endregion


        #region GetRandomCityByCountry

        [Test]
        public void GetRandomCityByCountry_InvalidCountry_ReturnsNull()
        {
            Assert.Null(LocationQueries.GetRandomCityByCountry(Context, -1));
        }

        [Test]
        public void GetRandomCityByCountry_ValidCountry_ReturnsACityFromRequestedCountry()
        {
            var result = LocationQueries.GetRandomCityByCountry(Context, FixedID.ActiveCountry);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.ActiveCountry, result.CityRegion.Country);
        }

        #endregion


        #region GetClimbById

        [Test]
        public void GetClimbById_InvalidIdClimb_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetClimbById(Context, -1));
        }

        [Test]
        public void GetClimbById_ValidIdClimb_ReturnsClimb()
        {
            Assert.IsNotNull(LocationQueries.GetClimbById(Context, FixedID.Climb));
        }

        #endregion


        #region GetCountryById

        [Test]
        public void GetCountryById_InvalidIdCountry_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetCountryById(Context, -1));
        }

        [Test]
        public void GetCountryById_ValidIdCountry_ReturnsCountry()
        {
            Assert.IsNotNull(LocationQueries.GetCountryById(Context, FixedID.Country));
        }

        #endregion


        #region GetCountryByName

        [Test]
        public void GetCountryByName_UnknownName_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetCountryByName(Context, RandomUtil.RandomString()));
        }

        [Test]
        public void GetCountryByName_KnownName_ReturnsCountry()
        {
            var country = CreateForTestingPurpose<Country>();
            var result = LocationQueries.GetCountryByName(Context, country.Name);
            Assert.IsNotNull(result);
            Assert.AreEqual(country.Name, result.Name);
        }

        #endregion


        #region GetRandomActiveCountry

        [Test]
        public void GetRandomActiveCountry_ReturnsACountryWithActiveTrueAndNameDifferentFromWorld()
        {
            var result = LocationQueries.GetRandomActiveCountry(Context);
            Assert.NotNull(result);
            Assert.True(result.Active);
            Assert.AreNotEqual("World", result.Name);
        }

        #endregion

        
        #region GetRegionByCity

        [Test]
        public void GetRegionByCity_InvalidIdCity_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetRegionByCity(Context, -1));
        }

        [Test]
        public void GetRegionByCity_ValidIdCity_ReturnsRegion()
        {
            Assert.IsNotNull(LocationQueries.GetRegionByCity(Context, FixedID.City));
        }

        #endregion


        #region GetLocationById

        [Test]
        public void GetLocationById_InvalidIdLocation_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetLocationById(Context, -1));
        }

        [Test]
        public void GetLocationById_ValidIdLocation_ReturnsLocation()
        {
            Assert.IsNotNull(LocationQueries.GetLocationById(Context, FixedID.Location));
        }

        #endregion


        #region GetAll

        [Test]
        public void GetAll_ReturnsAllLocations()
        {
            Assert.Contains(FixedID.Location, LocationQueries.GetAll(Context).Select(loc => loc.Id).ToList());
        }

        #endregion


        #region GetLocationTextById

        [Test]
        public void GetLocationTextById_ValidId_ReturnsTextWithCountryAndCity()
        {
            var location = LocationQueries.GetLocationById(Context, FixedID.Location);
            var cityName = $"{location.LocationCity.Name} {(location.LocationCity.Name != location.LocationCity.Village ? $"({location.LocationCity.Village})": string.Empty)}".Trim();
            string expected = $"[[CNTRY({location.LocationCountry.Item})]] {cityName}";
            Assert.AreEqual(expected, LocationQueries.GetLocationTextById(Context, FixedID.Location));
        }

        #endregion


        #region GetCountryText

        [Test]
        public void GetCountryText_CountryIsNull_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetCountryText(null));
        }

        [Test]
        public void GetCountryText_CountryNotNull_ReturnsCountryFunction()
        {
            Assert.AreEqual($"[[CNTRY({FixedID.Country})]]", LocationQueries.GetCountryText(FixedID.Country));
        }

        #endregion


        #region GetCityText

        [Test]
        public void GetCityText_CityIsNull_ReturnsNull()
        {
            Assert.IsNull(LocationQueries.GetCityText(null));
        }

        [Test]
        public void GetCityText_CityAndVillageEqual_ReturnsOnlyCityName()
        {
            var city = CreateForTestingPurpose<City>();
            Assert.AreEqual(city.Name, city.Village);

            var result = LocationQueries.GetCityText(city);
            Assert.NotNull(result);
            Assert.AreEqual(city.Name, result);
        }

        [Test]
        public void GetCityText_CityAndVillageNotEqual_ReturnsCityAndVillageName()
        {
            var city = CreateForTestingPurpose<City>(new { village = RandomUtil.RandomCharString() });
            Assert.AreNotEqual(city.Name, city.Village);

            var result = LocationQueries.GetCityText(city);
            Assert.NotNull(result);
            Assert.AreEqual($"{city.Name} ({city.Village})", result);
        }

        #endregion
    }
}
