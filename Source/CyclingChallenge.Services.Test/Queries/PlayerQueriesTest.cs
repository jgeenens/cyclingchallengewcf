﻿using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Test.Utils;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class PlayerQueriesTest : TestFixtureBase
    {
        #region GetPlayerById

        [Test]
        public void GetPlayerById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(PlayerQueries.GetPlayerById(Context, -1));
        }

        [Test]
        public void GetPlayerById_ValidId_ReturnsPlayer()
        {
            var player = CreateForTestingPurpose<Player>();
            Assert.NotNull(player);
            Assert.AreEqual(player.LastName, PlayerQueries.GetPlayerById(Context, player.Id)?.LastName);
        }

        #endregion


        #region GetPlayerValuesById

        [Test]
        public void GetPlayerValuesById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(PlayerQueries.GetPlayerValuesByPlayer(Context, -1));
        }

        [Test]
        public void GetPlayerValuesById_ValidId_ReturnsPlayerValues()
        {
            var playerId = CreateIdForTestingPurpose<Player>();
            var playerValue = CreateForTestingPurpose<PlayerValue>(new { playerId });
            Assert.NotNull(playerValue);
            Assert.AreEqual(playerValue.Player, PlayerQueries.GetPlayerValuesByPlayer(Context, playerId)?.Player);
        }

        #endregion


        #region GetPlayersForTeam

        [Test]
        public void GetPlayersForTeam_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetPlayersForTeam(Context, -1));
        }

        [Test]
        public void GetPlayersForTeam_ValidIdNoValues_ReturnsEmptyList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = idTeam });
            Assert.NotNull(player);
            Assert.IsEmpty(PlayerQueries.GetPlayersForTeam(Context, idTeam));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetPlayersForTeam_ValidIdYouthNoYouth_ReturnsEmptyList(bool forYouth)
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = idTeam });
            CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id, isYouth = !forYouth });
            Assert.NotNull(player);
            Assert.IsEmpty(PlayerQueries.GetPlayersForTeam(Context, idTeam, forYouth));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetPlayersForTeam_ValidId_ReturnsListWithPlayerValues(bool forYouth)
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var player = CreateForTestingPurpose<Player>(new { teamId = idTeam });
            CreateForTestingPurpose<PlayerValue>(new { playerId = player.Id, isYouth = forYouth });
            Assert.NotNull(player);

            var result = PlayerQueries.GetPlayersForTeam(Context, idTeam, forYouth);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(p => p.IsYouth == forYouth));
            Assert.True(result.All(p => p.PlayerValuePlayer != null));
            Assert.True(result.All(p => p.PlayerValuePlayer.Team == idTeam));
        }

        #endregion


        #region GetPlayersForRace

        [Test]
        public void GetPlayersForRace_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetPlayersForRace(Context, -1));
        }

        [Test]
        public void GetPlayersForRace_ValidId_ReturnsList()
        {
            var raceId = CreateIdForTestingPurpose<Race>();
            var teamId = CreateIdForTestingPurpose<Team>();
            var playerId = CreateIdForTestingPurpose<Player>(new { teamId });
            var playerValue = CreateForTestingPurpose<PlayerValue>(new { playerId, raceId });

            var result = PlayerQueries.GetPlayersForRace(Context, raceId).ToList();
            Assert.IsNotEmpty(result);
            Assert.Contains(playerValue, result);
        }

        #endregion


        #region GetPlayersByName

        [Test, TestCase(null, null), TestCase("", ""), TestCase(null, ""), TestCase("", null)]
        public void GetPlayersByName_FirstNameAndLastNameEmptyOrNull_ReturnsNull(string firstName, string lastName)
        {
            Assert.IsNull(PlayerQueries.GetPlayersByName(Context, firstName, lastName));
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetPlayersByName_InvalidFirstNameOrLastName_ReturnsEmptyList(bool firstNameInvalid)
        {
            var firstName = firstNameInvalid ? Environment.NewLine : string.Empty;
            var lastName = firstNameInvalid ? string.Empty : Environment.NewLine;
            Assert.IsEmpty(PlayerQueries.GetPlayersByName(Context, firstName, lastName));
        }

        [Test, TestCase(true, false), TestCase(false, true), TestCase(true, true)]
        public void GetPlayersByName_ValidFirstNameOrLastName_ReturnsList(bool firstNameNeedleProvided, bool lastNameNeedleProvided)
        {
            var player = CreateForTestingPurpose<Player>();
            Assert.GreaterOrEqual(player.FirstName.Length, 3);
            Assert.GreaterOrEqual(player.LastName.Length, 3);
            var firstName = firstNameNeedleProvided ? player.FirstName.Substring(0, 3) : string.Empty;
            var lastName = lastNameNeedleProvided ? player.LastName.Substring(0, 3) : string.Empty;

            var result = PlayerQueries.GetPlayersByName(Context, firstName, lastName).ToList();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(p => p.FirstName.ToLower().StartsWith(firstName.ToLower()) && p.LastName.ToLower().StartsWith(lastName.ToLower())));
        }

        #endregion


        #region GetPlayersFromRanking

        [Test]
        public void GetPlayersFromRanking_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetPlayersFromRanking(Context, -1));
            Assert.IsEmpty(PlayerQueries.GetPlayersFromRanking(Context, FixedID.Race, -1));
        }

        [Test]
        public void GetPlayersFromRanking_ValidId_ReturnsList()
        {
            var raceId = CreateIdForTestingPurpose<Race>();
            var teamId = CreateIdForTestingPurpose<Team>();
            var riders = new List<Player>();
            for (var i = 0; i < 3; i++)
            {
                riders.Add(CreateForTestingPurpose<Player>(new { teamId }));
            }
            var ranking = string.Join("|", riders.Select(r => $"{RandomUtil.RandomNumber(5)},{r.Id}").OrderBy(s => s));
            CreateForTestingPurpose<RaceRanking>(new { raceId, ranking });

            var result = PlayerQueries.GetPlayersFromRanking(Context, raceId).ToList();
            Assert.IsNotEmpty(result);
            riders.ForEach(expected => Assert.IsNotNull(result.FirstOrDefault(r => r.Id == expected.Id)));
        }

        #endregion


        #region GetPlayerProgressForPlayer

        [Test]
        public void GetPlayerProgressForPlayer_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetPlayerProgressForPlayer(Context, -1));
        }

        [Test]
        public void GetPlayerProgressForPlayer_ValidId_ReturnsList()
        {
            var idPlayer = CreateIdForTestingPurpose<Player>();
            var idPlayerProgress = CreateIdForTestingPurpose<PlayerProgress>(new { playerId = idPlayer });

            var result = PlayerQueries.GetPlayerProgressForPlayer(Context, idPlayer);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(pp => pp.Player == idPlayer));
            Assert.Contains(idPlayerProgress, result.Select(pp => pp.Id).ToList());
        }

        #endregion


        #region GetScoutedPlayers

        [Test]
        public void GetScoutedPlayers_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetScoutedPlayers(Context, -1));
        }

        [Test]
        public void GetScoutedPlayers_ValidId_ReturnsListWithPlayer()
        {
            var idScout = CreateIdForTestingPurpose<Scout>();
            var scoutedPlayer = CreateForTestingPurpose<ScoutedPlayer>(new { scoutId = idScout });
            Assert.NotNull(scoutedPlayer?.Team);

            var result = PlayerQueries.GetScoutedPlayers(Context, FixedID.Team).ToList();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.Contains(scoutedPlayer, result);
        }

        #endregion


        #region GetTransferHistoryForRider

        [Test]
        public void GetTransferHistoryForRider_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetTransferHistoryForRider(Context, -1, DateTime.Now.AddYears(-10)));
        }

        [Test]
        public void GetTransferHistoryForRider_NoneWithinTimeFrame_ReturnsEmptyList()
        {
            var idPlayer = CreateIdForTestingPurpose<Player>();
            var idTeam = CreateIdForTestingPurpose<Team>();
            CreateForTestingPurpose<TransferHistory>(new { playerId = idPlayer, teamId = idTeam });
            Assert.IsEmpty(PlayerQueries.GetTransferHistoryForRider(Context, idTeam, DateTime.Now.AddYears(-10), DateTime.Now.AddYears(-9)));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetTransferHistoryForRider_ValidId_ReturnsList(bool withEndLimit)
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var playerId = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            CreateForTestingPurpose<TransferHistory>(new
            {
                playerId,
                fromTeamId = idTeam,
                date = DateTime.Now
            });

            var from = DateTime.Now.AddDays(-1);
            var to = withEndLimit ? DateTime.Now.AddDays(1) : (DateTime?)null;
            var result = PlayerQueries.GetTransferHistoryForRider(Context, playerId, from, to);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Player == playerId));
            Assert.True(result.All(h => h.Type == Enums.Rider.Transfer.Finished));
            Assert.True(result.All(t => t.DateTransfer >= from));
            Assert.True(result.All(t => t.DateTransfer <= (to ?? DateTime.MaxValue)));
        }

        #endregion


        #region GetYouthHistoriesByTeam

        [Test]
        public void GetYouthHistoriesByTeam_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(PlayerQueries.GetYouthHistoriesByTeam(Context, -1));
        }

        [Test]
        public void GetYouthHistoriesByTeam_ValidIdTeam_ReturnsYouthHistoriesForTeam()
        {
            var teamId = CreateIdForTestingPurpose<Team>();
            var playerId = CreateIdForTestingPurpose<Player>(new { teamId, age = 16 });
            var youth = CreateForTestingPurpose<Youth>(new { playerId });

            var result = PlayerQueries.GetYouthHistoriesByTeam(Context, teamId).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(y => y.YouthPlayer.Team == teamId));
            Assert.Contains(youth, result);
        }

        #endregion


        #region GetNameMaxValueByCountry

        [Test, TestCase(true), TestCase(false)]
        public void GetNameMaxValueByCountry_InvalidCountry_ReturnsNull(bool isFirstName)
        {
            Assert.Null(PlayerQueries.GetNameMaxValueByCountry(Context, -1, isFirstName));
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetNameMaxValueByCountry_ValidCountry_ReturnsANumber(bool isFirstName)
        {
            var result = PlayerQueries.GetNameMaxValueByCountry(Context, FixedID.ActiveCountry, isFirstName);
            Assert.IsNotNull(result);
            Assert.Less(0, result);
        }

        #endregion


        #region GetRandomNameByCountry

        [Test, TestCase(true), TestCase(false)]
        public void GetRandomNameByCountry_InvalidCountry_ReturnsNull(bool isFirstName)
        {
            Assert.Null(PlayerQueries.GetRandomNameByCountry(Context, -1, RandomUtil.RandomNumber(), isFirstName));
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetRandomNameByCountry_MaxNameOutOfRange_ReturnsNull(bool isFirstName)
        {
            Assert.Null(PlayerQueries.GetRandomNameByCountry(Context, FixedID.ActiveCountry, -1, isFirstName));
        }

        [Test, TestCase(true), TestCase(false)]
        public void GetRandomNameByCountry_ValidCountryAndMaxNameInRange_ReturnsAName(bool isFirstName)
        {
            var result = PlayerQueries.GetRandomNameByCountry(Context, FixedID.ActiveCountry, RandomUtil.RandomNumber(), isFirstName);
            Assert.IsNotNull(result);
            Assert.IsNotNullOrEmpty(result);
        }

        #endregion
    }
}