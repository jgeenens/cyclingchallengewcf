﻿using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Linq;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class ProductQueriesTest : TestFixtureBase
    {
        #region GetById

        [Test]
        public void GetById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(ProductQueries.GetById(Context, 0));
        }

        [Test]
        public void GetById_ValidId_ReturnsProduct()
        {
            Assert.IsNotNull(ProductQueries.GetById(Context, FixedID.Product));
        }

        #endregion


        #region GetAll

        [Test]
        public void GetAll_ReturnsAllProducts()
        {
            Assert.Contains(FixedID.Product, ProductQueries.GetAll(Context).Select(p => p.Id).ToList());
        }

        #endregion
    }
}
