﻿using System;
using System.Linq;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class RaceQueriesTest : TestFixtureBase
    {
        #region GetTrackStageById

        [Test]
        public void GetTrackByStageId_InvalidId_ReturnsNull()
        {
            Assert.Null(RaceQueries.GetTrackByStageId(Context, -1));
        }

        [Test]
        public void GetTrackByStageId_ValidId_ReturnsTrack()
        {
            var raceDescriptionId = CreateIdForTestingPurpose<RaceDescription>();
            CreateForTestingPurpose<Race>(new { raceDescriptionId });
            var stage = CreateForTestingPurpose<Stage>(new { raceDescriptionId });

            var result = RaceQueries.GetTrackByStageId(Context, stage.Id);

            Assert.NotNull(result);
            Assert.AreEqual(FixedID.Track, result.Id);
            Assert.AreEqual(stage.StageTrack.Name, result.Name);
        }

        #endregion


        #region GetTrackByRaceAndStage

        [Test, TestCase(true), TestCase(false)]
        public void GetTrackByRaceAndStage_InvalidId_ReturnsNull(bool isIdStageValid)
        {
            var invalidId = -1;
            var result = isIdStageValid
                ? RaceQueries.GetTrackByRaceAndStage(Context, FixedID.Race, (short)invalidId)
                : RaceQueries.GetTrackByRaceAndStage(Context, invalidId, FixedID.Stage);

            Assert.Null(result);
        }

        [Test]
        public void GetTrackByRaceAndStage_ValidId_ReturnsTrack()
        {
            var track = CreateForTestingPurpose<Track>();
            var idRaceDescription = CreateIdForTestingPurpose<RaceDescription>();
            var idRace = CreateIdForTestingPurpose<Race>(new { raceDescriptionId = idRaceDescription });
            var stage = CreateForTestingPurpose<Stage>(new { trackId = track.Id, raceDescriptionId = idRaceDescription });

            var result = RaceQueries.GetTrackByRaceAndStage(Context, idRace, stage.Index);

            Assert.NotNull(result);
            Assert.AreEqual(track.Id, result.Id);
            Assert.AreEqual(track.Name, result.Name);
        }

        #endregion


        #region GetRaceById

        [Test]
        public void GetRaceById_InvalidId_ReturnsNull()
        {
            Assert.Null(RaceQueries.GetRaceById(Context, -1));
        }

        [Test]
        public void GetRaceById_ValidId_ReturnsRaceWithRaceDescription()
        {
            var raceDescription = CreateForTestingPurpose<RaceDescription>();
            var race = CreateForTestingPurpose<Race>(new { raceDescriptionId = raceDescription.Id });

            var result = RaceQueries.GetRaceById(Context, race.Id);
            Assert.NotNull(result);
            Assert.AreEqual(race.Id, result.Id);
            Assert.AreEqual(race.StartDate, result.StartDate);
            Assert.AreEqual(race.RaceDescription, raceDescription.Id);
            Assert.AreEqual(raceDescription.Name, result.RaceRaceDescription.Name);
        }

        #endregion


        #region GetStageById

        [Test]
        public void GetStageById_InvalidId_ReturnsNull()
        {
            Assert.Null(RaceQueries.GetStageById(Context, -1));
        }

        [Test]
        public void GetStageById_ValidId_ReturnsStage()
        {
            var result = RaceQueries.GetStageById(Context, FixedID.Stage);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.Stage, result.Id);
        }

        #endregion


        #region GetStagesByRaceDescription

        [Test]
        public void GetStagesByRaceDescription_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(RaceQueries.GetStagesByRaceDescription(Context, -1));
        }

        [Test]
        public void GetStagesByRaceDescription_ValidId_ReturnsList()
        {
            var result = RaceQueries.GetStagesByRaceDescription(Context, FixedID.RaceDescription).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(s => s.RaceDescription == FixedID.RaceDescription));
        }

        #endregion


        #region GetStageByRaceDescriptionAndIndex

        [Test]
        public void GetStageByRaceDescriptionAndIndex_InvalidId_ReturnsNull()
        {
            Assert.IsNull(RaceQueries.GetStageByRaceDescriptionAndIndex(Context, -1, 1));
            Assert.IsNull(RaceQueries.GetStageByRaceDescriptionAndIndex(Context, FixedID.RaceDescription, -1));
        }

        [Test]
        public void GetStageByRaceDescriptionAndIndex_ValidId_ReturnsStage()
        {
            var result = RaceQueries.GetStageByRaceDescriptionAndIndex(Context, FixedID.TourRaceDescription, 1);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.TourRaceDescription, result.RaceDescription);
            Assert.AreEqual(1, result.Index);
        }

        #endregion


        #region GetRaceDescriptionById

        [Test]
        public void GetRaceDescriptionById_InvalidId_ReturnsNull()
        {
            Assert.Null(RaceQueries.GetRaceDescriptionById(Context, -1));
        }

        [Test]
        public void GetRaceDescriptionById_ValidId_ReturnsRaceDescription()
        {
            var result = RaceQueries.GetRaceDescriptionById(Context, FixedID.RaceDescription);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.RaceDescription, result.Id);
        }

        #endregion


        #region GetRaceHistoryForRider

        [Test]
        public void GetRaceHistoryForRider_InvalidRider_ReturnsEmptyList()
        {
            Assert.IsEmpty(RaceQueries.GetRaceHistoryForRider(Context, -1));
        }

        [Test]
        public void GetRaceHistoryForRider_ValidRider_ReturnsList()
        {
            var idPlayer = CreateIdForTestingPurpose<Player>();
            CreateForTestingPurpose<RaceHistory>(new { playerId = idPlayer });

            var result = RaceQueries.GetRaceHistoryForRider(Context, idPlayer);

            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Player == idPlayer));
        }

        #endregion


        #region GetByDivisionWithinTimeFrame

        [Test]
        public void GetByDivisionWithinTimeFrame_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(RaceQueries.GetByDivisionWithinTimeFrame(Context, -1, DateTime.Now.AddYears(-20), DateTime.Now.AddYears(20)));
        }

        [Test]
        public void GetByDivisionWithinTimeFrame_WrongTimeFrame_ReturnsEmptyList()
        {
            Assert.IsEmpty(RaceQueries.GetByDivisionWithinTimeFrame(Context, FixedID.Division, DateTime.Today.AddYears(-20), DateTime.Today.AddYears(-10)));
            Assert.IsEmpty(RaceQueries.GetByDivisionWithinTimeFrame(Context, FixedID.Division, DateTime.Today.AddYears(10), DateTime.Today.AddYears(20)));
        }

        [Test]
        public void GetByDivisionWithinTimeFrame_ValidIdAndTimeFrame_ReturnsRacesWithRaceDescription()
        {
            var divisionId = CreateIdForTestingPurpose<Division>();
            var expectedRaceDescription = CreateForTestingPurpose<RaceDescription>();
            var expectedRace = CreateForTestingPurpose<Race>(new { divisionId, raceDescriptionId = expectedRaceDescription.Id });

            var result = RaceQueries.GetByDivisionWithinTimeFrame(Context, divisionId, DateTime.Now.AddDays(-5), DateTime.Now.AddDays(5));

            Assert.IsNotEmpty(result);
            var race = result.FirstOrDefault(r => r.Id == expectedRace.Id);
            Assert.NotNull(race);
            Assert.AreEqual(expectedRace.RaceDescription, race.RaceDescription);
            Assert.AreEqual(expectedRaceDescription.Name, race.RaceRaceDescription.Name);
            Assert.LessOrEqual(DateTime.Now.AddDays(-5), race.StartDate);
            Assert.GreaterOrEqual(DateTime.Now.AddDays(5), race.EndDate);
        }

        #endregion


        #region GetRaceOrders

        [Test]
        public void GetRaceOrders_InvalidId_ReturnsEmptyList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idPlayer = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            var raceOrder = CreateForTestingPurpose<RaceOrder>(new { teamId = idTeam, playerId = idPlayer });
            Assert.IsEmpty(RaceQueries.GetRaceOrders(Context, -1));
            Assert.IsEmpty(RaceQueries.GetRaceOrders(Context, raceOrder.Race, -1));
        }

        [Test]
        public void GetRaceOrders_ValidId_ReturnsList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idPlayer = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            var raceOrder = CreateForTestingPurpose<RaceOrder>(new { teamId = idTeam, playerId = idPlayer });

            var result = RaceQueries.GetRaceOrders(Context, raceOrder.Race, raceOrder.Stage);
            Assert.IsNotEmpty(result);
            Assert.Contains(raceOrder.Id, result.Select(o => o.Id).ToList());
        }

        #endregion


        #region GetRaceEvent

        [Test]
        public void GetRaceEvents_InvalidId_ReturnsEmptyList()
        {
            var raceEvent = CreateForTestingPurpose<RaceEvent>();
            Assert.IsEmpty(RaceQueries.GetRaceEvents(Context, -1));
            Assert.IsEmpty(RaceQueries.GetRaceEvents(Context, raceEvent.Race, -1));
        }

        [Test]
        public void GetRaceEvents_ValidId_ReturnsList()
        {
            var raceEvent = CreateForTestingPurpose<RaceEvent>();

            var result = RaceQueries.GetRaceEvents(Context, raceEvent.Race, raceEvent.Stage);
            Assert.IsNotEmpty(result);
            Assert.Contains(raceEvent.Id, result.Select(e => e.Id).ToList());
        }

        #endregion


        #region GetRaceRankings

        [Test]
        public void GetRaceRankings_InvalidId_ReturnsEmptyList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idPlayer = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            var raceRanking = CreateForTestingPurpose<RaceRanking>(new { rankings = $"{idPlayer},{RandomUtil.RandomNumber()}", teams = idTeam.ToString() });
            Assert.IsEmpty(RaceQueries.GetRaceRankings(Context, -1));
            Assert.IsEmpty(RaceQueries.GetRaceRankings(Context, raceRanking.Race, -1));
        }

        [Test]
        public void GetRaceRankings_ValidId_ReturnsList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idPlayer = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            var raceRanking = CreateForTestingPurpose<RaceRanking>(new { rankings = $"{idPlayer},{RandomUtil.RandomNumber()}", teams = idTeam.ToString() });

            var result = RaceQueries.GetRaceRankings(Context, raceRanking.Race);
            Assert.IsNotEmpty(result);
            Assert.Contains(raceRanking.Id, result.Select(r => r.Id).ToList());
            Assert.True(result.Any(r => r.Race == raceRanking.Race));

            result = RaceQueries.GetRaceRankings(Context, raceRanking.Race, raceRanking.Stage);
            Assert.IsNotEmpty(result);
            Assert.Contains(raceRanking.Id, result.Select(r => r.Id).ToList());
            Assert.True(result.Any(r => r.Race == raceRanking.Race && r.Stage == raceRanking.Stage));
        }

        #endregion


        #region GetRaceHistoryForTeam

        [Test]
        public void GetRaceHistoryForTeam_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(RaceQueries.GetRaceHistoryForTeam(Context, -1));
        }

        [Test]
        public void GetRaceHistoryForTeam_ValidId_ReturnsList()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idPlayer = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            CreateForTestingPurpose<RaceHistory>(new { playerId = idPlayer, teamId = idTeam });

            var result = RaceQueries.GetRaceHistoryForTeam(Context, idTeam);

            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Team == idTeam));
        }

        #endregion


        #region GetRaceHistoryForRace

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceHistoryForRace_InvalidId_ReturnsEmptyList(bool includeStages)
        {
            Assert.IsEmpty(RaceQueries.GetRaceHistoryForRace(Context, -1, includeStages));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetRaceHistoryForRace_ValidId_ReturnsList(bool includeStages)
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var idPlayer = CreateIdForTestingPurpose<Player>(new { teamId = idTeam });
            var raceDescriptionId = CreateIdForTestingPurpose<RaceDescription>();
            var idStage = CreateIdForTestingPurpose<Stage>(new { raceDescriptionId });
            var idRace = CreateIdForTestingPurpose<Race>(new { raceDescriptionId });
            CreateForTestingPurpose<RaceHistory>(new { playerId = idPlayer, teamId = idTeam, raceId = idRace });
            CreateForTestingPurpose<RaceHistory>(new { playerId = idPlayer, teamId = idTeam, raceId = idRace, stageId = idStage });

            var result = RaceQueries.GetRaceHistoryForRace(Context, idRace, includeStages);

            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Race == idRace));
            Assert.AreEqual(includeStages, result.Any(h => h.Stage.HasValue));
        }

        #endregion
    }
}