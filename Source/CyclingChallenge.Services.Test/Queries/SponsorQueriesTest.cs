﻿using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;
using System.Linq;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class SponsorQueriesTest : TestFixtureBase
    {
        #region GetById

        [Test]
        public void GetById_InvalidId_ReturnsNull()
        {
            Assert.Null(SponsorQueries.GetById(Context, -1));
        }

        [Test]
        public void GetById_ValidId_ReturnsSponsor()
        {
            var sponsor = CreateForTestingPurpose<Sponsor>();
            Assert.NotNull(sponsor);
            Assert.AreEqual(sponsor.Name, SponsorQueries.GetById(Context, sponsor.Id)?.Name);
        }

        #endregion


        #region GetSponsorByTeam

        [Test]
        public void GetSponsorByTeam_InvalidIdTeam_ReturnsNull()
        {
            Assert.IsNull(SponsorQueries.GetSponsorByTeam(Context, -1));
        }

        [Test]
        public void GetSponsorByTeam_WeeksToGoIsZero_ReturnsNull()
        {
            var team = CreateForTestingPurpose<Team>();
            var sponsor = CreateForTestingPurpose<Sponsor>(new { teamId = team.Id, weeksToGo = 0 });
            Assert.IsNull(SponsorQueries.GetSponsorByTeam(Context, sponsor.Team.Value));
        }

        [Test]
        public void GetSponsorByTeam_ValidIdAndStillWeeksToGo_ReturnsSponsor()
        {
            var team = CreateForTestingPurpose<Team>();
            var sponsor = CreateForTestingPurpose<Sponsor>(new { teamId = team.Id, weeksToGo = 1 });
            Assert.NotNull(sponsor);
            Assert.AreEqual(sponsor.Name, SponsorQueries.GetSponsorByTeam(Context, sponsor.Team.Value)?.Name);
        }

        #endregion


        #region GetSponsorsByDivision

        [Test]
        public void GetSponsorsByDivision_InvalidIdDivision_ReturnsEmptyList()
        {
            Assert.IsEmpty(SponsorQueries.GetSponsorsByDivision(Context, -1));
        }

        [Test]
        public void GetSponsorsByDivison_AllSponsorsAssigned_ReturnsEmptyList()
        {
            var division = CreateForTestingPurpose<Division>();
            var sponsor = CreateForTestingPurpose<Sponsor>(new { teamId = FixedID.Team, divisionId = division.Id });
            Assert.IsEmpty(SponsorQueries.GetSponsorsByDivision(Context, division.Id));
        }

        [Test]
        public void GetSponsorsByDivison_NotAllSponsorsAssigned_ReturnsUnassignedSponsorsList()
        {
            var sponsor = CreateForTestingPurpose<Sponsor>();
            var result = SponsorQueries.GetSponsorsByDivision(Context, sponsor.Division);
            Assert.IsNotEmpty(result);
            Assert.False(result.Any(s => s.Team.HasValue));
        }

        #endregion
    }
}
