﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class StaffQueriesTest : TestFixtureBase
    {
        #region GetCoachById

        [Test]
        public void GetCoachById_InvalidId_ReturnsNull()
        {
            Assert.IsNull(StaffQueries.GetCoachById(Context, -1));
        }

        [Test]
        public void GetCoachById_ValidId_ReturnsCoach()
        {
            var coach = CreateForTestingPurpose<Coach>();
            Assert.NotNull(coach);
            Assert.AreEqual(coach.LastName, StaffQueries.GetCoachById(Context, coach.Id)?.LastName);
        }

        #endregion


        #region GetCoachByTeam

        [Test]
        public void GetForTeam_InvalidIdTeam_ReturnsNull()
        {
            Assert.IsNull(StaffQueries.GetCoachByTeam(Context, -1));
        }

        [Test]
        public void GetForTeam_ValidIdTeam_ReturnsCoach()
        {
            var team = CreateForTestingPurpose<Team>();
            var coach = CreateForTestingPurpose<Coach>(new { teamId = team.Id });
            Assert.NotNull(coach?.Team);
            Assert.AreEqual(coach.LastName, StaffQueries.GetCoachByTeam(Context, coach.Team)?.LastName);
        }

        #endregion


        #region GetScouts

        [Test]
        public void GetScouts_InvalidIdTeam_ReturnsEmptyList()
        {
            Assert.IsEmpty(StaffQueries.GetScouts(Context, -1));
        }

        [Test]
        public void GetScouts_ValidIdTeam_ReturnsListOfScouts()
        {
            var team = CreateForTestingPurpose<Team>();
            var scout = CreateForTestingPurpose<Scout>(new { teamId = team.Id });
            Assert.NotNull(scout?.Team);

            var result = StaffQueries.GetScouts(Context, team.Id)?.ToList();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.Contains(scout, result);
        }

        #endregion

        
        #region GetManagers

        [Test]
        public void GetManagers_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(StaffQueries.GetManagers(Context, -1));
        }

        [Test]
        public void GetManagers_ValidId_ReturnsManagers()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var shopManager = CreateForTestingPurpose<Manager>(new { teamId = idTeam });
            var raceManager = CreateForTestingPurpose<Manager>(new { teamId = idTeam, type = Managers.Type.Race });
            var staffManager = CreateForTestingPurpose<Manager>(new { teamId = idTeam, type = Managers.Type.Staff });

            var result = StaffQueries.GetManagers(Context, idTeam);

            Assert.NotNull(result);
            Assert.True(result.All(m => m.Team == idTeam));
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual(shopManager.Id, result.FirstOrDefault(m => m.Type == Managers.Type.Shop)?.Id);
            Assert.AreEqual(shopManager.FirstName, result.FirstOrDefault(m => m.Type == Managers.Type.Shop)?.FirstName);
            Assert.AreEqual(raceManager.Id, result.FirstOrDefault(m => m.Type == Managers.Type.Race)?.Id);
            Assert.AreEqual(raceManager.FirstName, result.FirstOrDefault(m => m.Type == Managers.Type.Race)?.FirstName);
            Assert.AreEqual(staffManager.Id, result.FirstOrDefault(m => m.Type == Managers.Type.Staff)?.Id);
            Assert.AreEqual(staffManager.FirstName, result.FirstOrDefault(m => m.Type == Managers.Type.Staff)?.FirstName);
        }

        #endregion


        #region GetShopManager

        [Test]
        public void GetShopManager_InvalidId_ReturnsNull()
        {
            Assert.Null(StaffQueries.GetShopManager(Context, -1));
        }

        [Test]
        public void GetShopManager_ValidId_ReturnsShopManager()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var shopManager = CreateForTestingPurpose<Manager>(new { teamId = idTeam });

            var result = StaffQueries.GetShopManager(Context, idTeam);
            Assert.NotNull(result);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(Managers.Type.Shop, result.Type);
            Assert.AreEqual(shopManager.FirstName, result.FirstName);
            Assert.AreEqual(shopManager.LastName, result.LastName);
        }

        #endregion


        #region GetRaceManager

        [Test]
        public void GetRaceManager_InvalidId_ReturnsNull()
        {
            Assert.Null(StaffQueries.GetRaceManager(Context, -1));
        }

        [Test]
        public void GetRaceManager_ValidId_ReturnsRaceManager()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var raceManager = CreateForTestingPurpose<Manager>(new { teamId = idTeam, type = Managers.Type.Race });

            var result = StaffQueries.GetRaceManager(Context, idTeam);
            Assert.NotNull(result);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(Managers.Type.Race, result.Type);
            Assert.AreEqual(raceManager.FirstName, result.FirstName);
            Assert.AreEqual(raceManager.LastName, result.LastName);
        }

        #endregion
    }
}