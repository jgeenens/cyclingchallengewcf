﻿using NUnit.Framework;
using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class TeamQueriesTest : TestFixtureBase
    {
        #region GetTeamById

        [Test]
        public void GetTeamById_InvalidId_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetTeamById(Context, -1));
        }

        [Test]
        public void GetTeamById_ValidId_ReturnsTeam()
        {
            var result = TeamQueries.GetTeamById(Context, FixedID.Team);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.Team, result.Id);
        }

        #endregion


        #region GetDivisionById

        [Test]
        public void GetDivisionById_InvalidId_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetDivisionById(Context, -1));
        }

        [Test]
        public void GetDivisionById_ValidId_ReturnsDivision()
        {
            var result = TeamQueries.GetDivisionById(Context, FixedID.Division);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.Division, result.Id);
        }

        #endregion


        #region GetDivisionsByName

        [Test]
        public void GetDivisionsByName_NameNullOrEmpty_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetDivisionsByName(Context, null));
            Assert.Null(TeamQueries.GetDivisionsByName(Context, string.Empty));
        }

        [Test]
        public void GetDivisionsByName_InvalidName_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetDivisionsByName(Context, Environment.NewLine));
        }

        [Test]
        public void GetDivisionsByName_ValidName_ReturnsList()
        {
            var division = CreateForTestingPurpose<Division>();
            Assert.GreaterOrEqual(division.Name.Length, 3);
            var needle = division.Name.Substring(0, 3);

            var result = TeamQueries.GetDivisionsByName(Context, needle)?.ToList();
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(d => d.Name.ToLower().StartsWith(needle.ToLower())));
        }

        #endregion


        #region GetDivisionsByCountry

        [Test]
        public void GetDivisionsByCountry_InvalidCountry_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetDivisionsByCountry(Context, -1));
        }

        [Test]
        public void GetDivisionsByCountry_ValidCountry_ReturnsList()
        {
            var division = CreateForTestingPurpose<Division>();
            var result = TeamQueries.GetDivisionsByCountry(Context, FixedID.Country).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(d => d.Country == FixedID.Country));
            Assert.Contains(division, result);
        }

        #endregion


        #region GetTeamHistoryForTeam

        [Test]
        public void GetTeamHistoryForTeam_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetTeamHistoryForTeam(Context, -1));
        }

        [Test]
        public void GetTeamHistoryForTeam_ValidId_ReturnsTeamHistory()
        {
            var teamHistory = CreateForTestingPurpose<TeamHistory>();
            var result = TeamQueries.GetTeamHistoryForTeam(Context, FixedID.Team);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Team == FixedID.Team));
        }

        #endregion


        #region GetTransferHistoryForTeam

        [Test]
        public void GetTransferHistoryForTeam_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetTransferHistoryForTeam(Context, -1, DateTime.Now.AddYears(-10)));
        }

        [Test]
        public void GetTransferHistoryForTeam_NoneWithinTimeFrame_ReturnsEmptyList()
        {
            var idPlayer = CreateIdForTestingPurpose<Player>();
            var idTeam = CreateIdForTestingPurpose<Team>();
            CreateForTestingPurpose<TransferHistory>(new { playerId = idPlayer, teamId = idTeam });
            Assert.IsEmpty(TeamQueries.GetTransferHistoryForTeam(Context, idTeam, DateTime.Now.AddYears(-10), DateTime.Now.AddYears(-9)));
        }

        [Test, TestCase(false), TestCase(true)]
        public void GetTransferHistoryForTeam_ValidId_ReturnsList(bool withEndLimit)
        {
            CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = CreateIdForTestingPurpose<Player>(),
                fromTeamId = CreateIdForTestingPurpose<Team>(),
                date = DateTime.Now
            });
            CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = CreateIdForTestingPurpose<Player>(),
                teamId = CreateIdForTestingPurpose<Team>(),
                date = DateTime.Now
            });

            var from = DateTime.Now.AddDays(-1);
            var to = withEndLimit ? DateTime.Now.AddDays(1) : (DateTime?)null;
            var result = TeamQueries.GetTransferHistoryForTeam(Context, FixedID.Team, from, to).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Team == FixedID.Team || h.FromTeam == FixedID.Team));
            Assert.True(result.All(h => h.Type == Rider.Transfer.Finished));
            Assert.True(result.All(t => t.DateTransfer >= from));
            Assert.True(result.All(t => t.DateTransfer <= (to ?? DateTime.MaxValue)));
        }

        #endregion


        #region GetTransfersForTeam

        [Test]
        public void GetTransfersForTeam_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetTransfersForTeam(Context, -1));
        }

        [Test]
        public void GetTransfersForTeam_ValidId_ReturnsList()
        {
            CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = CreateIdForTestingPurpose<Player>(),
                fromTeamId = CreateIdForTestingPurpose<Team>(),
                type = Rider.Transfer.InProgress,
                date = DateTime.Now
            });
            CreateForTestingPurpose<TransferHistory>(new
            {
                playerId = CreateIdForTestingPurpose<Player>(),
                teamId = CreateIdForTestingPurpose<Team>(),
                type = Rider.Transfer.Processing,
                date = DateTime.Now
            });

            var result = TeamQueries.GetTransfersForTeam(Context, FixedID.Team).ToList();
            Assert.IsNotEmpty(result);
            Assert.True(result.All(h => h.Team == FixedID.Team || h.FromTeam == FixedID.Team));
            Assert.True(result.All(h => h.Type != Rider.Transfer.Finished));
        }

        #endregion


        #region GetShop

        [Test]
        public void GetShop_InvalidId_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetShop(Context, -1));
        }

        [Test]
        public void GetShop_ValidId_ReturnsShop()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var shop = CreateForTestingPurpose<Clubshop>(new { teamId = idTeam });

            var result = TeamQueries.GetShop(Context, idTeam);
            Assert.NotNull(result);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(shop.Hint, result.Hint);
        }

        #endregion


        #region GetLogoById

        [Test]
        public void GetLogoById_InvalidId_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetLogoById(Context, 0));
        }

        [Test]
        public void GetLogoById_ValidId_ReturnsLogo()
        {
            var result = TeamQueries.GetLogoById(Context, FixedID.Logo);
            Assert.NotNull(result);
            Assert.AreEqual(FixedID.Logo, result.Id);
        }

        #endregion


        #region GetRandomLogo

        [Test]
        public void GetRandomLogo_ReturnsAFreeLogo()
        {
            var result = TeamQueries.GetRandomLogo(Context);
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Tokens);
        }

        #endregion


        #region GetTeamsByDivision

        [Test]
        public void GetTeamsByDivision_InvalidId_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetTeamsByDivision(Context, -1));
        }

        [Test]
        public void GetTeamsByDivision_ValidId_ReturnsList()
        {
            var idDivision = CreateIdForTestingPurpose<Division>();
            var team = CreateForTestingPurpose<Team>(new { divisionId = idDivision });
            var result = TeamQueries.GetTeamsByDivision(Context, idDivision);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(t => t.Division == idDivision));
            Assert.Contains(team.Id, result.Select(t => t.Id).ToList());
        }

        #endregion


        #region GetTeamsByUsername

        [Test]
        public void GetTeamsByUsername_UsernameNullOrEmpty_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetTeamsByUsername(Context, null));
            Assert.Null(TeamQueries.GetTeamsByUsername(Context, string.Empty));
        }

        [Test]
        public void GetTeamsByUsername_InvalidUsername_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetTeamsByUsername(Context, Environment.NewLine));
        }

        [Test]
        public void GetTeamsByUsername_ValidUsername_ReturnsList()
        {
            var team = CreateForTestingPurpose<Team>();
            var user = CreateForTestingPurpose<User>(new { teamId = team.Id });
            Assert.GreaterOrEqual(user.Username.Length, 3);
            var needle = user.Username.Substring(0, 3);

            var result = TeamQueries.GetTeamsByUsername(Context, needle);
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.Contains(team.Id, result.Select(t => t.Id).ToList());
        }

        #endregion


        #region GetTeamByShortName

        [Test]
        public void GetTeamByShortName_InvalidName_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetTeamByShortName(Context, string.Empty));
        }

        [Test]
        public void GetTeamByShortName_ValidName_ReturnsTeam()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = TeamQueries.GetTeamByShortName(Context, team.ShortName);
            Assert.NotNull(result);
            Assert.AreEqual(team.ShortName, result.ShortName);
        }

        #endregion


        #region GetTeamByName

        [Test]
        public void GetTeamByName_InvalidName_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetTeamByName(Context, string.Empty));
        }

        [Test]
        public void GetTeamByName_ValidName_ReturnsTeam()
        {
            var team = CreateForTestingPurpose<Team>();
            var result = TeamQueries.GetTeamByName(Context, team.Name);
            Assert.NotNull(result);
            Assert.AreEqual(team.Name, result.Name);
        }

        #endregion


        #region GetTeamsByName

        [Test]
        public void GetTeamsByName_NameNullOrEmpty_ReturnsNull()
        {
            Assert.Null(TeamQueries.GetTeamsByName(Context, null));
            Assert.Null(TeamQueries.GetTeamsByName(Context, string.Empty));
        }

        [Test]
        public void GetTeamsByName_InvalidName_ReturnsEmptyList()
        {
            Assert.IsEmpty(TeamQueries.GetTeamsByName(Context, Environment.NewLine));
        }

        [Test]
        public void GetTeamsByName_ValidName_ReturnsList()
        {
            var team = CreateForTestingPurpose<Team>();
            Assert.GreaterOrEqual(team.Name.Length, 3);
            var needle = team.Name.Substring(0, 3);

            var result = TeamQueries.GetTeamsByName(Context, needle);
            Assert.NotNull(result);
            Assert.IsNotEmpty(result);
            Assert.True(result.All(t => t.Name.ToLower().StartsWith(needle)));
        }

        #endregion

        
        #region GetLastNameChange

        [Test]
        public void GetLastNameChange_InvalidId_ReturnsDateTimeMinValue()
        {
            var result = TeamQueries.GetLastNameChange(Context, -1);
            Assert.NotNull(result);
            Assert.AreEqual(DateTime.MinValue, result);
        }

        [Test]
        public void GetLastNameChange_ValidId_ReturnsTimeOfLastNameChange()
        {
            CreateForTestingPurpose<TeamHistory>(new { eventType = Events.Team.NameChange });
            var lastNameChange = CreateForTestingPurpose<TeamHistory>(new { date = DateTime.Now.AddYears(1), eventType = Events.Team.NameChange });

            var result = TeamQueries.GetLastNameChange(Context, FixedID.Team);
            Assert.NotNull(result);
            Assert.AreEqual(lastNameChange.Date, result);
        }

        #endregion
    }
}