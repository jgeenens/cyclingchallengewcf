﻿using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Test.Utils;
using NUnit.Framework;

namespace CyclingChallenge.Services.Test.Queries
{
    [TestFixture]
    public class UserQueriesTest : TestFixtureBase
    {
        #region GetById

        [Test]
        public void GetById_InvalidId_ReturnsNull()
        {
            Assert.Null(UserQueries.GetById(Context, -1));
        }

        [Test]
        public void GetById_ValidId_ReturnsUser()
        {
            var user = CreateForTestingPurpose<User>();
            var result = UserQueries.GetById(Context, user.Id);
            Assert.NotNull(result);
            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(user.Username, result.Username);
        }

        #endregion


        #region GetUserByFacebookId

        [Test]
        public void GetUserByFacebookId_InvalidId_ReturnsNull()
        {
            Assert.Null(UserQueries.GetUserByFacebookId(Context, -1));
        }

        [Test]
        public void GetUserByFacebookId_ValidId_ReturnsUser()
        {
            var user = CreateForTestingPurpose<User>();
            var result = UserQueries.GetUserByFacebookId(Context, user.FacebookId);
            Assert.NotNull(result);
            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(user.FacebookId, result.FacebookId);
            Assert.AreEqual(user.Username, result.Username);
        }

        #endregion


        #region GetByTeam

        [Test]
        public void GetByTeam_InvalidId_ReturnsNull()
        {
            Assert.Null(UserQueries.GetByTeam(Context, -1));
        }

        [Test]
        public void GetByTeam_ValidId_ReturnsUser()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var user = CreateForTestingPurpose<User>(new { teamId = idTeam });
            var result = UserQueries.GetByTeam(Context, idTeam);
            Assert.NotNull(result);
            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(user.Username, result.Username);
        }

        #endregion


        #region GetByUsername

        [Test]
        public void GetByUsername_NullOrEmptyString_ReturnsNull()
        {
            Assert.Null(UserQueries.GetByUsername(Context, null));
            Assert.Null(UserQueries.GetByUsername(Context, string.Empty));
        }

        [Test]
        public void GetByUsername_UnknownUsername_ReturnsNull()
        {
            Assert.Null(UserQueries.GetByUsername(Context, RandomUtil.RandomString()));
        }

        [Test]
        public void GetByUsername_KnownUsername_ReturnsUser()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var user = CreateForTestingPurpose<User>(new { teamId = idTeam });
            var result = UserQueries.GetByUsername(Context, user.Username);
            Assert.NotNull(result);
            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(user.Username, result.Username);
        }

        #endregion


        #region GetByEmail

        [Test]
        public void GetByEmail_NullOrEmptyString_ReturnsNull()
        {
            Assert.Null(UserQueries.GetByEmail(Context, null));
            Assert.Null(UserQueries.GetByEmail(Context, string.Empty));
        }

        [Test]
        public void GetByEmail_UnknownEmail_ReturnsNull()
        {
            Assert.Null(UserQueries.GetByEmail(Context, RandomUtil.RandomString()));
        }

        [Test]
        public void GetByEmail_KnownEmail_ReturnsUser()
        {
            var idTeam = CreateIdForTestingPurpose<Team>();
            var user = CreateForTestingPurpose<User>(new { teamId = idTeam });
            var result = UserQueries.GetByEmail(Context, user.Email);
            Assert.NotNull(result);
            Assert.AreEqual(user.Id, result.Id);
            Assert.AreEqual(idTeam, result.Team);
            Assert.AreEqual(user.Email, result.Email);
        }

        #endregion
    }
}