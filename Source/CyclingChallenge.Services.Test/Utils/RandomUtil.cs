﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CyclingChallenge.Services.Test.Utils
{
    public static class RandomUtil
    {
        #region Fields

        private const string _stringChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789çéèêëàáâäãíìîïùúûüóòôöõ!?.,;:=%&|=+-*\\{}()[]\"'";
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private static Random _random = new Random();

        #endregion


        #region Random methods

        public static string RandomString(int length = 10)
        {
            return new string(Enumerable.Repeat(_chars, length).Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        public static string RandomCharString(int length = 10)
        {
            return new string(Enumerable.Repeat(_chars, length).Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        public static DateTime RandomDateTime()
        {
            var start = new DateTime(1950, 1, 1);
            var range = (DateTime.Today - start).Days;
            return start.AddDays(_random.Next(range));
        }

        public static long RandomNumber(int length = 2)
        {
            return _random.Next((int)Math.Pow(10, length));
        }

        public static T RandomElement<T>(List<T> list)
        {
            var rnd = new Random();
            return list[rnd.Next(list.Count)];
        }

        #endregion
    }
}