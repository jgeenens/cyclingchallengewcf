﻿using System;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Commands
{
    public static class CommunicationCommands
    {
        public static PrivateMessage CreatePrivateMessage(ICyclingChallengeModelContainer database, User sender, User recipient, string subject, string message, byte type = Enums.Message.Type.Private)
        {
            return database.PrivateMessages.Add(new PrivateMessage
            {
                Subject = subject,
                Message = message,
                Sender = sender.Id,
                Recipient = recipient.Id,
                Date = DateTime.Now,
                Read = false,
                Type = type,
                DeletedByRecipient = false,
                DeletedBySender = false
            });
        }

        public static Message CreateMessage(ICyclingChallengeModelContainer database, long idThread, long idUser, string userIp, string message, long? idMessageRepliedTo = null)
        {
            var maxMessageInThread = CommunicationQueries.GetMaxMessageInThread(database, idThread) ?? 0;
            return database.Messages.Add(new Message
            {
                Thread = idThread,
                User = idUser,
                MessageInThread = (byte)(maxMessageInThread + 1),
                ReplyTo = idMessageRepliedTo,
                Date = DateTime.Now,
                Content = message,
                UserIp = userIp,
                Obsolete = false
            });
        }

        public static void UpdateMessage(Message message, long userId, string text)
        {
            if (message == null)
            {
                return;
            }

            message.Content = text;
            message.LastChange = DateTime.Now;
            message.UserChange = userId;
        }
    }
}