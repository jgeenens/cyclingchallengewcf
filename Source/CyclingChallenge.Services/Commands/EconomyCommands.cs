﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Queries;
using System;

namespace CyclingChallenge.Services.Commands
{
    public static class EconomyCommands
    { 
        #region Commands

        public static Economy CreateEconomy(ICyclingChallengeModelContainer database, long idTeam, long wages)
        {
            var weekAndDay = GameQueries.GetWeekAndDayInSeason(database);
            return database.Economies.Add(new Economy
            {
                Team = idTeam,
                ReferenceDate = DateTime.Now.AddDays(7),
                Week = weekAndDay.Week,
                Day = weekAndDay.Day,
                Expense2 = wages,
                Active = Enums.Economy.Status.Active
            });
        }

        public static void InitializeSponsor(Sponsor sponsor)
        {
            if (sponsor == null)
            {
                return;
            }

            sponsor.Team = null;
            sponsor.WeeksToGo = 0;
        }

        #endregion
    }
}