﻿using System;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Game;

namespace CyclingChallenge.Services.Commands
{
    public static class GameCommands
    { 
        #region Commands

        public static Registration CreateRegistration(ICyclingChallengeModelContainer database, short idCountry)
        {
            return database.Registrations.Add(new Registration
            {
                Date = DateTime.Today,
                Country = idCountry,
                Quantity = 1
            });
        }

        public static Sale CreateSale(ICyclingChallengeModelContainer database, long idUser, Logo logo)
        {
            return database.Sales.Add(new Sale
            {
                User = idUser,
                Amount = 1,
                Addon = AddOns.Logo,
                Date = DateTime.Now,
                Price = logo.Tokens,
                Status = logo.Id.ToString()
            });
        }

        #endregion
    }
}