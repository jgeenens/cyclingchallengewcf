﻿using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Utils;

namespace CyclingChallenge.Services.Commands
{
    public static class PlayerCommands
    {
        #region Commands

        public static Player CreatePlayer(ICyclingChallengeModelContainer database, long idTeam, short idCountry, bool isYouth = true, long? maxFirstNameValue = null, long? maxLastNameValue = null)
        {
            if (!maxFirstNameValue.HasValue)
            {
                maxFirstNameValue = PlayerQueries.GetNameMaxValueByCountry(database, idCountry, true);
            }

            if (!maxLastNameValue.HasValue)
            {
                maxLastNameValue = PlayerQueries.GetNameMaxValueByCountry(database, idCountry);
            }

            return database.Players.Add(new Player
            {
                FirstName = PlayerQueries.GetRandomNameByCountry(database, idCountry, maxFirstNameValue.Value, true),
                LastName = PlayerQueries.GetRandomNameByCountry(database, idCountry, maxLastNameValue.Value),
                Team = idTeam,
                Nationality = idCountry,
                Age = isYouth
                    ? (byte)(RandomUtil.BetterRandomNumber(2) + 16)
                    : (byte)(RandomUtil.BetterRandomNumber(20) + 18),
                Retired = false,
                City = LocationQueries.GetRandomCityByCountry(database, idCountry).Id,
                Face = (byte)(RandomUtil.BetterRandomNumber(6) + 1),
                Hair = (byte)(RandomUtil.BetterRandomNumber(25) + 1),
                Eyes = (byte)(RandomUtil.BetterRandomNumber(13) + 1),
                Lips = (byte)(RandomUtil.BetterRandomNumber(17) + 1),
                Nose = (byte)(RandomUtil.BetterRandomNumber(13) + 1)
            });
        }

        public static PlayerValue CreatePlayerValues(ICyclingChallengeModelContainer database, Player player, long minValues, long maxValues)
        {
            var values = GetRandomValues(minValues, maxValues);
            return database.PlayerValues.Add(new PlayerValue
            {
                PlayerValuePlayer = player,
                Team = player.PlayerTeam.ShortName,
                Climbing = values[0],
                Sprint = values[1],
                Stamina = values[2],
                Technique = values[3],
                Descending = values[4],
                Timetrial = values[5],
                Experience = values[6],
                Plains = values[7],
                Cobblestones = values[8],
                Hills = values[9],
                Mountains = values[10],
                Form = 1000,
                FormTarget = 1000,
                City = player.City,
                IsYouth = player.Age < 18
            });
        }

        public static void SetWage(PlayerValue playerValues)
        {
            if (playerValues.IsYouth)
            {
                return;
            }

            var wage = Math.Pow(playerValues.Plains + playerValues.Mountains + playerValues.Hills + playerValues.Cobblestones, 2) / 3000000;
            wage *= playerValues.Stamina / 12.0;
            wage += Math.Pow(playerValues.Technique + playerValues.Climbing + playerValues.Descending + playerValues.Timetrial + playerValues.Sprint, 2) / 300;
            playerValues.Wage = (long)Math.Floor(Math.Max(1000, wage / 2500000));
        }

        #endregion


        #region Private Methods

        private static List<long> GetRandomValues(long minValues, long maxValues)
        {
            var values = new List<long>();
            for (var i = 0; i < 11; i++)
            {
                values.Add(RandomUtil.BetterRandomNumber(50000));
            }

            var randomAverage = RandomUtil.BetterRandomNumber(maxValues - minValues) + minValues;
            var weight = (decimal)randomAverage * 11 / values.Sum();
            for (var i = 0; i < 11; i++)
            {
                values[i] = (long)Math.Ceiling(weight * values[i]);
            }

            return values;
        }

        #endregion
    }
}