﻿using System;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Utils;

namespace CyclingChallenge.Services.Commands
{
    public static class StaffCommands
    {
        #region Commands

        public static Manager CreateManager(ICyclingChallengeModelContainer database, long idTeam, byte type)
        {
            var idCountry = LocationQueries.GetRandomActiveCountry(database).Id;
            var maxFirstNameValue = PlayerQueries.GetNameMaxValueByCountry(database, idCountry, true) ?? 0;
            var maxLastNameValue = PlayerQueries.GetNameMaxValueByCountry(database, idCountry) ?? 0;
            
            return database.Managers.Add(new Manager
            {
                Team = idTeam,
                Type = type,
                FirstName = PlayerQueries.GetRandomNameByCountry(database, idCountry, maxFirstNameValue, true),
                LastName = PlayerQueries.GetRandomNameByCountry(database, idCountry, maxLastNameValue),
                Face = (byte)(RandomUtil.BetterRandomNumber(6) + 1),
                Hair = (byte)(RandomUtil.BetterRandomNumber(25) + 1),
                Eyes = (byte)(RandomUtil.BetterRandomNumber(13) + 1),
                Lips = (byte)(RandomUtil.BetterRandomNumber(17) + 1),
                Nose = (byte)(RandomUtil.BetterRandomNumber(13) + 1)
            });
        }

        public static void InitializeManager(Manager manager)
        {
            manager.Learning = null;
            manager.Level = Managers.Level.Apprentice;
            manager.Status = Managers.Status.Locked;
        }

        public static Scout CreateScout(ICyclingChallengeModelContainer database, long idTeam, short idCountry, byte status, long? maxFirstNameValue = null, long? maxLastNameValue = null)
        {
            if (!maxFirstNameValue.HasValue)
            {
                maxFirstNameValue = PlayerQueries.GetNameMaxValueByCountry(database, idCountry, true);
            }

            if (!maxLastNameValue.HasValue)
            {
                maxLastNameValue = PlayerQueries.GetNameMaxValueByCountry(database, idCountry);
            }

            return database.Scouts.Add(new Scout
            {
                Team = idTeam,
                Status = status,
                Nationality = idCountry,
                FirstName = PlayerQueries.GetRandomNameByCountry(database, idCountry, maxFirstNameValue.Value, true),
                LastName = PlayerQueries.GetRandomNameByCountry(database, idCountry, maxLastNameValue.Value),
                Face = (byte)(RandomUtil.BetterRandomNumber(6) + 1),
                Hair = (byte)(RandomUtil.BetterRandomNumber(25) + 1),
                Eyes = (byte)(RandomUtil.BetterRandomNumber(13) + 1),
                Lips = (byte)(RandomUtil.BetterRandomNumber(17) + 1),
                Nose = (byte)(RandomUtil.BetterRandomNumber(13) + 1),
                Created = DateTime.Now
            });
        }

        public static void InitializeScout(Scout scout)
        {
            scout.Age = (byte)(RandomUtil.BetterRandomNumber(25) + 25);
            scout.WeeksToGo = 0;
            scout.LookingForYouth = false;
            scout.Skill = Scouts.Level.Apprentice;
            scout.TotalWeeks = 0;
            scout.Wage = 1000;
        }

        #endregion
    }
}