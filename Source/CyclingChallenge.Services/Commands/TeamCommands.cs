﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Utils;

namespace CyclingChallenge.Services.Commands
{
    public static class TeamCommands
    {
        public static void InitializeTeam(ICyclingChallengeModelContainer database, Team team, string teamName, long initialMoney, short initialSupporters)
        {
            team.IsActive = true;
            team.IsBot = false;
            team.Name = teamName;
            team.Money = initialMoney;
            team.Predicted = initialMoney;
            team.Transactions = 0;
            team.HasShop = false;
            team.LastCamp = null;
            team.Supporters = initialSupporters;
            team.Training = Rider.Skill.Stamina;
            team.YouthTraining = Rider.Terrain.Plains;
            team.NotParticipated = 0;
            team.Logo = TeamQueries.GetRandomLogo(database).Id;
        }

        public static TeamHistory CreateTeamHistory(ICyclingChallengeModelContainer database, long idTeam, byte idEvent, string data = null)
        {
            return database.TeamHistories.Add(new TeamHistory
            {
                Team = idTeam,
                Date = DateTime.Now,
                Event = idEvent,
                Data = data,
                Seen = false
            });
        }

        public static string GetUniqueTeamName(ICyclingChallengeModelContainer database, short idCountry)
        {
            string teamName;
            do
            {
                teamName = GetTeamNameSuggestion(database, idCountry);
            }
            while (TeamQueries.GetTeamsByName(database, teamName).Any());

            return teamName;
        }

        public static string GetTeamNameSuggestion(ICyclingChallengeModelContainer database, short? idCountry)
        {
            var randomNumber = RandomUtil.BetterRandomNumber(1000);
            if (randomNumber < 600)
            {
                var the = randomNumber % 4 == 1 ? GameQueries.GetRandomWordByType(database, Word.The).Value : string.Empty;
                var adverb = randomNumber < 250 ? GameQueries.GetRandomWordByType(database, Word.Adverb).Value : string.Empty;
                var adjective = GameQueries.GetRandomWordByType(database, Word.Adjective).Value;
                var noun = GameQueries.GetRandomWordByType(database, Word.Noun).Value;
                return $"{the} {adverb} {adjective} {noun}".Replace("  ", " ").Trim();
            };

            var royal = randomNumber < 850 && randomNumber % 4 == 1 ? GameQueries.GetRandomWordByType(database, Word.Royal).Value : string.Empty;
            var team = GameQueries.GetRandomWordByType(database, randomNumber < 750 || randomNumber >= 850 ? Word.Team : Word.Sparta).Value;
            var name = !idCountry.HasValue || randomNumber % 3 != 1
                ? GameQueries.GetRandomWordByType(database, Word.Name).Value
                : LocationQueries.GetRandomCityByCountry(database, idCountry.Value).Name;
            return randomNumber < 850 ? $"{royal} {team} {name}".Replace("  ", " ").Trim() : $"{name} {team}";
        }
    }
}