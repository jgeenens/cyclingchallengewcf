﻿using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Utils;

namespace CyclingChallenge.Services.Commands
{
    public static class UserCommands
    {
        public static User CreateUser(ICyclingChallengeModelContainer database, Dictionary<long, long> settings, long facebookId, Country country, byte language, string firstName = null, string lastName = null, string email = null, DateTime? birthDate = null)
        {
            if (!settings.ContainsKey(Settings.Tokens) || country == null)
            {
                return null;
            }

            var username = GetUserNameSuggestions(firstName, lastName, birthDate)?.FirstOrDefault(o => UserQueries.GetByUsername(database, o) == null);
            if (string.IsNullOrEmpty(username))
            {
                return null;
            }

            return database.Users.Add(new User
            {
                FacebookId = facebookId,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Birthday = birthDate,
                Country = country.Active
                    ? country.Id
                    : LocationQueries.GetRandomActiveCountry(database).Id,
                RealCountry = country.Id,
                Prefix = string.Empty,
                Username = username,
                Registration = DateTime.Now,
                Reference = RandomUtil.RandomCharString(8),
                Online = true,
                OnlineSince = DateTime.Now,
                EmailHidden = true,
                LoginHistory = DateTime.Now.ToString("yyyyMMddHHmm"),
                LastHit = DateTime.Now,
                Tokens = (int)settings[Settings.Tokens],
                Live = string.Empty,
                Vacation = false,
                VacationSince = DateTime.Now.AddYears(-1),
                UserLevel = 0,
                Language = language
            });
        }

        public static IEnumerable<string> GetUserNameSuggestions(string firstName, string lastName, DateTime? birthDate)
        {
            firstName = firstName == null ? null : string.Join(string.Empty, firstName.Where(char.IsLetter)).ToLower();
            lastName = lastName == null ? null : string.Join(string.Empty, lastName.Where(char.IsLetter)).ToLower();

            if (birthDate == null && (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName)) ||
                string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName))
            {
                return null;
            }

            var options = new List<string>();
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                options.Add($"{firstName[0]}{lastName}");
                options.Add($"{lastName}{firstName[0]}");
                options.Add($"{firstName}{lastName[0]}");
                options.Add($"{lastName[0]}{firstName}");
            }

            if (!string.IsNullOrEmpty(firstName) && firstName.Length > 4)
            {
                options.Add(firstName);
            }

            if (!string.IsNullOrEmpty(lastName) && lastName.Length > 4)
            {
                options.Add(lastName);
            }

            if (birthDate.HasValue)
            {

                options.AddRange(options.Select(s => $"{s}{birthDate.Value:yy}").ToList());
            }

            return options;
        }
    }
}