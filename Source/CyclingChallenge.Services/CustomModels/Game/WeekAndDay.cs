﻿namespace CyclingChallenge.Services.CustomModels.Game
{
    public class WeekAndDay
    {
        public byte Week { get; set; }
        
        public byte Day { get; set; }
    }
}