﻿using System;
using CyclingChallenge.Services.Processors;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.YouthRider;
using CyclingChallenge.Services.Contracts.ServiceContracts;
using ServiceDictionary = CyclingChallenge.Services.Contracts.DataContracts.Dictionary;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Game;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;

namespace CyclingChallenge.Services
{
    public class CyclingChallengeService : ICyclingChallengeService
    {
        #region Constructor

        public CyclingChallengeService()
        {
        }

        ~CyclingChallengeService()
        {
        }

        #endregion


        #region Private methods

        private static TResult HandleApiCall<TProcessor, TResult>(params object[] parameters)
            where TProcessor : ProcessorBase<TResult>
            where TResult : ResultBase, new()
        {
            try
            {
                var processor = (TProcessor) Activator.CreateInstance(typeof(TProcessor), parameters);
                return processor.Process();
            }
            catch (Exception e)
            {
                return new TResult {ErrorCode = e.Message + (e.Message.Length > 5 ? $"{Environment.NewLine}Stacktrace:{e.StackTrace ?? e.InnerException?.StackTrace}{Environment.NewLine}{e.InnerException?.Message}" : "") };
            }
        }

        #endregion


        #region Public Get methods

        public AccountDetails GetAccountDetails(long idUser, long? idRequestingUser = null)
        {
            return HandleApiCall<GetAccountDetailsProcessor, AccountDetails>(idUser, idRequestingUser);
        }

        public Board GetBoard(byte idBoard, long? idUser = null)
        {
            return HandleApiCall<GetBoardProcessor, Board>(idBoard, idUser);
        }

        public MessageBoard GetBoards()
        {
            return HandleApiCall<GetBoardsProcessor, MessageBoard>();
        }

        public TrainingCamp GetCampForTeam(long idTeam)
        {
            return HandleApiCall<GetCampForTeamProcessor, TrainingCamp>(idTeam);
        }

        public ServiceDictionary GetDictionary(byte idLanguage)
        {
            return HandleApiCall<GetDictionaryProcessor, ServiceDictionary>(idLanguage);
        }

        public Economy GetEconomy(long idTeam)
        {
            return HandleApiCall<GetEconomyProcessor, Economy>(idTeam);
        }

        public Forum GetForum(short idForum, long? idUser = null)
        {
            return HandleApiCall<GetForumProcessor, Forum>(idForum, idUser);
        }

        public Inbox GetInbox(long idTeam, int? page = null)
        {
            return HandleApiCall<GetInboxProcessor, Inbox>(idTeam, page);
        }

        public Message GetMessage(long idMessage, long? idUser = null)
        {
            return HandleApiCall<GetMessageProcessor, Message>(idMessage, idUser);
        }

        public OfficialMessages GetOfficialMessages(long idTeam, int? page = null)
        {
            return HandleApiCall<GetOfficialMessagesProcessor, OfficialMessages>(idTeam, page);
        }

        public Outbox GetOutbox(long idTeam, int? page = null)
        {
            return HandleApiCall<GetOutboxProcessor, Outbox>(idTeam, page);
        }

        public PressMessages GetPressMessages(long idTeam, int? page = null)
        {
            return HandleApiCall<GetPressMessagesProcessor, PressMessages>(idTeam, page);
        }

        public Contracts.DataContracts.Race.RaceDescription GetRace(long idRace)
        {
            return HandleApiCall<GetRaceProcessor, Contracts.DataContracts.Race.RaceDescription>(idRace);
        }

        public Contracts.DataContracts.Race.RaceHistory GetRaceHistory(long idRace, bool includeStages = false)
        {
            return HandleApiCall<GetRaceHistoryProcessor, Contracts.DataContracts.Race.RaceHistory>(idRace, includeStages);
        }

        public Contracts.DataContracts.Race.RaceRanking GetRaceParticipants(long idRace)
        {
            return HandleApiCall<GetRaceParticipantsProcessor, Contracts.DataContracts.Race.RaceRanking>(idRace);
        }

        public Contracts.DataContracts.Race.RaceDescription GetRaceProfile(long idRace)
        {
            return HandleApiCall<GetRaceProfileProcessor, Contracts.DataContracts.Race.RaceDescription>(idRace);
        }

        public Contracts.DataContracts.Race.RaceRanking GetRaceRanking(long idRace)
        {
            return HandleApiCall<GetRaceRankingProcessor, Contracts.DataContracts.Race.RaceRanking>(idRace);
        }

        public Contracts.DataContracts.Race.RaceReport GetRaceReport(long idRace)
        {
            return HandleApiCall<GetRaceReportProcessor, Contracts.DataContracts.Race.RaceReport>(idRace);
        }

        public Ranking GetRanking(long idDivision)
        {
            return HandleApiCall<GetRankingProcessor, Ranking>(idDivision);
        }

        public Rider GetRider(long idRider)
        {
            return HandleApiCall<GetRiderProcessor, Rider>(idRider);
        }

        public RiderEvents GetRiderEvents(long idRider)
        {
            return HandleApiCall<GetRiderEventsProcessor, RiderEvents>(idRider);
        }

        public RiderHistory GetRiderHistory(long idRider)
        {
            return HandleApiCall<GetRiderHistoryProcessor, RiderHistory>(idRider);
        }

        public TeamRiders GetRidersForTeam(long idTeam)
        {
            return HandleApiCall<GetRidersForTeamProcessor, TeamRiders>(idTeam);
        }

        public SearchResults GetSearchResults(byte searchType, string firstSearchValue, string secondSearchValue = null)
        {
            return HandleApiCall<GetSearchResultsProcessor, SearchResults>(searchType, firstSearchValue, secondSearchValue);
        }

        public Shop GetShop(long idTeam)
        {
            return HandleApiCall<GetShopProcessor, Shop>(idTeam);
        }

        public Sponsors GetSponsors(long idTeam)
        {
            return HandleApiCall<GetSponsorsProcessor, Sponsors>(idTeam);
        }

        public Contracts.DataContracts.Race.StageDescription GetStage(long idStage)
        {
            return HandleApiCall<GetStageProcessor, Contracts.DataContracts.Race.StageDescription>(idStage);
        }

        public Contracts.DataContracts.Race.RaceRanking GetStageRanking(long idRace, short idStage)
        {
            return HandleApiCall<GetStageRankingProcessor, Contracts.DataContracts.Race.RaceRanking>(idRace, idStage);
        }

        public Contracts.DataContracts.Race.RaceReport GetStageReport(long idRace, short idStage)
        {
            return HandleApiCall<GetStageReportProcessor, Contracts.DataContracts.Race.RaceReport>(idRace, idStage);
        }

        public Store GetStore(long idUser)
        {
            return HandleApiCall<GetStoreProcessor, Store>(idUser);
        }

        public TeamDetails GetTeamDetails(long idTeam, long? idUser = null)
        {
            return HandleApiCall<GetTeamDetailsProcessor, TeamDetails>(idTeam, idUser);
        }

        public TeamDetails GetTeamDetailsForFacebookId(string facebookId)
        {
            return HandleApiCall<GetTeamDetailsForFacebookIdProcessor, TeamDetails>(facebookId);
        }

        public TeamHistory GetTeamHistory(long idTeam)
        {
            return HandleApiCall<GetTeamHistoryProcessor, TeamHistory>(idTeam);
        }

        public Thread GetThread(long idThread, long? idUser = null)
        {
            return HandleApiCall<GetThreadProcessor, Thread>(idThread, idUser);
        }

        public Training GetTraining(long idTeam)
        {
            return HandleApiCall<GetTrainingProcessor, Training>(idTeam);
        }

        public TeamTransactions GetTransactions(long idTeam, DateTime from, DateTime? to = null)
        {
            return HandleApiCall<GetTransactionsProcessor, TeamTransactions>(idTeam, from, to);
        }

        public RiderTransferHistory GetTransferHistoryForRider(long idRider, DateTime from, DateTime? to = null)
        {
            return HandleApiCall<GetTransferHistoryForRiderProcessor, RiderTransferHistory>(idRider, from, to);
        }

        public TeamTransferHistory GetTransferHistoryForTeam(long idTeam, DateTime from, DateTime? to = null)
        {
            return HandleApiCall<GetTransferHistoryForTeamProcessor, TeamTransferHistory>(idTeam, from, to);
        }

        public TeamTransfers GetTransfers(long idTeam)
        {
            return HandleApiCall<GetTransfersProcessor, TeamTransfers>(idTeam);
        }

        public TeamYouthRiders GetYouthRidersForTeam(long idTeam)
        {
            return HandleApiCall<GetYouthRidersForTeamProcessor, TeamYouthRiders>(idTeam);
        }

        #endregion


        #region Public Post methods

        public ResultBase BuyShopProducts(long idTeam, ProductBase[] products)
        {
            return HandleApiCall<BuyShopProductsProcessor, ResultBase>(idTeam, products);
        }

        public ResultBase CreateUser(string facebookId, CreateUserRequest createUserRequest)
        {
            return HandleApiCall<CreateUserProcessor, ResultBase>(facebookId, createUserRequest);
        }

        public ResultBase EditMessage(long idMessage, long idUser, string message)
        {
            return HandleApiCall<EditMessageProcessor, ResultBase>(idMessage, idUser, message);
        }

        public ResultBase PostMessage(long idThread, long idUser, string userIp, string message, long? idMessageRepliedTo = null)
        {
            return HandleApiCall<PostMessageProcessor, ResultBase>(idThread, idUser, userIp, message, idMessageRepliedTo);
        }

        public ResultBase SendMessage(long idUser, string recipientUserName, string subject, string message)
        {
            return HandleApiCall<SendMessageProcessor, ResultBase>(idUser, recipientUserName, subject, message);
        }

        public ResultBase SetRiderCategory(long idTeam, long idRider, byte category)
        {
            return HandleApiCall<SetRiderCategoryProcessor, ResultBase>(idTeam, idRider, category);
        }

        public ResultBase SetShopPrices(long idTeam, ProductBase[] products)
        {
            return HandleApiCall<SetShopPricesProcessor, ResultBase>(idTeam, products);
        }

        public ResultBase SetTeamDetails(long idTeam, string shortName, string longName, byte idLogo)
        {
            return HandleApiCall<SetTeamDetailsProcessor, ResultBase>(idTeam, shortName, longName, idLogo);
        }

        public ResultBase SetTraining(long idTeam, byte training)
        {
            return HandleApiCall<SetTrainingProcessor, ResultBase>(idTeam, training);
        }

        public ResultBase SetUserDetails(long idUser, string userName, string email, byte language, bool hideEmail)
        {
            return HandleApiCall<SetUserDetailsProcessor, ResultBase>(idUser, userName, email, language, hideEmail);
        }

        public ResultBase StartShop(long idTeam)
        {
            return HandleApiCall<StartShopProcessor, ResultBase>(idTeam);
        }

        #endregion
    }
}