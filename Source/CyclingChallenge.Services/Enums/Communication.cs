﻿namespace CyclingChallenge.Services.Enums
{
    public class Communication
    {
        public struct Language
        {
            public const byte Dutch = 1;
            public const byte English = 2;
        }

        public struct BoardType
        {
            public const byte National = 1;
            public const byte Game = 2;

            public static string GetDescription(byte type)
            {
                switch (type)
                {
                    case National: return "National";
                    case Game: return "Game";
                    default: return string.Empty;
                }
            }
        }
    }
}