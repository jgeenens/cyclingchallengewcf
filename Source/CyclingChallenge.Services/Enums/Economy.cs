﻿namespace CyclingChallenge.Services.Enums
{
    public class Economy
    {
        public struct Status
        {
            public const byte Inactive = 0;
            public const byte Active = 1;
            public const byte Processing = 2;
        }

        public struct Currency
        {
            public const short Euro = 1;
            public const short Dollar = 2;
            public const short Pound = 3;
        }
    }
}