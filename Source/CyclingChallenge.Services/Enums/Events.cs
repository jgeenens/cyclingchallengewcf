﻿namespace CyclingChallenge.Services.Enums
{
    public class Events
    {
        public struct Team
        {
            public const int NewTeam = 1;
            public const int NameChange = 2;
        }

        public struct Winner
        {
            public const short Yellow = 1;
            public const short Green = 2;
            public const short Polka = 3;
            public const short Youth = 4;
        }
    }
}