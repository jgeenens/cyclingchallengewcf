﻿namespace CyclingChallenge.Services.Enums
{
    public struct Items
    {
        public const int And = 418;
        public const int First = 118;
        public const int Second = 1023;
        public const int Third = 1024;
        public const int Fourth = 1025;
        public const int Fifth = 1026;
        public const int Sixth = 1027;
        public const int Seventh = 1028;
        public const int Eighth = 1029;
        public const int Ninth = 1030;
        public const int Tenth = 1031;
        public const int Eleventh = 1032;
        public const int Twelfth = 1033; 
        public const int HistoryFirstInRace = 674;
        public const int HistoryFirstInStage = 1004;
        public const int HistoryNameChange = 790;
        public const int HistoryNewTeam = 789;
        public const int ParticipationHint = 779;
        public const int EconomySponsors = 27;
        public const int EconomyTransactions = 296;
        public const int EconomyPriceMoney = 312;
        public const int EconomyTransfers = 28;
        public const int EconomyShopRevenues = 50;
        public const int EconomyOther = 313;
        public const int EconomyStaff = 305;
        public const int EconomyWages = 306;
        public const int EconomyYouth = 42;
        public const int EconomyRegistration = 314;
        public const int EconomyShopExpenses = 307;
        public const int EconomyTraining = 24;
        public const int EconomyShopOpening = 347;
        public const int MessageCampDeparture = 579;
        public const int MessageCampProgress = 588;
        public const int MessageCampImprovement = 589;
        public const int MessageShopNoSales = 581;
        public const int MessageShopSales = 582;
        public const int MessageShopUnitsSold = 805;
        public const int TrainingImprovement = 580;
        public const int TrainingTechnique = 359;
        public const int TrainingStamina = 361;
        public const int TrainingDescending = 362;
        public const int TrainingSpring = 363;
        public const int TrainingTimeTrial = 364;
        public const int TrainingClimbing = 365;
        public const int TrainingPlains = 128;
        public const int TrainingMountains = 129;
        public const int TrainingHills = 130;
        public const int TrainingCobblestones = 131;
        public const int TransactionShopOpening = 347;
        public const int TransactionShopPurchase = 348;
        public const int TransactionShopSales = 349;
        public const int TransactionRiderBought = 315;
        public const int TransactionRiderSold = 316;
        public const int TransactionTrainingCamp = 136;
        public const int TransactionNewTrainer = 317;
        public const int TransactionFirstInRace = 320;
        public const int TransactionBonus = 319;
    }

    public struct AddOns
    {
        public const byte ActivateShopManager = 3;
        public const byte BeginnerShopManager = 4;
        public const byte BasicShopManager = 5;
        public const byte IntermediateShopManager = 6;
        public const byte AdvancedShopManager = 7;
        public const byte ExpertShopManager = 8;
        public const byte WorldClassShopManager = 9;
        public const byte ActivateRaceManager = 10;
        public const byte BeginnerRaceManager = 11;
        public const byte BasicRaceManager = 12;
        public const byte IntermediateRaceManager = 13;
        public const byte AdvancedRaceManager = 14;
        public const byte ExpertRaceManager = 15;
        public const byte WorldClassRaceManager = 16;
        public const byte ActivateStaffManager = 17;
        public const byte BeginnerStaffManager = 18;
        public const byte BasicStaffManager = 19;
        public const byte IntermediateStaffManager = 20;
        public const byte AdvancedStaffManager = 21;
        public const byte ExpertStaffManager = 22;
        public const byte WorldClassStaffManager = 23;
        public const byte Logo = 25;
    }

    public struct Countries
    {
        public const short Belgium = 102;
        public const short Netherlands = 103;
        public const short UnitedKingdom = 104;
        public const short Germany = 105;
        public const short France = 106;
        public const short Austria = 107;
        public const short Italy = 108;
        public const short Qatar = 109;
        public const short Spain = 110;
        public const short World = 1151;
    }

    public struct Nationalities
    {
        public const short Belgian = 1007;
        public const short Dutchman = 1008;
        public const short Brit = 1009;
        public const short German = 1010;
        public const short Frenchman = 1011;
        public const short Austrian = 1012;
        public const short Italian = 1013;
        public const short Qatari = 1014;
        public const short Spaniard = 1015;
    }

    public struct Products
    {
        public const short CCDiamonds12000 = 1152;
        public const short CCDiamonds6000 = 1153;
        public const short CCDiamonds3000 = 1154;
        public const short CCDiamonds1000 = 1155;
        public const short CCDiamonds600 = 1156;
        public const short CCDiamonds250 = 1157;
    }

    public struct ProductDescriptions
    {
        public const short CCDiamonds12000 = 1158;
        public const short CCDiamonds6000 = 1159;
        public const short CCDiamonds3000 = 1160;
        public const short CCDiamonds1000 = 1161;
        public const short CCDiamonds600 = 1162;
        public const short CCDiamonds250 = 1163;
    }

    public struct RaceEvents
    {
        public const long RaceStartsOnSunnyDay = 3;
        public const long EscapeFromPelotonThreeRiders = 16;
    }

    public struct Weather
    {
        public const int Sunny = 655;
        public const int Cloudy = 656;
        public const int Windy = 657;
        public const int Stormy = 658;
        public const int Rainy = 659;
    }
}