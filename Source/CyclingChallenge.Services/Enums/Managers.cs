﻿namespace CyclingChallenge.Services.Enums
{
    public class Managers
    {
        public struct Type
        {
            public const byte Shop = 1;
            public const byte Race = 2;
            public const byte Staff = 3;
        }

        public struct Status
        {
            public const byte Locked = 0;
            public const byte Active = 1;
            public const byte Learning = 2;
        }

        public struct Level
        {
            public const byte Apprentice = 0;
            public const byte Beginner = 1;
            public const byte Basic = 2;
            public const byte Intermediate = 3;
            public const byte Advanced = 4;
            public const byte Expert = 5;
            public const byte WorldClass = 6;
        }
    }
}