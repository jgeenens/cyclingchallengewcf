﻿namespace CyclingChallenge.Services.Enums
{
    public class Message
    {
        public struct Type
        {
            public const byte Private = 0;
            public const byte Official = 1;
        }
    }
}