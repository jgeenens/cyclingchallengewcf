﻿namespace CyclingChallenge.Services.Enums
{
    public struct Order
    {
        public const bool Descending = false;
        public const bool Ascending = true;
    }
}