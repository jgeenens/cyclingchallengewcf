﻿namespace CyclingChallenge.Services.Enums
{
    public struct Profile
    {
        public const byte Hills = 0;
        public const byte CobbleStonesReverse = 1;
        public const byte Mountains = 2;
        public const byte TimeTrial = 3;
        public const byte HighMountains = 4;
        public const byte TeamTimeTrial = 6;
        public const byte Plains = 7;
        public const byte CobbleStones = 9;
        public const byte PlainsTimeTrial = 10;
        public const byte MountainTimeTrial = 11;
    }
}