﻿namespace CyclingChallenge.Services.Enums
{
    public struct Promotion
    {
        public const byte NoPromotion = 0;
        public const byte TopTwoPromote = 1;
        public const byte TopFourPromote = 2;
    }
}