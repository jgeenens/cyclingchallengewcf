﻿namespace CyclingChallenge.Services.Enums
{
    public struct RaceStatus
    {
        public const byte Open = 0;
        public const byte Closed = 1;
        public const byte Running = 2;
        public const byte BetweenStages = 3;
        public const byte Finished = 4;
    }
}