﻿namespace CyclingChallenge.Services.Enums
{
    public class Rider
    {
        public struct Skill
        {
            public const int Technique = 0;
            public const int Stamina = 1;
            public const int Descending = 2;
            public const int Sprint = 3;
            public const int TimeTrial = 4;
            public const int Climbing = 5;
        }

        public struct Terrain
        {
            public const int Plains = 0;
            public const int Mountains = 1;
            public const int Hills = 2;
            public const int Cobblestones = 3;
        }

        public struct Transfer
        {
            public const byte InProgress = 1;
            public const byte Processing = 2;
            public const byte Finished = 3;
        }
    }
}