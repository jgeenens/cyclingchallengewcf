﻿namespace CyclingChallenge.Services.Enums
{
    public struct SearchType
    {
        public const byte Division = 1;
        public const byte Rider = 2;
        public const byte Team = 3;
    }
}