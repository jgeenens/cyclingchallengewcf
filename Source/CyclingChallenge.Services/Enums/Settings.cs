﻿namespace CyclingChallenge.Services.Enums
{
    public struct Settings
    {
        public const long SeasonStart = 1;
        public const long Ticket = 2;
        public const long Meal = 3;
        public const long EngineTime = 4;
        public const long CampUpdate = 5;
        public const long Credit = 29;
        public const long LastChange = 27;
        public const long SellProduct1 = 6;
        public const long SellProduct2 = 7;
        public const long SellProduct3 = 8;
        public const long SellProduct4 = 9;
        public const long SellProduct5 = 10;
        public const long Points1 = 11;
        public const long Points2 = 12;
        public const long Points3 = 13;
        public const long Points4 = 14;
        public const long Points5 = 15;
        public const long Points6 = 16;
        public const long Points7 = 17;
        public const long Points8 = 18;
        public const long Points9 = 21;
        public const long Points10 = 22;
        public const long MaxValues = 35;
        public const long MaxYouthValues = 37;
        public const long MinValues = 34;
        public const long MinYouthValues = 36;
        public const long Money1 = 19;
        public const long Money2 = 23;
        public const long Money3 = 24;
        public const long Riders = 32;
        public const long Shop = 28;
        public const long Supporters = 30;
        public const long Supporters1 = 20;
        public const long Supporters2 = 25;
        public const long Season = 26;
        public const long Tokens = 31;
        public const long YouthRiders = 33;
    }
}