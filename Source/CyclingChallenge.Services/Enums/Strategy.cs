﻿namespace CyclingChallenge.Services.Enums
{
    public class Strategy
    {
        public struct Type
        {
            public const byte None = 0;
            public const byte Captain = 1;
            public const byte Domestique = 2;
            public const byte Escapee = 3;
            public const byte Puncher = 4;
            public const byte Climber = 5;
            public const byte Sprinter = 6;
            public const byte Finisher = 7;
            public const byte Adventurer = 8;
        }

        public struct Job
        {
            public const byte StayInThePack = 1;
            public const byte EarlyEscape = 2;
            public const byte WaitForSpecialty = 3;
            public const byte Finish = 4;
        }

        public struct Intensity
        {
            public const byte Easy = 1;
            public const byte Normal = 2;
            public const byte High = 3;
            public const byte Outperform = 4;
        }
    }
}