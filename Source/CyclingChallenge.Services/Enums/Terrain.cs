﻿namespace CyclingChallenge.Services.Enums
{
    public struct Terrain
    {
        public const byte Plains = 0;
        public const byte CobbleStones = 1;
        public const byte Hills = 2;
        public const byte Mountains = 3;
    }
}