﻿namespace CyclingChallenge.Services.Enums
{
    public struct Topics
    {
        public const short Languages = 1;
        public const short Countries = 2;
        public const short Locations = 3;
        public const short Login = 4;
        public const short Menu = 5;
        public const short Registration = 6;
        public const short Footer = 7;
        public const short Races = 8;
        public const short None = 9;
        public const short Products = 10;
        public const short Reports = 11;
    }
}