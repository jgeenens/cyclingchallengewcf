﻿namespace CyclingChallenge.Services.Enums
{
    public struct Type
    {
        public const byte TourDeFrance = 1;
        public const byte GrandTours = 2;
        public const byte SmallTours = 3;
        public const byte GrandDayRaces = 4;
        public const byte SmallDayRaces = 5;
        public const byte Giro = 7;
        public const byte WorldChampionship = 8;
        public const byte WorldChampionshipTimeTrial = 9;
    }
}