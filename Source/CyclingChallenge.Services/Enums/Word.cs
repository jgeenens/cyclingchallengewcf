﻿namespace CyclingChallenge.Services.Enums
{
    public struct Word
    {
        public const byte The = 0;
        public const byte Adverb = 1;
        public const byte Adjective = 2;
        public const byte Noun = 3;
        public const byte Royal = 4;
        public const byte Team = 5;
        public const byte Sparta = 6;
        public const byte Name = 7;
    }
}