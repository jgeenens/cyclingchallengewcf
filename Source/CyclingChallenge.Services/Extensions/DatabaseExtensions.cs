﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace CyclingChallenge
{
    public static class DatabaseExtensions
    {
        public static T RandomOrDefault<T>(this DbSet<T> set) where T: class
        {
            return set.RandomOrDefault(_ => true);
        }

        public static T RandomOrDefault<T>(this DbSet<T> set, Expression<Func<T, bool>> predicate) where T: class
        {
            return set.Where(predicate).ToList().OrderBy(r => Guid.NewGuid()).FirstOrDefault();
        }

        public static T Random<T>(this DbSet<T> set) where T: class
        {
            return set.Random(_ => true);
        }

        public static T Random<T>(this DbSet<T> set, Expression<Func<T, bool>> predicate) where T : class
        {
            return set.Where(predicate).ToList().OrderBy(r => Guid.NewGuid()).First();
        }
    }
}