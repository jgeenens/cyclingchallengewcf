﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CyclingChallenge
{
    public static class ListExtensions
    {
        private static Random rnd = new Random();

        public static T RandomItem<T>(this IEnumerable<T> list, Func<T, bool> filter = null)
        {
            var selection = list.Where(filter ?? (s => true));
            return selection.ElementAt(rnd.Next(selection.Count()));
        }

        public static T RandomItemOrDefault<T>(this IEnumerable<T> list, Func<T, bool> filter = null)
        {
            var selection = list.Where(filter ?? (s => true));
            return selection.Any() ? selection.ElementAt(rnd.Next(selection.Count())) : default(T);
        }
    }
}