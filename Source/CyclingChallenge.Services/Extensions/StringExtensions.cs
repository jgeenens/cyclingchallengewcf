﻿using System;

namespace CyclingChallenge
{
    public static class StringExtensions
    {
        public static string ReplaceLast(this string str, string oldValue, string newValue)
        {
            var index = str.LastIndexOf(oldValue, StringComparison.InvariantCultureIgnoreCase);
            return index < 0
                ? str
                : str.Remove(index, oldValue.Length).Insert(index, newValue);
        }

        public static byte? ToNullableByte(this string str)
        {
            byte result;
            var isByte = byte.TryParse(str, out result);
            if (!isByte)
            {
                return null;
            }

            return result;
        }

        public static short? ToNullableShort(this string str)
        {
            short result;
            var isShort = short.TryParse(str, out result);
            if (!isShort)
            {
                return null;
            }

            return result;
        }

        public static long? ToNullableLong(this string str)
        {
            long result;
            var isLong = long.TryParse(str, out result);
            if (!isLong)
            {
                return null;
            }

            return result;
        }
    }
}