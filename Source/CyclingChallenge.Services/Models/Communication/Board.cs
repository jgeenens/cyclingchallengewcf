﻿using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Boards", Schema = "dbo")]
    public class Board
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public virtual string Name { get; set; }

        [Column("Description"), Required, MaxLength(255)]
        public virtual string Description { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Country"), ForeignKey("BoardCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country BoardCountry { get; set; }
    }
}