﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Dictionary", Schema = "dbo")]
    public class Dictionary
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Item"), ForeignKey("DictionaryItem"), Required]
        public virtual short Item { get; set; }

        public virtual Item DictionaryItem { get; set; }

        [Column("Language"), ForeignKey("DictionaryLanguage"), Required]
        public virtual byte Language { get; set; }

        public virtual Language DictionaryLanguage { get; set; }

        [Column("Text"), Required, MaxLength(-1)]
        public virtual string Text { get; set; }
    }
}