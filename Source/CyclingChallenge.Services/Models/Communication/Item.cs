﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Items", Schema = "dbo")]
    public class Item
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Topic"), ForeignKey("ItemTopic"), Required]
        public virtual short Topic { get; set; }

        public virtual Topic ItemTopic { get; set; }
    }
}