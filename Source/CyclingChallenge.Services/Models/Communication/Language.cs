﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Languages", Schema = "dbo")]
    public class Language
    {
        [Column("Id"), Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Required]
        public virtual byte Id { get; set; }

        [Column("Name"), ForeignKey("LanguageName"), Required]
        public virtual short Name { get; set; }

        public virtual Item LanguageName { get; set; }

        [Column("Abbreviations"), Required]
        public virtual string Abbreviations { get; set; }
    }
}