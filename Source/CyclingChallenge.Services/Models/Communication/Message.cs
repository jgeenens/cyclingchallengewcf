﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Messages", Schema = "dbo")]
    public class Message
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Thread"), ForeignKey("MessageThread"), Required]
        public virtual long Thread { get; set; }

        public virtual Thread MessageThread { get; set; }

        [Column("MessageInThread"), Required]
        public virtual byte MessageInThread { get; set; }

        [Column("User"), ForeignKey("MessageUser"), Required]
        public virtual long User { get; set; }

        public virtual User MessageUser { get; set; }

        [Column("ReplyTo"), ForeignKey("MessageReplyTo")]
        public virtual long? ReplyTo { get; set; }

        public virtual Message MessageReplyTo { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Content"), Required, MaxLength(-1)]
        public virtual string Content { get; set; }

        [Column("LastChange")]
        public virtual DateTime? LastChange { get; set; }

        [Column("UserChange"), ForeignKey("MessageUserChange")]
        public virtual long? UserChange { get; set; }

        public virtual User MessageUserChange { get; set; }

        [Column("UserIp"), Required, MaxLength(15)]
        public virtual string UserIp { get; set; }

        [Column("Obsolete"), Required]
        public virtual bool Obsolete { get; set; }
    }
}