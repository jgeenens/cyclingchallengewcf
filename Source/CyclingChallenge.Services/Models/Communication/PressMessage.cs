﻿using CyclingChallenge.Services.Models.Team;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("PressMessages", Schema = "dbo")]
    public class PressMessage
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("User"), ForeignKey("PressMessageUser"), Required]
        public virtual long User { get; set; }

        public virtual User PressMessageUser { get; set; }

        [Column("Title"), Required, MaxLength(50)]
        public virtual string Title { get; set; }

        [Column("Body"), Required, MaxLength(-1)]
        public virtual string Body { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Message"), ForeignKey("PressMessageMessage")]
        public virtual long? Message { get; set; }

        public virtual PressMessage PressMessageMessage { get; set; }
    }
}