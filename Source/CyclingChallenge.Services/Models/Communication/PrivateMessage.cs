﻿using CyclingChallenge.Services.Models.Team;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("PrivateMessages", Schema = "dbo")]
    public class PrivateMessage
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Subject"), Required, MaxLength(60)]
        public virtual string Subject { get; set; }

        [Column("Message"), Required, MaxLength(-1)]
        public virtual string Message { get; set; }

        [Column("Sender"), ForeignKey("PrivateMessageSender")]
        public virtual long? Sender { get; set; }

        public virtual User PrivateMessageSender { get; set; }

        [Column("Recipient"), ForeignKey("PrivateMessageRecipient")]
        public virtual long? Recipient { get; set; }

        public virtual User PrivateMessageRecipient { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Read"), Required]
        public virtual bool Read { get; set; }

        [Column("DeletedBySender"), Required]
        public virtual bool DeletedBySender { get; set; }

        [Column("DeletedByRecipient"), Required]
        public virtual bool DeletedByRecipient { get; set; }
    }
}