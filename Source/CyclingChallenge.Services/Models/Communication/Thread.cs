﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Threads", Schema = "dbo")]
    public class Thread
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Board"), ForeignKey("ThreadBoard"), Required]
        public virtual short Board { get; set; }

        public virtual Board ThreadBoard { get; set; }

        [Column("Title"), Required, MaxLength(60)]
        public virtual string Title { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Originator"), ForeignKey("ThreadOriginator"), Required]
        public virtual long Originator { get; set; }

        public virtual User ThreadOriginator { get; set; }

        [Column("LastChange"), Required]
        public virtual DateTime LastChange { get; set; }

        [Column("LastPost"), Required]
        public virtual long LastPost { get; set; }

        [Column("LastOriginator"), ForeignKey("ThreadLastOriginator"), Required]
        public virtual long LastOriginator { get; set; }

        public virtual User ThreadLastOriginator { get; set; }
    }
}