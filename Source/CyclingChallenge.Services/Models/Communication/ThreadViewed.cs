﻿using CyclingChallenge.Services.Models.Team;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("ThreadsViewed", Schema = "dbo")]
    public class ThreadViewed
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Thread"), ForeignKey("ThreadViewedThread"), Required]
        public virtual long Thread { get; set; }

        public virtual Thread ThreadViewedThread { get; set; }

        [Column("User"), ForeignKey("ThreadViewedUser"), Required]
        public virtual long User { get; set; }

        public virtual User ThreadViewedUser { get; set; }

        [Column("LastView"), Required]
        public virtual DateTime LastView { get; set; }
    }
}