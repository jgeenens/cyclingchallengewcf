﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Communication
{
    [Table("Topics", Schema = "dbo")]
    public class Topic
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public virtual string Name { get; set; }
    }
}