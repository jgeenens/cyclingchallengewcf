﻿using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Training;
using System.Data.Entity;

namespace CyclingChallenge.Services.Models
{
    public partial class CyclingChallengeModelContainer : DbContext, ICyclingChallengeModelContainer
    {
        #region Communication

        public DbSet<Board> Boards { get; set; }
        public DbSet<Dictionary> Dictionaries { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<PressMessage> PressMessages { get; set; }
        public DbSet<PrivateMessage> PrivateMessages { get; set; }
        public DbSet<Thread> Threads { get; set; }
        public DbSet<ThreadViewed> ThreadsViewed { get; set; }
        public DbSet<Topic> Topics { get; set; }

        #endregion


        #region Economy

        public DbSet<Clubshop> Clubshops { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Economy.Economy> Economies { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        #endregion


        #region Game

        public DbSet<Addon> Addons { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Logo> Logos { get; set; }
        public DbSet<Name> Names { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Word> Words { get; set; }

        #endregion


        #region Location

        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Location.Location> Locations { get; set; }
        public DbSet<Region> Regions { get; set; }

        #endregion


        #region Race

        public DbSet<Climb> Climbs { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Race.Race> Races { get; set; }
        public DbSet<RaceDescription> RaceDescriptions { get; set; }
        public DbSet<RaceEvent> RaceEvents { get; set; }
        public DbSet<RaceHistory> RaceHistories { get; set; }
        public DbSet<RaceOrder> RaceOrders { get; set; }
        public DbSet<RaceRanking> RaceRankings { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<TimeTable> TimeTables { get; set; }
        public DbSet<Track> Tracks { get; set; }

        #endregion


        #region Rider

        public DbSet<Calendar> Calendars { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<PlayerProgress> PlayerProgresses { get; set; }
        public DbSet<PlayerValue> PlayerValues { get; set; }
        public DbSet<ScoutedPlayer> ScoutedPlayers { get; set; }
        public DbSet<TransferHistory> TransferHistories { get; set; }
        public DbSet<Youth> Youths { get; set; }

        #endregion


        #region Staff

        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Scout> Scouts { get; set; }

        #endregion

        
        #region Team

        public DbSet<Division> Divisions { get; set; }
        public DbSet<Team.Team> Teams { get; set; }
        public DbSet<TeamHistory> TeamHistories { get; set; }
        public DbSet<User> Users { get; set; }

        #endregion


        #region Training

        public DbSet<Booking> Bookings { get; set; }

        #endregion
    }
}