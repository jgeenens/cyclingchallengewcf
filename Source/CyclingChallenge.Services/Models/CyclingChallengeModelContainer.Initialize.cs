﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Security.Principal;

namespace CyclingChallenge.Services.Models
{
    public partial class CyclingChallengeModelContainer
    {
        #region Fields

        private WindowsImpersonationContext _context;
        private static readonly string _name;

        #endregion


        #region Constructors

        static CyclingChallengeModelContainer()
        {
            _name = "CyclingChallenge";
        }

        protected CyclingChallengeModelContainer(int timeout)
            : base(_name)
        {
            Configuration.ValidateOnSaveEnabled = false;
            SetTimeout(timeout);
        }

        protected CyclingChallengeModelContainer(string nameOrConnectionString, int timeout)
            : base(nameOrConnectionString)
        {
            Configuration.ValidateOnSaveEnabled = false;
            SetTimeout(timeout);
        }

        protected CyclingChallengeModelContainer(WindowsImpersonationContext context, int timeout)
            : base(_name)
        {
            this._context = context;
            Configuration.ValidateOnSaveEnabled = false;
            SetTimeout(timeout);
        }

        private void SetTimeout(int timeout)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = timeout;
        }

        #endregion


        #region Factory Methods

        public static ICyclingChallengeModelContainer CreateDbContext(string connectionString = null, int timeout = 120)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                return new CyclingChallengeModelContainer(connectionString, timeout);
            }

            return new CyclingChallengeModelContainer(timeout);
        }

        public static RuntimeManager.DbContextFactoryDelegate CreateDbContextFactory()
        {
            return CreateDbContext;
        }

        #endregion


        #region Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (_context != null)
            {
                _context.Undo();
                _context.Dispose();
                _context = null;
            }
        }

        #endregion


        #region Methods

        public static void Initialize()
        {
            Database.SetInitializer<CyclingChallengeModelContainer>(null);
        }

        #endregion
    }
}