﻿using CyclingChallenge.Services.Models.Communication;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Economy
{
    [Table("Clubshop", Schema = "dbo")]
    public class Clubshop
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("ClubshopTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team ClubshopTeam { get; set; }

        [Column("Positions"), Required]
        public virtual byte Positions { get; set; }

        [Column("Participated"), Required]
        public virtual byte Participated { get; set; }

        [Column("Points"), Required]
        public virtual short Points { get; set; }

        [Column("Top"), Required]
        public virtual byte Top { get; set; }

        [Column("Wins"), Required]
        public virtual byte Wins { get; set; }

        [Column("Quantity1"), Required]
        public virtual short Quantity1 { get; set; }

        [Column("Quantity2"), Required]
        public virtual short Quantity2 { get; set; }

        [Column("Quantity3"), Required]
        public virtual short Quantity3 { get; set; }

        [Column("Quantity4"), Required]
        public virtual short Quantity4 { get; set; }

        [Column("Quantity5"), Required]
        public virtual short Quantity5 { get; set; }

        [Column("Price1")]
        public virtual short? Price1 { get; set; }

        [Column("Price2")]
        public virtual short? Price2 { get; set; }

        [Column("Price3")]
        public virtual short? Price3 { get; set; }

        [Column("Price4")]
        public virtual short? Price4 { get; set; }

        [Column("Price5")]
        public virtual short? Price5 { get; set; }

        [Column("Hint"), ForeignKey("ClubshopHint"), Required]
        public virtual short Hint { get; set; }

        public virtual Item ClubshopHint { get; set; }
    }
}