﻿using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Economy
{
    [Table("Currencies", Schema = "dbo")]
    public class Currency
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Country"), ForeignKey("CurrencyCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country CurrencyCountry { get; set; }

        [Column("Rate"), Required]
        public virtual decimal Rate { get; set; }

        [Column("Currency"), Required, MaxLength(10)]
        public virtual string Description { get; set; }

        [Column("Separator"), Required, MaxLength(1)]
        public virtual string Separator { get; set; }

        [Column("DecimalSeparator"), Required, MaxLength(1)]
        public virtual string DecimalSeparator { get; set; }

        [Column("Code"), Required, MaxLength(3)]
        public virtual string Code { get; set; }
    }
}