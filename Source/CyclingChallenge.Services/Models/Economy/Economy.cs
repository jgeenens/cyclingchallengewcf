﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Economy
{
    [Table("Economy", Schema = "dbo")]
    public class Economy
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("EconomyTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team EconomyTeam { get; set; }

        [Column("ReferenceDate"), Required]
        public virtual DateTime ReferenceDate { get; set; }

        [Column("Week"), Required]
        public virtual byte Week { get; set; }

        [Column("Day"), Required]
        public virtual byte Day { get; set; }

        [Column("Income1"), Required]
        public virtual long Income1 { get; set; }

        [Column("Income2"), Required]
        public virtual long Income2 { get; set; }

        [Column("Expense1"), Required]
        public virtual long Expense1 { get; set; }

        [Column("Expense2"), Required]
        public virtual long Expense2 { get; set; }

        [Column("Expense3"), Required]
        public virtual long Expense3 { get; set; }

        [Column("Expense4"), Required]
        public virtual long Expense4 { get; set; }

        [Column("Expense5"), Required]
        public virtual long Expense5 { get; set; }

        [Column("TransactionIn1"), Required]
        public virtual long TransactionIn1 { get; set; }

        [Column("TransactionIn2"), Required]
        public virtual long TransactionIn2 { get; set; }

        [Column("TransactionIn3"), Required]
        public virtual long TransactionIn3 { get; set; }

        [Column("TransactionIn4"), Required]
        public virtual long TransactionIn4 { get; set; }

        [Column("TransactionOut1"), Required]
        public virtual long TransactionOut1 { get; set; }

        [Column("TransactionOut2"), Required]
        public virtual long TransactionOut2 { get; set; }

        [Column("TransactionOut3"), Required]
        public virtual long TransactionOut3 { get; set; }

        [Column("TransactionOut4"), Required]
        public virtual long TransactionOut4 { get; set; }

        [Column("TransactionOut5"), Required]
        public virtual long TransactionOut5 { get; set; }

        [Column("Active"), Required]
        public virtual byte Active { get; set; }
    }
}