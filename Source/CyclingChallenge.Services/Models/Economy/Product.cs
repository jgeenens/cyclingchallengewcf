﻿using CyclingChallenge.Services.Models.Communication;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Economy
{
    [Table("Products", Schema = "dbo")]
    public class Product
    {
        [Column("Id"), Key, Required]
        public virtual byte Id { get; set; }

        [Column("Name"), ForeignKey("ProductName"), Required]
        public virtual short Name { get; set; }

        public virtual Item ProductName { get; set; }

        [Column("Buy"), Required]
        public virtual int Buy { get; set; }

        [Column("Sell"), Required]
        public virtual int Sell { get; set; }
    }
}