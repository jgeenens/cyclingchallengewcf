﻿using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Team;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Economy
{
    [Table("Sponsors", Schema = "dbo")]
    public class Sponsor
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public virtual string Name { get; set; }

        [Column("Weeks"), Required]
        public virtual byte Weeks { get; set; }

        [Column("Signing"), Required]
        public virtual long Signing { get; set; }

        [Column("Weekly"), Required]
        public virtual int Weekly { get; set; }

        [Column("Team"), ForeignKey("SponsorTeam")]
        public virtual long? Team { get; set; }

        public virtual Team.Team SponsorTeam { get; set; }

        [Column("Division"), ForeignKey("SponsorDivision"), Required]
        public virtual long Division { get; set; }

        public virtual Division SponsorDivision { get; set; }

        [Column("WeeksToGo"), Required]
        public virtual byte WeeksToGo { get; set; }

        [Column("Country"), ForeignKey("SponsorCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country SponsorCountry { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }
    }
}