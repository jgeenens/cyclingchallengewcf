﻿using CyclingChallenge.Services.Models.Communication;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Economy
{
    [Table("Transactions", Schema = "dbo")]
    public class Transaction
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("TransactionTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team TransactionTeam { get; set; }

        [Column("Outgoing"), Required]
        public virtual bool Outgoing { get; set; }

        [Column("DateTransaction"), Required]
        public virtual DateTime DateTransaction { get; set; }

        [Column("Value"), Required]
        public virtual long Value { get; set; }

        [Column("Description"), ForeignKey("TransactionDescription"), Required]
        public virtual short Description { get; set; }

        public virtual Item TransactionDescription { get; set; }

        [Column("Checked"), Required]
        public virtual bool Checked { get; set; }

        [Column("Info"), Required, MaxLength(25)]
        public virtual string Info { get; set; }
    }
}