﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Addons", Schema = "dbo")]
    public class Addon
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Tokens"), Required]
        public virtual short Tokens { get; set; }

        [Column("Description"), Required, MaxLength(255)]
        public virtual string Description { get; set; }
    }
}