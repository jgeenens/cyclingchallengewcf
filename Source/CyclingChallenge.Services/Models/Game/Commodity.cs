﻿using CyclingChallenge.Services.Models.Communication;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("OgProducts", Schema = "dbo")]
    public class Commodity
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Title"), ForeignKey("CommodityTitle"), Required]
        public virtual short Title { get; set; }

        public virtual Item CommodityTitle { get; set; }

        [Column("Description"), ForeignKey("CommodityDescription"), Required]
        public virtual short Description { get; set; }

        public virtual Item CommodityDescription { get; set; }

        [Column("Value"), Required]
        public virtual int Value { get; set; }
    }
}