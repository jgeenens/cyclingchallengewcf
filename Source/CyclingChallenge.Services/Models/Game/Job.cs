﻿using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Location;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Jobs", Schema = "dbo")]
    public class Job
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Scheduled"), Required]
        public virtual DateTime Scheduled { get; set; }

        [Column("Description"), Required]
        public virtual byte Description { get; set; }

        [Column("Country"), ForeignKey("JobCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country JobCountry { get; set; }

        [Column("Percentage"), Required]
        public virtual byte Percentage { get; set; }

        [Column("Done"), Required]
        public virtual bool Done { get; set; }

        [Column("Caption"), ForeignKey("JobCaption"), Required]
        public virtual short Caption { get; set; }

        public virtual Item JobCaption { get; set; }

        [Column("Interval"), Required]
        public virtual int Interval { get; set; }

        [Column("Explanation"), ForeignKey("JobExplanation"), Required]
        public virtual short Explanation { get; set; }

        public virtual Item JobExplanation { get; set; }
    }
}