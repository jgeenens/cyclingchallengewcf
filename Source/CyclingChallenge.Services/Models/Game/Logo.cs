﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Logos", Schema = "dbo")]
    public class Logo
    {
        [Column("Id"), Key, Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual byte Id { get; set; }

        [Column("Tokens"), Required]
        public virtual short Tokens { get; set; }
    }
}