﻿using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Names", Schema = "dbo")]
    public class Name
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("IsFirstName"), Required]
        public virtual bool IsFirstName { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public virtual string Description { get; set; }

        [Column("Country"), ForeignKey("NameCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country NameCountry { get; set; }

        [Column("Begin"), Required]
        public virtual long Begin { get; set; }

        [Column("End"), Required]
        public virtual long End { get; set; }
    }
}