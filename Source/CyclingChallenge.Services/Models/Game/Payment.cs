﻿using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Team;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("OgPayments", Schema = "dbo")]
    public class Payment
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Product"), ForeignKey("PaymentCommodity"), Required]
        public virtual short Commodity { get; set; }

        public virtual Commodity PaymentCommodity { get; set; }

        [Column("User"), ForeignKey("PaymentUser"), Required]
        public virtual long User { get; set; }

        public virtual User PaymentUser { get; set; }

        [Column("Price"), Required]
        public virtual int Price { get; set; }

        [Column("Payed")]
        public virtual int? Payed { get; set; }

        [Column("Currency"), ForeignKey("PaymentCurrency")]
        public virtual short? Currency { get; set; }

        public virtual Currency PaymentCurrency { get; set; }

        [Column("DateOrdered"), Required]
        public virtual DateTime DateOrdered { get; set; }

        [Column("DatePayed")]
        public virtual DateTime? DatePayed { get; set; }

        [Column("FaceBookPaymentID"), MaxLength(50)]
        public virtual string FaceBookPaymentID { get; set; }
    }
}