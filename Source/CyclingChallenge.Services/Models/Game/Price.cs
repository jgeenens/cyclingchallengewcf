﻿using CyclingChallenge.Services.Models.Economy;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("OgPrices", Schema = "dbo")]
    public class Price
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Product"), ForeignKey("PriceCommodity"), Required]
        public virtual short Commodity { get; set; }

        public virtual Commodity PriceCommodity { get; set; }

        [Column("Currency"), ForeignKey("PriceCurrency"), Required]
        public virtual short Currency { get; set; }

        public virtual Currency PriceCurrency { get; set; }

        [Column("Amount"), Required]
        public virtual long Amount { get; set; }
    }
}