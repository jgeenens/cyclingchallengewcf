﻿using CyclingChallenge.Services.Models.Location;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Registrations", Schema = "dbo")]
    public class Registration
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Country"), ForeignKey("RegistrationCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country RegistrationCountry { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Quantity"), Required]
        public virtual short Quantity { get; set; }
    }
}