﻿using CyclingChallenge.Services.Models.Team;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Sales", Schema = "dbo")]
    public class Sale
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("User"), ForeignKey("SaleUser"), Required]
        public virtual long User { get; set; }

        public virtual User SaleUser { get; set; }

        [Column("Addon"), ForeignKey("SaleAddon"), Required]
        public virtual short Addon { get; set; }

        public virtual Addon SaleAddon { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Amount"), Required]
        public virtual long Amount { get; set; }

        [Column("Price"), Required]
        public virtual long Price { get; set; }

        [Column("Status"), Required, MaxLength(100)]
        public virtual string Status { get; set; }
    }
}