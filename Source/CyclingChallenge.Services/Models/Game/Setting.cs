﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Settings", Schema = "dbo")]
    public class Setting
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Variable"), Required, MaxLength(20)]
        public virtual string Variable { get; set; }

        [Column("Value"), Required, MaxLength(80)]
        public virtual string Value { get; set; }

        [Column("Parent"), ForeignKey("SettingParent")]
        public virtual long? Parent { get; set; }

        public virtual Setting SettingParent { get; set; }
    }
}