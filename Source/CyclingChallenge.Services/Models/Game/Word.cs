﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Game
{
    [Table("Words", Schema = "dbo")]
    public class Word
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Word"), Required]
        public virtual string Value { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }
    }
}