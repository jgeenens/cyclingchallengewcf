﻿using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Training;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;

namespace CyclingChallenge.Services.Models
{
    public interface ICyclingChallengeModelContainer : IObjectContextAdapter, IDisposable
    {
        #region Database & context

        Database Database { get; }
        DbChangeTracker ChangeTracker { get; }
        DbContextConfiguration Configuration { get; }

        #endregion


        #region Database DbSet

        #region Communication

        DbSet<Board> Boards { get; set; }
        DbSet<Dictionary> Dictionaries { get; set; }
        DbSet<Item> Items { get; set; }
        DbSet<Language> Languages { get; set; }
        DbSet<Message> Messages { get; set; }
        DbSet<PressMessage> PressMessages { get; set; }
        DbSet<PrivateMessage> PrivateMessages { get; set; }
        DbSet<Communication.Thread> Threads { get; set; }
        DbSet<ThreadViewed> ThreadsViewed { get; set; }
        DbSet<Topic> Topics { get; set; }

        #endregion

        #region Economy

        DbSet<Clubshop> Clubshops { get; set; }
        DbSet<Currency> Currencies { get; set; }
        DbSet<Economy.Economy> Economies { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<Sponsor> Sponsors { get; set; }
        DbSet<Transaction> Transactions { get; set; }

        #endregion

        #region Game

        DbSet<Addon> Addons { get; set; }
        DbSet<Commodity> Commodities { get; set; }
        DbSet<Job> Jobs { get; set; }
        DbSet<Logo> Logos { get; set; }
        DbSet<Name> Names { get; set; }
        DbSet<Payment> Payments { get; set; }
        DbSet<Price> Prices { get; set; }
        DbSet<Registration> Registrations { get; set; }
        DbSet<Sale> Sales { get; set; }
        DbSet<Setting> Settings { get; set; }
        DbSet<Word> Words { get; set; }

        #endregion

        #region Location

        DbSet<City> Cities { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Location.Location> Locations { get; set; }
        DbSet<Region> Regions { get; set; }

        #endregion

        #region Race

        DbSet<Climb> Climbs { get; set; }
        DbSet<Event> Events { get; set; }
        DbSet<Race.Race> Races { get; set; }
        DbSet<RaceDescription> RaceDescriptions { get; set; }
        DbSet<RaceEvent> RaceEvents { get; set; }
        DbSet<RaceHistory> RaceHistories { get; set; }
        DbSet<RaceOrder> RaceOrders { get; set; }
        DbSet<RaceRanking> RaceRankings { get; set; }
        DbSet<Stage> Stages { get; set; }
        DbSet<TimeTable> TimeTables { get; set; }
        DbSet<Track> Tracks { get; set; }

        #endregion

        #region Rider

        DbSet<Calendar> Calendars { get; set; }
        DbSet<Player> Players { get; set; }
        DbSet<PlayerProgress> PlayerProgresses { get; set; }
        DbSet<PlayerValue> PlayerValues { get; set; }
        DbSet<ScoutedPlayer> ScoutedPlayers { get; set; }
        DbSet<TransferHistory> TransferHistories { get; set; }
        DbSet<Youth> Youths { get; set; }

        #endregion

        #region Staff

        DbSet<Coach> Coaches { get; set; }
        DbSet<Manager> Managers { get; set; }
        DbSet<Scout> Scouts { get; set; }

        #endregion

        #region Team

        DbSet<Division> Divisions { get; set; }
        DbSet<Team.Team> Teams { get; set; }
        DbSet<TeamHistory> TeamHistories { get; set; }
        DbSet<User> Users { get; set; }

        #endregion

        #region Training

        DbSet<Booking> Bookings { get; set; }

        #endregion

        #endregion


        #region Methods

        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet Set(Type entityType);
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        DbEntityEntry Entry(object entity);
        string ToString();
        bool Equals(object obj);
        int GetHashCode();
        Type GetType();

        #endregion
    }
}