﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Location
{
    [Table("Cities", Schema = "dbo")]
    public class City
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("City"), MaxLength(60)]
        public virtual string Name { get; set; }

        [Column("Village"), MaxLength(100)]
        public virtual string Village { get; set; }

        [Column("Region"), ForeignKey("CityRegion")]
        public virtual short? Region { get; set; }

        public virtual Region CityRegion { get; set; }
    }
}