﻿using CyclingChallenge.Services.Models.Communication;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Location
{
    [Table("Countries", Schema = "dbo")]
    public class Country
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Country"), Required, MaxLength(100)]
        public virtual string Name { get; set; }

        [Column("Abbreviation"), MaxLength(3)]
        public virtual string Abbreviation { get; set; }

        [Column("Tld"), MaxLength(10)]
        public virtual string Tld { get; set; }

        [Column("Active"), Required]
        public virtual bool Active { get; set; }

        [Column("Item"), ForeignKey("CountryItem"), Required]
        public virtual short Item { get; set; }

        public virtual Item CountryItem { get; set; }

        [Column("Nationality"), ForeignKey("CountryNationality")]
        public virtual short? Nationality { get; set; }

        public virtual Item CountryNationality { get; set; }
    }
}