﻿using CyclingChallenge.Services.Models.Communication;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Location
{
    [Table("Locations", Schema = "dbo")]
    public class Location
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Name"), ForeignKey("LocationName"), Required]
        public virtual short Name { get; set; }

        public virtual Item LocationName { get; set; }

        [Column("Country"), ForeignKey("LocationCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country LocationCountry { get; set; }

        [Column("City"), ForeignKey("LocationCity"), Required]
        public virtual long City { get; set; }

        public virtual City LocationCity { get; set; }

        [Column("Picture"), Required, MaxLength(8)]
        public virtual string Picture { get; set; }

        [Column("Rooms"), Required]
        public virtual byte Rooms { get; set; }

        [Column("PrimarySkill"), Required]
        public virtual byte PrimarySkill { get; set; }

        [Column("SecondarySkill"), Required]
        public virtual byte SecondarySkill { get; set; }

        [Column("Price"), Required]
        public virtual short Price { get; set; }

        [Column("Description"), Required]
        public virtual short Description { get; set; }
    }
}