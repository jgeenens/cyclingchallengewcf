﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Location
{
    [Table("Regions", Schema = "dbo")]
    public class Region
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public virtual string Name { get; set; }

        [Column("Country"), ForeignKey("RegionCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country RegionCountry { get; set; }

        [Column("Scouting"), Required]
        public virtual bool Scouting { get; set; }

        [Column("Weather"), Required]
        public virtual byte Weather { get; set; }
    }
}