﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("Climbs", Schema = "dbo")]
    public class Climb
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public virtual string Name { get; set; }

        [Column("Top"), Required]
        public virtual byte Top { get; set; }

        [Column("Altitude"), Required]
        public virtual short Altitude { get; set; }

        [Column("ShowStart"), Required]
        public virtual bool ShowStart { get; set; }

        [Column("ShowTop"), Required]
        public virtual bool ShowTop { get; set; }

        [Column("IsClimb"), Required]
        public virtual bool IsClimb { get; set; }
    }
}