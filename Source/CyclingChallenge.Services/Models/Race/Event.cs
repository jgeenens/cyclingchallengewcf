﻿using CyclingChallenge.Services.Models.Communication;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("Events", Schema = "dbo")]
    public class Event
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("EventClass"), Required]
        public virtual byte Class { get; set; }

        [Column("EventType"), Required]
        public virtual byte Type { get; set; }

        [Column("EventItem"), Required]
        public virtual byte Index { get; set; }

        [Column("Item"), ForeignKey("EventItem"), Required]
        public virtual short Item { get; set; }

        public virtual Item EventItem { get; set; }
    }
}