﻿using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("Races", Schema = "dbo")]
    public class Race
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("RaceDescription"), ForeignKey("RaceRaceDescription"), Required]
        public virtual short RaceDescription { get; set; }

        public virtual RaceDescription RaceRaceDescription { get; set; }

        [Column("Division"), ForeignKey("RaceDivision"), Required]
        public virtual long Division { get; set; }

        public virtual Division RaceDivision { get; set; }

        [Column("StartDate"), Required]
        public virtual DateTime StartDate { get; set; }

        [Column("EndDate"), Required]
        public virtual DateTime EndDate { get; set; }

        [Column("DaysInSeason"), Required]
        public virtual byte DaysInSeason { get; set; }

        [Column("Winner"), ForeignKey("RaceWinner")]
        public virtual long? Winner { get; set; }

        public virtual Player RaceWinner { get; set; }

        [Column("Participants"), Required]
        public virtual byte Participants { get; set; }

        [Column("LiveTime"), Required]
        public virtual int LiveTime { get; set; }

        [Column("Yellow"), ForeignKey("RaceYellow")]
        public virtual long? Yellow { get; set; }

        public virtual Player RaceYellow { get; set; }

        [Column("Green"), ForeignKey("RaceGreen")]
        public virtual long? Green { get; set; }

        public virtual Player RaceGreen { get; set; }

        [Column("Poka"), ForeignKey("RacePoka")]
        public virtual long? Poka { get; set; }

        public virtual Player RacePoka { get; set; }

        [Column("CurrentStage"), ForeignKey("RaceCurrentStage")]
        public virtual short? CurrentStage { get; set; }

        public virtual Stage RaceCurrentStage { get; set; }

        [Column("CurrentDay"), Required]
        public virtual byte CurrentDay { get; set; }

        [Column("Status"), Required]
        public virtual byte Status { get; set; }
    }
}