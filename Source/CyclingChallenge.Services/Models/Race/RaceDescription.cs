﻿using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("RaceDescription", Schema = "dbo")]
    public class RaceDescription
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("Country"), ForeignKey("RaceDescriptionCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country RaceDescriptionCountry { get; set; }

        [Column("Name"), ForeignKey("RaceDescriptionName"), Required]
        public virtual short Name { get; set; }

        public virtual Item RaceDescriptionName { get; set; }

        [Column("IsTour"), Required]
        public virtual bool IsTour { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Description"), ForeignKey("RaceDescriptionDescription")]
        public virtual short? Description { get; set; }

        public virtual Item RaceDescriptionDescription { get; set; }

        [Column("Source"), Required, MaxLength(256)]
        public virtual string Source { get; set; }

        [Column("IsOfficial"), Required]
        public virtual bool IsOfficial { get; set; }
    }
}