﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("RaceEvents", Schema = "dbo")]
    public class RaceEvent
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Race"), ForeignKey("RaceEventRace"), Required]
        public virtual long Race { get; set; }

        public virtual Race RaceEventRace { get; set; }

        [Column("Stage"), ForeignKey("RaceEventStage"), Required]
        public virtual short Stage { get; set; }

        public virtual Stage RaceEventStage { get; set; }

        [Column("Index"), Required]
        public virtual short Index { get; set; }

        [Column("Seconds"), Required]
        public virtual short Seconds { get; set; }

        [Column("Km"), Required]
        public virtual int Km { get; set; }

        [Column("Event"), ForeignKey("RaceEventEvent"), Required]
        public virtual long Event { get; set; }

        public virtual Event RaceEventEvent { get; set; }

        [Column("Info"), Required, MaxLength(255)]
        public virtual string Info { get; set; }
    }
}