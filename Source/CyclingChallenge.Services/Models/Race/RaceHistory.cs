﻿using CyclingChallenge.Services.Models.Rider;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("RaceHistory", Schema = "dbo")]
    public class RaceHistory
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("When"), Required]
        public virtual DateTime When { get; set; }

        [Column("Race"), ForeignKey("RaceHistoryRace"), Required]
        public virtual long Race { get; set; }

        public virtual Race RaceHistoryRace { get; set; }

        [Column("Stage"), ForeignKey("RaceHistoryStage")]
        public virtual short? Stage { get; set; }

        public virtual Stage RaceHistoryStage { get; set; }

        [Column("Position"), Required]
        public virtual byte Position { get; set; }

        [Column("Player"), ForeignKey("RaceHistoryPlayer"), Required]
        public virtual long Player { get; set; }

        public virtual Player RaceHistoryPlayer { get; set; }

        [Column("Team"), ForeignKey("RaceHistoryTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team RaceHistoryTeam { get; set; }

        [Column("TeamName"), Required, MaxLength(50)]
        public virtual string TeamName { get; set; }

        [Column("Season"), Required]
        public virtual byte Season { get; set; }
    }
}