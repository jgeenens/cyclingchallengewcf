﻿using CyclingChallenge.Services.Models.Rider;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("RaceOrders", Schema = "dbo")]
    public class RaceOrder
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Race"), ForeignKey("RaceOrderRace"), Required]
        public virtual long Race { get; set; }

        public virtual Race RaceOrderRace { get; set; }

        [Column("Stage"), ForeignKey("RaceOrderStage"), Required]
        public virtual short Stage { get; set; }

        public virtual Stage RaceOrderStage { get; set; }

        [Column("Team"), ForeignKey("RaceOrderTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team RaceOrderTeam { get; set; }

        [Column("Rank"), Required]
        public virtual byte Rank { get; set; }

        [Column("Player"), ForeignKey("RaceOrderPlayer"), Required]
        public virtual long Player { get; set; }

        public virtual Player RaceOrderPlayer { get; set; }

        [Column("MaxAhead")]
        public virtual short? MaxAhead { get; set; }

        [Column("Head"), Required]
        public virtual bool Head { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Job"), Required]
        public virtual byte Job { get; set; }

        [Column("Intensity"), Required]
        public virtual byte Intensity { get; set; }
    }
}