﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("RaceRanking", Schema = "dbo")]
    public class RaceRanking
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Race"), ForeignKey("RaceRankingRace"), Required]
        public virtual long Race { get; set; }

        public virtual Race RaceRankingRace { get; set; }

        [Column("Stage"), ForeignKey("RaceRankingStage"), Required]
        public virtual short Stage { get; set; }

        public virtual Stage RaceRankingStage { get; set; }

        [Column("Ranking"), Required, MaxLength(-1)]
        public virtual string Ranking { get; set; }

        [Column("Teams"), Required, MaxLength(255)]
        public virtual string Teams { get; set; }

        [Column("PointsRanking"), MaxLength(-1)]
        public virtual string PointsRanking { get; set; }

        [Column("MountainsRanking"), MaxLength(-1)]
        public virtual string MountainsRanking { get; set; }
    }
}