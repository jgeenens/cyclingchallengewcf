﻿using System.Collections.Generic;

namespace CyclingChallenge.Services.Models.Race
{
    public class ReportInfo
    {
        public List<Rider> Riders;
        public List<InfoTeam> Teams;
        public List<Group> Groups;
        public long? Climb;
        public short? Ahead;
    }

    public class Group
    {
        public byte Id;
        public long IdRider;
    }

    public class Rider
    {
        public long Id;
        public long? IdTeam;
        public byte? Rank;
        public short? Time;
    }

    public class InfoTeam
    {
        public long Id;
        public string Name;
    }
}