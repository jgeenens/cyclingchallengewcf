﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("Stages", Schema = "dbo")]
    public class Stage
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("RaceDescription"), ForeignKey("StageRaceDescription"), Required]
        public virtual short RaceDescription { get; set; }

        public virtual RaceDescription StageRaceDescription { get; set; }

        [Column("Stage"), Required]
        public virtual byte Index { get; set; }

        [Column("Day"), Required]
        public virtual byte Day { get; set; }

        [Column("Track"), ForeignKey("StageTrack"), Required]
        public virtual short Track { get; set; }

        public virtual Track StageTrack { get; set; }
    }
}