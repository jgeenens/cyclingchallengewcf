﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("Timetable", Schema = "dbo")]
    public class TimeTable
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Race"), ForeignKey("TimeTableRace"), Required]
        public virtual long Race { get; set; }

        public virtual Race TimeTableRace { get; set; }

        [Column("Stage"), ForeignKey("TimeTableStage"), Required]
        public virtual short Stage { get; set; }

        public virtual Stage TimeTableStage { get; set; }

        [Column("Chronos"), Required, MaxLength(-1)]
        public virtual string Chronos { get; set; }
    }
}