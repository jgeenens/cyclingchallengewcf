﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Race
{
    [Table("Tracks", Schema = "dbo")]
    public class Track
    {
        [Column("Id"), Key, Required]
        public virtual short Id { get; set; }

        [Column("DifficultyPlains"), Required]
        public virtual short DifficultyPlains { get; set; }

        [Column("DifficultyMountains"), Required]
        public virtual short DifficultyMountains { get; set; }

        [Column("DifficultyHills"), Required]
        public virtual short DifficultyHills { get; set; }

        [Column("DifficultyCobblestones"), Required]
        public virtual short DifficultyCobblestones { get; set; }

        [Column("Sum"), Required]
        public virtual long Sum { get; set; }

        [Column("Kilometers"), Required]
        public virtual short Kilometers { get; set; }

        [Column("Track"), Required, MaxLength(-1)]
        public virtual string Terrain { get; set; }

        [Column("Description"), Required, MaxLength(-1)]
        public virtual string Description { get; set; }

        [Column("Altitude"), Required, MaxLength(-1)]
        public virtual string Altitude { get; set; }

        [Column("Climbs"), Required, MaxLength(256)]
        public virtual string Climbs { get; set; }

        [Column("Profile"), Required]
        public virtual byte Profile { get; set; }

        [Column("Name"), Required, MaxLength(80)]
        public virtual string Name { get; set; }

        [Column("Checkpoints"), Required, MaxLength(12)]
        public virtual string Checkpoints { get; set; }

        [Column("Scale"), Required]
        public virtual byte Scale { get; set; }

        [Column("CobbleHillMountain"), Required]
        public virtual byte CobbleHillMountain { get; set; }

        [Column("LastChange"), Required]
        public virtual DateTime LastChange { get; set; }
    }
}