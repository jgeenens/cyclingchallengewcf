﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CyclingChallenge.Services.Models.Race
{
    public class Terrain
    {
        public char Type { get; set; }
        public long? Climb { get; set; }
    }

    public class TrackDescription
    {
        #region Private Fields

        private readonly Dictionary<int, Terrain> _description;
        private readonly Dictionary<int, long> _cities;

        #endregion


        #region Properties

        public readonly Dictionary<int, long> Altitudes;
        public bool Success;

        #endregion


        #region Constructor

        public TrackDescription(string terrain, string cities, string altitudes = null)
        {
            if (string.IsNullOrEmpty(terrain) || string.IsNullOrEmpty(cities))
            {
                return;
            }

            _description = new Dictionary<int, Terrain>();
            var parts = terrain.Split('|');
            var km = 0;
            foreach (var part in parts)
            {
                var indexType = part.IndexOfAny(new[] {'k', 'v', 'h', 'c', 'b'});
                var distance = 0;
                var startsWithNumber = indexType > 0 && int.TryParse(part.Substring(0, indexType), out distance);
                if (!startsWithNumber)
                {
                    _description = null;
                    return;
                }

                var type = part[indexType];
                var idClimb = part.Length <= indexType + 1
                    ? (long?) null
                    : long.Parse(part.Substring(indexType + 1, part.Length - indexType - 2));

                if (!_description.ContainsKey(km))
                {
                    _description.Add(km, new Terrain
                    {
                        Type = type,
                        Climb = idClimb
                    });
                }

                km += distance;
            }

            const string regex = @"((\d+)-(\d+)\|*)";
            if (!Regex.IsMatch(cities, regex) || !cities.StartsWith("0-"))
            {
                _description = null;
                return;
            }
            _cities = cities.Split('|').ToDictionary(c => int.Parse(c.Split('-')[0]), c => long.Parse(c.Split('-')[1]));

            if (!string.IsNullOrEmpty(altitudes))
            {
                if (!Regex.IsMatch(altitudes, regex) || !altitudes.StartsWith("0-"))
                {
                    _description = null;
                    _cities = null;
                    return;
                }
                Altitudes = altitudes.Split('|').ToDictionary(a => int.Parse(a.Split('-')[0]), p => long.Parse(p.Split('-')[1]));
            }

            Success = true;
        }

        #endregion


        #region Methods

        public Dictionary<int, Terrain> GetHills()
        {
            return _description?.Where(t => t.Value.Type == 'h' && t.Value.Climb.HasValue).ToDictionary(t => t.Key, t => t.Value);
        }

        public Dictionary<int, Terrain> GetMountains()
        {
            return _description?.Where(t => t.Value.Type == 'b' && t.Value.Climb.HasValue).ToDictionary(t => t.Key, t => t.Value);
        }

        public Dictionary<int, Terrain> GetPavedSectors()
        {
            return _description?.Where(t => (t.Value.Type == 'c' || t.Value.Type == 'k') && t.Value.Climb.HasValue).ToDictionary(t => t.Key, t => t.Value);
        }

        public long? GetStart() => _cities?[0];

        public long? GetFinish() => _cities?[_cities.Keys.Max()];

        #endregion
    }
}