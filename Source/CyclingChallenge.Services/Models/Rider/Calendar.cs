﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("Calendar", Schema = "dbo")]
    public class Calendar
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Player"), ForeignKey("CalendarPlayer"), Required]
        public virtual long Player { get; set; }

        public virtual Player CalendarPlayer { get; set; }

        [Column("Team"), ForeignKey("CalendarTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team CalendarTeam { get; set; }

        [Column("StartDate"), Required]
        public virtual DateTime StartDate { get; set; }

        [Column("EndDate"), Required]
        public virtual DateTime EndDate { get; set; }

        [Column("Event"), Required, MaxLength(16)]
        public virtual string Event { get; set; }

        [Column("Description"), Required, MaxLength(60)]
        public virtual string Description { get; set; }
    }
}