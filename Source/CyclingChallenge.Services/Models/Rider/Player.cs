﻿using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("Players", Schema = "dbo")]
    public class Player
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("FirstName"), Required, MaxLength(50)]
        public virtual string FirstName { get; set; }

        [Column("LastName"), Required, MaxLength(50)]
        public virtual string LastName { get; set; }

        [Column("Team"), ForeignKey("PlayerTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team PlayerTeam { get; set; }

        [Column("Nationality"), ForeignKey("PlayerNationality"), Required]
        public virtual short Nationality { get; set; }

        public virtual Country PlayerNationality { get; set; }

        [Column("Age"), Required]
        public virtual byte Age { get; set; }

        [Column("Retired"), Required]
        public virtual bool Retired { get; set; }

        [Column("City"), ForeignKey("PlayerCity"), Required]
        public virtual long City { get; set; }

        public virtual City PlayerCity { get; set; }

        [Column("Face"), Required]
        public virtual byte Face { get; set; }

        [Column("Hair"), Required]
        public virtual byte Hair { get; set; }

        [Column("Eyes"), Required]
        public virtual byte Eyes { get; set; }

        [Column("Lips"), Required]
        public virtual byte Lips { get; set; }

        [Column("Nose"), Required]
        public virtual byte Nose { get; set; }
    }
}