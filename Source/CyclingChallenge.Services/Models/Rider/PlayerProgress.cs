﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("PlayerProgress", Schema = "dbo")]
    public class PlayerProgress
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Player"), ForeignKey("PlayerProgressPlayer"), Required]
        public virtual long Player { get; set; }

        public Player PlayerProgressPlayer { get; set; }

        [Column("Skill"), Required]
        public virtual byte Skill { get; set; }

        [Column("Level"), Required]
        public virtual byte Level { get; set; }

        [Column("Change"), Required]
        public virtual DateTime Change { get; set; }
    }
}