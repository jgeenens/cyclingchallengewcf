﻿using CyclingChallenge.Services.Models.Location;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("PlayerValues", Schema = "dbo")]
    public class PlayerValue
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Player"), ForeignKey("PlayerValuePlayer"), Required]
        public virtual long Player { get; set; }

        public virtual Player PlayerValuePlayer { get; set; }

        [Column("Team"), Required, MaxLength(40)]
        public virtual string Team { get; set; }

        [Column("Value"), Required]
        public virtual long Value { get; set; }

        [Column("Plains"), Required]
        public virtual long Plains { get; set; }

        [Column("Mountains"), Required]
        public virtual long Mountains { get; set; }

        [Column("Hills"), Required]
        public virtual long Hills { get; set; }

        [Column("Cobblestones"), Required]
        public virtual long Cobblestones { get; set; }

        [Column("Technique"), Required]
        public virtual long Technique { get; set; }

        [Column("Experience"), Required]
        public virtual long Experience { get; set; }

        [Column("Stamina"), Required]
        public virtual long Stamina { get; set; }

        [Column("Descending"), Required]
        public virtual long Descending { get; set; }

        [Column("Sprint"), Required]
        public virtual long Sprint { get; set; }

        [Column("Timetrial"), Required]
        public virtual long Timetrial { get; set; }

        [Column("Climbing"), Required]
        public virtual long Climbing { get; set; }

        [Column("Race"), ForeignKey("PlayerValueRace")]
        public virtual long? Race { get; set; }

        public virtual Race.Race PlayerValueRace { get; set; }

        [Column("Wage"), Required]
        public virtual long Wage { get; set; }

        [Column("IsOnTransfer"), Required]
        public virtual bool IsOnTransfer { get; set; }

        [Column("StartBid"), Required]
        public virtual long StartBid { get; set; }

        [Column("Bid"), Required]
        public virtual long Bid { get; set; }

        [Column("Deadline")]
        public virtual DateTime? Deadline { get; set; }

        [Column("BidTeam"), ForeignKey("PlayerValueBidTeam")]
        public virtual long? BidTeam { get; set; }

        public virtual Team.Team PlayerValueBidTeam { get; set; }

        [Column("IsYouth"), Required]
        public virtual bool IsYouth { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Form"), Required]
        public virtual int Form { get; set; }

        [Column("FormTarget"), Required]
        public virtual int FormTarget { get; set; }

        [Column("Fatigue"), Required]
        public virtual int Fatigue { get; set; }

        [Column("IsChampion"), Required]
        public virtual bool IsChampion { get; set; }

        [Column("City"), ForeignKey("PlayerValueCity"), Required]
        public virtual long City { get; set; }

        public virtual City PlayerValueCity { get; set; }

        [Column("Points"), Required]
        public virtual int Points { get; set; }

        [Column("Popularity"), Required]
        public virtual long Popularity { get; set; }

        [Column("CampDays"), Required]
        public virtual int CampDays { get; set; }

        [Column("TotalCampDays"), Required]
        public virtual int TotalCampDays { get; set; }
    }
}