﻿using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Staff;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("ScoutedPlayers", Schema = "dbo")]
    public class ScoutedPlayer
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("FirstName"), Required, MaxLength(50)]
        public virtual string FirstName { get; set; }

        [Column("LastName"), Required, MaxLength(50)]
        public virtual string LastName { get; set; }

        [Column("Team"), ForeignKey("ScoutedPlayerTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team ScoutedPlayerTeam { get; set; }

        [Column("Nationality"), ForeignKey("ScoutedPlayerNationality"), Required]
        public virtual short Nationality { get; set; }

        public virtual Country ScoutedPlayerNationality { get; set; }

        [Column("Age"), Required]
        public virtual byte Age { get; set; }

        [Column("Value"), Required]
        public virtual long Value { get; set; }

        [Column("Plains"), Required]
        public virtual long Plains { get; set; }

        [Column("Mountains"), Required]
        public virtual long Mountains { get; set; }

        [Column("Hills"), Required]
        public virtual long Hills { get; set; }

        [Column("Cobblestones"), Required]
        public virtual long Cobblestones { get; set; }

        [Column("Technique"), Required]
        public virtual long Technique { get; set; }

        [Column("Experience"), Required]
        public virtual long Experience { get; set; }

        [Column("Stamina"), Required]
        public virtual long Stamina { get; set; }

        [Column("Descending"), Required]
        public virtual long Descending { get; set; }

        [Column("Sprint"), Required]
        public virtual long Sprint { get; set; }

        [Column("Timetrial"), Required]
        public virtual long Timetrial { get; set; }

        [Column("Climbing"), Required]
        public virtual long Climbing { get; set; }

        [Column("IsYouth"), Required]
        public virtual bool IsYouth { get; set; }

        [Column("Scout"), ForeignKey("ScoutedPlayerScout"), Required]
        public virtual long Scout { get; set; }

        public virtual Scout ScoutedPlayerScout { get; set; }

        [Column("City"), ForeignKey("ScoutedPlayerCity"), Required]
        public virtual long City { get; set; }

        public virtual City ScoutedPlayerCity { get; set; }

        [Column("Face"), Required]
        public virtual byte Face { get; set; }

        [Column("Hair"), Required]
        public virtual byte Hair { get; set; }

        [Column("Eyes"), Required]
        public virtual byte Eyes { get; set; }

        [Column("Lips"), Required]
        public virtual byte Lips { get; set; }

        [Column("Nose"), Required]
        public virtual byte Nose { get; set; }
    }
}