﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("TransferHistory", Schema = "dbo")]
    public class TransferHistory
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("TransferHistoryTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team TransferHistoryTeam { get; set; }

        [Column("DateTransfer"), Required]
        public virtual DateTime DateTransfer { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Player"), ForeignKey("TransferHistoryPlayer"), Required]
        public virtual long Player { get; set; }

        public virtual Player TransferHistoryPlayer { get; set; }

        [Column("FromTeam"), ForeignKey("TransferHistoryFromTeam"), Required]
        public virtual long FromTeam { get; set; }

        public virtual Team.Team TransferHistoryFromTeam { get; set; }

        [Column("Bid"), Required]
        public virtual long Bid { get; set; }

        [Column("Value"), Required]
        public virtual long Value { get; set; }
    }
}