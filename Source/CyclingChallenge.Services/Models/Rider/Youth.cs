﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Rider
{
    [Table("Youth", Schema = "dbo")]
    public class Youth
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Player"), ForeignKey("YouthPlayer"), Required]
        public virtual long Player { get; set; }

        public virtual Player YouthPlayer { get; set; }

        [Column("History"), Required, MaxLength(255)]
        public virtual string History { get; set; }
    }
}