﻿using System.Configuration;
using System.Transactions;

namespace CyclingChallenge.Services.Models
{
    public static class RuntimeManager
    {
        #region Delegates

        public delegate ICyclingChallengeModelContainer DbContextFactoryDelegate(string connectionString = null, int timeout = 60);

        #endregion


        #region Enums

        public enum eApplicationMode
        {
            Normal = 0,
            ReadOnly = 1,
            UnitTest = 3,
            Development = 4
        }

        #endregion


        #region Fields

        private static DbContextFactoryDelegate _dbContextFactory;

        #endregion


        #region Properties

        public static eApplicationMode ApplicationMode { get; private set; }

        public static bool IsReadOnlyMode => ApplicationMode == eApplicationMode.ReadOnly;

        public static bool IsNormalMode => ApplicationMode == eApplicationMode.Normal;

        public static bool IsDevelopmentMode => ApplicationMode == eApplicationMode.Development;

        public static bool IsUnitTestMode => ApplicationMode == eApplicationMode.UnitTest;

        #endregion


        #region Constructor

        static RuntimeManager()
        {
            ApplicationMode = eApplicationMode.Normal;
            _dbContextFactory = CyclingChallengeModelContainer.CreateDbContextFactory();
        }

        #endregion


        #region Initialization Methods

        public static void SetApplicationMode(string mode)
        {
            switch (mode.ToLower())
            {
                case "normal":
                    ApplicationMode = eApplicationMode.Normal;
                    break;

                case "readonly":
                    ApplicationMode = eApplicationMode.ReadOnly;
                    break;

                case "unittest":
                    ApplicationMode = eApplicationMode.UnitTest;
                    break;

                case "development":
                    ApplicationMode = eApplicationMode.Development;
                    break;
            }
        }

        public static void Initialize()
        {
            if (ConfigurationManager.AppSettings["ApplicationMode"] != null)
                SetApplicationMode(ConfigurationManager.AppSettings["ApplicationMode"]);
        }

        #endregion


        #region Factory Methods

        public static ICyclingChallengeModelContainer CreateDbContext(string connectionString = null, int timeout = 120)
        {
            return _dbContextFactory(connectionString, timeout);
        }

        #endregion


        #region Static Methods

        public static TransactionScope CreateTransactionScope()
        {
            return new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            });
        }

        #endregion
    }
}