﻿using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Staff
{
    [Table("Coaches", Schema = "dbo")]
    public class Coach
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("CoachTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team CoachTeam { get; set; }

        [Column("FirstName"), Required, MaxLength(50)]
        public virtual string FirstName { get; set; }

        [Column("LastName"), Required, MaxLength(50)]
        public virtual string LastName { get; set; }

        [Column("Nationality"), ForeignKey("CoachNationality"), Required]
        public virtual short Nationality { get; set; }

        public virtual Country CoachNationality { get; set; }

        [Column("Age"), Required]
        public virtual byte Age { get; set; }

        [Column("Training"), Required]
        public virtual byte Training { get; set; }

        [Column("Skill"), Required]
        public virtual byte Skill { get; set; }

        [Column("Wage"), Required]
        public virtual short Wage { get; set; }

        [Column("Face"), Required]
        public virtual byte Face { get; set; }

        [Column("Hair"), Required]
        public virtual byte Hair { get; set; }

        [Column("Eyes"), Required]
        public virtual byte Eyes { get; set; }

        [Column("Lips"), Required]
        public virtual byte Lips { get; set; }

        [Column("Nose"), Required]
        public virtual byte Nose { get; set; }
    }
}