﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Staff
{
    [Table("Managers", Schema = "dbo")]
    public class Manager
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("ManagerTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team ManagerTeam { get; set; }

        [Column("Type"), Required]
        public virtual byte Type { get; set; }

        [Column("Level"), Required]
        public virtual byte Level { get; set; }

        [Column("FirstName"), Required, MaxLength(50)]
        public virtual string FirstName { get; set; }

        [Column("LastName"), Required, MaxLength(50)]
        public virtual string LastName { get; set; }

        [Column("Face"), Required]
        public virtual byte Face { get; set; }

        [Column("Hair"), Required]
        public virtual byte Hair { get; set; }

        [Column("Eyes"), Required]
        public virtual byte Eyes { get; set; }

        [Column("Lips"), Required]
        public virtual byte Lips { get; set; }

        [Column("Nose"), Required]
        public virtual byte Nose { get; set; }

        [Column("Learning")]
        public virtual DateTime? Learning { get; set; }

        [Column("Status"), Required]
        public virtual byte Status { get; set; }
    }
}