﻿using CyclingChallenge.Services.Models.Location;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Staff
{
    [Table("Scout", Schema = "dbo")]
    public class Scout
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("FirstName"), Required, MaxLength(50)]
        public virtual string FirstName { get; set; }

        [Column("LastName"), Required, MaxLength(50)]
        public virtual string LastName { get; set; }

        [Column("Team"), ForeignKey("ScoutTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team ScoutTeam { get; set; }

        [Column("Nationality"), ForeignKey("ScoutNationality"), Required]
        public virtual short Nationality { get; set; }

        public virtual Country ScoutNationality { get; set; }

        [Column("Age"), Required]
        public virtual byte Age { get; set; }

        [Column("Skill"), Required]
        public virtual byte Skill { get; set; }

        [Column("Wage"), Required]
        public virtual short Wage { get; set; }

        [Column("Region"), ForeignKey("ScoutRegion")]
        public virtual short? Region { get; set; }

        public virtual Region ScoutRegion { get; set; }

        [Column("TotalWeeks"), Required]
        public virtual byte TotalWeeks { get; set; }

        [Column("WeeksToGo"), Required]
        public virtual byte WeeksToGo { get; set; }

        [Column("LookingForYouth"), Required]
        public virtual bool LookingForYouth { get; set; }

        [Column("Departs")]
        public virtual DateTime? Departs { get; set; }

        [Column("Face"), Required]
        public virtual byte Face { get; set; }

        [Column("Hair"), Required]
        public virtual byte Hair { get; set; }

        [Column("Eyes"), Required]
        public virtual byte Eyes { get; set; }

        [Column("Lips"), Required]
        public virtual byte Lips { get; set; }

        [Column("Nose"), Required]
        public virtual byte Nose { get; set; }

        [Column("Status"), Required]
        public virtual byte Status { get; set; }

        [Column("Created"), Required]
        public virtual DateTime Created { get; set; }
    }
}