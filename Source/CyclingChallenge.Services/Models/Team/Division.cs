﻿using CyclingChallenge.Services.Models.Location;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Team
{
    [Table("Division", Schema = "dbo")]
    public class Division
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Country"), ForeignKey("DivisionCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country DivisionCountry { get; set; }

        [Column("Name"), Required, MaxLength(7)]
        public virtual string Name { get; set; }

        [Column("Level"), Required]
        public virtual byte Level { get; set; }
    }
}