﻿using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Location;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Team
{
    [Table("Teams", Schema = "dbo")]
    public class Team
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("ShortName"), Required, MaxLength(15)]
        public virtual string ShortName { get; set; }

        [Column("Name"), Required, MaxLength(40)]
        public virtual string Name { get; set; }

        [Column("Country"), ForeignKey("TeamCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country TeamCountry { get; set; }

        [Column("Money"), Required]
        public virtual long Money { get; set; }

        [Column("Predicted"), Required]
        public virtual long Predicted { get; set; }

        [Column("Points"), Required]
        public virtual int Points { get; set; }

        [Column("Wins"), Required]
        public virtual byte Wins { get; set; }

        [Column("Stage"), Required]
        public virtual byte Stage { get; set; }

        [Column("Champions"), Required]
        public virtual byte Champions { get; set; }

        [Column("Price"), Required]
        public virtual long Price { get; set; }

        [Column("Division"), ForeignKey("TeamDivision"), Required]
        public virtual long Division { get; set; }

        public virtual Division TeamDivision { get; set; }

        [Column("Rank"), Required]
        public virtual byte Rank { get; set; }

        [Column("IsBot"), Required]
        public virtual bool IsBot { get; set; }

        [Column("IsActive"), Required]
        public virtual bool IsActive { get; set; }

        [Column("Transactions"), Required]
        public virtual long Transactions { get; set; }

        [Column("Training"), Required]
        public virtual byte Training { get; set; }

        [Column("YouthTraining"), Required]
        public virtual byte YouthTraining { get; set; }

        [Column("LastCamp")]
        public virtual DateTime? LastCamp { get; set; }

        [Column("HasShop"), Required]
        public virtual bool HasShop { get; set; }

        [Column("Supporters"), Required]
        public virtual short Supporters { get; set; }

        [Column("Logo"), ForeignKey("TeamLogo"), Required]
        public virtual byte Logo { get; set; }

        public virtual Logo TeamLogo { get; set; }

        [Column("NotParticipated"), Required]
        public virtual byte NotParticipated { get; set; }

        [Column("EconomyUpdateStatus"), Required]
        public virtual byte EconomyUpdateStatus { get; set; }

        [Column("TrainingUpdateStatus"), Required]
        public virtual byte TrainingUpdateStatus { get; set; }

        [Column("CampUpdateStatus"), Required]
        public virtual byte CampUpdateStatus { get; set; }
    }
}