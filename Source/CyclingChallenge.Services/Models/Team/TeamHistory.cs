﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Team
{
    [Table("TeamHistory", Schema = "dbo")]
    public class TeamHistory
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Team"), ForeignKey("TeamHistoryTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team TeamHistoryTeam { get; set; }

        [Column("Event"), Required]
        public virtual byte Event { get; set; }

        [Column("Data"), MaxLength(255)]
        public virtual string Data { get; set; }

        [Column("Date"), Required]
        public virtual DateTime Date { get; set; }

        [Column("Seen"), Required]
        public virtual bool Seen { get; set; }
    }
}