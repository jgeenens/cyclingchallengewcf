﻿using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Location;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Team
{
    [Table("Users", Schema = "dbo")]
    public partial class User
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("FacebookId"), Required]
        public virtual long FacebookId { get; set; }

        [Column("Username"), MaxLength(255)]
        public virtual string Username { get; set; }

        [Column("Password"), MaxLength(64)]
        public virtual string Password { get; set; }

        [Column("Team"), ForeignKey("UserTeam")]
        public virtual long? Team { get; set; }

        public virtual Team UserTeam { get; set; }

        [Column("Reference"), Required, MaxLength(8)]
        public virtual string Reference { get; set; }

        [Column("FirstName"), Required, MaxLength(50)]
        public virtual string FirstName { get; set; }

        [Column("LastName"), Required, MaxLength(50)]
        public virtual string LastName { get; set; }

        [Column("Email"), MaxLength(255)]
        public virtual string Email { get; set; }

        [Column("Birthday")]
        public virtual DateTime? Birthday { get; set; }

        [Column("Info"), Required]
        public virtual byte Info { get; set; }

        [Column("Referrer"), Required]
        public virtual long Referrer { get; set; }

        [Column("Registration"), Required]
        public virtual DateTime Registration { get; set; }

        [Column("Language"), ForeignKey("UserLanguage"), Required]
        public virtual byte Language { get; set; }

        public virtual Language UserLanguage { get; set; }

        [Column("Country"), ForeignKey("UserCountry"), Required]
        public virtual short Country { get; set; }

        public virtual Country UserCountry { get; set; }

        [Column("TeamName"), Required, MaxLength(40)]
        public virtual string TeamName { get; set; }

        [Column("UserLevel"), Required]
        public virtual byte UserLevel { get; set; }

        [Column("Online"), Required]
        public virtual bool Online { get; set; }

        [Column("OnlineSince"), Required]
        public virtual DateTime OnlineSince { get; set; }

        [Column("EmailHidden"), Required]
        public virtual bool EmailHidden { get; set; }

        [Column("LoginHistory"), Required, MaxLength(140)]
        public virtual string LoginHistory { get; set; }

        [Column("Prefix"), Required, MaxLength(4)]
        public virtual string Prefix { get; set; }

        [Column("LastHit"), Required]
        public virtual DateTime LastHit { get; set; }

        [Column("Tokens"), Required]
        public virtual int Tokens { get; set; }

        [Column("Live"), Required, MaxLength(160)]
        public virtual string Live { get; set; }

        [Column("Vacation"), Required]
        public virtual bool Vacation { get; set; }

        [Column("Currency"), ForeignKey("UserCurrency"), Required]
        public virtual short Currency { get; set; }

        public virtual Currency UserCurrency { get; set; }

        [Column("RealCountry"), ForeignKey("UserRealCountry"), Required]
        public virtual short RealCountry { get; set; }

        public virtual Country UserRealCountry { get; set; }

        [Column("VacationSince")]
        public virtual DateTime? VacationSince { get; set; }

        public virtual bool IsModerator => UserLevel % 4 >= 2;
    }
}