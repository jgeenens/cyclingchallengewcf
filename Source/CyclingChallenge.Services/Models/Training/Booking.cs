﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CyclingChallenge.Services.Models.Training
{
    [Table("Bookings", Schema = "dbo")]
    public class Booking
    {
        [Column("Id"), Key, Required]
        public virtual long Id { get; set; }

        [Column("Location"), ForeignKey("BookingLocation"), Required]
        public virtual short Location { get; set; }

        public virtual Location.Location BookingLocation { get; set; }

        [Column("Team"), ForeignKey("BookingTeam"), Required]
        public virtual long Team { get; set; }

        public virtual Team.Team BookingTeam { get; set; }

        [Column("Departure"), Required]
        public virtual DateTime Departure { get; set; }

        [Column("Week"), Required]
        public virtual short Week { get; set; }

        [Column("Day"), Required]
        public virtual byte Day { get; set; }

        [Column("Duration"), Required]
        public virtual byte Duration { get; set; }

        [Column("Players"), Required, MaxLength(255)]
        public virtual string Players { get; set; }

        [Column("Checked"), Required]
        public virtual bool Checked { get; set; }
    }
}