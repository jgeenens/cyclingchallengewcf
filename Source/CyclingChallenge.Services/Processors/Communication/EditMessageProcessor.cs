﻿using System;
using System.Linq;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;
using Message = CyclingChallenge.Services.Models.Communication.Message;

namespace CyclingChallenge.Services.Processors
{
    public class EditMessageProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private User _user;
        private Message _message;
        private string _text;

        #endregion


        #region Constructor

        public EditMessageProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(3);

            var idMessage = GetTypeValidatedParameter<long>(Parameters.First());
            _message = GetValidatedMessage(idMessage);

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);

            _text = GetTypeValidatedParameter<string>(Parameters.Skip(2).First());
            if (string.IsNullOrWhiteSpace(_text))
            {
                throw new Exception(ErrorCodes.MessageCannotBeEmpty);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            CommunicationCommands.UpdateMessage(_message, _user.Id, _text);
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}