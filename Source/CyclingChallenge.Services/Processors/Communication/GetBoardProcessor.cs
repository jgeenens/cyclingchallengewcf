﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using Board = CyclingChallenge.Services.Models.Communication.Board;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Processors
{
    public class GetBoardProcessor : ProcessorBase<Contracts.DataContracts.Communication.Board>
    {
        #region Private fields

        private User _user;
        private IEnumerable<Board> _boards;

        #endregion


        #region Constructor

        public GetBoardProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var type = GetTypeValidatedParameter<byte>(Parameters.First());
            _boards = CommunicationQueries.GetByType(Database, type).ToList();
            if (!_boards.Any())
            {
                throw new Exception(ErrorCodes.IdBoardNotFound);
            }

            if (Parameters.Count < 2)
            {
                return;
            }

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);
        }

        protected internal override Contracts.DataContracts.Communication.Board ProcessInternal()
        {
            return new Contracts.DataContracts.Communication.Board
            {
                Success = true,
                Id = _boards.First().Type,
                Name = Communication.BoardType.GetDescription(_boards.First().Type),
                Forums = _boards.ToList().Select(CreateForum).ToList()
            };
        }

        #endregion


        #region Private methods

        private Forum CreateForum(Board board)
        {
            var threads = CommunicationQueries.GetThreadsByBoard(Database, board.Id).ToList();
            var hasNewMessages = _user == null || threads.Any(t => t.LastChange > CommunicationQueries.GetThreadViewedByUser(Database, t.Id, _user.Id));

            return new Forum
            {
                Id = board.Id,
                Name = board.Name,
                Description = board.Description,
                Type = board.Type,
                NumberOfThreads = threads.Count,
                HasNewMessages = hasNewMessages,
                Country = new Contracts.DataContracts.Location.Country
                {
                    Id = board.Country,
                    Name = board.BoardCountry.Item
                }
            };
        }

        #endregion
    }
}