﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using System.Collections.Generic;

namespace CyclingChallenge.Services.Processors
{
    public class GetBoardsProcessor : ProcessorBase<MessageBoard>
    {
        #region Constructor

        public GetBoardsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters() { }

        protected internal override MessageBoard ProcessInternal()
        {
            return new MessageBoard
            {
                Success = true,
                Boards = new List<Board>
                {
                    CreateBoard(Enums.Communication.BoardType.Game),
                    CreateBoard(Enums.Communication.BoardType.National)
                }
            };
        }

        #endregion


        #region Private methods

        private static Board CreateBoard(byte type)
        {
            return new Board
            {
                Id = type,
                Name = Enums.Communication.BoardType.GetDescription(type)
            };
        }

        #endregion
    }
}