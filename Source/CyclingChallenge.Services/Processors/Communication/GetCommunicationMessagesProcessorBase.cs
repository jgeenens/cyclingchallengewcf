﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Processors
{
    public abstract class GetCommunicationMessagesProcessorBase<TResult> : GetMessagesProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Constructor

        protected GetCommunicationMessagesProcessorBase(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override TResult ProcessInternal()
        {
            var user = UserQueries.GetByTeam(Database, Team.Id);
            var messages = GetMessages(user.Id);
            return CreateResultMessage(messages);
        }

        protected internal abstract List<PrivateMessage> GetMessages(long idUser);

        protected internal abstract TResult CreateResultMessage(List<PrivateMessage> messages);

        #endregion


        #region Private methods

        protected internal Contracts.DataContracts.Communication.PrivateMessage CreatePrivateMessage(PrivateMessage msg)
        {
            return new Contracts.DataContracts.Communication.PrivateMessage
            {
                Id = msg.Id,
                Title = ReplacePrefix(msg.Subject),
                Text = ProcessMessage(Database, msg.Message, msg.Subject),
                Sender = GetUserFromMessage(msg.PrivateMessageSender),
                Recipient = GetUserFromMessage(msg.PrivateMessageRecipient),
                Date = msg.Date,
                IsNew = !msg.Read
            };
        }

        private static string ProcessMessage(ICyclingChallengeModelContainer database, string message, string title)
        {
            switch (title.Split('|')[0])
            {
                case "[camp]":
                    return ProcessCampMessage(database, message);
                case "[training]":
                    return ProcessTrainingMessage(message);
                case "[shop]":
                    return ProcessShopMessage(database, message);
                default:
                    return message;
            }
        }

        #region Shop

        private static string ProcessShopMessage(ICyclingChallengeModelContainer database, string message)
        {
            var parts = message.Split('|');
            return parts.Length == 1 ? $"[[{Items.MessageShopNoSales}]]" : GetSalesMessage(database, message);           
        }

        private static string GetSalesMessage(ICyclingChallengeModelContainer database, string message)
        {
            return $"[[{Items.MessageShopSales}]]{Environment.NewLine}{GetProductLines(database, message)}";
        }

        private static string GetProductLines(ICyclingChallengeModelContainer database, string message)
        {
            const string regex = @"(\d+)\|(\d+)\|(\d+)";
            return Regex.Replace(message, regex, m => GetProductLine(database, m)).Replace(" |", $"{Environment.NewLine}");
        }

        private static string GetProductLine(ICyclingChallengeModelContainer database, Match m)
        {            
            return $"[[{GetProductName(database, m)}]]: {GetProductQuantity(m)}, {GetProductTotal(m)} ";
        }

        private static short? GetProductName(ICyclingChallengeModelContainer database, Match m)
        {
            var idProduct = m.Groups[1].Value.ToNullableByte() ?? 0;
            return ProductQueries.GetById(database, idProduct)?.Name;
        }

        private static string GetProductQuantity(Match m)
        {
            var quantity = m.Groups[2].Value;
            return GetReplaceFunction(Items.MessageShopUnitsSold, "[nsold]", quantity);
        }

        private static string GetProductTotal(Match m)
        {
            var total = m.Groups[3].Value;
            return GetCurrencyFunction(total);
        }

        #endregion

        #region Training

        private static string ProcessTrainingMessage(string message)
        {
            var parts = message.Split('|');
            return GetNewLevel(GetOldLevel(GetSkill(GetRider($"[[{Items.TrainingImprovement}]]", parts[0]), parts[1]), parts[2]), parts[3]);
        }

        private static string GetSkill(string s, string skill)
        {
            var sk = skill.ToNullableByte() ?? 0;
            return GetReplaceFunction(s, "[type]", $"[[{Items.TrainingTechnique + sk + (sk > 0 ? 1 : 0)}]]");
        }

        #endregion

        #region Camp

        private static string ProcessCampMessage(ICyclingChallengeModelContainer database, string message)
        {
            var parts = message.Split('|');
            var firstPart = parts[0].Split(',');
            return firstPart.Length == 1 ? GetDeparture(database, message) : GetImprovementRiders(message);
        }

        private static string GetImprovementRiders(string message)
        {
            return $"[[{Items.MessageCampProgress}]]{Environment.NewLine}{Environment.NewLine}{GetProgressRiders(message)}";
        }

        private static string GetProgressRiders(string message)
        {
            const string regex = @"(\d+),(\d+),(\d+),(\d+)";
            return Regex.Replace(message, regex, m => $"{GetProgressRider(m)} ").Replace(" |", Environment.NewLine).Trim();
        }

        private static string GetProgressRider(Match m)
        {
            var terrain = m.Groups[2].Value.ToNullableByte();
            var result = GetTerrain($"[[{Items.MessageCampImprovement}]]", terrain);
            result = GetNewLevel(GetOldLevel(result, m.Groups[3].Value), m.Groups[4].Value);
            return GetRider(result, m.Groups[1].Value);
        }

        private static string GetDeparture(ICyclingChallengeModelContainer database, string message)
        {
            var parts = message.Split('|');
            return GetLocation(database, GetDepartureMessage(parts[1]), parts[0]);            
        }

        private static string GetLocation(ICyclingChallengeModelContainer database, string message, string loc)
        {
            var idLocation = loc.ToNullableShort() ?? 0;
            return GetReplaceFunction(message, "[loc]", LocationQueries.GetLocationTextById(database, idLocation));
        }

        private static string GetDepartureMessage(string riders)
        {
            return GetReplaceFunction(Items.MessageCampDeparture, "[all]", GetDepartingRiders(riders));
        }

        private static string GetDepartingRiders(string message)
        {
            const string regex = @"(\d+)";
            return Regex.Replace(message, regex, GetDepartingRider).Replace(",", Environment.NewLine);
        }

        private static string GetDepartingRider(Match m)
        {
            var idRider = m.Groups[0].Value.ToNullableLong() ?? 0;
            return GetRiderFunction(idRider.ToString());
        }

        #endregion

        private static string ReplacePrefix(string subject)
        {
            var result = subject.Replace("[camp]", $"[[{Items.TransactionTrainingCamp}]]");
            result = result.Replace("[training]", $"[[{Items.EconomyTraining}]]");
            return result.Replace("[shop]", $"[[{Items.EconomyShopRevenues}]]");
        }

        private static Contracts.DataContracts.Communication.User GetUserFromMessage(User user)
        {
            return user != null
                ? new Contracts.DataContracts.Communication.User
                {
                    Id = user.Id,
                    Name = $"{user.FirstName} {user.LastName}".Trim(),
                    Alias = user.Username
                }
                : null;
        }

        #endregion
    }
}