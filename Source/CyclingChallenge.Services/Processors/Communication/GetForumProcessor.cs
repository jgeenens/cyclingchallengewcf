﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using System;
using System.Linq;
using Board = CyclingChallenge.Services.Models.Communication.Board;
using Thread = CyclingChallenge.Services.Models.Communication.Thread;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Processors
{
    public class GetForumProcessor : ProcessorBase<Forum>
    {
        #region Private fields

        private User _user;
        private Board _board;

        #endregion


        #region Constructor

        public GetForumProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();
            
            var idBoard = GetTypeValidatedParameter<short>(Parameters.First());
            _board = CommunicationQueries.GetBoardById(Database, idBoard);
            if (_board == null)
            {
                throw new Exception(ErrorCodes.IdForumNotFound);
            }

            if (Parameters.Count < 2)
            {
                return;
            }

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);
        }

        protected internal override Forum ProcessInternal()
        {
            var threads = CommunicationQueries.GetThreadsByBoard(Database, _board.Id).ToList();
            var hasNewMessages = _user == null || !threads.ToList().Any(t => CommunicationQueries.GetThreadViewedByUser(Database, t.Id, _user.Id) > DateTime.MinValue);

            return new Forum
            {
                Success = true,
                Id = _board.Id,
                Name = _board.Name,
                Type = _board.Type,
                HasNewMessages = hasNewMessages,
                Threads = threads.Select(CreateThread).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Communication.Thread CreateThread(Thread thread)
        {
            var messages = CommunicationQueries.GetMessagesByThread(Database, thread.Id);
            var hasNewMessages = _user == null || thread.LastChange > CommunicationQueries.GetThreadViewedByUser(Database, thread.Id, _user.Id);

            return new Contracts.DataContracts.Communication.Thread
            {
                Id = thread.Id,
                Name = thread.Title,
                NumberOfMessages = messages.Count(),
                HasNewMessages = hasNewMessages,
                Originator = CreateUser(thread.ThreadOriginator),
                LastChange = thread.LastChange,
                LastOriginator = CreateUser(thread.ThreadLastOriginator)
            };
        }

        #endregion
    }
}