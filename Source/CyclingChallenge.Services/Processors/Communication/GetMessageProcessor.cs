﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using System;
using System.Linq;
using Message = CyclingChallenge.Services.Models.Communication.Message;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Processors
{
    public class GetMessageProcessor : ProcessorBase<Contracts.DataContracts.Communication.Message>
    {
        #region Private fields

        private User _user;
        private Message _message;

        #endregion


        #region Constructor

        public GetMessageProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idMessage = GetTypeValidatedParameter<long>(Parameters.First());
            _message = GetValidatedMessage(idMessage, Parameters.Count != 1);

            if (Parameters.Count < 2)
            {
                return;
            }

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);

            if (_message.Obsolete && !_user.IsModerator)
            {
                throw new Exception(ErrorCodes.IdMessageNotFound);
            }
        }

        protected internal override Contracts.DataContracts.Communication.Message ProcessInternal()
        {
            return new Contracts.DataContracts.Communication.Message
            {
                Success = true,
                Id = _message.Id,
                Thread = _message.Thread,
                MessageInThread = _message.MessageInThread,
                ReplyTo = CreateReplyToMessage(_message.MessageReplyTo),
                Text = _message.Content,
                Date = _message.Date,
                User = CreateUser(_message.MessageUser),
                IsDeleted = _message.Obsolete,
                CanChange = (_user?.IsModerator ?? false) || (_message.User == _user?.Id),
                ChangeDate = _message.LastChange,
                ChangeUser = _message.User != _message.UserChange
                    ? CreateUser(_message.MessageUserChange)
                    : null
            };
        }

        #endregion


        #region Private methods

        private static MessageBase CreateReplyToMessage(Message message)
        {
            return message != null
                ? new MessageBase
                {
                    Id = message.Id,
                    Thread = message.Thread,
                    MessageInThread = message.MessageInThread
                }
                : null;
        }

        #endregion
    }
}