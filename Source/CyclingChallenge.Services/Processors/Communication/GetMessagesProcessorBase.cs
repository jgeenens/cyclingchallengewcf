﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Processors
{
    public abstract class GetMessagesProcessorBase<TResult> : ProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Constants

        protected const short PageSize = 10;

        #endregion


        #region Properties

        protected Team Team;
        protected int Page = 1;

        #endregion


        #region Constructor

        protected GetMessagesProcessorBase(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            Team = GetValidatedTeam(idTeam);

            if (Parameters.Count < 2)
            {
                return;
            }

            Page = GetTypeValidatedParameter<int>(Parameters.Skip(1).First());
        }

        #endregion


        #region Shared methods

        protected internal static string GetRider(string s, string idRider)
        {
            return GetReplaceFunction(s, "[cycl]", GetRiderFunction(idRider));
        }

        protected internal static string GetTerrain(string s, byte? terrain)
        {
            return GetReplaceFunction(s, "[where]", $"[[{Items.TrainingPlains + terrain}]]");
        }

        protected internal static string GetOldLevel(string s, string oldLevel)
        {
            return GetReplaceFunction(s, "[before]", oldLevel);
        }

        protected internal static string GetNewLevel(string s, string newLevel)
        {
            return GetReplaceFunction(s, "[after]", newLevel);
        }

        protected internal static string GetReplaceFunction(string s, string needle, string replacement)
        {
            return $"[[REP({s}||{needle}||{replacement})]]";
        }

        protected internal static string GetReplaceFunction(int item, string needle, string replacement)
        {
            return $"[[REP([[{item}]]||{needle}||{replacement})]]";
        }

        protected internal static string GetCurrencyFunction(string total)
        {
            return $"[[CRNCY({total})]]";
        }

        protected internal static string GetRiderFunction(string idRider)
        {
            return $"[[RIDER({idRider})]]";
        }

        #endregion
    }
}