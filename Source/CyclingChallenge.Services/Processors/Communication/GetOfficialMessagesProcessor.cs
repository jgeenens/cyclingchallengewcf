﻿using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using PrivateMessage = CyclingChallenge.Services.Models.Communication.PrivateMessage;

namespace CyclingChallenge.Services.Processors
{
    public class GetOfficialMessagesProcessor : GetCommunicationMessagesProcessorBase<OfficialMessages>
    {
        #region Constructor

        public GetOfficialMessagesProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override List<PrivateMessage> GetMessages(long idUser)
        {
            return CommunicationQueries.GetOfficialMessagesForUser(Database, idUser, (Page - 1) * PageSize).ToList();
        }

        protected internal override OfficialMessages CreateResultMessage(List<PrivateMessage> messages)
        {
            return new OfficialMessages
            {
                Success = true,
                Id = Team.Id,
                Name = Team.Name,
                Messages = messages.Select(CreatePrivateMessage).ToList()
            };
        }

        #endregion
    }
}