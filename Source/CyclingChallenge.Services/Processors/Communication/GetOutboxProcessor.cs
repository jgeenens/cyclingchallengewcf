﻿using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using PrivateMessage = CyclingChallenge.Services.Models.Communication.PrivateMessage;

namespace CyclingChallenge.Services.Processors
{
    public class GetOutboxProcessor : GetCommunicationMessagesProcessorBase<Outbox>
    {
        #region Constructor

        public GetOutboxProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override List<PrivateMessage> GetMessages(long idUser)
        {
            return CommunicationQueries.GetMessagesForUser(Database, idUser, (Page - 1) * PageSize, false).ToList();
        }

        protected internal override Outbox CreateResultMessage(List<PrivateMessage> messages)
        {
            return new Outbox
            {
                Success = true,
                Id = Team.Id,
                Name = Team.Name,
                Messages = messages.Select(CreatePrivateMessage).ToList()
            };
        }

        #endregion
    }
}