﻿using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using PressMessage = CyclingChallenge.Services.Models.Communication.PressMessage;

namespace CyclingChallenge.Services.Processors
{
    public class GetPressMessagesProcessor : GetMessagesProcessorBase<PressMessages>
    {
        #region Constructor

        public GetPressMessagesProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override PressMessages ProcessInternal()
        {
            var user = UserQueries.GetByTeam(Database, Team.Id);
            var messages = GetMessages(user.Id);
            return CreateResultMessage(messages);
        }

        #endregion


        #region Private Methods

        private IEnumerable<PressMessage> GetMessages(long idUser)
        {
            return CommunicationQueries.GetPressMessagesForUser(Database, idUser, (Page - 1) * PageSize).ToList();
        }

        private PressMessages CreateResultMessage(IEnumerable<PressMessage> messages)
        {
            return new PressMessages
            {
                Success = true,
                Id = Team.Id,
                Name = Team.Name,
                Messages = messages.Select(CreatePressMessage).ToList()
            };
        }

        private static Contracts.DataContracts.Communication.PressMessage CreatePressMessage(PressMessage msg)
        {
            return new Contracts.DataContracts.Communication.PressMessage
            {
                Id = msg.Id,
                Title = msg.Title,
                Text = msg.Body,
                Date = msg.Date,
                Parent = msg.Message
            };
        }

        #endregion
    }
}