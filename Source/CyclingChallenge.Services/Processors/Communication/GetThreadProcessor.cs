﻿using CyclingChallenge.Services.Contracts.DataContracts.Communication;
using CyclingChallenge.Services.Queries;
using System.Linq;
using Message = CyclingChallenge.Services.Models.Communication.Message;
using Thread = CyclingChallenge.Services.Models.Communication.Thread;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Processors
{
    public class GetThreadProcessor : ProcessorBase<Contracts.DataContracts.Communication.Thread>
    {
        #region Private fields

        private User _user;
        private Thread _thread;

        #endregion


        #region Constructor

        public GetThreadProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idThread = GetTypeValidatedParameter<long>(Parameters.First());
            _thread = GetValidatedThread(idThread);

            if (Parameters.Count < 2)
            {
                return;
            }

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);
        }

        protected internal override Contracts.DataContracts.Communication.Thread ProcessInternal()
        {
            var messages = CommunicationQueries.GetMessagesByThread(Database, _thread.Id, true).ToList();
            return new Contracts.DataContracts.Communication.Thread
            {
                Success = true,
                Id = _thread.Id,
                Name = _thread.Title,
                Messages = messages.Select(CreateMessage).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Communication.Message CreateMessage(Message message)
        {
            return new Contracts.DataContracts.Communication.Message
            {
                Id = message.Id,
                Thread = message.Thread,
                MessageInThread = message.MessageInThread,
                ReplyTo = CreateReplyToMessage(message.MessageReplyTo),
                Text = message.Obsolete && !(_user?.IsModerator ?? false) ? string.Empty : message.Content,
                Date = message.Date,
                User = CreateUser(message.MessageUser),
                IsDeleted = message.Obsolete,
                CanChange = (_user?.IsModerator ?? false) || (message.User == _user?.Id),
                ChangeDate = message.LastChange,
                ChangeUser = message.User != message.UserChange
                    ? CreateUser(message.MessageUserChange)
                    : null
            };
        }

        private static MessageBase CreateReplyToMessage(Message message)
        {
            return message != null
                ? new MessageBase
                {
                    Id = message.Id,
                    Thread = message.Thread,
                    MessageInThread = message.MessageInThread
                }
                : null;
        }

        #endregion
    }
}