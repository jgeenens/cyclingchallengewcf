﻿using System;
using System.Linq;
using System.Net;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using Message = CyclingChallenge.Services.Models.Communication.Message;

namespace CyclingChallenge.Services.Processors
{
    public class PostMessageProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private Thread _thread;
        private User _user;
        private Message _messageRepliedTo;
        private string _message;
        private string _userIp;

        #endregion


        #region Constructor

        public PostMessageProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(4);

            var idThread = GetTypeValidatedParameter<long>(Parameters.First());
            _thread = GetValidatedThread(idThread);

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);

            _userIp = GetTypeValidatedParameter<string>(Parameters.Skip(2).First());
            if (string.IsNullOrWhiteSpace(_userIp))
            {
                throw new Exception(ErrorCodes.UserIpCannotBeEmpty);
            }

            if (!IPAddress.TryParse(_userIp, out _))
            {
                throw new Exception(ErrorCodes.InvalidFormatUserIp);
            }

            _message = GetTypeValidatedParameter<string>(Parameters.Skip(3).First());
            if (string.IsNullOrWhiteSpace(_message))
            {
                throw new Exception(ErrorCodes.MessageCannotBeEmpty);
            }

            if (Parameters.Skip(4).FirstOrDefault() == null)
            {
                return;
            }

            var idMessage = GetTypeValidatedParameter<long>(Parameters.Skip(4).First());
            _messageRepliedTo = GetValidatedMessage(idMessage);
        }

        protected internal override ResultBase ProcessInternal()
        {
            CommunicationCommands.CreateMessage(Database, _thread.Id, _user.Id, _userIp, _message, _messageRepliedTo?.Id);
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}