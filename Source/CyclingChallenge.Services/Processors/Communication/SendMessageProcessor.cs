﻿using System;
using System.Linq;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Processors
{
    public class SendMessageProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private User _sender;
        private User _recipient;
        private string _subject;
        private string _message;

        #endregion


        #region Constructor

        public SendMessageProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(4);

            var idUser = GetTypeValidatedParameter<long>(Parameters.First());
            _sender = GetValidatedUser(idUser);

            var userName = GetTypeValidatedParameter<string>(Parameters.Skip(1).First());
            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new Exception(ErrorCodes.RecipientUserNameCannotBeEmpty);
            }

            _recipient = UserQueries.GetByUsername(Database, userName);
            if (_recipient == null)
            {
                throw new Exception(ErrorCodes.UserNameNotFound);
            }

            _subject = GetTypeValidatedParameter<string>(Parameters.Skip(2).First());
            if (string.IsNullOrWhiteSpace(_subject))
            {
                throw new Exception(ErrorCodes.SubjectCannotBeEmpty);
            }

            _message = GetTypeValidatedParameter<string>(Parameters.Skip(3).First());
            if (string.IsNullOrWhiteSpace(_message))
            {
                throw new Exception(ErrorCodes.MessageCannotBeEmpty);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            CommunicationCommands.CreatePrivateMessage(Database, _sender, _recipient, _subject, _message);
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}