﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;
using Transaction = CyclingChallenge.Services.Models.Economy.Transaction;

namespace CyclingChallenge.Services.Processors
{
    public class BuyShopProductsProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private Team _team;
        private ProductBase[] _products;
        private long _price;

        #endregion


        #region Constructor

        public BuyShopProductsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            if (!_team.HasShop)
            {
                throw new Exception(ErrorCodes.ShopNotExists);
            }

            _products = GetTypeValidatedParameter<ProductBase[]>(Parameters.Skip(1).First());
            if (_products.Any(p => ProductQueries.GetById(Database, p.Id) == null))
            {
                throw new Exception(ErrorCodes.IdProductNotFound);
            }

            if (_products.Any(p => p.Quantity < 0) || _products.Sum(p => p.Quantity) == 0)
            {
                throw new Exception(ErrorCodes.InvalidProductQuantity);
            }

            var balance = EconomyQueries.GetBalanceCredit(Database, idTeam);
            _price = _products.Sum(p => p.Quantity * ProductQueries.GetById(Database, p.Id).Buy);
            if (balance <= _price)
            {
                throw new Exception(ErrorCodes.NotEnoughMoney);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            var shop = TeamQueries.GetShop(Database, _team.Id);
            shop.Quantity1 += _products.FirstOrDefault(p => p.Id == 1)?.Quantity ?? 0;
            shop.Quantity2 += _products.FirstOrDefault(p => p.Id == 2)?.Quantity ?? 0;
            shop.Quantity3 += _products.FirstOrDefault(p => p.Id == 3)?.Quantity ?? 0;
            shop.Quantity4 += _products.FirstOrDefault(p => p.Id == 4)?.Quantity ?? 0;
            shop.Quantity5 += _products.FirstOrDefault(p => p.Id == 5)?.Quantity ?? 0;

            Database.Transactions.Add(new Transaction
            {
                Team = _team.Id,
                Description = Items.TransactionShopPurchase,
                Outgoing = true,
                Value = _price,
                DateTransaction = DateTime.Now,
                Info = string.Empty
            });
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}