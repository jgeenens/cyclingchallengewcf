﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using System.Collections.Generic;
using CyclingChallenge.Services.Models.Team;
using Economy = CyclingChallenge.Services.Models.Economy.Economy;
using Transaction = CyclingChallenge.Services.Models.Economy.Transaction;

namespace CyclingChallenge.Services.Processors
{
    public class GetEconomyProcessor : ProcessorBase<Contracts.DataContracts.Economy.Economy>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetEconomyProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override Contracts.DataContracts.Economy.Economy ProcessInternal()
        {
            var economy = EconomyQueries.GetByTeam(Database, _team.Id);
            return new Contracts.DataContracts.Economy.Economy
            {
                Success = true,
                Id = _team.Id,
                Money = _team.Money,
                MoneyProjected = GetProjected(economy),
                Revenues = new List<Revenue>
                {
                    CreateRevenue(Items.EconomySponsors, economy.Income1),
                    CreateRevenue(Items.EconomyTransactions, economy.Income2),
                    CreateRevenue(Items.EconomyPriceMoney, economy.TransactionIn1),
                    CreateRevenue(Items.EconomyTransfers, economy.TransactionIn2),
                    CreateRevenue(Items.EconomyShopRevenues, economy.TransactionIn3),
                    CreateRevenue(Items.EconomyOther, economy.TransactionIn4)
                },
                Expenses = new List<Expense>
                {
                    CreateExpense(Items.EconomyStaff, economy.Expense1),
                    CreateExpense(Items.EconomyWages, economy.Expense2),
                    CreateExpense(Items.EconomyYouth, economy.Expense3),
                    CreateExpense(Items.EconomyShopOpening, economy.Expense4),
                    CreateExpense(Items.EconomyTransactions, economy.Expense5),
                    CreateExpense(Items.EconomyRegistration, economy.TransactionOut1),
                    CreateExpense(Items.EconomyTransfers, economy.TransactionOut2),
                    CreateExpense(Items.EconomyShopExpenses, economy.TransactionOut3),
                    CreateExpense(Items.EconomyTraining, economy.TransactionOut4),
                    CreateExpense(Items.EconomyOther, economy.TransactionOut5)
                }
            };
        }

        #endregion


        #region Private methods

        private long GetProjected(Economy economy)
        {
            var transactions = EconomyQueries.GetTransactionsByTeam(Database, _team.Id, false).ToList();
            AddTransactionsToEconomy(economy, transactions);
            return _team.Predicted + transactions.Sum(transaction => transaction.Value * (transaction.Outgoing ? -1 : 1));
        }

        private static void AddTransactionsToEconomy(Economy economy, IEnumerable<Transaction> transactions)
        {
            foreach (var transaction in transactions)
            {
                AddTransactionToEconomy(economy, transaction);
            }
        }

        private static void AddTransactionToEconomy(Economy economy, Transaction transaction)
        {
            switch (transaction.Description)
            {
                case Items.TransactionFirstInRace: economy.TransactionIn1 += transaction.Value;
                    break;
                case Items.TransactionRiderSold: economy.TransactionIn2 += transaction.Value;
                    break;
                case Items.TransactionShopSales: economy.TransactionIn3 += transaction.Value;
                    break;
                case Items.TransactionBonus: economy.TransactionIn4 += transaction.Value;
                    break;
                case Items.EconomyRegistration: economy.TransactionOut1 += transaction.Value;
                    break;
                case Items.TransactionRiderBought: economy.TransactionOut2 += transaction.Value;
                    break;
                case Items.TransactionShopPurchase: economy.TransactionOut3 += transaction.Value;
                    break;
                case Items.TransactionTrainingCamp: economy.TransactionOut4 += transaction.Value;
                    break;
                case Items.TransactionNewTrainer: economy.TransactionOut5 += transaction.Value;
                    break;
            }
        }

        private static Revenue CreateRevenue(int revenueType, long amount)
        {
            return new Revenue
            {
                Type = revenueType,
                Amount = amount
            };
        }

        private static Expense CreateExpense(int expenseType, long amount)
        {
            return new Expense
            {
                Type = expenseType,
                Amount = amount
            };
        }

        #endregion
    }
}