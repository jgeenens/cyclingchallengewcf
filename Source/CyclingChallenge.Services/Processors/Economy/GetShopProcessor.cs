﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using Manager = CyclingChallenge.Services.Models.Staff.Manager;
using Product = CyclingChallenge.Services.Models.Economy.Product;

namespace CyclingChallenge.Services.Processors
{
    public class GetShopProcessor : ProcessorBase<Shop>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetShopProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override Shop ProcessInternal()
        {
            var shop = TeamQueries.GetShop(Database, _team.Id);
            var manager = StaffQueries.GetShopManager(Database, _team.Id);
            return new Shop
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                Level = _team.TeamDivision.Level,
                IsClosed = !_team.HasShop,
                Products = CreateProducts(Database, shop, manager),
                Manager = CreateManager(Database, manager),
                Hint = shop?.Hint
            };
        }

        #endregion


        #region Private methods

        private static List<Contracts.DataContracts.Economy.Product> CreateProducts(ICyclingChallengeModelContainer context, Clubshop clubshop, Manager manager)
        {
            return clubshop != null 
                ? new List<Contracts.DataContracts.Economy.Product>
                {
                    CreateProduct(ProductQueries.GetById(context, 1), clubshop.Quantity1, manager != null ? clubshop.Price1 : null),
                    CreateProduct(ProductQueries.GetById(context, 2), clubshop.Quantity2, manager != null ? clubshop.Price2 : null),
                    CreateProduct(ProductQueries.GetById(context, 3), clubshop.Quantity3, manager != null ? clubshop.Price3 : null),
                    CreateProduct(ProductQueries.GetById(context, 4), clubshop.Quantity4, manager != null ? clubshop.Price4 : null),
                    CreateProduct(ProductQueries.GetById(context, 5), clubshop.Quantity5, manager != null ? clubshop.Price5 : null),
                }
                : null;
        }

        private static Contracts.DataContracts.Economy.Product CreateProduct(Product product, short quantity, short? price)
        {
            return new Contracts.DataContracts.Economy.Product
            {
                Id = product.Id,
                Name = product.Name,
                Buy = product.Buy,
                Sell = product.Sell,
                Quantity = quantity,
                Price = price
            };
        }

        private static Contracts.DataContracts.Economy.Manager CreateManager(ICyclingChallengeModelContainer context, Manager manager)
        {
            return manager != null
                ? new Contracts.DataContracts.Economy.Manager
                {
                    Id = manager.Id,
                    Name = $"{manager.FirstName} {manager.LastName}",
                    IsActive = manager.Status >= Managers.Status.Active,
                    IsLearning = manager.Status == Managers.Status.Learning,
                    Education = CreateEducation(context, manager),
                    Picture = GetPictureFromManager(manager)
                }
                : null;
        }

        private static Education CreateEducation(ICyclingChallengeModelContainer context, Manager manager)
        {
            return new Education
            {
                Level = manager.Level,
                Start = manager.Learning,
                Duration = (short)((manager.Level + 1) * 12),
                Price = GameQueries.GetAddonById(context, GetShopManagerAddOnId(manager.Status, manager.Level)).Tokens
            };
        }

        private static short GetShopManagerAddOnId(byte status, byte level)
        {
            if (status == Managers.Status.Locked)
            {
                return AddOns.ActivateShopManager;
            }

            switch (level)
            {
                case Managers.Level.Apprentice: return AddOns.BeginnerShopManager;
                case Managers.Level.Beginner: return AddOns.BasicShopManager;
                case Managers.Level.Basic: return AddOns.IntermediateShopManager;
                case Managers.Level.Intermediate: return AddOns.AdvancedShopManager;
                case Managers.Level.Advanced: return AddOns.ExpertShopManager;
                case Managers.Level.Expert: return AddOns.WorldClassShopManager;
                default: return AddOns.ActivateShopManager;
            }
        }

        private static Picture GetPictureFromManager(Manager manager)
        {
            return new Picture
            {
                Face = manager.Face,
                Hair = manager.Hair,
                Eyes = manager.Eyes,
                Nose = manager.Nose,
                Lips = manager.Lips
            };
        }

        #endregion
    }
}