﻿using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using Sponsor = CyclingChallenge.Services.Models.Economy.Sponsor;

namespace CyclingChallenge.Services.Processors
{
    public class GetSponsorsProcessor: ProcessorBase<Sponsors>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetSponsorsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override Sponsors ProcessInternal()
        {
            var sponsor = SponsorQueries.GetSponsorByTeam(Database, _team.Id);
            var minType = (int)Math.Floor(_team.Rank / 4.0);
            return new Sponsors
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                HasSponsor = sponsor != null,
                Division = CreateDivision(_team.TeamDivision),
                Sponsor = sponsor != null 
                    ? new List<Contracts.DataContracts.Economy.Sponsor>() { CreateSponsor(sponsor) } 
                    : SponsorQueries.GetSponsorsByDivision(Database, _team.Division).Where(s => s.Type > minType).ToList().Select(CreateSponsor).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Team.Division CreateDivision(Division division)
        {
            return new Contracts.DataContracts.Team.Division
            {
                Id = _team.Division,
                Name = division.Name,
                Country = new Contracts.DataContracts.Location.Country
                {
                    Id = division.Country,
                    Name = division.DivisionCountry.Item
                }
            };
        }

        private static Contracts.DataContracts.Economy.Sponsor CreateSponsor(Sponsor sponsor)
        {
            return sponsor != null
                ? new Contracts.DataContracts.Economy.Sponsor
                {
                    Id = sponsor.Id,
                    Name = sponsor.Name,
                    WeeksLeft = sponsor.WeeksToGo,
                    WeeksTotal = sponsor.Weeks,
                    Signing = sponsor.Signing,
                    Bonus = sponsor.Weekly,
                    Country = new Contracts.DataContracts.Location.Country
                    {
                        Id = sponsor.Country,
                        Name = sponsor.SponsorCountry.Item
                    }
                }
                : null;
        }

        #endregion
    }
}