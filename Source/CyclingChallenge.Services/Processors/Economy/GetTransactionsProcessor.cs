﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Models.Team;
using Transaction = CyclingChallenge.Services.Models.Economy.Transaction;

namespace CyclingChallenge.Services.Processors
{
    public class GetTransactionsProcessor : ProcessorBase<TeamTransactions>
    {
        #region Private fields

        private Team _team;
        private DateTime _from;
        private DateTime? _to;

        #endregion


        #region Constructor

        public GetTransactionsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            _from = GetTypeValidatedParameter<DateTime>(Parameters.Skip(1).First());

            if (Parameters.Count > 2)
            {
                _to = GetTypeValidatedParameter<DateTime>(Parameters.Skip(2).First());
            }
        }

        protected internal override TeamTransactions ProcessInternal()
        {
            var transactions = EconomyQueries.GetTransactionsByTeamAndTimeFrame(Database, _team.Id, _from, _to).ToList();
            return new TeamTransactions
            {
                Success = true,
                Transactions = transactions.Select(CreateTransaction).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Economy.Transaction CreateTransaction(Transaction transaction)
        {
            return new Contracts.DataContracts.Economy.Transaction
            {
                Id = transaction.Id,
                Time = transaction.DateTransaction,
                IsRevenue = !transaction.Outgoing,
                Description = transaction.Description,
                Amount = transaction.Value,
                Rider = GetRiderForTransaction(transaction.Description, transaction.Info),
                Race = GetRaceForTransaction(transaction.Description, transaction.Info),
                Stage = GetStageForTransaction(transaction.Description, transaction.Info),
                Trainer = GetTrainerForTransaction(transaction.Description, transaction.Info),
                TrainingCamp = GetLocationForTransaction(transaction.Description, transaction.Info)
            };
        }

        private Location GetLocationForTransaction(int description, string info)
        {
            if (description != Items.TransactionTrainingCamp)
                return null;

            var isShort = short.TryParse(info, out var idLocation);
            if (!isShort)
                return null;

            var location = LocationQueries.GetLocationById(Database, idLocation);
            return location != null
                ? new Location
                {
                    Id = location.Id,
                    Name = location.Name,
                    Country = location.Country
                }
                : null;
        }

        private Trainer GetTrainerForTransaction(int description, string info)
        {
            if (description != Items.TransactionNewTrainer)
                return null;

            var isLong = long.TryParse(info, out var idTrainer);
            if (!isLong)
                return null;

            var trainer = StaffQueries.GetCoachById(Database, idTrainer);
            return trainer != null
                ? new Trainer
                {
                    Id = trainer.Id,
                    Name = $"{trainer.FirstName} {trainer.LastName}"
                }
                : null;
        }

        private Contracts.DataContracts.Race.RaceBase GetRaceForTransaction(int description, string info)
        {
            if (description != Items.TransactionFirstInRace && description != Items.EconomyRegistration)
                return null;

            var isLong = long.TryParse(info.Split('-')[0], out var idRace);
            if (!isLong)
                return null;

            var race = RaceQueries.GetRaceById(Database, idRace);
            return race != null
                ? new Contracts.DataContracts.Race.RaceBase
                {
                    Id = race.Id,
                    Name = race.RaceRaceDescription.Name
                }
                : null;
        }

        private Contracts.DataContracts.Race.Stage GetStageForTransaction(int description, string info)
        {
            if (description != Items.TransactionFirstInRace)
                return null;

            if (!info.Contains('-'))
                return null;

            var isLong = long.TryParse(info.Split('-')[0], out var idRace);
            if (!isLong)
                return null;

            var isShort = short.TryParse(info.Split('-')[1], out var idStage);
            if (!isShort)
                return null;

            var stage = RaceQueries.GetTrackByRaceAndStage(Database, idRace, idStage);
            return stage != null
                ? new Contracts.DataContracts.Race.Stage
                {
                    Id = idStage,
                    Name = stage.Name
                }
                : null;
        }

        private Contracts.DataContracts.Rider.Rider GetRiderForTransaction(int description, string info)
        {
            if (description != Items.TransactionRiderBought && description != Items.TransactionRiderSold)
                return null;

            var isLong = long.TryParse(info, out var idRider);
            if (!isLong)
                return null;

            var rider = PlayerQueries.GetPlayerById(Database, idRider);
            return rider != null
                ? new Contracts.DataContracts.Rider.Rider
                {
                    Id = rider.Id,
                    Name = $"{rider.FirstName} {rider.LastName}"
                }
                : null;
        }

        #endregion
    }
}