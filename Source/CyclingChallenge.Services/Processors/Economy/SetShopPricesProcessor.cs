﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Economy;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Processors
{
    public class SetShopPricesProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private Team _team;
        private Clubshop _shop;
        private ProductBase[] _products;

        #endregion


        #region Constructor

        public SetShopPricesProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            if (!_team.HasShop)
            {
                throw new Exception(ErrorCodes.ShopNotExists);
            }

            _shop = TeamQueries.GetShop(Database, _team.Id);

            if (StaffQueries.GetShopManager(Database, _team.Id) == null)
            {
                throw new Exception(ErrorCodes.TeamHasNoShopManager);
            }

            _products = GetTypeValidatedParameter<ProductBase[]>(Parameters.Skip(1).First());
            if (_products.Any(p => ProductQueries.GetById(Database, p.Id) == null))
            {
                throw new Exception(ErrorCodes.IdProductNotFound);
            }

            if (_products.Any(p => p.Id == 1 && _shop.Quantity1 == 0 || p.Id == 2 && _shop.Quantity2 == 0 ||
                                   p.Id == 3 && _shop.Quantity3 == 0 || p.Id == 4 && _shop.Quantity4 == 0 || 
                                   p.Id == 5 && _shop.Quantity5 == 0))
            {
                throw new Exception(ErrorCodes.NoStockForProduct);
            }

            if (_products.Any(p => !p.Price.HasValue || p.Price <= 0))
            {
                throw new Exception(ErrorCodes.InvalidProductPrice);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            _shop.Price1 = _products.FirstOrDefault(p => p.Id == 1)?.Price ?? _shop.Price1;
            _shop.Price2 = _products.FirstOrDefault(p => p.Id == 2)?.Price ?? _shop.Price2;
            _shop.Price3 = _products.FirstOrDefault(p => p.Id == 3)?.Price ?? _shop.Price3;
            _shop.Price4 = _products.FirstOrDefault(p => p.Id == 4)?.Price ?? _shop.Price4;
            _shop.Price5 = _products.FirstOrDefault(p => p.Id == 5)?.Price ?? _shop.Price5;
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}