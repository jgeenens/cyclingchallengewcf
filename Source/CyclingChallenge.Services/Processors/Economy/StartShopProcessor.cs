﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Processors
{
    public class StartShopProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private Team _team;
        private long _shopOpeningPrice;

        #endregion


        #region Constructor

        public StartShopProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            if (TeamQueries.GetShop(Database, idTeam) != null)
            {
                throw new Exception(ErrorCodes.ShopAlreadyExists);
            }

            var setting = GameQueries.GetSettingById(Database, Settings.Shop);
            if (setting == null)
            {
                throw new Exception(ErrorCodes.ShopSettingNotFound);
            }

            var isLong = long.TryParse(setting.Value, out _shopOpeningPrice);
            if (!isLong)
            {
                throw new Exception(ErrorCodes.ShopSettingInvalidType);
            }

            var balance = EconomyQueries.GetBalanceCredit(Database, idTeam);
            if (balance <= _shopOpeningPrice)
            {
                throw new Exception(ErrorCodes.NotEnoughMoney);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            Database.Clubshops.Add(new Clubshop
            {
                Team = _team.Id,
                Hint = Items.ParticipationHint
            });
            Database.Transactions.Add(new Transaction
            {
                Team = _team.Id,
                Description = Items.TransactionShopOpening,
                Outgoing = true,
                Value = _shopOpeningPrice,
                DateTransaction = DateTime.Now,
                Info = string.Empty
            });
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}