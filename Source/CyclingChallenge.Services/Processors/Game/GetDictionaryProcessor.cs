﻿using System;
using System.Linq;
using System.Collections.Generic;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Communication;
using ServiceDictionary = CyclingChallenge.Services.Contracts.DataContracts.Dictionary;

namespace CyclingChallenge.Services.Processors
{
    public class GetDictionaryProcessor : ProcessorBase<ServiceDictionary>
    {
        #region Private fields

        private Language _language;
        private List<Dictionary> _items;

        #endregion


        #region Constructor

        public GetDictionaryProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idLanguage = GetTypeValidatedParameter<byte>(Parameters.First());
            _language = DictionaryQueries.GetLanguageById(Database, idLanguage);
            if (_language == null)
            {
                throw new Exception(ErrorCodes.IdLanguageNotFound);
            }

            _items = DictionaryQueries.GetDictionaryItemsForLanguage(Database, idLanguage).ToList();
            if (!_items.Any())
            {
                throw new Exception(ErrorCodes.NoItemsInDictionary);
            }
        }

        protected internal override ServiceDictionary ProcessInternal()
        {
            return new ServiceDictionary
            {
                Success = true,
                Id = _language.Id,
                Language = _items.FirstOrDefault(i => i.Item == _language.Name)?.Text,
                Items = _items.Select(i => new Contracts.DataContracts.Item
                {
                    Id = i.Item,
                    Text = i.Text
                }).ToList()
            };
        }

        #endregion
    }
}