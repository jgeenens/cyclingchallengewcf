﻿using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Game;
using CyclingChallenge.Services.Contracts.DataContracts.Team;

namespace CyclingChallenge.Services.Processors
{
    public class GetSearchResultsProcessor : ProcessorBase<SearchResults>
    {
        #region Private fields

        private byte _searchType;
        private string _firstSearchValue;
        private string _secondSearchValue;

        #endregion


        #region Constructor

        public GetSearchResultsProcessor(params object[] parameters) : base(parameters) { }

        #endregion
        
        
        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            _searchType = GetTypeValidatedParameter<byte>(Parameters.First());
            if (!new [] { SearchType.Division, SearchType.Rider, SearchType.Team }.Contains(_searchType))
            {
                throw new Exception(ErrorCodes.InvalidSearchType);
            }

            if (!(Parameters.Skip(1).First() is string) && Parameters.Count < 3)
            {
                throw new Exception(ErrorCodes.InvalidParameterType);
            }

            _firstSearchValue = (string)Parameters.Skip(1).First();
            if (!string.IsNullOrEmpty(_firstSearchValue))
            {
                return;
            }

            if (_searchType == SearchType.Division)
            {
                throw new Exception(ErrorCodes.SearchStringEmpty);
            }

            if (Parameters.Skip(2).FirstOrDefault() == null)
            {
                throw new Exception(ErrorCodes.ParameterMissing);
            }

            _secondSearchValue = GetTypeValidatedParameter<string>(Parameters.Skip(2).First());
            if (string.IsNullOrEmpty(_secondSearchValue))
            {
                throw new Exception(ErrorCodes.SearchStringEmpty);
            }
        }

        protected internal override SearchResults ProcessInternal()
        {
            var divisions = _searchType == SearchType.Division
                ? TeamQueries.GetDivisionsByName(Database, _firstSearchValue)?.ToList()
                : null;
            var riders = _searchType == SearchType.Rider
                ? PlayerQueries.GetPlayersByName(Database, _firstSearchValue, _secondSearchValue)?.ToList()
                : null;
            var teams = _searchType == SearchType.Team
                ? GetTeamsForSearchParameters()
                : null;
            return new SearchResults
            {
                Success = true,
                SearchType = _searchType,
                Divisions = divisions?.Select(CreateDivision).ToList(),
                Riders = riders?.Select(CreateRider).ToList(),
                Teams = teams?.Select(CreateTeam).ToList()
            };
        }

        #endregion


        #region Private methods

        private List<Models.Team.Team> GetTeamsForSearchParameters()
        {
            if (string.IsNullOrEmpty(_firstSearchValue))
            {
                return TeamQueries.GetTeamsByUsername(Database, _secondSearchValue)?.ToList();
            }

            if (!long.TryParse(_firstSearchValue, out var idTeam))
            {
                return TeamQueries.GetTeamsByName(Database, _firstSearchValue)?.ToList();
            }

            var team = TeamQueries.GetTeamById(Database, idTeam);
            return team != null ? new List<Models.Team.Team> { team } : TeamQueries.GetTeamsByName(Database, _firstSearchValue)?.ToList();
        }

        private static Division CreateDivision(Models.Team.Division division)
        {
            return new Division
            {
                Id = division.Id,
                Name = division.Name,
                Country = CreateCountry(division.DivisionCountry)
            };
        }

        #endregion
    }
}