﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Game;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Game;
using Currency = CyclingChallenge.Services.Models.Economy.Currency;

namespace CyclingChallenge.Services.Processors
{
    public class GetStoreProcessor : ProcessorBase<Store>
    {
        #region Private fields

        private User _user;

        #endregion


        #region Constructor

        public GetStoreProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idUser = GetTypeValidatedParameter<long>(Parameters.First());
            _user = GetValidatedUser(idUser);
        }

        protected internal override Store ProcessInternal()
        {
            var products = GameQueries.GetAllProducts(Database).ToList();
            return new Store
            {
                Success = true,
                Id = _user.Id,
                Name = $"{_user.FirstName} {_user.LastName}".Trim(),
                Alias = _user.Username,
                Tokens = _user.Tokens,
                Products = products.Select(CreateProduct).ToList()
            };
        }

        #endregion


        #region Private methods

        private Product CreateProduct(Commodity product)
        {
            var price = GameQueries.GetPriceByProductAndCurrency(Database, product.Id, _user.Currency) 
                ?? GameQueries.GetPriceByProductAndCurrency(Database, product.Id, Economy.Currency.Dollar);

            return new Product
            {
                Id = product.Id,
                Title = product.Title,
                Description = product.Description,
                Value = product.Value,
                Price = price.Amount,
                Currency = CreateCurrency(price.PriceCurrency)
            };
        }

        private static Contracts.DataContracts.Game.Currency CreateCurrency(Currency currency)
        {
            return new Contracts.DataContracts.Game.Currency
            {
                Id = currency.Id,
                Name = currency.Description,
                Code = currency.Code
            };
        }

        #endregion
    }
}