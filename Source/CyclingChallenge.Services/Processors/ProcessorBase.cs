﻿using System;
using System.Linq;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using Message = CyclingChallenge.Services.Models.Communication.Message;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Processors
{
    public abstract class ProcessorBase<TResult> where TResult: ResultBase, new()
    {
        #region Properties

        protected ICyclingChallengeModelContainer Database;
        protected List<object> Parameters;

        #endregion


        #region Constructor

        protected ProcessorBase(params object[] parameters)
        {
            Database = RuntimeManager.CreateDbContext();
            Parameters = parameters.ToList();
        }

        ~ProcessorBase()
        {
            Database.Dispose();
        }

        #endregion


        #region Methods

        public TResult Process()
        {
            try
            {
                ValidateParameters();
                return ProcessInternal();
            }
            catch (Exception e)
            {
                var innerException = e.InnerException != null 
                    ? $"{e.InnerException.Message}{Environment.NewLine}Stacktrace: {e.InnerException.StackTrace}"
                    : string.Empty;
                var stackTrace = e.Message.Length > 4
                    ? $"{Environment.NewLine}Stacktrace: {e.StackTrace}{Environment.NewLine}{innerException}"
                    : string.Empty;
                return new TResult { ErrorCode = $"{e.Message}{stackTrace}" };
            }
        }

        protected internal abstract void ValidateParameters();

        protected internal abstract TResult ProcessInternal();

        protected internal void CheckRequiredParameters(byte numberOfRequiredParameters = 1)
        {
            if (Parameters.FirstOrDefault() == null || Parameters.Count < numberOfRequiredParameters)
            {
                throw new Exception(ErrorCodes.ParameterMissing);
            }
        }

        protected internal T GetTypeValidatedParameter<T>(object parameter)
        {
            if (parameter.GetType() != typeof(T))
            {
                throw new Exception(ErrorCodes.InvalidParameterType);
            }

            return (T) parameter;
        }

        protected internal User GetValidatedUser(long idUser)
        {
            var user = UserQueries.GetById(Database, idUser);
            if (user == null)
            {
                throw new Exception(ErrorCodes.IdUserNotFound);
            }

            return user;
        }

        protected internal Team GetValidatedTeam(long idTeam)
        {
            var team = TeamQueries.GetTeamById(Database, idTeam);
            if (team == null)
            {
                throw new Exception(ErrorCodes.IdTeamNotFound);
            }

            return team;
        }

        protected internal Thread GetValidatedThread(long idThread)
        {
            var thread = CommunicationQueries.GetThreadById(Database, idThread);
            if (thread == null)
            {
                throw new Exception(ErrorCodes.IdThreadNotFound);
            }

            return thread;
        }

        protected internal Message GetValidatedMessage(long idMessage, bool includeObsolete = false)
        {
            var message = CommunicationQueries.GetMessageById(Database, idMessage);
            if (message == null || message.Obsolete && !includeObsolete)
            {
                throw new Exception(ErrorCodes.IdMessageNotFound);
            }

            return message;
        }

        protected internal Race GetValidatedRace(long idRace)
        {
            var race = RaceQueries.GetRaceById(Database, idRace);
            if (race == null)
            {
                throw new Exception(ErrorCodes.IdRaceNotFound);
            }

            return race;
        }

        protected internal Player GetValidatedRider(long idRider)
        {
            var rider = PlayerQueries.GetPlayerById(Database, idRider);
            if (rider == null)
            {
                throw new Exception(ErrorCodes.IdRiderNotFound);
            }

            return rider;
        }

        #endregion


        #region Mapping methods

        protected internal static Contracts.DataContracts.Communication.User CreateUser(User user)
        {
            return user != null
                ? new Contracts.DataContracts.Communication.User
                {
                    Id = user.Id,
                    Name = $"{user.FirstName} {user.LastName}".Trim(),
                    Alias = user.Username,
                    Team = CreateTeam(user.UserTeam)
                }
                : null;
        }

        protected internal static TeamBase CreateTeam(Team team)
        {
            return team != null
                ? new TeamBase
                {
                    Id = team.Id,
                    ShortName = team.ShortName,
                    LongName = team.Name
                }
                : null;
        }

        protected internal static RiderBase CreateRider(Player rider)
        {
            return rider != null
                ? new RiderBase
                {
                    Id = rider.Id,
                    Name = $"{rider.FirstName} {rider.LastName}".Trim(),
                    Nationality = rider.Nationality
                }
                : null;
        }

        protected internal static Contracts.DataContracts.Location.Country CreateCountry(Models.Location.Country country)
        {
            return country != null
                ? new Contracts.DataContracts.Location.Country
                {
                    Id = country.Id,
                    Name = country.Item
                }
                : null;
        }

        #endregion
    }
}