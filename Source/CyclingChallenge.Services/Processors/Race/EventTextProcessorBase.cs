﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using Stage = CyclingChallenge.Services.Models.Race.Stage;

namespace CyclingChallenge.Services.Processors
{
    public abstract class EventTextProcessorBase<TResult> : ProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Private Fields

        private Stage _stage;
        private Dictionary<int, long> _cities;
        private readonly Dictionary<string, Func<string, string, RaceEvent, ReportInfo, string>> _functions;

        #endregion


        #region Constructor

        protected EventTextProcessorBase(params object[] parameters) : base(parameters)
        {
            _functions = new Dictionary<string, Func<string, string, RaceEvent, ReportInfo, string>>
            {
                { "[totkm]", GetDistance },
                { "[togo]", GetDistanceLeft },
                { "[city]", GetCity },
                { "[speed]", GetSpeed },
                { "[time]", GetTime },
                { "[ahead]", GetLeadersGap },
                { "[num]", GetNumberOfRiders },
                { "[climb]", GetClimb },
                { "[nth]", GetRank },
                { "[nationality]", GetNationality },
                { "[cycl]", GetRider },
                { "[team]", GetTeam },
                { "[cyclrem]", GetRidersInline },
                { "[all]", GetRiders },
                { "[group]", GetGroup },
                { "[sec]", GetTimeDifference }
            };
        }

        #endregion


        #region Shared methods

        protected internal static RiderBase CreateRider(PlayerValue rider)
        {
            return CreateRider(rider?.PlayerValuePlayer);
        }

        protected internal List<Contracts.DataContracts.Race.RaceEvent> GetProcessedEvents(Stage stage, IEnumerable<RaceEvent> events)
        {
            _stage = stage;
            _cities = GetCities(stage.StageTrack.Description);

            return events.Select(CreateEvent).ToList();
        }

        protected internal static string GetSpeed(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            return ev != null && ev.Seconds > 0
                ? GetReplaceFunction(s, "[speed]", $"[b]{((decimal) ev.Km / ev.Seconds * 3600):F2}[/b]")
                : s;
        }

        protected internal static string GetRank(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            var rank = info?.Riders?.FirstOrDefault()?.Rank;
            var ranks = new[] { Items.First, Items.Second, Items.Third, Items.Fourth, Items.Fifth, Items.Sixth,
                                Items.Seventh, Items.Eighth, Items.Ninth, Items.Tenth, Items.Eleventh, Items.Twelfth };
            return rank != null && rank < ranks.Length + 1 ? GetReplaceFunction(s, "[nth]", $"[b][[{ranks[rank.Value - 1]}]][/b]") : s;
        }

        protected internal static string GetTime(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            return ev?.Seconds != null
                ? GetReplaceFunction(s, "[time]", $"[b]{FormattedTime(ev.Seconds, true)}[/b]")
                : s;
        }

        protected internal static string GetTimeDifference(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            var time1 = info?.Riders?.FirstOrDefault()?.Time;
            var time2 = info?.Riders?.Skip(1).FirstOrDefault()?.Time;
            return time1.HasValue && time2.HasValue 
                ? GetReplaceFunction(s, "[sec]", $"[b]{Math.Max(time1.Value, time2.Value) - Math.Min(time1.Value, time2.Value)}[/b]")
                : s;
        }

        protected internal string GetDistance(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            return _stage?.StageTrack?.Kilometers != null
                ? GetReplaceFunction(s, "[totkm]", $"[b]{_stage.StageTrack.Kilometers}[/b]")
                : s;
        }

        protected internal string GetDistanceLeft(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            return _stage?.StageTrack?.Kilometers != null && ev?.Km != null
                ? GetReplaceFunction(s, "[togo]", $"[b]{_stage.StageTrack.Kilometers - ev.Km}[/b]")
                : s;
        }

        protected internal static string GetLeadersGap(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            return info?.Ahead != null ? GetReplaceFunction(s, "[ahead]", $"[b]{info.Ahead}[/b]") : s;
        }

        protected internal static string GetGroup(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            for (var index = 0; index < 4; index++)
            {
                var needle = $"[group{(index == 0 ? string.Empty : index.ToString())}]";
                var idGroup = info?.Groups?.Skip(Math.Max(0, index - 1)).FirstOrDefault()?.Id;
                var idRider = info?.Groups?.Skip(Math.Max(0, index - 1)).FirstOrDefault()?.IdRider;
                s = idGroup != null && idRider != null && itemText.Contains(needle)
                    ? GetReplaceFunction(s, needle, GetGroupFunction(idGroup.Value, idRider.Value))
                    : s;
            }

            return s;
        }

        protected internal string GetNationality(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            var idRider = info?.Riders?.FirstOrDefault()?.Id;
            return idRider.HasValue 
                ? GetReplaceFunction(s, "[nationality]", $"[b][[{PlayerQueries.GetPlayerById(Database, idRider.Value)?.PlayerNationality?.Nationality}]][/b]")
                : s;
        }

        protected internal static string GetRider(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            for (var index = 0; index < 4; index++)
            {
                var needle = $"[cycl{(index == 0 ? string.Empty : index.ToString())}]";
                var idRider = info?.Riders?.Skip(Math.Max(0, index - 1)).FirstOrDefault()?.Id;
                s = idRider != null && itemText.Contains(needle)
                    ? GetReplaceFunction(s, needle, GetRiderFunction(idRider.Value))
                    : s;
            }

            return s;
        }

        protected internal static string GetTeam(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            for (var index = 0; index < 4; index++)
            {
                var needle = $"[team{(index == 0 ? string.Empty : index.ToString())}]";
                var idTeam = info?.Teams?.Skip(Math.Max(0, index - 1)).FirstOrDefault()?.Id;
                var team = info?.Teams?.Skip(Math.Max(0, index - 1)).FirstOrDefault()?.Name;
                s = idTeam != null && itemText.Contains(needle)
                    ? GetReplaceFunction(s, needle, GetTeamFunction(idTeam.Value, team))
                    : s;
            }

            return s;
        }

        protected internal static string GetRiders(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            var idRiders = info?.Riders?.Select(r => r.Id).ToList();
            return idRiders?.Any() ?? false
                ? GetReplaceFunction(s, "[all]", GetRidersFunction(idRiders))
                : s;
        }

        protected internal static string GetRidersInline(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            var idRiders = info?.Riders?.Skip(3).Select(r => r.Id).ToList();
            return idRiders?.Any() ?? false 
                ? GetReplaceFunction(s, "[cyclrem]", GetRidersFunction(idRiders, true))
                : s;
        }

        protected internal static string GetNumberOfRiders(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            var idRiders = info?.Riders?.Select(r => r.Id).ToList();
            return idRiders != null ? GetReplaceFunction(s, "[num]", $"[b]{idRiders.Count}[/b]") : s;
        }

        protected internal string GetCity(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            if (ev?.Km == null || !_cities.ContainsKey(ev.Km))
            {
                return s;
            }

            var city = LocationQueries.GetCityById(Database, _cities[ev.Km]);
            return GetReplaceFunction(s, "[city]", $"[b]{LocationQueries.GetCityText(city)}[/b]");
        }

        protected internal string GetClimb(string itemText, string s, RaceEvent ev, ReportInfo info)
        {
            return info?.Climb != null
                ? GetReplaceFunction(s, "[climb]", $"[b]{LocationQueries.GetClimbById(Database, info.Climb.Value)?.Name}[/b]")
                : s;
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Race.RaceEvent CreateEvent(RaceEvent re)
        {
            return new Contracts.DataContracts.Race.RaceEvent
            {
                Id = re.Id,
                Class = re.RaceEventEvent.Class,
                Type = re.RaceEventEvent.Type,
                Index = re.RaceEventEvent.Index,
                Item = GetNeededFunctions(re),
                Distance = re.Km,
                Time = re.Seconds
            };
        }

        private string GetNeededFunctions(RaceEvent re)
        {
            var text = DictionaryQueries.GetDictionaryItemForLanguage(Database, re.RaceEventEvent.Item, Communication.Language.Dutch)?.Text;
            if (text == null)
            {
                return null;
            }

            var info = string.IsNullOrEmpty(re.Info)
                ? null
                : new JavaScriptSerializer().Deserialize<ReportInfo>(re.Info);

            var regex = new Regex(@"\[\w+\]");
            var item = $"[[{re.RaceEventEvent.Item}]]";
            foreach (Match match in regex.Matches(text))
            {
                if (_functions.ContainsKey(match.Value))
                {
                    item = _functions[match.Value].DynamicInvoke(text, item, re, info).ToString();
                }
            }

            return item;
        }

        private Dictionary<int, long> GetCities(string description)
        {
            var cities = description.Split('|').ToDictionary(c => int.Parse(c.Split('-')[0]), c => long.Parse(c.Split('-')[1]));
            long idCity = 0;
            for (var i = 0; i < _stage.StageTrack.Kilometers; i++)
            {
                if (cities.ContainsKey(i))
                {
                    idCity = cities[i];
                }
                else
                {
                    cities.Add(i, idCity);
                }
            }

            return cities;
        }

        private static string GetReplaceFunction(string s, string needle, string replacement)
        {
            return $"[[REP({s}||{needle}||{replacement})]]";
        }

        private static string FormattedTime(short time, bool isFirst)
        {
            var hrs = time / 3600;
            var min = (time % 3600) / 60;
            var sec = time % 60;

            var str = hrs > 0 ? $"{hrs}h" : string.Empty;
            str += hrs > 0 || min > 0 ? $"{(hrs > 0 ? min.ToString("D2") : min.ToString())}'" : string.Empty;
            return (isFirst ? string.Empty : "+") + str + (time > sec ? $"{sec:D2}\"" : $"{sec}\"");
        }

        private static string GetGroupFunction(byte idGroup, long idRider)
        {
            return $"[[GROUP({idGroup}||{idRider})]]";
        }

        private static string GetRidersFunction(IEnumerable<long> idRiders, bool inline = false)
        {
            var riders = string.Join(inline ? ", " : "[br]", idRiders.Select(GetRiderFunction));
            return inline ? riders.ReplaceLast(", ", $" [[{Items.And}]] ") : riders;
        }

        private static string GetRiderFunction(long idRider)
        {
            return $"[[RIDER({idRider})]]";
        }

        private static string GetTeamFunction(long idTeam, string team)
        {
            return $"[[TEAM({idTeam}||{team})]]";
        }

        #endregion
    }
}