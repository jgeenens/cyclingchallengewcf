﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Processors
{
    public class GetCalendarProcessor : RankingProcessorBase<Contracts.DataContracts.Race.Calendar>
    {
        #region Private fields

        private Models.Team.Division _division;
        private byte? _season;

        #endregion


        #region Constructor

        public GetCalendarProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idDivision = GetTypeValidatedParameter<long>(Parameters.First());
            _division = TeamQueries.GetDivisionById(Database, idDivision);
            if (_division == null)
            {
                throw new Exception(ErrorCodes.IdDivisionNotFound);
            }

            if (Parameters.Count == 1)
            {
                return;
            }

            _season = GetTypeValidatedParameter<byte>(Parameters.Skip(1).First());
        }

        protected internal override Contracts.DataContracts.Race.Calendar ProcessInternal()
        {
            var seasonStart = GameQueries.GetStartSeason(Database, _season);
            var races = RaceQueries.GetByDivisionWithinTimeFrame(Database, _division.Id, seasonStart, seasonStart.AddDays(7 * 12)).OrderBy(r => r.DaysInSeason).ToList();
            return new Contracts.DataContracts.Race.Calendar
            {
                Success = true,
                Id = _division.Id,
                Name = _division.Name,
                Country = CreateCountry(_division.DivisionCountry),
                StartSeason = seasonStart,
                Races = races.Select(CreateRace).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Race.Race CreateRace(Race race)
        {
            return new Contracts.DataContracts.Race.Race
            {
                Id = race.Id,
                Name = race.RaceRaceDescription.Name,
                Start = race.StartDate,
                End = race.EndDate,
                IsTour = race.RaceRaceDescription.IsTour,
                Status = race.Status,
                Stage = race.CurrentStage,
                Yellow = CreateRider(race.RaceWinner ?? race.RaceYellow ?? GetLeaderFromRanking(race))
            };
        }

        private Player GetLeaderFromRanking(Race race)
        {
            if (race.StartDate > DateTime.Now)
                return null;

            var raceRanking = SummarizeRanking(RaceQueries.GetRaceRankings(Database, race.Id)).OrderBy(p => p.Time ?? long.MaxValue);
            return PlayerQueries.GetPlayerById(Database, raceRanking.First(p => p.Time.HasValue).Id);
        }

        #endregion
    }
}