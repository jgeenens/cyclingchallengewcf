﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Processors
{
    public class GetRaceHistoryProcessor : ProcessorBase<Contracts.DataContracts.Race.RaceHistory>
    {
        #region Private fields

        private Race _race;
        private bool _includeStages;

        #endregion


        #region Constructor

        public GetRaceHistoryProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            if (Parameters.Count == 1)
            {
                return;
            }

            _includeStages = GetTypeValidatedParameter<bool>(Parameters.Skip(1).First());
        }

        protected internal override Contracts.DataContracts.Race.RaceHistory ProcessInternal()
        {
            return new Contracts.DataContracts.Race.RaceHistory
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                Events = RaceQueries.GetRaceHistoryForRace(Database, _race.Id, _includeStages).Where(rh => rh.Position == 1).ToList().Select(CreateEvent).ToList()
            };
        }

        #endregion


        #region Private methods

        private static Contracts.DataContracts.Race.Event CreateEvent(RaceHistory rh)
        {
            return new Contracts.DataContracts.Race.Event
            {
                Id = rh.Id,
                When = rh.When,
                Season = rh.Season,
                Type = Events.Winner.Yellow,
                Team = new Contracts.DataContracts.Team.TeamBase
                {
                    Id = rh.Team,
                    LongName = rh.TeamName
                },
                Rider = CreateRider(rh.RaceHistoryPlayer),
                Stage = CreateStage(rh.RaceHistoryStage)
            };
        }

        private static Contracts.DataContracts.Race.Stage CreateStage(Stage stage)
        {
            return stage != null
                ? new Contracts.DataContracts.Race.Stage
                {
                    Id = stage.Id,
                    Name = stage.StageTrack.Name,
                    IsTimeTrial = stage.StageTrack.Profile == Profile.TimeTrial,
                    IsTeamTimeTrial = stage.StageTrack.Profile == Profile.TeamTimeTrial
                }
                : null;
        }

        #endregion
    }
}