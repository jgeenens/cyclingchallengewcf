﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Processors
{
    public class GetRaceParticipantsProcessor : RankingProcessorBase<Contracts.DataContracts.Race.RaceRanking>
    {
        #region Private fields

        private Race _race;

        #endregion


        #region Constructor

        public GetRaceParticipantsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            if (!_race.RaceRaceDescription.IsTour && _race.StartDate < DateTime.Now)
            {
                throw new Exception(ErrorCodes.RaceAlreadyStarted);
            }

            if (_race.RaceRaceDescription.IsTour && _race.EndDate.AddSeconds(_race.LiveTime) < DateTime.Now)
            {
                throw new Exception(ErrorCodes.RaceAlreadyEnded);
            }
        }

        protected internal override Contracts.DataContracts.Race.RaceRanking ProcessInternal()
        {
            var participants = _race.StartDate < DateTime.Now
                    ? SummarizeRanking(RaceQueries.GetRaceRankings(Database, _race.Id).OrderBy(r => r.RaceRankingStage.Index))
                    : RaceQueries.GetRaceOrders(Database, _race.Id).ToList().Select(CreateRider).ToList();
            return new Contracts.DataContracts.Race.RaceRanking
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                Riders = participants,
                Yellow = participants.OrderBy(p => p.Time ?? long.MaxValue).FirstOrDefault(p => p.Time.HasValue)
            };
        }

        #endregion


        #region Private methods

        private RiderBase CreateRider(RaceOrder raceOrder)
        {
            return CreateRider(raceOrder.RaceOrderPlayer);
        }

        #endregion
    }
}