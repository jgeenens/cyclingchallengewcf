﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Location;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using Climb = CyclingChallenge.Services.Contracts.DataContracts.Location.Climb;

namespace CyclingChallenge.Services.Processors
{
    public class GetRaceProcessor : RankingProcessorBase<Contracts.DataContracts.Race.RaceDescription>
    {
        #region Private fields

        private Race _race;

        #endregion


        #region Constructor

        public GetRaceProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);
        }

        protected internal override Contracts.DataContracts.Race.RaceDescription ProcessInternal()
        {
            var riders = SummarizeRanking(RaceQueries.GetRaceRankings(Database, _race.Id).OrderBy(r => r.RaceRankingStage.Index), true);
            return new Contracts.DataContracts.Race.RaceDescription
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                Description = _race.RaceRaceDescription.Description,
                IsTour = _race.RaceRaceDescription.IsTour,
                Status = _race.Status,
                Yellow = _race.Status == RaceStatus.Finished || !_race.RaceRaceDescription.IsTour
                    ? CreateRider(_race.RaceYellow)
                    : riders.OrderBy(r => r.Rank).FirstOrDefault(),
                Green = CreateRider(_race.RaceGreen),
                Polkadot = CreateRider(_race.RacePoka),
                Stages = RaceQueries.GetStagesByRaceDescription(Database, _race.RaceDescription).ToList().Select(CreateStageDescription).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Race.StageDescription CreateStageDescription(Stage stage)
        {
            if (stage == null)
            {
                return null;
            }

            var trackDescription = new TrackDescription(stage.StageTrack.Terrain, stage.StageTrack.Description);
            var hills = trackDescription.Success ? trackDescription.GetHills() : null;
            var mountains = trackDescription.Success ? trackDescription.GetMountains() : null;
            var paved = trackDescription.Success ? trackDescription.GetPavedSectors() : null;
            var start = trackDescription.Success ? LocationQueries.GetCityById(Database, trackDescription.GetStart().Value) : null;

            return new Contracts.DataContracts.Race.StageDescription
            {
                Id = stage.Id,
                Name = stage.StageTrack.Name,
                Day = stage.Day,
                CobblestonesPercentage = stage.StageTrack.DifficultyCobblestones,
                PlainsPercentage = stage.StageTrack.DifficultyPlains,
                HillsPercentage = stage.StageTrack.DifficultyHills,
                MountainsPercentage = stage.StageTrack.DifficultyMountains,
                Profile = stage.StageTrack.Profile,
                Terrain = stage.StageTrack.CobbleHillMountain,
                Weather = start?.CityRegion?.Weather ?? Weather.Sunny,
                Hills = hills?.Select(c => CreateClimb(c.Key, c.Value)).ToList(),
                Mountains = mountains?.Select(c => CreateClimb(c.Key, c.Value)).ToList(),
                PavedSectors = paved?.Select(p => CreatePavedSector(p.Key, p.Value)).ToList()
            };
        }

        private Climb CreateClimb(int km, Models.Race.Terrain terrain)
        {
            var climb = terrain.Climb != null ? LocationQueries.GetClimbById(Database, terrain.Climb.Value) : null;
            return climb != null
                ? new Climb
                {
                    Id = climb.Id,
                    Kilometer = km,
                    Name = climb.Name
                }
                : null;
        }

        private PavedSector CreatePavedSector(int km, Models.Race.Terrain terrain)
        {
            var pavedSector = terrain.Climb != null ? LocationQueries.GetClimbById(Database, terrain.Climb.Value) : null;
            return pavedSector != null
                ? new PavedSector
                {
                    Id = pavedSector.Id,
                    Kilometer = km,
                    Name = pavedSector.Name
                }
                : null;
        }

        #endregion
    }
}