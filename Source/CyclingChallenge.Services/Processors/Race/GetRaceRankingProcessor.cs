﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Processors
{
    public class GetRaceRankingProcessor : RankingProcessorBase<Contracts.DataContracts.Race.RaceRanking>
    {
        #region Private fields

        private Race _race;

        #endregion


        #region Constructor

        public GetRaceRankingProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            if (_race.Status == RaceStatus.Open || _race.Status == RaceStatus.Closed)
            {
                throw new Exception(ErrorCodes.RaceNotStarted);
            }

            if (!_race.RaceRaceDescription.IsTour && _race.Status != RaceStatus.Finished)
            {
                throw new Exception(ErrorCodes.RaceNotFinished);
            }

            if (_race.RaceRaceDescription.IsTour && (_race.RaceCurrentStage?.Index ?? 0) <= 1 && _race.Status == RaceStatus.Running)
            {
                throw new Exception(ErrorCodes.FirstStageNotFinished);
            }
        }

        protected internal override Contracts.DataContracts.Race.RaceRanking ProcessInternal()
        {
            var riders = SummarizeRanking(RaceQueries.GetRaceRankings(Database, _race.Id).OrderBy(r => r.RaceRankingStage.Index), true);
            return new Contracts.DataContracts.Race.RaceRanking
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                IsTour = _race.RaceRaceDescription.IsTour,
                Yellow = _race.Status == RaceStatus.Finished || !_race.RaceRaceDescription.IsTour
                    ? CreateRider(_race.RaceYellow)
                    : riders.OrderBy(r => r.Rank).FirstOrDefault(),
                Green = CreateRider(_race.RaceGreen),
                Polkadot = CreateRider(_race.RacePoka),
                Riders = riders.OrderBy(r => r.Rank).ToList()
            };
        }

        #endregion
    }
}