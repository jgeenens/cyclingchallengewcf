﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Processors
{
    public class GetRaceReportProcessor : EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>
    {
        #region Private fields

        private Race _race;

        #endregion


        #region Constructor

        public GetRaceReportProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            if (_race.Status == RaceStatus.Open || _race.Status == RaceStatus.Closed)
            {
                throw new Exception(ErrorCodes.RaceNotStarted);
            }
        }

        protected internal override Contracts.DataContracts.Race.RaceReport ProcessInternal()
        {
            var events = RaceQueries.GetRaceEvents(Database, _race.Id, _race.CurrentStage).OrderBy(e => e.Index).ToList();
            var riders = _race.Status == RaceStatus.Finished || _race.Status == RaceStatus.BetweenStages
                ? PlayerQueries.GetPlayersFromRanking(Database, _race.Id, _race.CurrentStage).ToList().Select(CreateRider).ToList()
                : PlayerQueries.GetPlayersForRace(Database, _race.Id).ToList().Select(CreateRider).ToList();
            return new Contracts.DataContracts.Race.RaceReport
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                IsTour = _race.RaceRaceDescription.IsTour,
                Riders = riders,
                Events = GetProcessedEvents(_race.RaceCurrentStage, events)
            };
        }

        #endregion
    }
}