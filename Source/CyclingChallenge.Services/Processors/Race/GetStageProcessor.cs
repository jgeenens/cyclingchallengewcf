﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Location;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using Climb = CyclingChallenge.Services.Contracts.DataContracts.Location.Climb;

namespace CyclingChallenge.Services.Processors
{
    public class GetStageProcessor : RankingProcessorBase<Contracts.DataContracts.Race.StageDescription>
    {
        #region Private fields

        private Stage _stage;

        #endregion


        #region Constructor

        public GetStageProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idStage = GetTypeValidatedParameter<short>(Parameters.First());
            _stage = RaceQueries.GetStageById(Database, idStage);
            if (_stage == null)
            {
                throw new Exception(ErrorCodes.IdStageNotFound);
            }

            if (!_stage.StageRaceDescription.IsTour)
            {
                throw new Exception(ErrorCodes.RaceNotATour);
            }
        }

        protected internal override Contracts.DataContracts.Race.StageDescription ProcessInternal()
        {
            var trackDescription = new TrackDescription(_stage.StageTrack.Terrain, _stage.StageTrack.Description);
            var hills = trackDescription.Success ? trackDescription.GetHills() : null;
            var mountains = trackDescription.Success ? trackDescription.GetMountains() : null;
            var paved = trackDescription.Success ? trackDescription.GetPavedSectors() : null;
            var start = trackDescription.Success ? LocationQueries.GetCityById(Database, trackDescription.GetStart().Value) : null;

            return new Contracts.DataContracts.Race.StageDescription
            {
                Success = true,
                Id = _stage.Id,
                Name = _stage.StageTrack.Name,
                CobblestonesPercentage = _stage.StageTrack.DifficultyCobblestones,
                PlainsPercentage = _stage.StageTrack.DifficultyPlains,
                HillsPercentage = _stage.StageTrack.DifficultyHills,
                MountainsPercentage = _stage.StageTrack.DifficultyMountains,
                Profile = _stage.StageTrack.Profile,
                Terrain = _stage.StageTrack.CobbleHillMountain,
                Day = _stage.Day,
                Weather = start?.CityRegion?.Weather ?? Weather.Sunny,
                Hills = hills?.Select(c => CreateClimb(c.Key, c.Value)).ToList(),
                Mountains = mountains?.Select(c => CreateClimb(c.Key, c.Value)).ToList(),
                PavedSectors = paved?.Select(p => CreatePavedSector(p.Key, p.Value)).ToList()
            };
        }

        #endregion


        #region Private methods

        private Climb CreateClimb(int km, Models.Race.Terrain terrain)
        {
            var climb = terrain.Climb != null ? LocationQueries.GetClimbById(Database, terrain.Climb.Value) : null;
            return climb != null
                ? new Climb
                {
                    Id = climb.Id,
                    Kilometer = km,
                    Name = climb.Name
                }
                : null;
        }

        private PavedSector CreatePavedSector(int km, Models.Race.Terrain terrain)
        {
            var pavedSector = terrain.Climb != null ? LocationQueries.GetClimbById(Database, terrain.Climb.Value) : null;
            return pavedSector != null
                ? new PavedSector
                {
                    Id = pavedSector.Id,
                    Kilometer = km,
                    Name = pavedSector.Name
                }
                : null;
        }

        #endregion
    }
}