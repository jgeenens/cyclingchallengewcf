﻿using System;
using System.Collections.Generic;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts.Location;
using CyclingChallenge.Services.Contracts.DataContracts.Race;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;
using Climb = CyclingChallenge.Services.Contracts.DataContracts.Location.Climb;
using Race = CyclingChallenge.Services.Models.Race.Race;
using Stage = CyclingChallenge.Services.Models.Race.Stage;

namespace CyclingChallenge.Services.Processors
{
    public class GetStageProfileProcessor : RankingProcessorBase<Contracts.DataContracts.Race.RaceDescription>
    {
        #region Private fields

        private Race _race;
        private Stage _stage;

        #endregion


        #region Constructor

        public GetStageProfileProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            var idStage = GetTypeValidatedParameter<short>(Parameters.Skip(1).First());

            if (!_race.RaceRaceDescription.IsTour)
            {
                throw new Exception(ErrorCodes.RaceNotATour);
            }

            _stage = RaceQueries.GetStagesByRaceDescription(Database, _race.RaceDescription).FirstOrDefault(s => s.Id == idStage);
            if (_stage == null)
            {
                throw new Exception(ErrorCodes.InvalidStageForRace);
            }
        }

        protected internal override Contracts.DataContracts.Race.RaceDescription ProcessInternal()
        {
            return new Contracts.DataContracts.Race.RaceDescription
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                Stages = new List<StageDescription> { CreateStageDescription() }
            };
        }

        #endregion


        #region Private methods

        private StageDescription CreateStageDescription()
        {
            if (_stage == null)
            {
                return null;
            }

            var trackDescription = new TrackDescription(_stage.StageTrack.Terrain, _stage.StageTrack.Description, _stage.StageTrack.Altitude);
            var hills = trackDescription.Success ? trackDescription.GetHills() : null;
            var mountains = trackDescription.Success ? trackDescription.GetMountains() : null;
            var paved = trackDescription.Success ? trackDescription.GetPavedSectors() : null;
            var start = trackDescription.Success ? LocationQueries.GetCityById(Database, trackDescription.GetStart().Value) : null;
            var finish = trackDescription.Success ? LocationQueries.GetCityById(Database, trackDescription.GetFinish().Value) : null;

            return new StageDescription
            {
                Id = _stage.Id,
                Name = _stage.StageTrack.Name,
                CobblestonesPercentage = _stage.StageTrack.DifficultyCobblestones,
                PlainsPercentage = _stage.StageTrack.DifficultyPlains,
                HillsPercentage = _stage.StageTrack.DifficultyHills,
                MountainsPercentage = _stage.StageTrack.DifficultyMountains,
                Profile = _stage.StageTrack.Profile,
                Terrain = _stage.StageTrack.CobbleHillMountain,
                Day = _stage.Day,
                Weather = start?.CityRegion?.Weather ?? Weather.Sunny,
                Distance = _stage.StageTrack.Kilometers,
                Start = CreateCity(start),
                Finish = CreateCity(finish),
                Hills = hills?.Select(c => CreateClimb(c.Key, c.Value)).ToList(),
                Mountains = mountains?.Select(m => CreateClimb(m.Key, m.Value)).ToList(),
                PavedSectors = paved?.Select(p => CreatePavedSector(p.Key, p.Value)).ToList(),
                Altitudes = trackDescription.Altitudes?.Select(a => CreateAltitude(a.Key, a.Value)).ToList()
            };
        }

        private static Altitude CreateAltitude(int km, long height)
        {
            return new Altitude
            {
                Km = km,
                Height = height
            };
        }

        private static City CreateCity(Models.Location.City city)
        {
            return new City
            {
                Id = city.Id,
                Name = LocationQueries.GetCityText(city),
                Region = CreateRegion(city.CityRegion)
            };
        }

        private static Region CreateRegion(Models.Location.Region region)
        {
            return new Region
            {
                Id = region.Id,
                Name = region.Name,
                Country = CreateCountry(region.RegionCountry)
            };
        }

        private Climb CreateClimb(int km, Models.Race.Terrain terrain)
        {
            var climb = terrain.Climb != null ? LocationQueries.GetClimbById(Database, terrain.Climb.Value) : null;
            return climb != null
                ? new Climb
                {
                    Id = climb.Id,
                    Kilometer = km,
                    Name = climb.Name
                }
                : null;
        }

        private PavedSector CreatePavedSector(int km, Models.Race.Terrain terrain)
        {
            var pavedSector = terrain.Climb != null ? LocationQueries.GetClimbById(Database, terrain.Climb.Value) : null;
            return pavedSector != null
                ? new PavedSector
                {
                    Id = pavedSector.Id,
                    Kilometer = km,
                    Name = pavedSector.Name
                }
                : null;
        }

        #endregion
    }
}