﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Processors
{
    public class GetStageRankingProcessor : RankingProcessorBase<Contracts.DataContracts.Race.RaceRanking>
    {
        #region Private fields

        private Race _race;
        private Stage _stage;

        #endregion


        #region Constructor

        public GetStageRankingProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            var idStage = GetTypeValidatedParameter<short>(Parameters.Skip(1).First());

            if (!_race.RaceRaceDescription.IsTour)
            {
                throw new Exception(ErrorCodes.RaceNotATour);
            }

            _stage = RaceQueries.GetStagesByRaceDescription(Database, _race.RaceDescription).FirstOrDefault(s => s.Id == idStage);
            if (_stage == null)
            {
                throw new Exception(ErrorCodes.InvalidStageForRace);
            }

            if (_race.Status == RaceStatus.Open || _race.Status == RaceStatus.Closed)
            {
                throw new Exception(ErrorCodes.RaceNotStarted);
            }

            if (_stage.Index > _race.RaceCurrentStage.Index)
            {
                throw new Exception(ErrorCodes.StageNotStarted);
            }

            if (_stage.Id == _race.CurrentStage && _race.Status == RaceStatus.Running)
            {
                throw new Exception(ErrorCodes.StageNotFinished);
            }
        }

        protected internal override Contracts.DataContracts.Race.RaceRanking ProcessInternal()
        {
            var raceRiders = SummarizeRanking(RaceQueries.GetRaceRankings(Database, _race.Id).OrderBy(r => r.RaceRankingStage.Index), true);
            var riders = SummarizeRanking(RaceQueries.GetRaceRankings(Database, _race.Id, _stage.Id), true);
            return new Contracts.DataContracts.Race.RaceRanking
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                IsTour = _race.RaceRaceDescription.IsTour,
                Stage = CreateStage(_stage),
                Yellow = _race.Status == RaceStatus.Finished
                    ? CreateRider(_race.RaceYellow)
                    : raceRiders.OrderBy(r => r.Rank).FirstOrDefault(),
                Green = CreateRider(_race.RaceGreen),
                Polkadot = CreateRider(_race.RacePoka),
                Riders = riders.OrderBy(r => r.Rank).ToList()
            };
        }

        #endregion


        #region Private Methods

        private static Contracts.DataContracts.Race.Stage CreateStage(Stage stage)
        {
            return new Contracts.DataContracts.Race.Stage
            {
                Id = stage.Id,
                Name = stage.StageTrack.Name
            };
        }

        #endregion
    }
}