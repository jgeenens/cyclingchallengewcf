﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Processors
{
    public class GetStageReportProcessor : EventTextProcessorBase<Contracts.DataContracts.Race.RaceReport>
    {
        #region Private fields

        private Race _race;
        private Stage _stage;

        #endregion


        #region Constructor

        public GetStageReportProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idRace = GetTypeValidatedParameter<long>(Parameters.First());
            _race = GetValidatedRace(idRace);

            var idStage = GetTypeValidatedParameter<short>(Parameters.Skip(1).First());

            if (!_race.RaceRaceDescription.IsTour)
            {
                throw new Exception(ErrorCodes.RaceNotATour);
            }

            _stage = RaceQueries.GetStagesByRaceDescription(Database, _race.RaceDescription).FirstOrDefault(s => s.Id == idStage);
            if (_stage == null)
            {
                throw new Exception(ErrorCodes.InvalidStageForRace);
            }

            if (_race.Status == RaceStatus.Open || _race.Status == RaceStatus.Closed)
            {
                throw new Exception(ErrorCodes.RaceNotStarted);
            }

            if (_stage.Index > _race.RaceCurrentStage.Index)
            {
                throw new Exception(ErrorCodes.StageNotStarted);
            }
        }

        protected internal override Contracts.DataContracts.Race.RaceReport ProcessInternal()
        {
            var events = RaceQueries.GetRaceEvents(Database, _race.Id, _stage.Id).OrderBy(e => e.Index).ToList();
            var riders = _race.Status == RaceStatus.Finished || _race.Status == RaceStatus.BetweenStages
                ? PlayerQueries.GetPlayersFromRanking(Database, _race.Id, _stage.Id).ToList().Select(CreateRider).ToList()
                : PlayerQueries.GetPlayersForRace(Database, _race.Id).ToList().Select(CreateRider).ToList();
            return new Contracts.DataContracts.Race.RaceReport
            {
                Success = true,
                Id = _race.Id,
                Name = _race.RaceRaceDescription.Name,
                IsTour = _race.RaceRaceDescription.IsTour,
                Stage = CreateStage(_stage),
                Riders = riders,
                Events = GetProcessedEvents(_stage, events)
            };
        }

        #endregion


        #region Private Methods

        private static Contracts.DataContracts.Race.Stage CreateStage(Stage stage)
        {
            return new Contracts.DataContracts.Race.Stage
            {
                Id = stage.Id,
                Name = stage.StageTrack.Name
            };
        }

        #endregion
    }
}