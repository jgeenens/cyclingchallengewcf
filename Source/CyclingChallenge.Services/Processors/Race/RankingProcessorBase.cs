﻿using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CyclingChallenge.Services.Processors
{
    public abstract class RankingProcessorBase<TResult> : ProcessorBase<TResult> where TResult : ResultBase, new()
    {
        #region Constructor

        protected RankingProcessorBase(params object[] parameters) : base(parameters) { }

        #endregion


        #region Shared methods

        protected internal List<RiderBase> SummarizeRanking(IQueryable<RaceRanking> raceRankings, bool withRank = false)
        {
            var riders = new Dictionary<long, RiderBase>();
            foreach (var raceRanking in raceRankings.ToList())
            {
                foreach (Match match in GetRankingMatches(raceRanking.Ranking))
                {
                    AddValue(riders, match.Groups[2].Value, match.Groups[3].Value);
                }
                if (raceRanking.PointsRanking != null)
                {
                    foreach (Match match in GetRankingMatches(raceRanking.PointsRanking))
                    {
                        AddValue(riders, match.Groups[2].Value, valuePoints: match.Groups[3].Value);
                    }
                }
                if (raceRanking.MountainsRanking != null)
                {
                    foreach (Match match in GetRankingMatches(raceRanking.MountainsRanking))
                    {
                        AddValue(riders, match.Groups[2].Value, valueMountainPoints: match.Groups[3].Value);
                    }
                }
            }

            return withRank
                ? riders.Values.ToList().OrderBy(r => r.Time ?? long.MaxValue).Select((r,i) => SetRank(r, i + 1)).ToList()
                : riders.Values.ToList();
        }

        protected internal RiderBase CreateRider(Player rider, long? time = null, int? points = null, int? mountainPoints = null)
        {
            return rider != null
                ? new RiderBase
                {
                    Id = rider.Id,
                    Name = $"{rider.FirstName} {rider.LastName}".Trim(),
                    Age = rider.Age,
                    Nationality = rider.Nationality,
                    Team = CreateTeam(rider.PlayerTeam),
                    Time = time,
                    Points = points,
                    MountainPoints = mountainPoints
                }
                : null;
        }

        #endregion


        #region Private methods

        private static MatchCollection GetRankingMatches(string ranking)
        {
            var regex = @"((\d+),(\d+)\|*)";
            if (!Regex.IsMatch(ranking, regex))
            {
                throw new Exception(ErrorCodes.InvalidFormatRanking);
            }

            return Regex.Matches(ranking, regex);
        }

        private void AddValue(IDictionary<long, RiderBase> riders, string idRiderString, string valueTime = null, string valuePoints = null, string valueMountainPoints = null)
        {
            var idRider = long.Parse(idRiderString);
            var time = string.IsNullOrEmpty(valueTime) ? (long?)null : long.Parse(valueTime);
            var points = string.IsNullOrEmpty(valuePoints) ? (int?)null : int.Parse(valuePoints);
            var mountainPoints = string.IsNullOrEmpty(valueMountainPoints) ? (int?)null : int.Parse(valueMountainPoints);

            if (!riders.ContainsKey(idRider))
            {
                riders.Add(idRider, CreateRider(PlayerQueries.GetPlayerById(Database, idRider),0,0,0));
            }
            if (time != null)
            {
                riders[idRider].Time = (time == 99999 ? long.MaxValue : riders[idRider].Time + time);
            }
            if (points != null)
            {
                riders[idRider].Points += points;
            }
            if (mountainPoints != null)
            {
                riders[idRider].MountainPoints += mountainPoints;
            }
        }

        private static RiderBase SetRank(RiderBase rider, int rank)
        {
            rider.Rank = (byte)rank;
            return rider;
        }

        #endregion
    }
}