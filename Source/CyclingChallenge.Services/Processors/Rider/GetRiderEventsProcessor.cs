﻿using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Processors
{
    public class GetRiderEventsProcessor : ProcessorBase<RiderEvents>
    {
        #region Private fields

        private Player _rider;

        #endregion


        #region Constructor

        public GetRiderEventsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRider = GetTypeValidatedParameter<long>(Parameters.First());
            _rider = GetValidatedRider(idRider);
        }

        protected internal override RiderEvents ProcessInternal()
        {
            return new RiderEvents
            {
                Success = true,
                Id = _rider.Id,
                Name = $"{_rider.FirstName} {_rider.LastName}",
                Events = CalendarQueries.GetCalendarByRider(Database, _rider.Id).ToList().Select(cal => new Event
                {
                    Id = cal.Id,
                    Race = !cal.Event.StartsWith("2-") ? GetRaceFromCalendar(cal) : null,
                    Camp = cal.Event.StartsWith("2-") ? GetCampFromCalendar(cal) : null
                }).ToList()
            };
        }

        #endregion


        #region Private methods

        private Camp GetCampFromCalendar(Calendar cal)
        {
            var idLocation = short.Parse(cal.Description);
            var location = LocationQueries.GetLocationById(Database, idLocation);
            return new Camp
            {
                Id = idLocation,
                Name = location.Name,
                City = CreateCity(location.LocationCity),
                Start = cal.StartDate,
                End = cal.EndDate
            };
        }

        private static Contracts.DataContracts.Location.City CreateCity(Models.Location.City city)
        {
            return new Contracts.DataContracts.Location.City
            {
                Id = city.Id,
                Name = LocationQueries.GetCityText(city),
                Region = CreateRegion(city.CityRegion)
            };
        }

        private static Contracts.DataContracts.Location.Region CreateRegion(Models.Location.Region region)
        {
            return new Contracts.DataContracts.Location.Region
            {
                Id = region.Id,
                Name = region.Name,
                Country = CreateCountry(region.RegionCountry)
            };
        }

        private static Contracts.DataContracts.Race.RaceBase GetRaceFromCalendar(Calendar cal)
        {
            return new Contracts.DataContracts.Race.RaceBase
            {
                Id = long.Parse(cal.Event.Split('-')[1]),
                Name = short.Parse(cal.Description),
                Start = cal.StartDate,
                End = cal.EndDate
            };
        }

        #endregion
    }
}