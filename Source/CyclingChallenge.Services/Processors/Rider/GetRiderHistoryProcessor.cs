﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Race;
using Race = CyclingChallenge.Services.Models.Race.Race;
using RaceHistory = CyclingChallenge.Services.Models.Race.RaceHistory;
using Stage = CyclingChallenge.Services.Models.Race.Stage;

namespace CyclingChallenge.Services.Processors
{
    public class GetRiderHistoryProcessor : ProcessorBase<RiderHistory>
    {
        #region Private fields

        private Player _rider;

        #endregion


        #region Constructor

        public GetRiderHistoryProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRider = GetTypeValidatedParameter<long>(Parameters.First());
            _rider = GetValidatedRider(idRider);
        }

        protected internal override RiderHistory ProcessInternal()
        {
            var riderHistories = RaceQueries.GetRaceHistoryForRider(Database, _rider.Id).Where(rh => rh.Position < 4).ToList();
            return new RiderHistory
            {
                Success = true,
                Id = _rider.Id,
                Name = $"{_rider.FirstName} {_rider.LastName}",
                Achievements = riderHistories.Select(CreateAchievement).ToList()
            };
        }

        #endregion


        #region Private methods

        private static Achievement CreateAchievement(RaceHistory rh)
        {
            return new Achievement
            {
                Id = rh.Id,
                When = rh.When,
                Season = rh.Season,
                Item = Items.HistoryFirstInRace + rh.Position - 1,
                Team = new Contracts.DataContracts.Team.TeamBase
                {
                    Id = rh.Team,
                    LongName = rh.TeamName
                },
                Race = CreateRace(rh.RaceHistoryRace),
                Stage = CreateStage(rh.RaceHistoryStage)
            };
        }

        private static RaceBase CreateRace(Race race)
        {
            return new RaceBase
            {
                Id = race.Id,
                Name = race.RaceRaceDescription.Name,
                IsTour = race.RaceRaceDescription.IsTour
            };
        }

        private static Contracts.DataContracts.Race.Stage CreateStage(Stage stage)
        {
            return stage != null
                ? new Contracts.DataContracts.Race.Stage
                {
                    Id = stage.Id,
                    Name = stage.StageTrack.Name,
                    IsTimeTrial = stage.StageTrack.Profile == Profile.TimeTrial,
                    IsTeamTimeTrial = stage.StageTrack.Profile == Profile.TeamTimeTrial
                }
                : null;
        }

        #endregion
    }
}