﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Processors
{
    public class GetRiderProcessor : ProcessorBase<Contracts.DataContracts.Rider.Rider>
    {
        #region Private fields

        private PlayerValue _rider;

        #endregion


        #region Constructor

        public GetRiderProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idRider = GetTypeValidatedParameter<long>(Parameters.First());
            _rider = PlayerQueries.GetPlayerValuesByPlayer(Database, idRider);
            if (_rider == null)
            {
                throw new Exception(ErrorCodes.IdRiderNotFound);
            }
        }

        protected internal override Contracts.DataContracts.Rider.Rider ProcessInternal()
        {
            return new Contracts.DataContracts.Rider.Rider
            {
                Success = true,
                Id = _rider.Player,
                Name = $"{_rider.PlayerValuePlayer.FirstName} {_rider.PlayerValuePlayer.LastName}",
                Age = _rider.PlayerValuePlayer.Age,
                Nationality = _rider.PlayerValuePlayer.Nationality,
                Residence = LocationQueries.GetCityText(_rider.PlayerValuePlayer.PlayerCity),
                Wage = _rider.Wage,
                Value = _rider.Value,
                Category = _rider.Type,
                IsRetired = _rider.PlayerValuePlayer.Retired,
                IsOnTrainingCamp = _rider.CampDays > 0,
                IsOnTransfer = _rider.IsOnTransfer,
                Team = _rider.PlayerValuePlayer.Retired ? null : CreateTeam(_rider.PlayerValuePlayer.PlayerTeam),
                Transfer = !_rider.IsOnTransfer ? null : GetTransferForRider(),
                Picture = GetPictureFromPlayer(_rider.PlayerValuePlayer),
                Medals = GetMedalsForRider(),
                Values = new RiderValues
                {
                    Plains = _rider.Plains,
                    Cobblestones = _rider.Cobblestones,
                    Hills = _rider.Hills,
                    Mountains = _rider.Mountains,
                    Technique = _rider.Technique,
                    Stamina = _rider.Stamina,
                    Descending = _rider.Descending,
                    Sprint = _rider.Sprint,
                    TimeTrial = _rider.Timetrial,
                    Climbing = _rider.Climbing,
                    Form = _rider.Form,
                    Fatigue = _rider.Fatigue
                }
            };
        }

        #endregion


        #region Private methods

        private Transfer GetTransferForRider()
        {
            return _rider.IsOnTransfer 
                ? new Transfer
                {
                    Deadline = _rider.Deadline ?? DateTime.MinValue,
                    StartBid = _rider.StartBid,
                    Bid = _rider.Bid,
                    BidTeam = CreateTeam(_rider.PlayerValueBidTeam)
                } 
                : null;
        }

        private static Picture GetPictureFromPlayer(Player player)
        {
            return new Picture
            {
                Face = player.Face,
                Hair = player.Hair,
                Eyes = player.Eyes,
                Nose = player.Nose,
                Lips = player.Lips
            };
        }

        private RiderMedals GetMedalsForRider()
        {
            var stages = RaceQueries.GetRaceHistoryForRider(Database, _rider.Id);
            return new RiderMedals
            {
                Gold = (byte)stages.Count(s => s.Position == 1),
                Silver = (byte)stages.Count(s => s.Position == 2),
                Bronze = (byte)stages.Count(s => s.Position == 3)
            };
        }

        #endregion
    }
}