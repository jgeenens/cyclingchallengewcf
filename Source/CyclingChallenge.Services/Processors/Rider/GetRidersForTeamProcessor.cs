﻿using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Processors
{
    public class GetRidersForTeamProcessor : ProcessorBase<TeamRiders>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetRidersForTeamProcessor(params object[] parameters) : base(parameters) { }

        #endregion
        
        
        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override TeamRiders ProcessInternal()
        {
            var riders = PlayerQueries.GetPlayersForTeam(Database, _team.Id).ToList();
            return new TeamRiders
            {
                Success = true,
                Id = _team.Id,
                ShortName = _team.ShortName,
                LongName = _team.Name,
                Riders = riders.Select(CreateRider).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Rider.Rider CreateRider(PlayerValue r)
        {
            return new Contracts.DataContracts.Rider.Rider
            {
                Id = r.Player,
                Name = $"{r.PlayerValuePlayer.FirstName} {r.PlayerValuePlayer.LastName}",
                Age = r.PlayerValuePlayer.Age,
                Nationality = r.PlayerValuePlayer.Nationality,
                Category = r.Type,
                IsOnTrainingCamp = r.CampDays > 0,
                IsOnTransfer = r.IsOnTransfer,
                Picture = GetPictureFromPlayer(r.PlayerValuePlayer),
                Team = CreateTeam(_team),
                Values = new RiderValues
                {
                    Plains = r.Plains,
                    Cobblestones = r.Cobblestones,
                    Hills = r.Hills,
                    Mountains = r.Mountains
                }
            };
        }

        private static Picture GetPictureFromPlayer(Player player)
        {
            return new Picture
            {
                Face = player.Face,
                Hair = player.Hair,
                Eyes = player.Eyes,
                Nose = player.Nose,
                Lips = player.Lips
            };
        }

        #endregion
    }
}