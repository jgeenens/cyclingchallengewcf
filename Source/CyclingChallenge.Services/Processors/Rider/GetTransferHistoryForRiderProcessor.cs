﻿using System;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using CyclingChallenge.Services.Models.Rider;
using TransferHistory = CyclingChallenge.Services.Models.Rider.TransferHistory;

namespace CyclingChallenge.Services.Processors
{
    public class GetTransferHistoryForRiderProcessor : ProcessorBase<RiderTransferHistory>
    {
        #region Private fields

        private Player _rider;
        private DateTime _from;
        private DateTime? _to;

        #endregion


        #region Constructor

        public GetTransferHistoryForRiderProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idRider = GetTypeValidatedParameter<long>(Parameters.First());
            _rider = GetValidatedRider(idRider);

            _from = GetTypeValidatedParameter<DateTime>(Parameters.Skip(1).First());

            if (Parameters.Count > 2)
            {
                _to = GetTypeValidatedParameter<DateTime>(Parameters.Skip(2).First());
            }
        }

        protected internal override RiderTransferHistory ProcessInternal()
        {
            var transfers = PlayerQueries.GetTransferHistoryForRider(Database, _rider.Id, _from, _to).OrderByDescending(h => h.DateTransfer).ToList();
            return new RiderTransferHistory
            {
                Success = true,
                Id = _rider.Id,
                Name = $"{_rider.FirstName} {_rider.LastName}".Trim(),
                Transfers = transfers.Select(CreateTransferHistory).ToList()
            };
        }

        #endregion


        #region Private methods

        private static Contracts.DataContracts.Rider.TransferHistory CreateTransferHistory(TransferHistory transferHistory)
        {
            return new Contracts.DataContracts.Rider.TransferHistory
            {
                Id = transferHistory.Id,
                Date = transferHistory.DateTransfer,
                Bid = transferHistory.Bid,
                PreviousTeam = CreateTeam(transferHistory.TransferHistoryFromTeam),
                CurrentTeam = CreateTeam(transferHistory.TransferHistoryTeam)
            };
        }

        #endregion
    }
}