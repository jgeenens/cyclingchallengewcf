﻿using System;
using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Models.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;
using TransferHistory = CyclingChallenge.Services.Models.Rider.TransferHistory;

namespace CyclingChallenge.Services.Processors
{
    public class GetTransferHistoryForTeamProcessor : ProcessorBase<TeamTransferHistory>
    {
        #region Private fields

        private Team _team;
        private DateTime _from;
        private DateTime? _to;

        #endregion


        #region Constructor

        public GetTransferHistoryForTeamProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            _from = GetTypeValidatedParameter<DateTime>(Parameters.Skip(1).First());

            if (Parameters.Count > 2)
            {
                _to = GetTypeValidatedParameter<DateTime>(Parameters.Skip(2).First());
            }
        }

        protected internal override TeamTransferHistory ProcessInternal()
        {
            var transfers = TeamQueries.GetTransferHistoryForTeam(Database, _team.Id, _from, _to);
            return new TeamTransferHistory
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                Incoming = transfers.Where(h => h.Team == _team.Id).ToList().Select(CreateTransferHistory).ToList(),
                Outgoing = transfers.Where(h => h.FromTeam == _team.Id).ToList().Select(CreateTransferHistory).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Rider.TransferHistory CreateTransferHistory(TransferHistory transferHistory)
        {
            return new Contracts.DataContracts.Rider.TransferHistory
            {
                Id = transferHistory.Id,
                Date = transferHistory.DateTransfer,
                Bid = transferHistory.Bid,
                Rider = transferHistory.Team == _team.Id
                    ? CreateRider(transferHistory.TransferHistoryPlayer, transferHistory.TransferHistoryFromTeam)
                    : CreateRider(transferHistory.TransferHistoryPlayer, transferHistory.TransferHistoryTeam)
            };
        }

        private static RiderBase CreateRider(Player rider, Team team)
        {
            return new RiderBase
            {
                Id = rider.Id,
                Name = $"{rider.FirstName} {rider.LastName}".Trim(),
                Team = CreateTeam(team)
            };
        }

        #endregion
    }
}