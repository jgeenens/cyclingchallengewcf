﻿using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;
using TransferHistory = CyclingChallenge.Services.Models.Rider.TransferHistory;

namespace CyclingChallenge.Services.Processors
{
    public class GetTransfersProcessor : ProcessorBase<TeamTransfers>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetTransfersProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override TeamTransfers ProcessInternal()
        {
            var transfers = TeamQueries.GetTransfersForTeam(Database, _team.Id);
            return new TeamTransfers
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                Incoming = transfers.Where(h => h.Team == _team.Id).ToList().Select(CreateTransfer).ToList(),
                Outgoing = transfers.Where(h => h.FromTeam == _team.Id).ToList().Select(CreateTransfer).ToList()
            };
        }

        #endregion


        #region Private methods

        private static Transfer CreateTransfer(TransferHistory transferHistory)
        {
            return new Transfer
            {
                Id = transferHistory.Player,
                Deadline = transferHistory.DateTransfer,
                Bid = transferHistory.Bid,
                BidTeam = CreateTeam(transferHistory.TransferHistoryTeam),
                Team = CreateTeam(transferHistory.TransferHistoryFromTeam),
                Name = $"{transferHistory.TransferHistoryPlayer.FirstName} {transferHistory.TransferHistoryPlayer.LastName}".Trim()
            };
        }

        #endregion
    }
}