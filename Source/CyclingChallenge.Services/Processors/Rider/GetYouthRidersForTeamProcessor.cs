﻿using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.YouthRider;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Location;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Processors
{
    public class GetYouthRidersForTeamProcessor : ProcessorBase<TeamYouthRiders>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetYouthRidersForTeamProcessor(params object[] parameters) : base(parameters) { }

        #endregion
        
        
        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override TeamYouthRiders ProcessInternal()
        {
            var riders = PlayerQueries.GetPlayersForTeam(Database, _team.Id).ToList();
            return new TeamYouthRiders
            {
                Success = true,
                Id = _team.Id,
                ShortName = _team.ShortName,
                LongName = _team.Name,
                Riders = riders.Select(CreateRider).ToList()
            };
        }

        #endregion


        #region Private methods

        private static YouthRider CreateRider(PlayerValue r)
        {
            return new YouthRider
            {
                Id = r.Player,
                Name = $"{r.PlayerValuePlayer.FirstName} {r.PlayerValuePlayer.LastName}",
                Age = r.PlayerValuePlayer.Age,
                Nationality = r.PlayerValuePlayer.Nationality,
                Residence = GetCityAndVillage(r.PlayerValuePlayer.PlayerCity),
                Picture = GetPictureFromPlayer(r.PlayerValuePlayer),
                Team = CreateTeam(r.PlayerValuePlayer.PlayerTeam),
                Values = new YouthRiderValues
                {
                    Terrain = GetYouthTerrainValue(r),
                    Skill = GetYouthSkillValue(r)
                }
            };
        }

        private static byte GetYouthSkillValue(PlayerValue r)
        {
            return (byte)((r.Climbing + r.Descending + r.Stamina + r.Sprint + 
                            r.Technique + r.Timetrial + r.Experience) / 218750);
        }

        private static byte GetYouthTerrainValue(PlayerValue r)
        {
            return (byte)((r.Plains + r.Hills + r.Mountains + r.Cobblestones) / 125000);
        }

        private static string GetCityAndVillage(City city)
        {
            var village = city.Name != city.Village ? $" ({city.Village})" : string.Empty;
            return $"{city.Name}{village}";
        }

        private static Picture GetPictureFromPlayer(Player player)
        {
            return new Picture
            {
                Face = player.Face,
                Hair = player.Hair,
                Eyes = player.Eyes,
                Nose = player.Nose,
                Lips = player.Lips
            };
        }

        #endregion
    }
}