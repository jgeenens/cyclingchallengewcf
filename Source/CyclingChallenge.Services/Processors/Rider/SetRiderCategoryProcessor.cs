﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Processors
{
    public class SetRiderCategoryProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private Team _team;
        private Player _rider;
        private byte _category;

        #endregion


        #region Constructor

        public SetRiderCategoryProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(3);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            var idRider = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _rider = GetValidatedRider(idRider);

            if (_rider.Team != _team.Id)
            {
                throw new Exception(ErrorCodes.RiderNotFromTeam);
            }

            _category = GetTypeValidatedParameter<byte>(Parameters.Skip(2).First());
            if (!new[] { Strategy.Type.Adventurer, Strategy.Type.Captain, Strategy.Type.Climber, Strategy.Type.Domestique, Strategy.Type.Escapee,
                         Strategy.Type.Finisher, Strategy.Type.Puncher, Strategy.Type.Sprinter, Strategy.Type.None }.Contains(_category))
            {
                throw new Exception(ErrorCodes.InvalidCategoryValue);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            var playerValues = PlayerQueries.GetPlayerValuesByPlayer(Database, _rider.Id);
            playerValues.Type = _category;
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}