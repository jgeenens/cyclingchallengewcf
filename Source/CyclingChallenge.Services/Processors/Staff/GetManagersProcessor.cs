﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Staff;
using CyclingChallenge.Services.Models.Team;
using Manager = CyclingChallenge.Services.Models.Staff.Manager;

namespace CyclingChallenge.Services.Processors
{
    public class GetManagersProcessor : ProcessorBase<TeamManagers>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetManagersProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override TeamManagers ProcessInternal()
        {
            var managers = StaffQueries.GetManagers(Database, _team.Id).ToList();
            return new TeamManagers
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                Managers = managers.Select(CreateManager).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Staff.Manager CreateManager(Manager manager)
        {
            return new Contracts.DataContracts.Staff.Manager
            {
                Id = manager.Id,
                Name = $"{manager.FirstName} {manager.LastName}".Trim(),
                Type = manager.Type,
                Level = manager.Level,
                IsActivated = manager.Status != Managers.Status.Locked,
                IsLearning = manager.Status == Managers.Status.Learning,
                TrainingCost = GetTrainingCost(manager.Status, manager.Type, manager.Level),
                TrainingTime = manager.Status == Managers.Status.Active ? (manager.Level + 1) * 12 : (int?)null,
                StartTraining = manager.Learning,
                Picture = GetPictureFromManager(manager)
            };
        }

        private short? GetTrainingCost(byte status, byte type, byte level)
        {
            if (level == Managers.Level.WorldClass)
            {
                return null;
            }

            switch (type)
            {
                case Managers.Type.Shop: return GameQueries.GetAddonById(Database, GetShopManagerAddOnId(status, level)).Tokens;
                case Managers.Type.Race: return GameQueries.GetAddonById(Database, GetRaceManagerAddOnId(status, level)).Tokens;
                case Managers.Type.Staff: return GameQueries.GetAddonById(Database, GetStaffManagerAddOnId(status, level)).Tokens;
                default: return 0;
            }
        }

        private short GetShopManagerAddOnId(byte status, byte level)
        {
            if (status == Managers.Status.Locked)
            {
                return AddOns.ActivateShopManager;
            }

            switch (level)
            {
                case Managers.Level.Apprentice: return AddOns.BeginnerShopManager;
                case Managers.Level.Beginner: return AddOns.BasicShopManager;
                case Managers.Level.Basic: return AddOns.IntermediateShopManager;
                case Managers.Level.Intermediate: return AddOns.AdvancedShopManager;
                case Managers.Level.Advanced: return AddOns.ExpertShopManager;
                case Managers.Level.Expert: return AddOns.WorldClassShopManager;
                default: return AddOns.ActivateShopManager;
            }
        }

        private short GetRaceManagerAddOnId(byte status, byte level)
        {
            if (status == Managers.Status.Locked)
            {
                return AddOns.ActivateRaceManager;
            }

            switch (level)
            {
                case Managers.Level.Apprentice: return AddOns.BeginnerRaceManager;
                case Managers.Level.Beginner: return AddOns.BasicRaceManager;
                case Managers.Level.Basic: return AddOns.IntermediateRaceManager;
                case Managers.Level.Intermediate: return AddOns.AdvancedRaceManager;
                case Managers.Level.Advanced: return AddOns.ExpertRaceManager;
                case Managers.Level.Expert: return AddOns.WorldClassRaceManager;
                default: return AddOns.ActivateRaceManager;
            }
        }

        private short GetStaffManagerAddOnId(byte status, byte level)
        {
            if (status == Managers.Status.Locked)
            {
                return AddOns.ActivateStaffManager;
            }

            switch (level)
            {
                case Managers.Level.Apprentice: return AddOns.BeginnerStaffManager;
                case Managers.Level.Beginner: return AddOns.BasicStaffManager;
                case Managers.Level.Basic: return AddOns.IntermediateStaffManager;
                case Managers.Level.Intermediate: return AddOns.AdvancedStaffManager;
                case Managers.Level.Advanced: return AddOns.ExpertStaffManager;
                case Managers.Level.Expert: return AddOns.WorldClassStaffManager;
                default: return AddOns.ActivateStaffManager;
            }
        }

        private static Picture GetPictureFromManager(Manager manager)
        {
            return new Picture
            {
                Face = manager.Face,
                Hair = manager.Hair,
                Eyes = manager.Eyes,
                Nose = manager.Nose,
                Lips = manager.Lips
            };
        }

        #endregion
    }
}