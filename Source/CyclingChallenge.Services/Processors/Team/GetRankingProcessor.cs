﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts.Team;
using CyclingChallenge.Services.Models.Rider;
using Team = CyclingChallenge.Services.Models.Team.Team;

namespace CyclingChallenge.Services.Processors
{
    public class GetRankingProcessor : ProcessorBase<Ranking>
    {
        #region Private fields

        private Models.Team.Division _division;

        #endregion


        #region Constructor

        public GetRankingProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idDivision = GetTypeValidatedParameter<long>(Parameters.First());
            _division = TeamQueries.GetDivisionById(Database, idDivision);
            if (_division == null)
            {
                throw new Exception(ErrorCodes.IdDivisionNotFound);
            }
        }

        protected internal override Ranking ProcessInternal()
        {
            var teams = TeamQueries.GetTeamsByDivision(Database, _division.Id).OrderBy(t => t.Rank).ToList();
            var players = new List<PlayerValue>();
            foreach (var team in teams)
            {
                players.AddRange(PlayerQueries.GetPlayersForTeam(Database, team.Id));
            }
            players = players.OrderByDescending(p => p.Points).ThenByDescending(p => p.Value).ThenBy(p => p.PlayerValuePlayer.FirstName).Take(10).ToList();

            return new Ranking
            {
                Success = true,
                Id = _division.Id,
                Name = _division.Name,
                Country = CreateCountry(_division.DivisionCountry),
                Teams = teams.Select(CreateTeamDetails).ToList(),
                Riders = players.Select(p => CreateRider(p, teams)).ToList()
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.Team.Team CreateTeamDetails(Team team)
        {
            return new Contracts.DataContracts.Team.Team
            {
                Id = team.Id,
                ShortName = team.ShortName,
                LongName = team.Name,
                Rank = team.Rank,
                Wins = team.Wins,
                Stage = team.Stage,
                Points = team.Points,
                Champions = team.Champions,
                WillPromote = WillPromote(team.Rank, _division.Level),
                WillDemote = team.Rank > 8
            };
        }

        private static bool WillPromote(byte rank, byte level)
        {
            switch (level)
            {
                case Promotion.NoPromotion: return false;
                case Promotion.TopTwoPromote: return rank <= 2;
                default: return rank <= 4;
            }
        }

        private static RiderBase CreateRider(PlayerValue player, List<Team> teams)
        {
            return new RiderBase
            {
                Id = player.Player,
                Name = $"{player.PlayerValuePlayer.FirstName} {player.PlayerValuePlayer.LastName}".Trim(),
                Age = player.PlayerValuePlayer.Age,
                Points = player.Points,
                Team = CreateTeam(player.PlayerValuePlayer.PlayerTeam)
            };
        }

        #endregion
    }
}