﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Communication;

namespace CyclingChallenge.Services.Processors
{
    public class GetTeamDetailsForFacebookIdProcessor : ProcessorBase<TeamDetails>
    {
        #region Private fields

        private User _user;

        #endregion


        #region Constructor

        public GetTeamDetailsForFacebookIdProcessor(params object[] parameters) : base(parameters) { }

        #endregion
        
        
        #region Overrides

        protected internal override void ValidateParameters()
        {
            if (string.IsNullOrEmpty(Parameters.FirstOrDefault()?.ToString()))
            {
                throw new Exception(ErrorCodes.ParameterMissing);
            }

            var facebookId = GetTypeValidatedParameter<string>(Parameters.First());
            var isBigInt = long.TryParse(facebookId, out var fbId);
            if (!isBigInt)
            {
                throw new Exception(ErrorCodes.FacebookIdNotANumber);
            }

            _user = UserQueries.GetUserByFacebookId(Database, fbId);
            if (_user == null)
            {
                throw new Exception(ErrorCodes.FacebookIdNotFound);
            }

            if (_user.UserTeam == null)
            {
                throw new Exception(ErrorCodes.NoTeamForUser);
            }
        }

        protected internal override TeamDetails ProcessInternal()
        {
            var press = CommunicationQueries.GetPressMessagesForUser(Database, _user.Id, 0).ToList();
            return new TeamDetails
            {
                Success = true,
                Id = _user.UserTeam.Id,
                ShortName = _user.UserTeam.ShortName,
                LongName = _user.UserTeam.Name,
                Money = _user.UserTeam.Money,
                User = new Contracts.DataContracts.User.User
                {
                    Id = _user.Id,
                    Name = $"{_user.FirstName} {_user.LastName}",
                    Alias = _user.Username,
                    Email = _user.Email,
                    Level = _user.UserLevel,
                    Birthday = _user.Birthday,
                    Language = _user.Language,
                    Currency = _user.Currency,
                    Tokens = _user.Tokens,
                    Country = CreateCountry(_user.UserCountry)
                },
                Division = CreateDivision(_user.UserTeam),
                Country = CreateCountry(_user.UserTeam.TeamCountry),
                PressMessages = press.Select(CreatePressMessage).ToList(),
                IsUser = _user.UserTeam.IsActive,
                IsOnline = _user.UserTeam.IsActive && _user.Online
            };
        }

        private static Contracts.DataContracts.Communication.PressMessage CreatePressMessage(PressMessage pm)
        {
            return new Contracts.DataContracts.Communication.PressMessage
            {
                Id = pm.Id,
                Title = pm.Title,
                Text = pm.Body,
                Date = pm.Date
            };
        }

        private static Contracts.DataContracts.Team.Division CreateDivision(Team team)
        {
            return new Contracts.DataContracts.Team.Division
            {
                Id = team.Division,
                Name = team.TeamDivision.Name,
                Rank = team.Rank
            };
        }

        #endregion
    }
}