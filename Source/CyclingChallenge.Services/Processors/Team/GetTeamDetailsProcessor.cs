﻿using System.Linq;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using System.Collections.Generic;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Communication;
using Division = CyclingChallenge.Services.Models.Team.Division;

namespace CyclingChallenge.Services.Processors
{
    public class GetTeamDetailsProcessor : ProcessorBase<TeamDetails>
    {
        #region Private fields

        private User _user;
        private Team _team;

        #endregion


        #region Constructor

        public GetTeamDetailsProcessor(params object[] parameters) : base(parameters) { }

        #endregion
        
        
        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            if (Parameters.Count < 2)
            {
                return;
            }

            var idUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _user = GetValidatedUser(idUser);
        }

        protected internal override TeamDetails ProcessInternal()
        {
            var user = UserQueries.GetByTeam(Database, _team.Id);
            return new TeamDetails
            {
                Success = true,
                Id = _team.Id,
                ShortName = _team.ShortName,
                LongName = _team.Name,
                Money = _team.IsActive && user.Id == _user?.Id ? _team.Money : (long?)null,
                User = CreateUserDetails(user),
                Division = CreateDivision(_team.TeamDivision, _team.Rank),
                Country = CreateCountry(_team.TeamCountry),
                PressMessages = user != null 
                    ? CommunicationQueries.GetPressMessagesForUser(Database, user.Id, 0).ToList().Select(CreatePressMessage).ToList()
                    : new List<Contracts.DataContracts.Communication.PressMessage>(),
                IsUser = _team.IsActive,
                IsOnline = _team.IsActive && (user?.Online ?? false)
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.User.User CreateUserDetails(User user)
        {
            return user != null
                ? new Contracts.DataContracts.User.User
                {
                    Id = user.Id,
                    Name = $"{user.FirstName} {user.LastName}".Trim(),
                    Alias = user.Username,
                    Email = user.Id == _user?.Id ? user.Email : null,
                    Level = user.UserLevel,
                    Birthday = user.Birthday,
                    Language = user.Language,
                    Currency = user.Currency,
                    Tokens = user.Id == _user?.Id ? user.Tokens : (int?)null,
                    Country = CreateCountry(user.UserCountry)
                }
                : null;
        }

        private static Contracts.DataContracts.Team.Division CreateDivision(Division division, byte rank)
        {
            return new Contracts.DataContracts.Team.Division
            {
                Id = division.Id,
                Name = division.Name,
                Rank = rank
            };
        }

        private static Contracts.DataContracts.Communication.PressMessage CreatePressMessage(PressMessage pm)
        {
            return new Contracts.DataContracts.Communication.PressMessage
            {
                Id = pm.Id,
                Title = pm.Title,
                Text = pm.Body,
                Date = pm.Date
            };
        }

        #endregion
    }
}