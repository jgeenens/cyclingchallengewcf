﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts;
using System.Collections.Generic;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Contracts.DataContracts.Race;
using RaceHistory = CyclingChallenge.Services.Models.Race.RaceHistory;
using Stage = CyclingChallenge.Services.Models.Race.Stage;
using TeamHistory = CyclingChallenge.Services.Models.Team.TeamHistory;

namespace CyclingChallenge.Services.Processors
{
    public class GetTeamHistoryProcessor : ProcessorBase<Contracts.DataContracts.TeamHistory>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetTeamHistoryProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override Contracts.DataContracts.TeamHistory ProcessInternal()
        {
            return new Contracts.DataContracts.TeamHistory
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                Events = GetTeamEvents()
            };
        }

        #endregion


        #region Private methods

        private List<TeamEvent> GetTeamEvents()
        {
            var raceEvents = RaceQueries.GetRaceHistoryForTeam(Database, _team.Id).Where(rh => rh.Position == 1).ToList().Select(CreateEvent).ToList();
            var teamEvents = TeamQueries.GetTeamHistoryForTeam(Database, _team.Id).ToList().Select(CreateEvent);
            raceEvents.AddRange(teamEvents);
            return raceEvents;
        }

        private static TeamEvent CreateEvent(RaceHistory rh)
        {
            return new TeamEvent
            {
                Id = rh.Id,
                When = rh.When,
                Season = rh.Season,
                Item = rh.Stage.HasValue ? Items.HistoryFirstInStage : Items.HistoryFirstInRace,
                Rider = CreateRider(rh.RaceHistoryPlayer),
                Team = new Contracts.DataContracts.Team.TeamBase
                {
                    Id = rh.Team,
                    LongName = rh.TeamName
                },
                Race = CreateRace(rh.RaceHistoryRace),
                Stage = CreateStage(rh.RaceHistoryStage)
            };
        }

        private static Contracts.DataContracts.Race.Stage CreateStage(Stage stage)
        {
            return stage != null
                ? new Contracts.DataContracts.Race.Stage
                {
                    Id = stage.Id,
                    Name = stage.StageTrack.Name,
                    IsTimeTrial = stage.StageTrack.Profile == Profile.TimeTrial,
                    IsTeamTimeTrial = stage.StageTrack.Profile == Profile.TeamTimeTrial
                }
                : null;
        }

        private static RaceBase CreateRace(Models.Race.Race race)
        {
            return new RaceBase
            {
                Id = race.Id,
                Name = race.RaceRaceDescription.Name,
                IsTour = race.RaceRaceDescription.IsTour
            };
        }

        private static TeamEvent CreateEvent(TeamHistory th)
        {
            return new TeamEvent
            {
                Id = th.Id,
                When = th.Date,
                Item = th.Event == Events.Team.NewTeam ? Items.HistoryNewTeam : Items.HistoryNameChange,
                Name = th.Data,
                Season = 1
            };
        }

        #endregion
    }
}