﻿using System;
using System.Linq;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Processors
{
    public class SetTeamDetailsProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private User _user;
        private Team _team;
        private Logo _logo;
        private string _shortName;
        private string _longName;
        private bool _boughtLogo = false;

        #endregion


        #region Constructor

        public SetTeamDetailsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(4);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            _shortName = GetTypeValidatedParameter<string>(Parameters.Skip(1).First());
            if (string.IsNullOrWhiteSpace(_shortName))
            {
                throw new Exception(ErrorCodes.TeamShortNameCannotBeEmpty);
            }

            if (_shortName.Trim().Length < 4)
            {
                throw new Exception(ErrorCodes.TeamShortNameTooShort);
            }

            var team = TeamQueries.GetTeamByShortName(Database, _shortName);
            if (team != null && team.Id != _team.Id)
            {
                throw new Exception(ErrorCodes.TeamShortNameMustBeUnique);
            }

            _longName = GetTypeValidatedParameter<string>(Parameters.Skip(2).First());
            if (string.IsNullOrWhiteSpace(_longName))
            {
                throw new Exception(ErrorCodes.TeamLongNameCannotBeEmpty);
            }

            if (_longName.Trim().Length < 6)
            {
                throw new Exception(ErrorCodes.TeamLongNameTooShort);
            }

            team = TeamQueries.GetTeamByName(Database, _longName);
            if (team != null && team.Id != _team.Id)
            {
                throw new Exception(ErrorCodes.TeamLongNameMustBeUnique);
            }

            if (_shortName != _team.ShortName || _longName != _team.Name)
            {
                var lastNameChange = TeamQueries.GetLastNameChange(Database, idTeam);
                if (lastNameChange >= GameQueries.GetStartSeason(Database))
                {
                    throw new Exception(ErrorCodes.TeamNameAlreadyChanged);
                }
            }

            var idLogo = GetTypeValidatedParameter<byte>(Parameters.Skip(3).First());
            _logo = TeamQueries.GetLogoById(Database, idLogo);
            if (_logo == null)
            {
                throw new Exception(ErrorCodes.IdLogoNotFound);
            }

            _user = UserQueries.GetByTeam(Database, _team.Id);
            if (_logo.Id == _team.Logo || GameQueries.UserAlreadyHasLogo(Database, _user.Id, _logo.Id))
            {
                return;
            }

            if (_logo.Tokens > _user.Tokens)
            {
                throw new Exception(ErrorCodes.NotEnoughTokens);
            }

            _boughtLogo = true;
        }

        protected internal override ResultBase ProcessInternal()
        {
            ProcessNameChange();
            ProcessLogoSale();

            _team.ShortName = _shortName;
            _team.Name = _longName;
            _team.Logo = _logo.Id;

            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion


        #region Private Methods

        private void ProcessNameChange()
        {
            if (_team.ShortName != _shortName)
            {
                var riders = PlayerQueries.GetPlayersForTeam(Database, _team.Id).ToList();
                riders.ForEach(r => r.Team = _shortName);
            }

            if (_team.Name == _longName)
            {
                return;
            }

            TeamCommands.CreateTeamHistory(Database, _team.Id, Events.Team.NameChange, _longName);
        }

        private void ProcessLogoSale()
        {
            if (!_boughtLogo)
            {
                return;
            }

            GameCommands.CreateSale(Database, _user.Id, _logo);
            _user.Tokens -= _logo.Tokens;
        }

        #endregion
    }
}