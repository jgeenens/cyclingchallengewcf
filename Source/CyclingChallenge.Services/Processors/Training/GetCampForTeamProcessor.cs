﻿using System;
using System.Linq;
using CyclingChallenge.Services.Queries;
using System.Collections.Generic;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Training;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Rider;
using Location = CyclingChallenge.Services.Models.Location.Location;

namespace CyclingChallenge.Services.Processors
{
    public class GetCampForTeamProcessor : ProcessorBase<TrainingCamp>
    {
        #region Private fields

        private const byte _daysBetweenCamps = 7;
        private Team _team;

        #endregion


        #region Constructor

        public GetCampForTeamProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override TrainingCamp ProcessInternal()
        {
            var camp = BookingQueries.GetLastBookingByTeam(Database, _team.Id);
            return new TrainingCamp
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                Camps = CreateCamps(camp),
                Riders = camp.Departure.AddDays(camp.Duration + 1) > DateTime.Today ? CreateRiders(camp) : null
            };
        }

        private List<Camp> CreateCamps(Booking camp)
        {
            return camp.Departure.AddDays(camp.Duration + 1) > DateTime.Today
                ? new List<Camp> { CreateCamp(camp) }
                : camp.Departure.AddDays(camp.Duration + 1 + _daysBetweenCamps) < DateTime.Today
                    ? LocationQueries.GetAll(Database).ToList().Select(CreateCamp).ToList()
                    : null;
        }

        #endregion


        #region Private methods

        private static Camp CreateCamp(Location location)
        {
            return CreateCampFromLocationAndBooking(location);
        }

        private static Camp CreateCamp(Booking camp)
        {
            return CreateCampFromLocationAndBooking(camp.BookingLocation, camp);
        }

        private static Camp CreateCampFromLocationAndBooking(Location location, Booking camp = null)
        {
            return new Camp
            {
                Id = camp?.Id ?? location.Id,
                Name = location.Name,
                City = CreateCity(location.LocationCity),
                Picture = location.Picture,
                PrimarySkill = location.PrimarySkill,
                SecondarySkill = location.SecondarySkill,
                Start = camp?.Departure,
                End = camp?.Departure.AddDays(camp.Duration)
            };
        }

        private static Contracts.DataContracts.Location.City CreateCity(City city)
        {
            return new Contracts.DataContracts.Location.City
            {
                Id = city.Id,
                Name = LocationQueries.GetCityText(city),
                Region = CreateRegion(city.CityRegion)
            };
        }

        private static Contracts.DataContracts.Location.Region CreateRegion(Region region)
        {
            return new Contracts.DataContracts.Location.Region
            {
                Id = region.Id,
                Name = region.Name,
                Country = CreateCountry(region.RegionCountry)
            };
        }

        private List<RiderProgress> CreateRiders(Booking camp)
        {
            var riders = camp?.Players.Split(',').Select(long.Parse);
            return riders?.Select(r => PlayerQueries.GetPlayerById(Database, r)).Select(p => new RiderProgress
            {
                Id = p.Id,
                Name = $"{p.FirstName} {p.LastName}".Trim(),
                Improvements = GetPlayerImprovementsDuringCamp(p, camp.Departure, camp.Departure.AddDays(camp.Duration))
            }).ToList();
        }

        private List<Improvement> GetPlayerImprovementsDuringCamp(Player p, DateTime departure, DateTime returnDate)
        {
            var playerImprovements = PlayerQueries.GetPlayerProgressForPlayer(Database, p.Id);
            return playerImprovements.Where(pr => pr.Change >= departure && pr.Change <= returnDate).Select(pr => new Improvement
            {
                Id = pr.Id,
                Skill = pr.Skill,
                Level = pr.Level,
                Change = pr.Change
            }).ToList();
        }

        #endregion
    }
}