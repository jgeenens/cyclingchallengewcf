﻿using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.Training;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Models.Staff;

namespace CyclingChallenge.Services.Processors
{
    public class GetTrainingProcessor : ProcessorBase<Training>
    {
        #region Private fields

        private Team _team;

        #endregion


        #region Constructor

        public GetTrainingProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);
        }

        protected internal override Training ProcessInternal()
        {
            return new Training
            {
                Success = true,
                Id = _team.Id,
                Name = _team.Name,
                CurrentTraining = _team.Training,
                Trainer = CreateTrainer(StaffQueries.GetCoachByTeam(Database, _team.Id))
            };
        }

        #endregion


        #region Private methods

        private static Trainer CreateTrainer(Coach coach)
        {
            return coach != null
                ? new Trainer
                {
                    Id = coach.Id,
                    Name = $"{coach.FirstName} {coach.LastName}",
                    Age = coach.Age,
                    Nationality = coach.Nationality,
                    Wage = coach.Wage,
                    Training = coach.Training,
                    Skill = coach.Skill,
                    Picture = GetPictureFromCoach(coach)
                } 
                : null;
        }

        private static Picture GetPictureFromCoach(Coach coach)
        {
            return new Picture
            {
                Face = coach.Face,
                Hair = coach.Hair,
                Eyes = coach.Eyes,
                Nose = coach.Nose,
                Lips = coach.Lips
            };
        }

        #endregion
    }
}