﻿using System;
using System.Linq;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Processors
{
    public class SetTrainingProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private Team _team;
        private byte _training;

        #endregion


        #region Constructor

        public SetTrainingProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var idTeam = GetTypeValidatedParameter<long>(Parameters.First());
            _team = GetValidatedTeam(idTeam);

            _training = GetTypeValidatedParameter<byte>(Parameters.Skip(1).First());
            if (!new[] { Rider.Skill.Climbing, Rider.Skill.Descending, Rider.Skill.Sprint, Rider.Skill.Stamina, Rider.Skill.Technique, Rider.Skill.TimeTrial}.Contains(_training))
            {
                throw new Exception(ErrorCodes.InvalidTrainingValue);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            _team.Training = _training;
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}