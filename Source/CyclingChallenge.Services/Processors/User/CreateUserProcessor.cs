﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CyclingChallenge.Services.Commands;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Location;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Processors
{
    public class CreateUserProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private long _facebookId;
        private byte _language;
        private Country _country;
        private DateTime? _birthDate;
        private CreateUserRequest _createUserRequest;

        private Dictionary<long, long> _settings = new Dictionary<long, long>
        {
            { Settings.Tokens, 0 },
            { Settings.Riders, 0 },
            { Settings.YouthRiders, 0 },
            { Settings.Credit, 0 },
            { Settings.Supporters, 0 },
            { Settings.MinValues, 0 },
            { Settings.MaxValues, 0 },
            { Settings.MinYouthValues, 0 },
            { Settings.MaxYouthValues, 0 }
        };
        private long _maxFirstNameValue;
        private long _maxLastNameValue;

        #endregion


        #region Constructor

        public CreateUserProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(2);

            var facebookId = GetTypeValidatedParameter<string>(Parameters.First());
            var isBigInt = long.TryParse(facebookId, out _facebookId);
            if (!isBigInt)
            {
                throw new Exception(ErrorCodes.FacebookIdNotANumber);
            }

            if (UserQueries.GetUserByFacebookId(Database, _facebookId) != null)
            {
                throw new Exception(ErrorCodes.AlreadyRegistered);
            }

            _createUserRequest = GetTypeValidatedParameter<CreateUserRequest>(Parameters.Skip(1).First());
            _language = DictionaryQueries.GetLanguageByAbbreviation(Database, _createUserRequest.Language)?.Id ?? Communication.Language.English;

            if (string.IsNullOrEmpty(_createUserRequest.Birthday) && (string.IsNullOrEmpty(_createUserRequest.FirstName) || string.IsNullOrEmpty(_createUserRequest.LastName)) ||
                string.IsNullOrEmpty($"{_createUserRequest.FirstName}{_createUserRequest.LastName}"))
            {
                throw new Exception(ErrorCodes.AtLeastTwoNeeded);
            }

            if (!string.IsNullOrEmpty(_createUserRequest.Birthday))
            {
                if (!DateTime.TryParse(_createUserRequest.Birthday, CultureInfo.InvariantCulture, DateTimeStyles.None, out var birthDate))
                {
                    throw new Exception(ErrorCodes.NotValidDateTime);
                }
                _birthDate = birthDate;
            }

            if (!string.IsNullOrEmpty(_createUserRequest.Country))
            {
                _country = LocationQueries.GetCountryByName(Database, _createUserRequest.Country);

                if (_country == null)
                {
                    throw new Exception(ErrorCodes.CountryNameNotFound);
                }
            }

            var originalCount = _settings.Count;
            _settings = GameQueries.GetSettingsById(Database, _settings.Select(s => s.Key)).ToDictionary(s => s.Id, s => long.Parse(s.Value));
            if (_settings.Count != originalCount)
            {
                throw new Exception(ErrorCodes.NotAllSettingsFound);
            }
        }

        protected internal override ResultBase ProcessInternal()
        {
            var user = CreateUser();
            var team = GetTeamForUser(user);

            _maxFirstNameValue = PlayerQueries.GetNameMaxValueByCountry(Database, team.Country, true) ?? 0;
            _maxLastNameValue = PlayerQueries.GetNameMaxValueByCountry(Database, team.Country) ?? 0;

            CreateRidersForTeam(team);
            CreateStaffForTeam(team);
            Database.SaveChanges();

            CreateEconomyForTeam(team);
            UpdateRegistrations();
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion


        #region Private Methods

        #region Create User and Team

        private User CreateUser()
        {
            var user = UserCommands.CreateUser(Database, _settings, _facebookId, _country, _language, _createUserRequest.FirstName, _createUserRequest.LastName, _createUserRequest.Email, _birthDate);
            user.TeamName = TeamCommands.GetUniqueTeamName(Database, user.Country);
            user.Currency = EconomyQueries.GetCurrencyByCountry(Database, user.Country).Id;
            return user;
        }

        private Team GetTeamForUser(User user)
        {
            var divisions = TeamQueries.GetDivisionsByCountry(Database, user.Country).OrderBy(d => d.Level).ToList();
            foreach (var division in divisions)
            {
                var team = TeamQueries.GetTeamsByDivision(Database, division.Id).FirstOrDefault(t => t.IsBot);
                if (team == null)
                {
                    continue;
                }

                return AssignTeamToUser(team, user);
            }

            return null;
        }

        private Team AssignTeamToUser(Team team, User user)
        {
            RemoveOldData(team);
            TeamCommands.InitializeTeam(Database, team, user.TeamName, _settings[Settings.Credit], (short)_settings[Settings.Supporters]);
            TeamCommands.CreateTeamHistory(Database, team.Id, Events.Team.NewTeam, team.Name);
            user.Team = team.Id;
            return team;
        }

        private void RemoveOldData(Team team)
        {
            var shop = TeamQueries.GetShop(Database, team.Id);
            if (shop != null)
            {
                Database.Clubshops.Remove(shop);
            }

            var coach = StaffQueries.GetCoachByTeam(Database, team.Id);
            if (coach != null)
            {
                Database.Coaches.Remove(coach);
            }

            EconomyCommands.InitializeSponsor(SponsorQueries.GetSponsorByTeam(Database, team.Id));
            PlayerQueries.GetPlayersForTeam(Database, team.Id).ToList().ForEach(r => r.PlayerValuePlayer.Retired = true);

            Database.ScoutedPlayers.RemoveRange(PlayerQueries.GetScoutedPlayers(Database, team.Id));
            Database.Economies.RemoveRange(EconomyQueries.GetEconomiesByTeam(Database, team.Id));
            Database.Transactions.RemoveRange(EconomyQueries.GetTransactionsByTeam(Database, team.Id));
            Database.Bookings.RemoveRange(BookingQueries.GetBookingsByTeam(Database, team.Id));
            Database.Calendars.RemoveRange(CalendarQueries.GetCalendarByTeam(Database, team.Id));
            Database.Youths.RemoveRange(PlayerQueries.GetYouthHistoriesByTeam(Database, team.Id));
            var youthRiders = PlayerQueries.GetPlayersForTeam(Database, team.Id, true);
            Database.PlayerValues.RemoveRange(youthRiders);
            Database.Players.RemoveRange(youthRiders.Select(yr => yr.PlayerValuePlayer));
        }

        #endregion

        #region Create Riders For Team

        private void CreateRidersForTeam(Team team)
        {
            for (var i = 0; i < _settings[Settings.Riders]; i++)
            {
                CreateRiderForTeam(team, false, _maxFirstNameValue, _maxLastNameValue);
            }
            for (var i = 0; i < _settings[Settings.YouthRiders]; i++)
            {
                CreateRiderForTeam(team, true, _maxFirstNameValue, _maxLastNameValue);
            }
        }

        private void CreateRiderForTeam(Team team, bool isYouth, long maxFirstNameValue, long maxLastNameValue)
        {
            var minValues = isYouth ? _settings[Settings.MinYouthValues] : _settings[Settings.MinValues];
            var maxValues = isYouth ? _settings[Settings.MaxYouthValues] : _settings[Settings.MaxValues];
            var player = PlayerCommands.CreatePlayer(Database, team.Id, team.Country, isYouth, maxFirstNameValue, maxLastNameValue);
            var playerValues = PlayerCommands.CreatePlayerValues(Database, player, minValues, maxValues);
            PlayerCommands.SetWage(playerValues);
        }

        #endregion

        #region Create Staff For Team

        private void CreateStaffForTeam(Team team)
        {
            CreateManagers(team);
            CreateScouts(team);
        }

        private void CreateManagers(Team team)
        {
            var managers = StaffQueries.GetManagers(Database, team.Id).ToList();
            if (!managers.Any())
            {
                managers.Add(StaffCommands.CreateManager(Database, team.Id, Managers.Type.Race));
                managers.Add(StaffCommands.CreateManager(Database, team.Id, Managers.Type.Shop));
                managers.Add(StaffCommands.CreateManager(Database, team.Id, Managers.Type.Staff));
            }
            managers.ForEach(StaffCommands.InitializeManager);
        }

        private void CreateScouts(Team team)
        {
            var scouts = StaffQueries.GetScouts(Database, team.Id).ToList();
            if (!scouts.Any())
            {
                scouts.Add(StaffCommands.CreateScout(Database, team.Id, team.Country, Scouts.Status.Active, _maxFirstNameValue, _maxLastNameValue));
                scouts.Add(StaffCommands.CreateScout(Database, team.Id, team.Country, Scouts.Status.Locked, _maxFirstNameValue, _maxLastNameValue));
                scouts.Add(StaffCommands.CreateScout(Database, team.Id, team.Country, Scouts.Status.Locked, _maxFirstNameValue, _maxLastNameValue));
            }
            scouts.ForEach(StaffCommands.InitializeScout);
        }

        #endregion

        #region Create Economy For Team

        private void CreateEconomyForTeam(Team team)
        {
            var wages = PlayerQueries.GetPlayersForTeam(Database, team.Id).Sum(r => r.Wage);
            wages += StaffQueries.GetScouts(Database, team.Id).Sum(s => s.Wage);
            EconomyCommands.CreateEconomy(Database, team.Id, wages);
        }

        #endregion

        #region Update Registrations

        private void UpdateRegistrations()
        {
            var registrations = GameQueries.GetRegistrationsForToday(Database, _country.Id);
            if (registrations == null)
            {
                GameCommands.CreateRegistration(Database, _country.Id);
            }
            else
            {
                registrations.Quantity += 1;
            }
        }

        #endregion

        #endregion
    }
}