﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Queries;
using CyclingChallenge.Services.Contracts.DataContracts.User;
using User = CyclingChallenge.Services.Models.Team.User;

namespace CyclingChallenge.Services.Processors
{
    public class GetAccountDetailsProcessor : ProcessorBase<AccountDetails>
    {
        #region Private fields

        private User _requestingUser, _user;

        #endregion


        #region Constructor

        public GetAccountDetailsProcessor(params object[] parameters) : base(parameters) { }

        #endregion
        
        
        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters();

            var idUser = GetTypeValidatedParameter<long>(Parameters.First());
            _user = GetValidatedUser(idUser);

            if (Parameters.Count < 2)
            {
                return;
            }

            var idRequestingUser = GetTypeValidatedParameter<long>(Parameters.Skip(1).First());
            _requestingUser = GetValidatedUser(idRequestingUser);
        }

        protected internal override AccountDetails ProcessInternal()
        {
            var raceManager = _user.Team.HasValue ? StaffQueries.GetRaceManager(Database, _user.Team.Value) : null;
            var hasRaceManager = raceManager?.Status == Managers.Status.Active || raceManager?.Status == Managers.Status.Learning;
            var isNameChangeAllowed = _user.Id == _requestingUser?.Id && _user.Team.HasValue
                && TeamQueries.GetLastNameChange(Database, _user.Team.Value) <= DateTime.Now.AddDays(-84);
            return new AccountDetails
            {
                Success = true,
                User = CreateUserDetails(_user),
                HasRaceManager = hasRaceManager,
                IsOnHoliday = hasRaceManager && _user.Vacation,
                StartOfHoliday = hasRaceManager && _user.Vacation ? _user.VacationSince : null,
                IsNameChangeAllowed = isNameChangeAllowed
            };
        }

        #endregion


        #region Private methods

        private Contracts.DataContracts.User.User CreateUserDetails(User user)
        {
            return user != null
                ? new Contracts.DataContracts.User.User
                {
                    Id = user.Id,
                    Name = $"{user.FirstName} {user.LastName}".Trim(),
                    Alias = user.Username,
                    Email = user.Id == _requestingUser?.Id ? user.Email : null,
                    Level = user.UserLevel,
                    Birthday = user.Birthday,
                    Language = user.Language,
                    Currency = user.Currency,
                    Tokens = user.Id == _requestingUser?.Id ? user.Tokens : (int?)null,
                    Country = CreateCountry(user.UserCountry)
                }
                : null;
        }

        #endregion
    }
}