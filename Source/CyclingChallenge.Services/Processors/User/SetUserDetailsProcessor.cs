﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using CyclingChallenge.Services.Contracts.DataContracts;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models.Team;
using CyclingChallenge.Services.Queries;

namespace CyclingChallenge.Services.Processors
{
    public class SetUserDetailsProcessor : ProcessorBase<ResultBase>
    {
        #region Private fields

        private User _user;
        private Language _language;
        private string _userName;
        private string _email;
        private bool _hideEmail;

        #endregion


        #region Constructor

        public SetUserDetailsProcessor(params object[] parameters) : base(parameters) { }

        #endregion


        #region Overrides

        protected internal override void ValidateParameters()
        {
            CheckRequiredParameters(5);

            var idUser = GetTypeValidatedParameter<long>(Parameters.First());
            _user = GetValidatedUser(idUser);

            _userName = GetTypeValidatedParameter<string>(Parameters.Skip(1).First());
            if (string.IsNullOrWhiteSpace(_userName))
            {
                throw new Exception(ErrorCodes.UserNameCannotBeEmpty);
            }

            if (_userName.Trim().Length < 6)
            {
                throw new Exception(ErrorCodes.UserNameTooShort);
            }

            var user = UserQueries.GetByUsername(Database, _userName);
            if (user != null && user.Id != _user.Id)
            {
                throw new Exception(ErrorCodes.UserNameMustBeUnique);
            }

            _email = GetTypeValidatedParameter<string>(Parameters.Skip(2).First()).Trim();
            if (!Regex.IsMatch(_email, @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$", RegexOptions.IgnoreCase))
            {
                throw new Exception(ErrorCodes.InvalidFormatEmail);
            }

            user = !string.IsNullOrEmpty(_email) ? UserQueries.GetByEmail(Database, _email) : null;
            if (user != null && user.Id != _user.Id)
            {
                throw new Exception(ErrorCodes.EmailMustBeUnique);
            }

            var idLanguage = GetTypeValidatedParameter<byte>(Parameters.Skip(3).First());
            _language = DictionaryQueries.GetLanguageById(Database, idLanguage);
            if (_language == null)
            {
                throw new Exception(ErrorCodes.IdLanguageNotFound);
            }

            _hideEmail = GetTypeValidatedParameter<bool>(Parameters.Skip(4).First());
        }

        protected internal override ResultBase ProcessInternal()
        {
            _user.Username = _userName;
            _user.Email = _email;
            _user.Language = _language.Id;
            _user.EmailHidden = _hideEmail;
            Database.SaveChanges();
            return new ResultBase { Success = true };
        }

        #endregion
    }
}