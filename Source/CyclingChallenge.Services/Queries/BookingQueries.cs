﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Training;
using System.Linq;

namespace CyclingChallenge.Services.Queries
{
    public static class BookingQueries
    {
        public static Booking GetLastBookingByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return GetBookingsByTeam(database, idTeam).OrderByDescending(b => b.Departure).FirstOrDefault();
        }

        public static IQueryable<Booking> GetBookingsByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Bookings.Where(b => b.Team == idTeam);
        }
    }
}