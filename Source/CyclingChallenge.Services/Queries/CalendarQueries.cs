﻿using System;
using System.Linq;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Rider;

namespace CyclingChallenge.Services.Queries
{
    public static class CalendarQueries
    {
        public static IQueryable<Calendar> GetCalendarByRider(ICyclingChallengeModelContainer database, long idRider)
        {
            return database.Calendars.Where(c => c.Player == idRider && c.EndDate > DateTime.Now).OrderBy(c => c.StartDate);
        }

        public static IQueryable<Calendar> GetCalendarByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Calendars.Where(c => c.Team == idTeam);
        }
    }
}