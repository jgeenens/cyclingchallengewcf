﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Communication;
using System;
using System.Linq;

namespace CyclingChallenge.Services.Queries
{
    public static class CommunicationQueries
    {
        #region Constants

        private const short PageSize = 10;

        #endregion

        public static PrivateMessage GetPrivateMessageById(ICyclingChallengeModelContainer database, long idMessage)
        {
            return database.PrivateMessages.FirstOrDefault(pm => pm.Id == idMessage);
        }

        public static PressMessage GetPressMessageById(ICyclingChallengeModelContainer database, long idMessage)
        {
            return database.PressMessages.FirstOrDefault(pm => pm.Id == idMessage);
        }

        public static IQueryable<PressMessage> GetPressMessagesForUser(ICyclingChallengeModelContainer database, long idUser, int q, bool descending = false)
        {
            var messages = descending
                ? database.PressMessages.Where(pm => pm.User == idUser).OrderByDescending(pm => pm.Date)
                : database.PressMessages.Where(pm => pm.User == idUser).OrderBy(pm => pm.Date);

            return messages.Skip(Math.Max(0, Math.Min(q, messages.Count() - PageSize))).Take(PageSize);
        }

        public static IQueryable<PrivateMessage> GetMessagesForUser(ICyclingChallengeModelContainer database, long idUser, int q, bool isRecipient = true, bool descending = true)
        {
            var messages = isRecipient
                ? database.PrivateMessages.Where(pm => pm.Recipient == idUser && !pm.DeletedByRecipient && pm.Type == Enums.Message.Type.Private)
                : database.PrivateMessages.Where(pm => pm.Sender == idUser && !pm.DeletedBySender && pm.Type == Enums.Message.Type.Private);

            messages = descending
                ? messages.OrderByDescending(pm => pm.Date)
                : messages.OrderBy(pm => pm.Date);

            return messages.Skip(Math.Max(0, Math.Min(q, messages.Count() - PageSize))).Take(PageSize);
        }

        public static IQueryable<PrivateMessage> GetOfficialMessagesForUser(ICyclingChallengeModelContainer database, long idUser, int q, bool descending = true)
        {
            var messages = database.PrivateMessages.Where(pm => pm.Recipient == idUser && !pm.DeletedByRecipient && pm.Type == Enums.Message.Type.Official);

            messages = descending
                ? messages.OrderByDescending(pm => pm.Date)
                : messages.OrderBy(pm => pm.Date);

            return messages.Skip(Math.Max(0, Math.Min(q, messages.Count() - PageSize))).Take(PageSize);
        }

        public static Board GetBoardById(ICyclingChallengeModelContainer database, short idBoard)
        {
            return database.Boards.FirstOrDefault(b => b.Id == idBoard);
        }

        public static Thread GetThreadById(ICyclingChallengeModelContainer database, long idThread)
        {
            return database.Threads.FirstOrDefault(t => t.Id == idThread);
        }

        public static Message GetMessageById(ICyclingChallengeModelContainer database, long idMessage)
        {
            return database.Messages.FirstOrDefault(m => m.Id == idMessage);
        }

        public static IQueryable<Board> GetByType(ICyclingChallengeModelContainer database, byte type)
        {
            return database.Boards.Where(b => b.Type == type);
        }

        public static IQueryable<Thread> GetThreadsByBoard(ICyclingChallengeModelContainer database, short idBoard)
        {
            return database.Threads.Where(t => t.Board == idBoard);
        }

        public static IQueryable<Message> GetMessagesByThread(ICyclingChallengeModelContainer database, long idThread, bool includeObsolete = false)
        {
            return database.Messages.Where(m => m.Thread == idThread && (includeObsolete || !m.Obsolete));
        }

        public static IQueryable<Board> GetAll(ICyclingChallengeModelContainer database)
        {
            return database.Boards;
        }

        public static DateTime GetThreadViewedByUser(ICyclingChallengeModelContainer database, long idThread, long idUser)
        {
            return database.ThreadsViewed.FirstOrDefault(tv => tv.Thread == idThread && tv.User == idUser)?.LastView ?? DateTime.MinValue;
        }

        public static byte? GetMaxMessageInThread(ICyclingChallengeModelContainer database, long idThread)
        {
            return database.Messages.Where(m => m.Thread == idThread).Max(m => (byte?)m.MessageInThread);
        }
    }
}