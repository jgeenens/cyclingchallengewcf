﻿using System.Linq;
using CyclingChallenge.Services.Models.Communication;
using CyclingChallenge.Services.Models;

namespace CyclingChallenge.Services.Queries
{
    public static class DictionaryQueries
    {
        public static Language GetLanguageById(ICyclingChallengeModelContainer database, short idLanguage)
        {
            return database.Languages.FirstOrDefault(l => l.Id == idLanguage);
        }

        public static Language GetLanguageByAbbreviation(ICyclingChallengeModelContainer database, string abbreviation)
        {
            return !string.IsNullOrEmpty(abbreviation) 
                ? database.Languages.ToList().FirstOrDefault(l => l.Abbreviations.Split(',').Contains(abbreviation))
                : null;
        }

        public static IQueryable<Dictionary> GetDictionaryItemsForLanguage(ICyclingChallengeModelContainer database, short idLanguage)
        {
            return database.Dictionaries.Where(d => d.Language == idLanguage);
        }

        public static Dictionary GetDictionaryItemForLanguage(ICyclingChallengeModelContainer database, short item, byte idLanguage)
        {
            return database.Dictionaries.SingleOrDefault(d => d.Item == item && d.Language == idLanguage);
        }
    }
}