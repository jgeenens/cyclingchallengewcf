﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using Economy = CyclingChallenge.Services.Models.Economy.Economy;

namespace CyclingChallenge.Services.Queries
{
    public static class EconomyQueries
    {
        public static Economy GetByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Economies.FirstOrDefault(e => e.Team == idTeam && e.Active == 1);
        }

        public static IQueryable<Economy> GetEconomiesByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Economies.Where(e => e.Team == idTeam);
        }

        public static IQueryable<Transaction> GetTransactionsByTeam(ICyclingChallengeModelContainer database, long idTeam, bool includeChecked = true)
        {
            return database.Transactions.Where(t => t.Team == idTeam && (includeChecked || !t.Checked));
        }

        public static IQueryable<Transaction> GetTransactionsByTeamAndTimeFrame(ICyclingChallengeModelContainer database, long idTeam, DateTime from, DateTime? to = null)
        {
            return database.Transactions.Where(t => t.Team == idTeam && t.DateTransaction >= from && (!to.HasValue || t.DateTransaction < to));
        }

        public static Currency GetCurrencyById(ICyclingChallengeModelContainer database, short idCurrency)
        {
            return database.Currencies.FirstOrDefault(c => c.Id == idCurrency);
        }

        public static Currency GetCurrencyByCountry(ICyclingChallengeModelContainer database, short idCountry)
        {
            return database.Currencies.FirstOrDefault(c => c.Country == idCountry);
        }

        public static long? GetBalanceCredit(ICyclingChallengeModelContainer database, long idTeam)
        {
            var team = TeamQueries.GetTeamById(database, idTeam);
            if (team == null)
            {
                return null;
            }

            var setting = GameQueries.GetSettingById(database, Settings.Credit);
            if (setting == null)
            {
                return null;
            }

            var transactions = GetTransactionsByTeam(database, idTeam, false).ToList();
            var balance = team.Predicted + transactions.Sum(transaction => transaction.Value * (transaction.Outgoing ? -1 : 1));
            return balance + long.Parse(setting.Value);
        }
    }
}