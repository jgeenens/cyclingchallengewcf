﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Game;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using CyclingChallenge.Services.CustomModels.Game;
using CyclingChallenge.Services.Enums;
using Word = CyclingChallenge.Services.Models.Game.Word;

namespace CyclingChallenge.Services.Queries
{
    public static class GameQueries
    {
        public static Addon GetAddonById(ICyclingChallengeModelContainer database, short idAddon)
        {
            return database.Addons.FirstOrDefault(a => a.Id == idAddon);
        }

        public static bool UserAlreadyHasLogo(ICyclingChallengeModelContainer database, long idUser, byte idLogo)
        {
            return database.Sales.Any(s => s.Addon == AddOns.Logo && s.User == idUser && s.Status == idLogo.ToString());
        }

        public static Registration GetRegistrationsForToday(ICyclingChallengeModelContainer database, short idCountry)
        {
            return database.Registrations.FirstOrDefault(r => r.Country == idCountry && DbFunctions.TruncateTime(r.Date) == DateTime.Today.Date);
        }

        public static IQueryable<Commodity> GetAllProducts(ICyclingChallengeModelContainer database)
        {
            return database.Commodities;
        }

        public static Price GetPriceByProductAndCurrency(ICyclingChallengeModelContainer database, short idProduct, short idCurrency)
        {
            return database.Prices.FirstOrDefault(p => p.Commodity == idProduct && p.Currency == idCurrency);
        }

        public static Word GetRandomWordByType(ICyclingChallengeModelContainer database, byte type)
        {
            return database.Words.RandomOrDefault(w => w.Type == type);
        }

        public static Setting GetSettingById(ICyclingChallengeModelContainer database, long idSetting)
        {
            return database.Settings.FirstOrDefault(s => s.Id == idSetting);
        }

        public static IQueryable<Setting> GetSettingsById(ICyclingChallengeModelContainer database, IEnumerable<long> idSettings)
        {
            return database.Settings.Where(s => idSettings.Contains(s.Id));
        }

        public static DateTime GetStartSeason(ICyclingChallengeModelContainer database, byte? season = null)
        {
            var seasonStartValue = GetSettingById(database, Enums.Settings.SeasonStart)?.Value;
            var seasonStart = seasonStartValue != null
                ? DateTime.ParseExact(seasonStartValue, "yyyy/MM/dd", CultureInfo.InvariantCulture)
                : DateTime.MinValue;

            var seasonValue = GetSettingById(database, Enums.Settings.Season)?.Value;
            var currentSeason = seasonValue != null 
                ? byte.Parse(seasonValue) 
                : (byte?)null;

            return seasonStart > DateTime.MinValue && currentSeason.HasValue
                ? seasonStart.AddDays(((season ?? currentSeason.Value) - currentSeason.Value) * 7 * 12)
                : seasonStart;
        }

        public static WeekAndDay GetWeekAndDayInSeason(ICyclingChallengeModelContainer database, byte? season = null)
        {
            var daysInSeason = (DateTime.Now - GetStartSeason(database, season)).TotalDays;
            return new WeekAndDay
            {
                Week = (byte) Math.Floor((daysInSeason - 1) / 7),
                Day = (byte) ((daysInSeason - 1) % 7 + 1)
            };
        }
    }
}