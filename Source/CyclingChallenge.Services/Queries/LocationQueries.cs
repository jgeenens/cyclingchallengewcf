﻿using System;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Location;
using System.Linq;
using CyclingChallenge.Services.Models.Race;

namespace CyclingChallenge.Services.Queries
{
    public static class LocationQueries
    {
        #region Queries

        public static City GetCityById(ICyclingChallengeModelContainer database, long idCity)
        {
            return database.Cities.SingleOrDefault(c => c.Id == idCity);
        }

        public static City GetRandomCityByCountry(ICyclingChallengeModelContainer database, short idCountry)
        {
            return database.Cities.RandomOrDefault(c => c.CityRegion.Country == idCountry);
        }

        public static Climb GetClimbById(ICyclingChallengeModelContainer database, long idClimb)
        {
            return database.Climbs.SingleOrDefault(c => c.Id == idClimb);
        }

        public static Country GetCountryById(ICyclingChallengeModelContainer database, long idCountry)
        {
            return database.Countries.SingleOrDefault(c => c.Id == idCountry);
        }

        public static Country GetCountryByName(ICyclingChallengeModelContainer database, string country)
        {
            return database.Countries.SingleOrDefault(c => c.Name.Equals(country, StringComparison.InvariantCultureIgnoreCase));
        }

        public static Country GetRandomActiveCountry(ICyclingChallengeModelContainer database)
        {
            return database.Countries.RandomOrDefault(c => c.Active && c.Name != "World");
        }

        public static Region GetRegionByCity(ICyclingChallengeModelContainer database, long idCity)
        {
            var idRegion = GetCityById(database, idCity)?.Region;
            return database.Regions.FirstOrDefault(r => r.Id == idRegion);
        }

        public static Location GetLocationById(ICyclingChallengeModelContainer database, short idLocation)
        {
            return database.Locations.SingleOrDefault(l => l.Id == idLocation);
        }

        public static IQueryable<Location> GetAll(ICyclingChallengeModelContainer database)
        {
            return database.Locations;
        }

        #endregion


        #region Text

        public static string GetLocationTextById(ICyclingChallengeModelContainer database, short idLocation)
        {
            var location = GetLocationById(database, idLocation);
            return location != null
                ? $"{GetCountryText(location.LocationCountry.Item)} {GetCityText(location.LocationCity)}"
                : null;
        }

        public static string GetCountryText(short? country)
        {
            return country != null ? $"[[CNTRY({country})]]" : null;
        }

        public static string GetCityText(City city)
        {
            return city != null 
                ? $"{city.Name} {(city.Name != city.Village ? $"({city.Village})" : string.Empty)}".Trim()
                : null;
        }

        #endregion
    }
}