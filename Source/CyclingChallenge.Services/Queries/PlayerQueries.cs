﻿using CyclingChallenge.Services.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Utils;

namespace CyclingChallenge.Services.Queries
{
    public static class PlayerQueries
    {
        #region Public Methods

        public static Player GetPlayerById(ICyclingChallengeModelContainer database, long idRider)
        {
            return database.Players.SingleOrDefault(p => p.Id == idRider);
        }

        public static PlayerValue GetPlayerValuesByPlayer(ICyclingChallengeModelContainer database, long idRider)
        {
            return database.PlayerValues.SingleOrDefault(p => p.Player == idRider);
        }

        public static IQueryable<PlayerValue> GetPlayersForTeam(ICyclingChallengeModelContainer database, long idTeam, bool youth = false)
        {
            return database.PlayerValues.Where(pv => pv.IsYouth == youth && pv.PlayerValuePlayer.Team == idTeam);
        }

        public static IQueryable<PlayerValue> GetPlayersForRace(ICyclingChallengeModelContainer database, long idRace)
        {
            return database.PlayerValues.Where(pv => pv.Race == idRace);
        }

        public static IQueryable<Player> GetPlayersByName(ICyclingChallengeModelContainer database, string firstName, string lastName)
        {
            return !string.IsNullOrEmpty($"{firstName}{lastName}")
                ? database.Players.Where(p => ((firstName ?? "") == "" || p.FirstName.ToLower().StartsWith(firstName.ToLower())) && ((lastName ?? "") == "" || p.LastName.ToLower().StartsWith(lastName.ToLower())))
                : null;
        }

        public static IEnumerable<Player> GetPlayersFromRanking(ICyclingChallengeModelContainer database, long idRace, short? idStage = null)
        {
            var ranking = RaceQueries.GetRaceRankings(database, idRace, idStage).SingleOrDefault();
            return ranking == null 
                ? new List<Player>() 
                : GetRankingMatches(ranking.Ranking).Cast<Match>().Select(m => GetPlayerById(database, long.Parse(m.Groups[2].Value)));
        }

        public static IQueryable<PlayerProgress> GetPlayerProgressForPlayer(ICyclingChallengeModelContainer database, long idPlayer)
        {
            return database.PlayerProgresses.Where(pp => pp.Player == idPlayer);
        }

        public static IQueryable<ScoutedPlayer> GetScoutedPlayers(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.ScoutedPlayers.Where(sp => sp.Team == idTeam);
        }

        public static IQueryable<TransferHistory> GetTransferHistoryForRider(ICyclingChallengeModelContainer database, long idRider, DateTime from, DateTime? to = null)
        {
            return database.TransferHistories.Where(th => th.Player == idRider && th.Type == Rider.Transfer.Finished && th.DateTransfer >= from && (!to.HasValue || th.DateTransfer < to));
        }

        public static IQueryable<Youth> GetYouthHistoriesByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Youths.Where(y => y.YouthPlayer.Team == idTeam);
        }

        public static long? GetNameMaxValueByCountry(ICyclingChallengeModelContainer database, short idCountry, bool isFirstName = false)
        {
            return database.Names.Where(n => n.Country == idCountry && n.IsFirstName == isFirstName).Select(n => (long?)n.End).Max();
        }

        public static string GetRandomNameByCountry(ICyclingChallengeModelContainer database, short idCountry, long maxNameValue, bool isFirstName = false)
        {
            var randNameValue = maxNameValue > 0 ? RandomUtil.BetterRandomNumber(maxNameValue) : maxNameValue;
            return database.Names.FirstOrDefault(n => n.Country == idCountry && n.IsFirstName == isFirstName && n.Begin <= randNameValue && n.End > randNameValue)?.Description;
        }

        #endregion


        #region Private Methods

        private static MatchCollection GetRankingMatches(string ranking)
        {
            var regex = @"(\d+),(\d+)\|*";
            if (!Regex.IsMatch(ranking, regex))
            {
                throw new Exception(ErrorCodes.InvalidFormatRanking);
            }

            return Regex.Matches(ranking, regex);
        }

        #endregion
    }
}