﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using System.Linq;

namespace CyclingChallenge.Services.Queries
{
    public static class ProductQueries
    {
        public static IQueryable<Product> GetAll(ICyclingChallengeModelContainer database)
        {
            return database.Products;
        }

        public static Product GetById(ICyclingChallengeModelContainer database, byte idProduct)
        {
            return database.Products.FirstOrDefault(p => p.Id == idProduct);
        }
    }
}