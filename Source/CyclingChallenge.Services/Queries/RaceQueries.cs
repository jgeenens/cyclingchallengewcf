﻿using System.Linq;
using System;
using CyclingChallenge.Services.Models.Race;
using CyclingChallenge.Services.Models;

namespace CyclingChallenge.Services.Queries
{
    public static class RaceQueries
    {
        public static Track GetTrackByStageId(ICyclingChallengeModelContainer database, short idStage)
        {
            return database.Stages.SingleOrDefault(s => s.Id == idStage)?.StageTrack;
        }

        public static Track GetTrackByRaceAndStage(ICyclingChallengeModelContainer database, long idRace, short idStage)
        {
            return (from s in database.Stages where s.Index == idStage
                    from r in database.Races where r.Id == idRace && r.RaceDescription == s.RaceDescription
                    select s.StageTrack).SingleOrDefault();
        }

        public static Race GetRaceById(ICyclingChallengeModelContainer database, long idRace)
        {
            return database.Races.SingleOrDefault(r => r.Id == idRace);
        }

        public static Stage GetStageById(ICyclingChallengeModelContainer database, short idStage)
        {
            return database.Stages.FirstOrDefault(s => s.Id == idStage);
        }

        public static IQueryable<Stage> GetStagesByRaceDescription(ICyclingChallengeModelContainer database, short idRaceDescription)
        {
            return database.Stages.Where(s => s.RaceDescription == idRaceDescription);
        }

        public static Stage GetStageByRaceDescriptionAndIndex(ICyclingChallengeModelContainer database, short idRaceDescription, int index)
        {
            return GetStagesByRaceDescription(database, idRaceDescription).FirstOrDefault(s => s.Index == index);
        }

        public static RaceDescription GetRaceDescriptionById(ICyclingChallengeModelContainer database, short idRaceDescription)
        {
            return database.RaceDescriptions.FirstOrDefault(rd => rd.Id == idRaceDescription);
        }

        public static IQueryable<RaceHistory> GetRaceHistoryForRider(ICyclingChallengeModelContainer database, long idRider)
        {
            return database.RaceHistories.Where(rh => rh.Player == idRider);
        }

        public static IQueryable<Race> GetByDivisionWithinTimeFrame(ICyclingChallengeModelContainer database, long idDivision, DateTime start, DateTime end)
        {
            return database.Races.Where(r => r.Division == idDivision && r.StartDate >= start && r.EndDate <= end);
        }

        public static IQueryable<RaceOrder> GetRaceOrders(ICyclingChallengeModelContainer database, long idRace, short? idStage = null)
        {
            return database.RaceOrders.Where(r => r.Race == idRace && r.Stage == (idStage ?? 1));
        }

        public static IQueryable<RaceEvent> GetRaceEvents(ICyclingChallengeModelContainer database, long idRace, short? idStage = null)
        {
            return database.RaceEvents.Where(r => r.Race == idRace && r.Stage == (idStage ?? 1));
        }

        public static IQueryable<RaceRanking> GetRaceRankings(ICyclingChallengeModelContainer database, long idRace, short? idStage = null)
        {
            return database.RaceRankings.Where(rr => rr.Race == idRace && (!idStage.HasValue || rr.Stage == idStage));
        }

        public static IQueryable<RaceHistory> GetRaceHistoryForTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.RaceHistories.Where(rh => rh.Team == idTeam);
        }

        public static IQueryable<RaceHistory> GetRaceHistoryForRace(ICyclingChallengeModelContainer database, long idRace, bool includeStages)
        {
            return database.RaceHistories.Where(rh => rh.Race == idRace && (includeStages || !rh.Stage.HasValue));
        }
    }
}