﻿using System.Linq;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models;

namespace CyclingChallenge.Services.Queries
{
    public static class SponsorQueries
    {
        public static Sponsor GetById(ICyclingChallengeModelContainer database, long idSponsor)
        {
            return database.Sponsors.FirstOrDefault(s => s.Id == idSponsor);
        }

        public static Sponsor GetSponsorByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Sponsors.FirstOrDefault(s => s.Team == idTeam && s.WeeksToGo > 0);
        }

        public static IQueryable<Sponsor> GetSponsorsByDivision(ICyclingChallengeModelContainer database, long idDivision)
        {
            return database.Sponsors.Where(s => s.Division == idDivision && !s.Team.HasValue).OrderBy(s => s.Id);
        }
    }
}