﻿using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Staff;

namespace CyclingChallenge.Services.Queries
{
    public static class StaffQueries
    {
        public static Coach GetCoachById(ICyclingChallengeModelContainer database, long idCoach)
        {
            return database.Coaches.SingleOrDefault(c => c.Id == idCoach);
        }

        public static Coach GetCoachByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Coaches.FirstOrDefault(c => c.Team == idTeam);
        }

        public static IQueryable<Scout> GetScouts(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Scouts.Where(s => s.Team == idTeam);
        }

        public static IQueryable<Manager> GetManagers(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Managers.Where(m => m.Team == idTeam);
        }

        public static Manager GetShopManager(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Managers.FirstOrDefault(m => m.Team == idTeam && m.Type == Managers.Type.Shop);
        }

        public static Manager GetRaceManager(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Managers.FirstOrDefault(m => m.Team == idTeam && m.Type == Managers.Type.Race);
        }
    }
}