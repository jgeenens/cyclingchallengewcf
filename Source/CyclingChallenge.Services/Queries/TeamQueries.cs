﻿using System;
using System.Linq;
using CyclingChallenge.Services.Enums;
using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Economy;
using CyclingChallenge.Services.Models.Game;
using CyclingChallenge.Services.Models.Rider;
using CyclingChallenge.Services.Models.Staff;
using CyclingChallenge.Services.Models.Team;

namespace CyclingChallenge.Services.Queries
{
    public static class TeamQueries
    {
        public static Team GetTeamById(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Teams.FirstOrDefault(t => t.Id == idTeam);
        }

        public static Division GetDivisionById(ICyclingChallengeModelContainer database, long idDivision)
        {
            return database.Divisions.FirstOrDefault(d => d.Id == idDivision);
        }

        public static IQueryable<Division> GetDivisionsByName(ICyclingChallengeModelContainer database, string name)
        {
            return !string.IsNullOrEmpty(name)
                ? database.Divisions.Where(d => d.Name.ToLower().StartsWith(name.ToLower()))
                : null;
        }

        public static IQueryable<Division> GetDivisionsByCountry(ICyclingChallengeModelContainer database, short idCountry)
        {
            return database.Divisions.Where(d => d.Country == idCountry);
        }

        public static IQueryable<TeamHistory> GetTeamHistoryForTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.TeamHistories.Where(th => th.Team == idTeam);
        }

        public static IQueryable<TransferHistory> GetTransferHistoryForTeam(ICyclingChallengeModelContainer database, long idTeam, DateTime from, DateTime? to = null)
        {
            return database.TransferHistories.Where(th => (th.Team == idTeam || th.FromTeam == idTeam) && th.Type == Rider.Transfer.Finished && th.DateTransfer >= from && (!to.HasValue || th.DateTransfer < to));
        }

        public static IQueryable<TransferHistory> GetTransfersForTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.TransferHistories.Where(th => (th.Team == idTeam || th.FromTeam == idTeam) && th.Type != Rider.Transfer.Finished);
        }

        public static Clubshop GetShop(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Clubshops.FirstOrDefault(c => c.Team == idTeam);
        }

        public static Logo GetLogoById(ICyclingChallengeModelContainer database, byte idLogo)
        {
            return database.Logos.FirstOrDefault(l => l.Id == idLogo);
        }

        public static Logo GetRandomLogo(ICyclingChallengeModelContainer database)
        {
            return database.Logos.RandomOrDefault(l => l.Tokens == 0);
        }

        public static IQueryable<Team> GetTeamsByDivision(ICyclingChallengeModelContainer database, long idDivision)
        {
            return database.Teams.Where(t => t.Division == idDivision);
        }

        public static IQueryable<Team> GetTeamsByUsername(ICyclingChallengeModelContainer database, string username)
        {
            return !string.IsNullOrEmpty(username)
                ? database.Users.Where(u => u.Team.HasValue && u.Username.ToLower().StartsWith(username.ToLower())).Select(u => u.UserTeam)
                : null;
        }

        public static Team GetTeamByShortName(ICyclingChallengeModelContainer database, string name)
        {
            return database.Teams.FirstOrDefault(t => t.ShortName.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public static Team GetTeamByName(ICyclingChallengeModelContainer database, string name)
        {
            return database.Teams.FirstOrDefault(t => t.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public static IQueryable<Team> GetTeamsByName(ICyclingChallengeModelContainer database, string name)
        {
            return !string.IsNullOrEmpty(name)
                ? database.Teams.Where(t => t.Name.ToLower().StartsWith(name))
                : null;
        }

        public static DateTime GetLastNameChange(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.TeamHistories.Where(th => th.Team == idTeam && th.Event == Events.Team.NameChange).OrderByDescending(h => h.Date).FirstOrDefault()?.Date ?? DateTime.MinValue;
        }
    }
}