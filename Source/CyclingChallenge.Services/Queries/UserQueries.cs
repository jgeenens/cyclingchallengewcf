﻿using CyclingChallenge.Services.Models;
using CyclingChallenge.Services.Models.Team;
using System;
using System.Linq;

namespace CyclingChallenge.Services.Queries
{
    public static class UserQueries
    {
        public static User GetById(ICyclingChallengeModelContainer database, long idUser)
        {
            return database.Users.FirstOrDefault(u => u.Id == idUser);
        }

        public static User GetUserByFacebookId(ICyclingChallengeModelContainer database, long facebookId)
        {
            return database.Users.FirstOrDefault(u => u.FacebookId == facebookId);
        }

        public static User GetByTeam(ICyclingChallengeModelContainer database, long idTeam)
        {
            return database.Users.FirstOrDefault(u => u.Team == idTeam);
        }

        public static User GetByUsername(ICyclingChallengeModelContainer database, string userName)
        {
            return !string.IsNullOrEmpty(userName) 
                ? database.Users.FirstOrDefault(u => u.Username.Equals(userName, StringComparison.InvariantCultureIgnoreCase))
                : null;
        }

        public static User GetByEmail(ICyclingChallengeModelContainer database, string email)
        {
            return !string.IsNullOrEmpty(email)
                ? database.Users.FirstOrDefault(u => u.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase))
                : null;
        }
    }
}